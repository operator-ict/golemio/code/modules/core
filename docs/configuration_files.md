# General

Configuration files are loaded and parsed by core module. Default values are no longer supported since august 2022.

Configuraton files are saved in JSON format and located in `config/` directory of services:

-   [Input Gateway](https://gitlab.com/operator-ict/golemio/code/input-gateway/-/tree/development/config)
-   [Integration Engine](https://gitlab.com/operator-ict/golemio/code/integration-engine/-/tree/development/config)
-   [Data Proxy](https://gitlab.com/operator-ict/golemio/code/data-proxy/-/tree/development/config)

# Input Gateway

## Save Raw Data Whitelist (`saveRawDataWhitelist.json`)

This config contains array of the whitelisted routes which automatically save request body to file (in S3 bucket).

Example:

```saveRawDataWhitelist.json
[
    {
        "route": "/raw/:provider_route",
        "withHeaders": true
    },
    {
        "route": "/general/:provider_route",
        "withHeaders": false
    },
    {
        "route": "/mos-be/tokens",
        "withHeaders": false
    },
    {
        "route": "/vehiclepositions",
        "withHeaders": false
    }
]
```

Where property `route` contains express route name and property `withHeaders` means the request headers is saved to file too.

# Integration Engine

## Datasources (`moduleConfig.json`)

This config contains all urls (apikes, credentials, headers, etc.) of resources which are used by integration-engine.

Example:

```moduleConfig.json
{
    "ExampleDatasource": "https://example.com/api",
    "ExampleSecrets: {
        "username": "user",
        "password": "password"
    }
}
```

## Queues Blacklist (`queuesBlacklist.json`)

This config contains all datasets (queues) which are currently not used by integration-engine. It means the integration-engine do not process the queues which are write in this config.

Example:

```queuesBlacklist.json
{
    "BicycleCounters": [],
    "Parkings": ["saveDataToHistory", "updateAverageOccupancy"]
}
```

Where the `BicycleCounters` dataset has blacklisted all queues and the `Parkings` dataset has blacklisted only mentioned queues, in this case `saveDataToHistory` and `updateAverageOccupancy`.

## Save Raw Data Whitelist (`saveRawDataWhitelist.json`)

Re-used configuration file from input gateway for datasources that we proactively consume in integration engine. For more detail see Input Gateway part of this document.

# Data Proxy

## Data Proxy Server Definitions (`dataProxyServerDefinitions.json`)

This is only file that is internaly parsed in the service itself. Configuration enables to dispatch incomining requests to multiple endpoints for testing or development purposes.

Example:

```dataProxyServerDefinitions.json
[
    {
        "id": "tcp-server",
        "name": "tcp server",
        "type": "tcp",
        "port": 3000,
        "targets": [
            { "id": "first-target", "host": "localhost", "port": 3001 },
            { "id": "second-target", "host": "localhost", "port": 3002, "saveStats": true }
        ]
    },
    {
        "id": "http-server",
        "name": "http server",
        "type": "http",
        "port": 4000,
        "targets": [
            {
                "id": "first-target",
                "url": "http://localhost:4001"
            },
            {
                "id": "second-target",
                "url": "https://other-host",
                "headers": {
                    "x-access-token": "xxx"
                },
                "saveStats": true
            }
        ]
    }
]

```
