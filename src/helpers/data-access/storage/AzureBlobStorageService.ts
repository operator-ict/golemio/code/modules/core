import { dateTime } from "#helpers";
import { ILogger } from "#helpers/logger";
import { TransferProgressEvent } from "@azure/core-rest-pipeline";
import { ClientSecretCredential } from "@azure/identity";
import { BlobDeleteOptions, BlobServiceClient, BlockBlobParallelUploadOptions, ContainerClient } from "@azure/storage-blob";
import { Readable } from "node:stream";
import { AbstractStorageService } from "./AbstractStorageService";

export interface IAzureBlobStorageConfig {
    clientId: string;
    tenantId: string;
    clientSecret: string;
    account: string;
    containerName: string;
    uploadTimeoutInSeconds: number;
}

const UPLOAD_STREAM_BUFFER_SIZE = 4 * 1024 * 1024; // 4 MB

export class AzureBlobStorageService extends AbstractStorageService {
    private readonly uploadTimeoutInMs: number;

    constructor(private config: IAzureBlobStorageConfig, private readonly log: ILogger) {
        super();
        this.uploadTimeoutInMs = config.uploadTimeoutInSeconds * 1000;
    }

    public async uploadStream(stream: Readable, pathPrefix: string, ext: string = "", fileName?: string): Promise<void> {
        const now = dateTime(new Date(), { timeZone: "Europe/Prague" });
        const fileNamePrefix = fileName ?? `${now.format("yyyy-MM-dd")}/${now.format("HH_mm_ss.SSS")}`;
        const fileNameKey = `${pathPrefix}/${fileNamePrefix}`;
        const fileNameExt = ext ? `.${ext}` : "";

        try {
            const options: BlockBlobParallelUploadOptions = {
                blobHTTPHeaders: { blobContentType: "application/octet-stream" },
                onProgress: this.handleUploadProgress(fileNameKey + fileNameExt),
                abortSignal: AbortSignal.timeout(this.uploadTimeoutInMs),
            };

            const containerClient = this.instantiateContainerClient();
            const blobClient = containerClient.getBlockBlobClient(`${fileNameKey}${fileNameExt}`);

            await blobClient.uploadStream(stream, UPLOAD_STREAM_BUFFER_SIZE, undefined, options);
            this.log.verbose(`[${this.constructor.name}] Data stream uploaded (${fileNameKey}${fileNameExt})`);
        } catch (err) {
            this.log.error(err, `[${this.constructor.name}] Saving of data stream failed (${fileNameKey}${fileNameExt})`);
        }
    }

    public async uploadFile(data: Buffer | string, pathPrefix: string, ext: string = "", fileName?: string): Promise<void> {
        const now = dateTime(new Date(), { timeZone: "Europe/Prague" });
        const fileNamePrefix = fileName ?? `${now.format("yyyy-MM-dd")}/${now.format("HH_mm_ss.SSS")}`;
        const fileNameKey = `${pathPrefix}/${fileNamePrefix}`;
        const fileNameExt = ext ? `.${ext}` : "";

        try {
            const options: BlockBlobParallelUploadOptions = {
                blobHTTPHeaders: { blobContentType: "application/octet-stream" },
                onProgress: this.handleUploadProgress(fileNameKey + fileNameExt, Buffer.byteLength(data)),
                abortSignal: AbortSignal.timeout(this.uploadTimeoutInMs),
            };

            if (!Buffer.isBuffer(data)) {
                data = Buffer.from(data);
            }

            const containerClient = this.instantiateContainerClient();
            const blobClient = containerClient.getBlockBlobClient(`${fileNameKey}${fileNameExt}`);

            await blobClient.uploadData(data, options);
            this.log.verbose(`[${this.constructor.name}] Data uploaded (${fileNameKey}${fileNameExt})`);
        } catch (err) {
            this.log.error(err, `[${this.constructor.name}] Saving of data failed (${fileNameKey}${fileNameExt})`);
        }
    }

    public async deleteFile(fileKey: string) {
        try {
            const options: BlobDeleteOptions = {
                deleteSnapshots: "include",
            };

            const containerClient = this.instantiateContainerClient();
            const blobClient = containerClient.getBlockBlobClient(fileKey);

            await blobClient.delete(options);
            this.log.verbose(`[${this.constructor.name}] Data deleted (${fileKey})`);
        } catch (err) {
            this.log.error(err, `[${this.constructor.name}] Deletion of data failed (${fileKey})`);
        }
    }

    public async getFileStream(fileKey: string): Promise<NodeJS.ReadableStream | undefined> {
        try {
            const containerClient = this.instantiateContainerClient();
            const blobClient = containerClient.getBlockBlobClient(fileKey);

            const downloadResponse = await blobClient.download();
            return downloadResponse.readableStreamBody;
        } catch (err) {
            this.log.error(err, `[${this.constructor.name}] Getting data failed (${fileKey})`);
        }
    }

    private instantiateContainerClient(): ContainerClient {
        // TODO: use @azure/identity-cache-persistence if Microsoft starts to reject auth requests
        const credentials = new ClientSecretCredential(this.config.tenantId, this.config.clientId, this.config.clientSecret);
        const blobServiceClient = new BlobServiceClient(`https://${this.config.account}.blob.core.windows.net`, credentials);
        return blobServiceClient.getContainerClient(this.config.containerName);
    }

    private handleUploadProgress(filePath: string, dataByteLength?: number) {
        return (progress: TransferProgressEvent) => {
            this.log.verbose(
                `[${this.constructor.name}] File upload ` +
                    `${filePath}: ${progress.loadedBytes} / ${dataByteLength ?? "unknown"}`
            );
        };
    }
}
