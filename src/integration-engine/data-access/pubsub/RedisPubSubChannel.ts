import { PubSubChannel } from "#helpers/data-access/pubsub/AbstractPubSubChannel";
import { getRedisChannel } from "#helpers/data-access/pubsub/helpers/getRedisChannel";
import { IRedisConnector } from "#helpers/data-access/redis/IRedisConnector";
import { config } from "#ie/config";
import { RedisSubscriber } from "#ie/data-access/pubsub/subscribers/RedisSubscriber";
import { log } from "#ie/helpers";
import { ContainerToken, IntegrationEngineContainer } from "#ie/ioc";

interface IChannelOptions {
    channelSuffix?: string;
}

interface ISubscriberOptions extends IChannelOptions {
    maxMessageCount: number;
}

export class RedisPubSubChannel extends PubSubChannel {
    private connector: IRedisConnector;

    constructor(private readonly channel: string) {
        super();
        this.connector = IntegrationEngineContainer.resolve<IRedisConnector>(ContainerToken.RedisConnector);
    }

    public async publishMessage(message: string, options: IChannelOptions = {}): Promise<void> {
        const redisChannel = getRedisChannel(this.channel, options.channelSuffix);
        await this.connector.getConnection().publish(redisChannel, message);
    }

    public createSubscriber(options: ISubscriberOptions): RedisSubscriber {
        return new RedisSubscriber({
            channelName: this.channel,
            channelSuffix: options.channelSuffix,
            redisConnectionString: config.REDIS_CONN,
            logger: log,
            maxMessageCount: options.maxMessageCount,
        });
    }
}
