import { CreationAttributes, Model, ModelAttributes } from "sequelize";

export type BulkSaveRecords<T extends object> = ReadonlyArray<Partial<T>>;
export type BulkSaveResult<T extends object> = Array<Model<T> & T>;

export interface ISequelizeSettings {
    attributesToRemove?: string[];
    outputSequelizeAttributes: ModelAttributes<any>;
    pgTableName: string;
    pgSchema?: string;
    savingType: "insertOnly" | "insertOrUpdate";
    sequelizeAdditionalSettings?: Record<string, any>;
    hasTmpTable?: boolean;
    addAuditAttributes?: boolean;
}

export interface IRedisSettings {
    isKeyConstructedFromData: boolean;
    prefix: string;
    encodeDataBeforeSave?: (raw: any) => any;
    decodeDataAfterGet?: (encoded: any) => any;
}

export interface IModel {
    name: string;
    save: (data: any, useTmpTable?: boolean) => Promise<any>;
    bulkSave: <T extends object>(
        records: BulkSaveRecords<T>,
        updateAttributes?: string[],
        isReturning?: boolean,
        useTmpTable?: boolean
    ) => Promise<BulkSaveResult<T>>;
    truncate: (useTmpTable?: boolean) => Promise<any>;
}

export interface IRedisModel {
    name: string;
    set: (key: string, data: any, ttlSeconds: number, expireTimestamp: number | undefined) => Promise<any>;
    hset: (key: string, data: any) => Promise<any>;
    get: (key: string) => Promise<any>;
    hget: (key: string) => Promise<any>;
    delete: (key: string) => Promise<any>;
    truncate: () => Promise<number>;
    flush: () => Promise<"OK">;
}
