/** The requested resource has not been modified (received HTTP response 304) */
export class ResourceNotModified {
    // empty
}
