import { IRedisConnector } from "#helpers/data-access/redis/IRedisConnector";
import { getSubProperty } from "#helpers/utils";
import { log } from "#ie/helpers";
import { ContainerToken, IntegrationEngineContainer } from "#ie/ioc";
import { IRedisModel, IRedisSettings } from "#ie/models";
import { GeneralError } from "@golemio/errors";
import { IValidator } from "@golemio/validator";
import { ReadStream } from "fs";
import { Redis } from "ioredis";
import RedisRStream from "redis-rstream";
import RedisWStream from "redis-wstream";
import { Readable, Writable } from "stream";

export class RedisModel implements IRedisModel {
    /** Model name */
    public name: string;
    /** The Redis Connection */
    protected connection: Redis;
    /** Defines key construction */
    protected isKeyConstructedFromData: boolean;
    /** Function for encoding data (typically to string) before saving to Redis */
    protected encodeDataBeforeSave: (raw: any) => any;
    /** Function for decoding data (typically from string) after getting from Redis */
    protected decodeDataAfterGet: (encoded: any) => any;
    /** Key namespace prefix to identify model */
    protected prefix: string;
    /** Validation helper */
    protected validator: IValidator | null;

    constructor(name: string, settings: IRedisSettings, validator: IValidator | null = null) {
        this.name = name;
        this.connection = IntegrationEngineContainer.resolve<IRedisConnector>(ContainerToken.RedisConnector).getConnection();
        this.isKeyConstructedFromData = settings.isKeyConstructedFromData;
        this.prefix = settings.prefix;
        this.validator = validator;

        this.encodeDataBeforeSave = settings.encodeDataBeforeSave ? settings.encodeDataBeforeSave : (raw) => raw;
        this.decodeDataAfterGet = settings.decodeDataAfterGet ? settings.decodeDataAfterGet : (raw) => raw;

        if (this.validator?.setLogger) {
            this.validator.setLogger(log);
        }
    }

    /**
     *
     * @param key
     * @param data
     * @param ttlSeconds Key timeout, in seconds
     * @param expireTimestamp Unix time at which the key will expire, in milliseconds
     */
    public set = async (key: string, data: any, ttlSeconds?: number, expireTimestamp?: number): Promise<any> => {
        await this.validate(data);

        if (data instanceof Array) {
            // start the redis transaction
            const multi = this.connection.multi();

            for (const dataItem of data) {
                const redisKey = `${this.prefix}:${this.buildKey(key, dataItem)}`;
                const encodedData = this.encodeDataBeforeSave(dataItem);

                if (ttlSeconds) {
                    return multi.setex(redisKey, ttlSeconds, encodedData);
                } else if (expireTimestamp) {
                    // PXAT timestamp in milliseconds
                    return multi.set(redisKey, encodedData, "PXAT", expireTimestamp);
                } else {
                    return multi.set(redisKey, encodedData);
                }
            }

            // redis transaction commit
            return multi.exec();
        } else {
            const redisKey = `${this.prefix}:${this.buildKey(key, data)}`;
            const encodedData = this.encodeDataBeforeSave(data);

            if (ttlSeconds) {
                return this.connection.setex(redisKey, ttlSeconds, encodedData);
            } else if (expireTimestamp) {
                // PXAT timestamp in milliseconds
                return this.connection.set(redisKey, encodedData, "PXAT", expireTimestamp);
            } else {
                return this.connection.set(redisKey, encodedData);
            }
        }
    };

    public hset = async (key: string, data: any): Promise<any> => {
        await this.validate(data);

        if (data instanceof Array) {
            // start the redis transaction
            const multi = this.connection.multi();

            data.forEach((d) => {
                const k = this.buildKey(key, d);
                // encoding and saving the data as redis hash
                return multi.hset(this.prefix, k, this.encodeDataBeforeSave(d));
            });

            // redis transaction commit
            return multi.exec();
        } else {
            const k = this.buildKey(key, data);
            // encoding and saving the data as redis hash
            return this.connection.hset(this.prefix, k, this.encodeDataBeforeSave(data));
        }
    };

    public get = async (key: string): Promise<any> => {
        // getting and decoding the data from redis hash
        return this.decodeDataAfterGet(await this.connection.get(`${this.prefix}:${key}`));
    };

    public hget = async (key: string): Promise<any> => {
        // getting and decoding the data from redis hash
        return this.decodeDataAfterGet(await this.connection.hget(this.prefix, key));
    };

    public mget = async <T = object | string>(keys: string[]): Promise<Array<T | null>> => {
        if (keys.length === 0) {
            return [];
        }

        const values = await this.connection.mget(...keys.map((key) => `${this.prefix}:${key}`));
        return values.map((value) => this.decodeDataAfterGet(value));
    };

    /**
     * Stream data into a key from file read stream via SET/APPEND
     */
    public pipeStream = async (key: string, readStream: ReadStream): Promise<void> => {
        const redisWriteStream: Writable = new RedisWStream(this.connection, `${this.prefix}:${key}`);

        return new Promise((resolve, reject) => {
            redisWriteStream.on("finish", () => {
                resolve();
            });

            redisWriteStream.on("error", (err) => {
                reject(err);
            });

            readStream.pipe(redisWriteStream);
        });
    };

    /**
     * Get a readable stream of data from a key via GETRANGE
     */
    public getReadableStream = (key: string): Readable => {
        return new RedisRStream(this.connection, `${this.prefix}:${key}`);
    };

    public delete = async (key?: string): Promise<any> => {
        if (key) {
            return this.connection.del(`${this.prefix}:${key}`);
        }
        return this.connection.del(`${this.prefix}`);
    };

    public truncate = (namespace = true): Promise<number> => {
        const prefix = `${this.prefix}${namespace ? ":" : ""}*`;
        const stream = this.connection.scanStream({ match: prefix, count: 500 });
        const pipeline = this.connection.pipeline();

        return new Promise((resolve, reject) => {
            stream.on("data", (keys: string[]) => {
                if (keys.length > 0) {
                    pipeline.unlink(...keys);
                }
            });

            stream.on("error", (err) => {
                log.error(err);
                return reject(err);
            });

            stream.on("end", async () => {
                try {
                    const result = await pipeline.exec();
                    const deleteCount = result?.reduce((acc: number, el) => acc + (el[1] as number), 0) ?? 0;
                    log.info(`Truncate complete for ${prefix}, deleted ${deleteCount} in (${result?.length ?? 0}) batches`);
                    resolve(deleteCount);
                } catch (err) {
                    log.error(err);
                    return reject(err);
                }
            });
        });
    };

    public flush = (): Promise<"OK"> => {
        return this.connection.flushdb();
    };

    private validate = async (data: any) => {
        // data validation
        if (this.validator) {
            try {
                await this.validator.Validate(data);
            } catch (err) {
                throw new GeneralError("Error while validating data.", this.name, err);
            }
        } else if (!this.validator && this.isKeyConstructedFromData) {
            log.warn(this.name + ": Model validator is not set.");
        }
    };

    private buildKey = (key: string, data: any) => {
        // checking if the value is type of object
        if (this.isKeyConstructedFromData && typeof data !== "object") {
            throw new GeneralError("The data must be a type of object.", this.constructor.name);
        }
        return this.isKeyConstructedFromData ? getSubProperty<string>(key, data) : key;
    };
}
