import { Request } from "express";

export interface IResponseTimesEventData {
    req: Request;
    name: string;
    elapsedMS: number;
}
