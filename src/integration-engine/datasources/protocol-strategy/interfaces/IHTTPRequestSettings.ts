import { Readable } from "stream";
import { IncomingHttpHeaders } from "undici/types/header";
import { IHTTPFetchSettings } from "./IHTTPFetchSettings";

export interface IHTTPRequestSettings extends Omit<IHTTPFetchSettings, "body" | "headers"> {
    rejectUnauthorized?: boolean;
    body?: string | Buffer | Uint8Array | Readable | null;
    headers?: IncomingHttpHeaders | string[] | Iterable<[string, string | string[] | undefined]> | null;
    compression?: "gzip";
}
