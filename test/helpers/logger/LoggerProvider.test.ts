import { GeneralError } from "@golemio/errors";
import { expect } from "chai";
import { formatGolemioErrorLog } from "#helpers";

describe("LoggerProvider", () => {
    const error1 = new GeneralError("oops", "TroubleMaker", { message: "It's all wrong" }, 500);
    const error2 = new GeneralError("gone bad", "BadClass", { message: "Sorry" }, 500, "Problematic", "problems");

    it("formats AbstractGolemioError correctly", () => {
        const expectation1 = {
            message: "oops",
            class_name: "TroubleMaker",
            status: 500,
            cause: '{"message":"It\'s all wrong"}',
            type: "GeneralError",
        };
        const expectation2 = {
            message: "gone bad",
            class_name: "BadClass",
            status: 500,
            cause: '{"message":"Sorry"}',
            type: "GeneralError",
            module_name: "Problematic",
            queue_name: "problems",
        };
        expect(formatGolemioErrorLog(error1)).to.deep.equal(expectation1);
        expect(formatGolemioErrorLog(error2)).to.deep.equal(expectation2);
    });
});
