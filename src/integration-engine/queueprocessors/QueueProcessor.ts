import AlternateExchangeCreator from "#helpers/data-access/amqp/AlternateExchangeCreator";
import { config } from "#ie/config";
import { IExtendedGolemioErrorObject, IntegrationErrorHandler, log } from "#ie/helpers";
import { IQueueDefinition } from "#ie/queueprocessors";
import { withSentryConsumerTrace } from "#monitoring/sentry/sentry-transactions";
import { AbstractGolemioError, FatalError, GeneralError } from "@golemio/errors";
import { Options, Channel, ConsumeMessage, Message } from "amqplib";
import { AbortableTask } from "#ie/workers/helpers/AbortableTask";
import { BaseWorker } from "#ie/workers";
import * as sentry from "@sentry/node";
import { Severity } from "@sentry/node";

export class QueueProcessor {
    protected channel: Channel;
    private definition: IQueueDefinition;
    private consumers: Set<string>;
    private isGracefulShutdown: boolean;
    private readonly moduleName?: string;

    constructor(channel: Channel, definition: IQueueDefinition, moduleName?: string) {
        this.channel = channel;
        this.definition = definition;
        this.consumers = new Set();
        this.isGracefulShutdown = false;
        this.moduleName = moduleName;
    }

    /**
     * Registering all queues from definition
     */
    public registerQueues = async (): Promise<void> => {
        const promises = this.definition.queues.map((q) => {
            return this.registerQueue(
                this.definition.queuePrefix + "." + q.name, // queue name
                "*." + this.definition.queuePrefix + "." + q.name, // queue exchange key
                q.customProcessFunction // queue processor
                    ? q.customProcessFunction // custom processor from definition
                    : async (msg) => {
                          // default processor
                          await this.defaultProcessor(msg, q.name, async () => {
                              if (q.consume) {
                                  return withSentryConsumerTrace(q.consume)(msg);
                              }

                              if (q.worker && q.workerMethod) {
                                  const worker = new q.worker();
                                  const workerMethod: unknown = worker[q.workerMethod as keyof BaseWorker];

                                  if (typeof workerMethod !== "function") {
                                      throw new FatalError(
                                          // eslint-disable-next-line max-len
                                          `[AMQP QueueProcessor] Worker method ${q.workerMethod} does not exist on worker ${worker.constructor.name}`,
                                          this.constructor.name
                                      );
                                  }

                                  return withSentryConsumerTrace((msg: Message | null) =>
                                      AbortableTask.from(
                                          workerMethod.bind(worker)(msg),
                                          `${this.definition.queuePrefix}.${q.workerMethod}`
                                      )
                                  )(msg);
                              }

                              throw new FatalError(
                                  "[AMQP QueueProcessor] validateAndExecute method or worker and its method must be defined",
                                  this.constructor.name
                              );
                          });
                      },
                q.options // queue options
            );
        });
        await Promise.all(promises);
    };

    /**
     * Cancel all registered consumers before shutting down
     */
    public cancelConsumers = async (): Promise<void> => {
        log.info("[AMQP QueueProcessor] Expecting a shutdown. Cancelling all consumer operations");
        this.isGracefulShutdown = true;

        // Cancel all consumer operations
        for (const consumerTag of this.consumers.values()) {
            await this.channel.cancel(consumerTag);
        }
    };

    /**
     * Default queue processor
     */
    protected defaultProcessor = async (msg: any, name: string, worker: () => Promise<any>): Promise<void> => {
        try {
            log.verbose("[>] " + this.definition.queuePrefix + "." + name + ": received some data.");

            // Expect to requeue the message before shutting down
            if (this.isGracefulShutdown) {
                this.channel.nack(msg, false, true);
                log.verbose("[<] " + this.definition.queuePrefix + "." + name + ": requeued");
                return;
            }

            await worker();

            this.channel.ack(msg);
            log.verbose("[<] " + this.definition.queuePrefix + "." + name + ": done");
        } catch (err) {
            // Handling critical errors or datasets warnings
            const errObject: IExtendedGolemioErrorObject = IntegrationErrorHandler.handle(
                this.fillQueueAndModuleNames(err, name)
            );
            // If error is not critical the warning is logged and message is acknowledged
            if (errObject.ack) {
                this.channel.ack(msg);
                // Critical errors non-acknowledge the message
            } else {
                this.channel.nack(msg, false, false);
            }

            if (errObject.exit) {
                sentry.captureMessage(`Exiting: ${err.toString()}`, Severity.Fatal);
                process.exit(1);
            }
        }
    };

    protected registerQueue = async (
        name: string,
        key: string,
        processor: (msg: ConsumeMessage | null) => any,
        queueOptions: Options.AssertQueue = {}
    ): Promise<any> => {
        if (!config.RABBIT_EXCHANGE_NAME) {
            throw new GeneralError("[AMQP] The ENV variable RABBIT_EXCHANGE_NAME cannot be undefined.", this.constructor.name);
        }

        // Create exchange if not exists
        await this.channel.assertExchange(config.RABBIT_EXCHANGE_NAME, "topic", {
            durable: false,
            alternateExchange: AlternateExchangeCreator.getAltExchangeName(config.RABBIT_EXCHANGE_NAME!),
        });
        // Create queue
        const q = await this.channel.assertQueue(name, {
            arguments: { "x-queue-type": "quorum" },
            durable: true,
            ...queueOptions,
        });
        // Bind queue to exchange
        await this.channel.bindQueue(q.queue, config.RABBIT_EXCHANGE_NAME, key);
        log.verbose("[*] Waiting for messages in " + name + ". {key " + key + "}");
        // Listen and consume messages in queue
        const { consumerTag } = await this.channel.consume(name, processor, { noAck: false });
        this.consumers.add(consumerTag);
    };

    /**
     * Here we mutate Errors to add relevant information to logs.
     *
     * @param err AbstractGolemioError
     * @param name string
     * @private
     */
    private fillQueueAndModuleNames(err: AbstractGolemioError, name: string): AbstractGolemioError {
        if (!err.queueName) {
            err.queueName = this.definition.queuePrefix + "." + name;
        }
        if (!err.moduleName) {
            err.moduleName = this.moduleName;
        }
        return err;
    }
}
