import { GeneralError } from "@golemio/errors";
import { NextFunction, Request, Response } from "express";

/**
 * Error-handling middleware preprocessor
 * HttpError based exceptions with pregenarated error status code are mapped to preferred AbstractGolemioError type.
 * Result is send to express.js next function with other custom or default error handler.
 */
export const mapErrorsToCustomType = (err: any, _request: Request, _response: Response, next: NextFunction) => {
    if (!(err instanceof GeneralError) && Number.isInteger(err.status)) {
        const statusCode = err.status >= 400 && err.status < 600 ? err.status : undefined;
        const error = new GeneralError(err.message, undefined, err, statusCode);
        next(error);
    } else {
        next(err);
    }
};
