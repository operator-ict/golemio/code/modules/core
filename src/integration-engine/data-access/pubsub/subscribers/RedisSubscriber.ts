import { ILogger } from "#helpers/logger";
import { ISubscribeMessage } from "#ie/data-access/pubsub/subscribers/interfaces/ISubscribeMessage";
import { GeneralError } from "@golemio/errors";
import Redis, { Redis as RedisInstance } from "ioredis";
import { getRedisChannel } from "../../../../helpers/data-access/pubsub/helpers/getRedisChannel";
import { Subscriber } from "../../../../helpers/data-access/pubsub/subscribers/AbstractSubscriber";

interface ISubscriberOptions {
    channelName: string;
    channelSuffix?: string;
    redisConnectionString?: string;
    logger: ILogger;
    maxMessageCount: number;
}

export type MessageCallback = (payload: ISubscribeMessage) => void;

/**
 * @deprecated Use RedisSubscriber from helpers/data-access/pubsub/subscribers/RedisSubscriber.ts
 * this implementation is too specific for pid module needs
 */

export class RedisSubscriber extends Subscriber {
    private connection: RedisInstance;
    private readonly redisChannel: string;
    private logger: ILogger;
    private maxMessageCount: number;

    constructor(options: ISubscriberOptions) {
        super();
        this.redisChannel = getRedisChannel(options.channelName, options.channelSuffix);
        this.logger = options.logger;
        this.maxMessageCount = options.maxMessageCount;

        this.connection = new Redis(options.redisConnectionString!, {
            connectionName: `Subscriber for ${this.redisChannel}`,
            lazyConnect: true,
            autoResubscribe: true,
        });

        this.connection.on("error", (err) => {
            throw new GeneralError("Error while connecting to Redis", this.constructor.name, err);
        });
    }

    public async subscribe(): Promise<RedisInstance> {
        if (this.connection.status === "ready") {
            this.logger.warn(`Already subscribed to Redis channel ${this.redisChannel}`);
        } else {
            await this.connection.connect();
            await this.connection.subscribe(this.redisChannel);
        }

        return this.connection;
    }

    public async unsubscribe(): Promise<void> {
        await this.connection.unsubscribe(this.redisChannel);
        this.connection.disconnect();
    }

    public listen(messageCallback: MessageCallback): Promise<void> {
        return new Promise((resolve, reject) => {
            let messageCount = 0;
            this.connection.on("message", (channel, message) => {
                if (channel !== this.redisChannel) {
                    return;
                }

                messageCount++;
                let isFinal = messageCount >= this.maxMessageCount;

                try {
                    messageCallback({ message, messageCount, isFinal });
                    if (isFinal) {
                        this.connection.removeAllListeners("message");
                        resolve();
                    }
                } catch (err) {
                    this.connection.removeAllListeners("message");
                    this.logger.error("Error while processing Redis message", { message, messageCount });
                    reject(err);
                }
            });
        });
    }
}
