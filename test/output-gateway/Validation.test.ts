import { AbstractRouter } from "#helpers/routing/AbstractRouter";
import { checkErrors, log } from "#og";
import { HTTPErrorHandler, IGolemioError } from "@golemio/errors";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import express, { NextFunction, Request, Response } from "express";
import { oneOf, query } from "express-validator";
import sinon, { SinonSandbox } from "sinon";
import supertest from "supertest";

chai.use(chaiAsPromised);

describe("OG API shared validations", () => {
    let sandbox: SinonSandbox;
    let router: AbstractRouter;
    let app: express.Express;

    class V1Router extends AbstractRouter {
        constructor() {
            super("v1", "test");
            this.initRoutes();
        }

        protected initRoutes() {
            this.getRouter().get("/empty", checkErrors, (_req: Request, res: Response, _next: NextFunction) =>
                res.status(200).send({ response: "success" })
            );
            this.getRouter().get(
                "/withParams",
                query("param").isNumeric().notEmpty(),
                query("param2").optional().isString().notEmpty(),
                query("param3").optional().isString().notEmpty(),
                checkErrors,
                (_req: Request, res: Response, _next: NextFunction) => res.status(200).send({ response: "success" })
            );
            this.getRouter().get(
                "/paramsInArray",
                [query("param").isNumeric().notEmpty(), query("param2").isString().notEmpty()],
                checkErrors,
                (_req: Request, res: Response, _next: NextFunction) => res.status(200).send({ response: "success" })
            );

            this.getRouter().get(
                "/paramsOneOf",
                [
                    query("phone_number").optional(),
                    query("email").optional(),
                    oneOf([query("phone_number").isNumeric(), query("email").isEmail()], {
                        message: "At least one valid contact method must be provided",
                    }),
                ],
                checkErrors,
                (_req: Request, res: Response, _next: NextFunction) => res.status(200).send({ response: "success" })
            );
        }
    }

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        router = new V1Router();
        app = express();

        app.use(router.getPath(), router.getRouter());
        app.use((err: any, _req: Request, res: Response, _next: NextFunction) => {
            const warnCodes = [400, 404];
            const errObject: IGolemioError = HTTPErrorHandler.handle(err, log, warnCodes.includes(err.status) ? "warn" : "error");

            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(errObject.error_status || 500).send(errObject);
        });
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should return a router", () => {
        expect(router.getRouter()).to.eq(router["router"]);
    });

    it("should return a path", () => {
        expect(router.getPath()).to.equal("/v1/test");
    });

    it("should have initRoutes method", () => {
        expect(router["initRoutes"]).not.to.be.undefined;
    });

    it("should test empty endpoint without params", async () => {
        const testApp = supertest(app);
        const response = await testApp.get("/v1/test/empty");

        expect(response.status).to.eq(200);
        expect(response.body).to.eql({ response: "success" });
    });

    it("should test empty endpoint  with non existent param", async () => {
        const testApp = supertest(app);
        const response = await testApp.get("/v1/test/empty?wrongParam=1");
        expect(response.status).to.eq(400);
        expect(response.body).to.deep.equal({
            error_message: "Bad request",
            error_status: 400,
            error_info: JSON.stringify({
                _unknown_fields: {
                    type: "unknown_fields",
                    msg: "Unknown field(s)",
                    fields: [{ path: "wrongParam", value: "1", location: "query" }],
                },
            }),
        });
    });

    it("should test withParams endpoint with correct param", async () => {
        const testApp = supertest(app);
        const response = await testApp.get("/v1/test/withParams?param=1");
        expect(response.status).to.eq(200);
        expect(response.body).to.eql({ response: "success" });
    });

    it("should test withParams endpoint with incorrect param", async () => {
        const testApp = supertest(app);
        const response = await testApp.get("/v1/test/withParams?param=abc&param3=abc");
        expect(response.status).to.eq(400);
        expect(response.body).to.deep.equal({
            error_message: "Bad request",
            error_status: 400,
            error_info: JSON.stringify({
                param: { type: "field", value: "abc", msg: "Invalid value", path: "param", location: "query" },
            }),
        });
    });

    it("should test withParams endpoint with non existent param", async () => {
        const testApp = supertest(app);
        const response = await testApp.get("/v1/test/withParams?param=1&param2=abc&param3=abc&param4=xxx");
        expect(response.status).to.eq(400);

        expect(response.body).to.deep.equal({
            error_message: "Bad request",
            error_status: 400,
            error_info: JSON.stringify({
                _unknown_fields: {
                    type: "unknown_fields",
                    msg: "Unknown field(s)",
                    fields: [{ path: "param4", value: "xxx", location: "query" }],
                },
            }),
        });
    });

    it("should test paramsInArray endpoint with correct param", async () => {
        const testApp = supertest(app);
        const response = await testApp.get("/v1/test/paramsInArray?param=1&param2=abc");
        expect(response.status).to.eq(200);
        expect(response.body).to.eql({ response: "success" });
    });

    it("should test paramsInArray endpoint with incorrect param", async () => {
        const testApp = supertest(app);
        const response = await testApp.get("/v1/test/paramsInArray?param=abc&param2=abc");

        expect(response.status).to.eq(400);
        expect(response.body).to.deep.equal({
            error_message: "Bad request",
            error_status: 400,
            error_info: JSON.stringify({
                param: { type: "field", value: "abc", msg: "Invalid value", path: "param", location: "query" },
            }),
        });
    });

    it("should test paramsInArray endpoint with non existent param", async () => {
        const testApp = supertest(app);
        const response = await testApp.get("/v1/test/paramsInArray?param=1&param2=abc&param3=xxx");
        expect(response.status).to.eq(400);

        expect(response.body).to.deep.equal({
            error_message: "Bad request",
            error_status: 400,
            error_info: JSON.stringify({
                _unknown_fields: {
                    type: "unknown_fields",
                    msg: "Unknown field(s)",
                    fields: [{ path: "param3", value: "xxx", location: "query" }],
                },
            }),
        });
    });

    it("should test paramsInArray endpoint with correct param", async () => {
        const testApp = supertest(app);
        const response = await testApp.get("/v1/test/paramsOneOf?phone_number=123456789");
        expect(response.status).to.eq(200);
        expect(response.body).to.eql({ response: "success" });
    });

    it("should test paramsInArray endpoint with incorrect param", async () => {
        const testApp = supertest(app);
        const response = await testApp.get("/v1/test/paramsOneOf?phone_number=zzzzzzzzz");

        expect(response.status).to.eq(400);

        expect(response.body).to.deep.equal({
            error_message: "Bad request",
            error_status: 400,
            error_info: JSON.stringify({
                _alternative_grouped: {
                    type: "alternative_grouped",
                    msg: "At least one valid contact method must be provided",
                    nestedErrors: [
                        [{ type: "field", value: "zzzzzzzzz", msg: "Invalid value", path: "phone_number", location: "query" }],
                        [{ type: "field", msg: "Invalid value", path: "email", location: "query" }],
                    ],
                },
            }),
        });
    });

    it("should test paramsInArray endpoint with non existent param", async () => {
        const testApp = supertest(app);
        const response = await testApp.get("/v1/test/paramsOneOf?param=1&phone_number=123456789");
        expect(response.status).to.eq(400);

        expect(response.body).to.deep.equal({
            error_message: "Bad request",
            error_status: 400,
            error_info: JSON.stringify({
                _unknown_fields: {
                    type: "unknown_fields",
                    msg: "Unknown field(s)",
                    fields: [{ path: "param", value: "1", location: "query" }],
                },
            }),
        });
    });
});
