# Coding guidelines

This is open source software. Consider the people who will read your code, and make it look nice for them.

## Names

1. Use PascalCase for type names.
2. Use `I` as a prefix for interface names.
3. Use PascalCase for enum values.
4. Use SCREAMING_SNAKE_CASE for constant names.
5. Use camelCase for function names.
6. Use camelCase for property names and local variables.
7. Do not use `_` as a prefix for private properties.
8. Use whole words in names when possible.
9. Use kebab-case for folder names.
10. Use lower-cased dataset/module names with no spaces/underscores and in the plural for endpoint urls.
11. Name variables and properties by their purpose.
12. Name functions and methods by their purpose, not their implementation.
13. Use `Abstract` as prefix for abstract classes.
14. Class names should have a noun or noun phrase. Class names should not be verbs.
15. For boolean variables, constants, and property names, prefer names that directly describe the state they represent.
    1. Frame the name in the affirmative form (what something is rather than what it is not). For example, use `isReady` instead of `isNotReady`.
    2. Use prefixes `is`, `has` and `should`

## Components

1. 1 file per logical component (e.g. datasource, worker, transformation).
2. use [dependency injection](CODING-GUIDELINES.md#dependency-injection)
3. Do not return various types from a single function. (e.g. use generics instead)

## Types

1. Do not use type `any`.
2. Do not export types/functions unless you need to share it across multiple components.
3. Do not introduce new types/values to the global namespace.
4. Shared types should be defined in `types.ts`.
5. Within a file, type definitions should come first.

## General Assumptions

1. Consider objects like Nodes, Symbols, etc. as immutable outside the component that created them. Do not change them.
2. Consider arrays as immutable by default after creation.
3. We use ES6 and its features, array destructing, spread, rest,...

## Classes

1. For consistency, do not use classes in the Express middlewares.
2. All instance variables must be initialized at declaration or in the constructor. Do not use the definite assignment assertion operator (!).

```ts
// Wrong
class Example {
    private settings!: Settings;
}

// Correct
class Example {
    private settings: Settings;

    constructor() {
        this.settings = new Settings();
    }
}

// Also correct
class Example {
    private settings = new Settings();
}
```

## Comments

1. Use JSDoc style comments for functions, interfaces, enums, and classes.
2. Do not use @vars, @returns, etc. in the comments.

## Strings

1. Use double quotes for strings.
2. Use \` quotes for long strings or strings that contains variables.

## Null and Undefined

1. Use `null` as the default "empty" value and for representing "no value" in returns:

```ts
// Correct
class UserService {
    public findUser = async (id: string): Promise<User | null> => {
        let user: User | null = null;

        try {
            const userDto = await this.repository.findUser(id);
            if (userDto && userDto.isActive) {
                user = userDto;
            }
        } catch (error) {
            // error handling
        }

        return user;
    };
}

// Wrong
class UserService {
    public findUser = async (id: string): Promise<User | undefined> => {
        let user: User | undefined;

        try {
            const userDto = await this.repository.findUser(id);
            if (userDto && userDto.isActive) {
                user = userDto;
            }
        } catch (error) {
            // error handling
        }

        return user;
    };
}
```

2. Use `undefined` only when:
    - Working with legacy code that uses undefined
    - Using external libraries that primarily use undefined
    - Dealing with optional parameters or properties

```ts
const findUsers = (validFromDateTime?: Date) {
    // ...
};
```

## Diagnostic Messages

1. Use a period at the end of a sentence.
2. Use indefinite articles for indefinite entities.
3. Definite entities should be named (this is for a variable name, type name, etc..).
4. When stating a rule, the subject should be in the singular (e.g. "An external module cannot..." instead of "External modules cannot...").
5. Use present tense.

## General Constructs

1. Use `for..of` and `for..in` instead of `Array.forEach` etc.
2. Use async/await for asynchronous stuff (try to avoid callbacks or Promises).
3. Use try/catch for error handling.
4. Use `const` or `let` instead of `var`.

## Style

1. Use arrow functions over anonymous function expressions.
2. Always surround arrow function parameters.
3. Always surround loop and conditional bodies with curly braces. Statements on the same line are allowed to omit braces.
4. Open curly braces always go on the same line as whatever necessitates them.
5. Parenthesized constructs should have no surrounding whitespace. <br />A single space follows commas, colons, and semicolons in those constructs. For example:
    - `for (var i = 0, n = str.length; i < 10; i++) { }`
    - `if (x < 10) { }`
    - `function f(x: number, y: string): void { }`
    - `[1, 2, 3]`
    - `x += 1`
6. Use a single declaration per variable statement <br />(i.e. use `const x = 1; const y = 2;` over `const x = 1, y = 2;`).
7. `else` goes on a same line as the closing curly brace.
8. Use 4 spaces per indentation.

## Dependency Injection

1. Global container can be found in core module under path `/src/helpers/ioc/Di` and it’s used to register instances that can be shared across services.
2. Service containers are available in core module e.g. for input gateway `/src/input-gateway/ioc/Di` and are used for instances that work only in the context of the service.
3. For context of module new child container based on relevant service container is created. Its structure and path are like others.
4. To register and resolve instances we create symbols in separate file. e.g. `/src/input-gateway/ioc/ContainerToken`. Using magic strings or class reference is not allowed.
5. Register components and instances only in `Di.ts` file.
6. Every registered component implements an interface which shall be used in the code.
7. Do not use `registerInstance` unless it won’t be needed in tests.

## Configuration

1. Use `SimpleConfig` from `core` module for configuration values.
2. Organize configuration values for dataplatform services in `config/moduleConfig.json` by module names.

## Database

1. Use snake_case for all names (databases, schemas, tables, columns, etc.).
2. Table names:

-   Table name is always in English.
-   Table name is the lower-cased dataset/module name with no spaces/underscores and in the plural. E.g. `citydistricts` or `parkomats`.
-   For sub-tables (more tables for the one dataset/module) use undescore to as delimiter. E.g. `mobileappstatistics_appstore` or `bicyclecounters_detections`,

3. Column names:

-   Column names can be in Czech or in English, but always one language for whole columns in the table.
-   Audit column names like `id` or `updated_at` are always in English.
