import contentType from "content-type";
import { NextFunction, Request, Response } from "express";

export const checkContentTypeMiddleware = (allowed: string[]) => (req: Request, res: Response, next: NextFunction) => {
    if (!(this as any).checkContentType(req, allowed)) {
        res.setHeader("Accept", allowed);
        return res.sendStatus(406);
    } else {
        return next();
    }
};

export const checkContentType = (req: Request, allowed: string[]): boolean => {
    try {
        const parsedcontentType: contentType.ParsedMediaType = contentType.parse(req);
        if (!allowed.includes(parsedcontentType.type)) {
            return false;
        } else {
            return true;
        }
    } catch (err) {
        return false;
    }
};

export const getContentType = (req: Request): string | null => {
    try {
        const parsedcontentType: contentType.ParsedMediaType = contentType.parse(req);
        return parsedcontentType.type;
    } catch (err) {
        return null;
    }
};
