import { FTPReturnType, FTPTargetType, IFile, IFTPSettings } from "#ie/datasources/protocol-strategy/IProtocolStrategy";
import { ProtocolStrategy } from "#ie/datasources/protocol-strategy/ProtocolStrategy";
import { log } from "#ie/helpers";
import { AbstractGolemioError, GeneralError } from "@golemio/errors";
import { Client } from "basic-ftp";
import fs from "fs";
import os from "os";
import path from "path";
import { FTPFileProcessor } from "./helpers/FTPFileProcessor";

interface IRawDataOutput {
    data: IFile | IFile[];
}

const defaultSettings: Partial<IFTPSettings> = {
    tmpDir: os.tmpdir(),
    targetType: FTPTargetType.FILE,
    returnType: FTPReturnType.File,
};

export class FTPProtocolStrategy extends ProtocolStrategy {
    protected connectionSettings!: IFTPSettings;
    private ftpClient: Client;
    private fileProcessor: FTPFileProcessor;
    private downloadPath: string;

    constructor(settings: IFTPSettings) {
        super({ ...defaultSettings, ...settings });
        this.ftpClient = new Client();
        this.ftpClient.ftp.log = log.silly.bind(log);
        this.ftpClient.ftp.verbose = true;
        this.downloadPath = this.connectionSettings.tmpDir!;
        this.fileProcessor = new FTPFileProcessor(this.connectionSettings, this.downloadPath);
    }

    public setConnectionSettings = (settings: IFTPSettings): void => {
        this.connectionSettings = { ...defaultSettings, ...settings };
        this.downloadPath = this.connectionSettings.tmpDir!;
        this.fileProcessor = new FTPFileProcessor(this.connectionSettings, this.downloadPath);
    };

    public getRawData = async (): Promise<IRawDataOutput> => {
        try {
            const dirHandle = await fs.promises.open(this.downloadPath, fs.constants.O_DIRECTORY);
            await dirHandle.close();
        } catch (err) {
            throw new GeneralError("FTP download path is not a directory", this.constructor.name, err);
        }

        try {
            await this.ftpClient.access(this.connectionSettings.url);
            await this.ftpClient.cd(this.connectionSettings.path);
            await this.ftpClient.downloadTo(
                fs.createWriteStream(path.join(this.downloadPath, this.connectionSettings.filename)),
                this.connectionSettings.filename
            );
        } catch (err) {
            throw new GeneralError("Error while downloading FTP files", this.constructor.name, err);
        } finally {
            this.ftpClient.close();
        }

        const prefix = path.parse(this.connectionSettings.filename).name + "/";
        let result: IFile | IFile[];

        try {
            if (this.connectionSettings.targetType === FTPTargetType.COMPRESSED) {
                result = await this.fileProcessor.processTargetCompressed(prefix);
            } else if (this.connectionSettings.targetType === FTPTargetType.FOLDER) {
                result = await this.fileProcessor.processTargetFolder(prefix);
            } else {
                result = await this.fileProcessor.processTargetFile();
            }
        } catch (err) {
            throw new GeneralError("Error while processing downloaded FTP files", this.constructor.name, err);
        }

        return { data: result };
    };

    public getLastModified = async (): Promise<string | null> => {
        try {
            await this.ftpClient.access(this.connectionSettings.url);
            await this.ftpClient.cd(this.connectionSettings.path);
            const lastModified = await this.ftpClient.lastMod(this.connectionSettings.filename);

            if (!lastModified) {
                throw new GeneralError("Last modified date is not available", this.constructor.name);
            }

            return lastModified.toISOString();
        } catch (err) {
            if (err instanceof AbstractGolemioError) {
                throw err;
            }

            throw new GeneralError("Error while getting last modified date", this.constructor.name, err);
        } finally {
            this.ftpClient.close();
        }
    };
}
