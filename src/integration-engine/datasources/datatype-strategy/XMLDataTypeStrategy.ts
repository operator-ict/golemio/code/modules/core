import { getSubProperty } from "#helpers/utils";
import { IDataTypeStrategy, IXMLSettings } from "#ie/datasources/datatype-strategy/IDataTypeStrategy";
import { GeneralError } from "@golemio/errors";
import xml2js from "xml2js";

export class XMLDataTypeStrategy implements IDataTypeStrategy {
    private dataTypeSettings: IXMLSettings;
    private filter: ((item: any) => any) | undefined;

    constructor(settings: IXMLSettings) {
        this.dataTypeSettings = settings;
        this.filter = undefined;
    }

    public setDataTypeSettings(settings: IXMLSettings): void {
        this.dataTypeSettings = settings;
    }

    public setFilter(filterFunction: (item: any) => any) {
        this.filter = filterFunction;
    }

    public async parseData(data: any): Promise<any> {
        try {
            let parsed = await xml2js.parseStringPromise(data, this.dataTypeSettings.xml2jsParams);
            parsed = getSubProperty(this.dataTypeSettings.resultsPath, parsed);
            if (this.filter) {
                parsed = parsed.filter(this.filter);
            }
            return parsed;
        } catch (err) {
            throw new GeneralError("Error while parsing source data.", this.constructor.name, err);
        }
    }
}
