import { DateTime } from "#helpers";
import {
    CreateDeleteEntityAction,
    TableClient,
    TableEntity,
    TableEntityResult,
    TableEntityResultPage,
    TableTransactionResponse,
} from "@azure/data-tables";
import { ClientSecretCredential } from "@azure/identity";
import { ILogger } from "@golemio/validator/dist/Logger";
import crypto from "node:crypto";
import { IAzureTableStorageConfig } from "./interfaces/IAzureTableStorageConfig";
import { ITableStorageService } from "./interfaces/ITableStorageService";

const MAX_ALLOWED_PAGE_SIZE = 1000;
/** Azure tables may start returning 503 if we go over this number */
const MAX_ADMISSIBLE_PARALLEL_REQUESTS = 30;

export class AzureTableStorageService implements ITableStorageService {
    private readonly maxPageSize: number;
    private readonly pageQueueLimit: number;
    private tableClientDict: Map<string, TableClient>;

    constructor(private config: IAzureTableStorageConfig, private log: ILogger) {
        this.maxPageSize = config.maxPageSize ? Math.min(config.maxPageSize, MAX_ALLOWED_PAGE_SIZE) : MAX_ALLOWED_PAGE_SIZE;
        this.pageQueueLimit = Math.ceil(2 * ((MAX_ADMISSIBLE_PARALLEL_REQUESTS * config.entityBatchSize) / this.maxPageSize));
        this.tableClientDict = new Map();
    }

    public async createEntities<T extends object>(tableName: string, entities: T[]): Promise<void> {
        if (!entities.length) {
            return;
        }

        const nowDateTime = new DateTime(new Date(), { timeZone: "Europe/Prague" });
        const fallbackPartitionKey = nowDateTime.format("yyyy-MM-dd");
        const rowKeyPrefix = nowDateTime.format("HH_mm_ss.SSS") + "_" + crypto.randomBytes(3).toString("hex");
        const tableEntities = entities.map((entity, i) => {
            if (!this.isEntityValid(entity)) {
                entity = {
                    partitionKey: fallbackPartitionKey,
                    rowKey: rowKeyPrefix + "_" + i,
                    ...entity,
                };
            }

            return entity as TableEntity<T>;
        });

        const tableClient = this.getTableClient(tableName);

        for (let i = 0; i < tableEntities.length; i += this.config.entityBatchSize) {
            const entityChunk = tableEntities.slice(i, i + this.config.entityBatchSize);
            const actions: CreateDeleteEntityAction[] = entityChunk.map((entity) => ["create", entity]);

            await tableClient.submitTransaction(actions);
        }
    }

    public async deleteEntitiesOlderThan<T extends object>(tableName: string, timestamp: string): Promise<void> {
        const tableClient = this.getTableClient(tableName);
        const resultsIterator = tableClient
            .listEntities<T>({
                queryOptions: {
                    filter: `Timestamp lt datetime'${timestamp}'`,
                    select: ["PartitionKey", "RowKey"],
                },
            })
            .byPage({ maxPageSize: this.maxPageSize });

        let pageQueue: Array<TableEntityResultPage<T>> = [];

        for await (const page of resultsIterator) {
            if (page.length === 0 && pageQueue.length === 0) {
                break;
            }

            pageQueue.push(page);

            if (pageQueue.length < this.pageQueueLimit && page.length === this.maxPageSize) {
                continue;
            }

            const partitionKeyMap: Map<string, Array<TableEntityResult<T>>> = pageQueue
                .flat()
                .reduce((groups, el) => groups.set(el.partitionKey, [...(groups.get(el.partitionKey) || []), el]), new Map());

            let promiseQueue: Array<Promise<TableTransactionResponse>> = [];

            for (const [_, entities] of partitionKeyMap) {
                for (let i = 0; i < entities.length; i += this.config.entityBatchSize) {
                    const entityChunk = entities.slice(i, i + this.config.entityBatchSize);
                    const actions: CreateDeleteEntityAction[] = entityChunk.map((entity) => ["delete", entity as TableEntity<T>]);
                    promiseQueue.push(tableClient.submitTransaction(actions));

                    if (promiseQueue.length >= MAX_ADMISSIBLE_PARALLEL_REQUESTS) {
                        await Promise.all(promiseQueue).catch((err) => resultsIterator.throw?.(err));
                        promiseQueue = [];
                    }
                }
            }

            if (promiseQueue.length > 0) {
                await Promise.all(promiseQueue).catch((err) => resultsIterator.throw?.(err));
            }

            pageQueue = [];
        }

        await resultsIterator.return?.([]);
    }

    private getTableClient(tableName: string): TableClient {
        if (this.tableClientDict.has(tableName)) {
            return this.tableClientDict.get(tableName)!;
        }
        const credentials = new ClientSecretCredential(this.config.tenantId, this.config.clientId, this.config.clientSecret);
        const tableClient = new TableClient(`https://${this.config.account}.table.core.windows.net`, tableName, credentials);
        this.tableClientDict.set(tableName, tableClient);
        return tableClient;
    }

    private isEntityValid<T extends object>(entity: T): entity is TableEntity<T> {
        return entity.hasOwnProperty("partitionKey") && entity.hasOwnProperty("rowKey");
    }
}
