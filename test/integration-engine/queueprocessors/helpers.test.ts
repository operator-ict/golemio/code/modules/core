import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { IQueueDefinition, filterQueueDefinitions } from "#ie/queueprocessors";

chai.use(chaiAsPromised);

describe("Queue Processor Helpers", () => {
    describe("filterQueueDefinitions", () => {
        it("should filter out all datasets", () => {
            const queues: IQueueDefinition[] = [
                {
                    name: "test1",
                    queuePrefix: "textex" + "." + "test1",
                    queues: [
                        {
                            name: "checkForNewData",
                            options: {
                                deadLetterExchange: "testex",
                                deadLetterRoutingKey: "dead",
                                messageTtl: 19 * 60 * 1000, // 19 minutes
                            },
                            worker: {} as any,
                            workerMethod: "checkForNewData",
                        },
                    ],
                },
            ];

            const blacklist = { test1: [] };
            const filteredQueues = filterQueueDefinitions(queues, blacklist);
            expect(filteredQueues).to.have.length(0);
        });

        it("should filter out one queue (case insensitive)", () => {
            const queues: IQueueDefinition[] = [
                {
                    name: "test2",
                    queuePrefix: "textex" + "." + "test2",
                    queues: [
                        {
                            name: "checkForNewData",
                            options: {
                                deadLetterExchange: "testex",
                                deadLetterRoutingKey: "dead",
                                messageTtl: 19 * 60 * 1000,
                            },
                            worker: {} as any,
                            workerMethod: "checkForNewData",
                        },
                        {
                            name: "checkForOldData",
                            options: {
                                deadLetterExchange: "testex",
                                deadLetterRoutingKey: "dead",
                                messageTtl: 19 * 60 * 1000,
                            },
                            worker: {} as any,
                            workerMethod: "checkForOldData",
                        },
                    ],
                },
            ];

            const blacklist = { test2: ["CHECKFOROLDDATA"] };
            const filteredQueues = filterQueueDefinitions(queues, blacklist);
            expect(filteredQueues[0].queues).to.have.length(1);
        });

        it("should filter out whole dataset (case insensitive)", () => {
            const queues: IQueueDefinition[] = [
                {
                    name: "test3",
                    queuePrefix: "textex" + "." + "test3",
                    queues: [
                        {
                            name: "checkForNewData",
                            options: {
                                deadLetterExchange: "testex",
                                deadLetterRoutingKey: "dead",
                                messageTtl: 19 * 60 * 1000,
                            },
                            worker: {} as any,
                            workerMethod: "checkForNewData",
                        },
                        {
                            name: "checkForOldData",
                            options: {
                                deadLetterExchange: "testex",
                                deadLetterRoutingKey: "dead",
                                messageTtl: 19 * 60 * 1000,
                            },
                            worker: {} as any,
                            workerMethod: "checkForOldData",
                        },
                    ],
                },
            ];

            const blacklist = { TEST3: [] };
            const filteredQueues = filterQueueDefinitions(queues, blacklist);
            expect(filteredQueues).to.have.length(0);
        });
    });
});
