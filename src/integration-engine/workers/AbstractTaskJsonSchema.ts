import { storageService } from "#ie/data-access";
import { AbstractGolemioError, GeneralError, ValidationError } from "@golemio/errors";
import { JSONSchemaValidator } from "@golemio/validator";
import { Message } from "amqplib";
import { AbstractTaskWithoutValidation } from "./AbstractTaskWithoutValidation";

export abstract class AbstractTaskJsonSchema<TRawData extends object> extends AbstractTaskWithoutValidation<TRawData> {
    protected abstract schema: JSONSchemaValidator;

    constructor(queuePrefix: string) {
        super(queuePrefix);
    }

    public async validateAndExecute(msg: Message | null) {
        try {
            if (!this.schema) {
                return this.execute({} as TRawData, msg?.properties);
            }

            const rawData = msg?.content.toString();
            const data = rawData && rawData.length > 0 ? JSON.parse(rawData) : undefined;
            await this.validate(data);

            return this.execute(data as TRawData, msg?.properties);
        } catch (err) {
            if (err instanceof AbstractGolemioError) {
                throw err;
            } else {
                throw new GeneralError(err.message, this.constructor.name, err);
            }
        }
    }

    private async validate(data: any) {
        try {
            await this.schema.Validate(data);
        } catch (err) {
            const validationError = new ValidationError(err.message, this.constructor.name, err);
            storageService.uploadFile(
                JSON.stringify({ exception: validationError.toObject(), data }),
                `validation-errors/${this.queuePrefix}.${this.queueName}`,
                "json"
            );

            throw validationError;
        }
    }
}
