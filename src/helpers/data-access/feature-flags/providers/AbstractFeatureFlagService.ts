import { IFeatureFlagService } from "../interfaces/IFeatureFlagService";

export abstract class AbstractFeatureFlagService implements IFeatureFlagService {
    abstract connect(): Promise<void>;
    abstract disconnect(): Promise<void>;
    abstract isFeatureEnabled(flagKey: string, defaultValue?: boolean): Promise<boolean>;
}
