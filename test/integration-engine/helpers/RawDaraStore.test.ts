import { ILogger } from "#helpers";
import { ISimpleConfig } from "#helpers/configuration/ISimpleConfig";
import SimpleConfig from "#helpers/configuration/SimpleConfig";
import { AbstractStorageService } from "#helpers/data-access/storage";
import { CoreToken } from "#helpers/ioc/CoreToken";
import RawDataStorage from "#ie/data-access/RawDataStorage";
import { ContainerToken } from "#ie/ioc";
import { expect } from "chai";
import sinon, { createSandbox, SinonFakeTimers } from "sinon";
import { container, Lifecycle } from "tsyringe";

describe("RawDataStore", () => {
    const sandbox = createSandbox();
    const uploadFileMock = sandbox.stub();

    let clock: SinonFakeTimers;

    const data = { key: "val" };

    const meta = {
        headers: {
            "content-type": "text/x-json;charset=UTF-8",
        },
        statusCode: 200,
    };

    const loggerMock = {
        error: sinon.stub(),
    } as unknown as ILogger;

    const storageMock = {
        uploadStream: sinon.stub(),
        uploadFile: uploadFileMock,
        deleteFile: sinon.stub(),
        getFileStream: sinon.stub(),
    };

    const createContainer = (whitelist: Record<string, any> = []) =>
        container
            .createChildContainer()
            .register(ContainerToken.Config, {
                useValue: { storage: { enabled: true }, saveRawDataWhitelist: whitelist },
            })
            .registerInstance<ISimpleConfig>(
                CoreToken.SimpleConfig,
                new SimpleConfig({
                    env: {
                        STORAGE_ENABLED: "true",
                    },
                })
            )
            .registerInstance<ILogger>(ContainerToken.Logger, loggerMock)
            .register<AbstractStorageService>(
                ContainerToken.StorageService,
                class DummyStorageService {
                    uploadStream = storageMock.uploadStream;
                    uploadFile = uploadFileMock;
                    deleteFile = storageMock.deleteFile;
                    getFileStream = storageMock.getFileStream;
                },
                { lifecycle: Lifecycle.Singleton }
            )
            .resolve<RawDataStorage>(RawDataStorage);

    beforeEach(() => {
        clock = sandbox.useFakeTimers(new Date("2019-10-01T00:00:00.000+02:00").getTime());
    });

    afterEach(() => {
        container.clearInstances();
        sandbox.reset();
        clock.restore();
    });

    after(() => {
        sandbox.restore();
        sinon.restore();
    });

    it("should have save method", async () => {
        const rawDataStore = createContainer();
        expect(rawDataStore.save).not.to.be.undefined;
    });

    it("should call upload to storage with correct arguments", async () => {
        const rawDataStore = createContainer({
            test: { saveHeaders: true },
        });

        await rawDataStore.save(data, meta, "test");

        expect(uploadFileMock.callCount).to.equal(1);
        expect(
            uploadFileMock.calledWith(
                {
                    key: "val",
                },
                "test",
                "json",
                "2019-10-01/00_00_00.000"
            )
        );
    });

    it("should upload to storage both body and headers if non 200 status code is provided", async () => {
        const rawDataStore = createContainer({
            test: { saveHeaders: true },
        });

        await rawDataStore.save(
            "data",
            {
                headers: {},
                statusCode: 400,
            },
            "test"
        );

        expect(uploadFileMock.callCount).to.equal(2);
        expect(uploadFileMock.getCall(0).args).to.deep.eq(["data", "test", "", "2019-10-01/00_00_00.000"]);
        expect(uploadFileMock.getCall(1).args).to.deep.eq(["{}", "test", "json", "2019-10-01/00_00_00.000_headers"]);
    });
});
