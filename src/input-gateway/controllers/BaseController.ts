import IQueueConnector from "#helpers/connector/interfaces/IQueueConnector";
import AlternateExchangeCreator from "#helpers/data-access/amqp/AlternateExchangeCreator";
import { config } from "#ig/config";
import { ContainerToken, InputGatewayContainer } from "#ig/ioc";
import { withSentryProducerTrace } from "#monitoring/sentry/sentry-transactions";
import { GeneralError } from "@golemio/errors";
import { IValidator } from "@golemio/validator";

/**
 * Base controller class, defines basic structure of a controller. ProcessData function, mandatory name
 */
export abstract class BaseController {
    /** Name of the model */
    public name: string;
    /** Abstract function for data processing (transformation, validation,...) */
    public abstract processData: (inputData: any) => Promise<void>;
    /** Input data validator */
    protected validator: IValidator;
    /** RabbitMQ queue prefix */
    protected queuePrefix: string;

    constructor(name: string, validator: IValidator) {
        this.name = name + "Controller";
        this.validator = validator;
        this.queuePrefix = config.rabbit_exchange_name + "." + name.toLowerCase();
    }

    /**
     * Sends message to RabbitMQ Exchange
     */
    private sendMessage = async (key: string, msg: any, options: object = {}): Promise<void> => {
        try {
            const ch = InputGatewayContainer.resolve<IQueueConnector>(ContainerToken.AmqpConnector).getChannel();
            await ch.assertExchange(config.rabbit_exchange_name as string, "topic", {
                durable: false,
                alternateExchange: AlternateExchangeCreator.getAltExchangeName(config.rabbit_exchange_name!),
            });
            await ch.publish(config.rabbit_exchange_name as string, key, Buffer.from(msg), options);
        } catch (err) {
            throw new GeneralError(`Sending the message to exchange failed. (${key})`, "BaseController", err, 500);
        }
    };

    protected sendMessageToExchange = withSentryProducerTrace(this.sendMessage);
}
