import fs from "fs";
import path from "path";
import { Request, Response, NextFunction } from "express";

/**
 * Abstract application to be used in projects
 */
export abstract class BaseApp {
    public abstract start: () => Promise<void>;

    /**
     * Load the commit SHA of the current build
     */
    protected loadCommitSHA = (): string => {
        const projectRootPath = fs.realpathSync(process.cwd());

        try {
            return fs.readFileSync(path.join(projectRootPath, "commitsha")).toString();
        } catch (err) {
            return "N/A";
        }
    };

    /**
     * Common headers middleware
     */
    protected commonHeaders = (_req: Request, res: Response, next: NextFunction) => {
        res.removeHeader("x-powered-by");
        res.setHeader("Access-Control-Allow-Origin", "*");
        next();
    };
}
