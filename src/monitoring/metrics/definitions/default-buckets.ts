import prometheus from "prom-client";

export const durationSecondsBuckets = [0.01, 0.025, 0.05, 0.1, 0.5, 1, 2.5, 5, 10];
export const sizeBytesBuckets = prometheus.exponentialBuckets(10, 10, 8);
export const countBuckets = prometheus.exponentialBuckets(1, 2, 18);
