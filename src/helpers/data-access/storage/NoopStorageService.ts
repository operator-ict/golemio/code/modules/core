import { Readable } from "stream";
import { ILogger } from "#helpers/logger";
import { AbstractStorageService } from "./AbstractStorageService";

export class NoopStorageService extends AbstractStorageService {
    constructor(private readonly log: ILogger) {
        super();
    }

    public async uploadStream(stream: Readable, pathPrefix: string, ext: string, fileName?: string): Promise<void> {
        this.log.silly("[NoopStorageService] Storage is disabled. Skipping stream upload.");
    }

    public async uploadFile(data: Buffer | string, pathPrefix: string, ext: string, fileName?: string): Promise<void> {
        this.log.silly("[NoopStorageService] Storage is disabled. Skipping file upload.");
    }

    public async deleteFile(fileKey: string) {
        this.log.silly("[NoopStorageService] Storage is disabled. Skipping file deletion.");
    }

    public async getFileStream(fileKey: string): Promise<undefined> {
        this.log.silly("[NoopStorageService] Storage is disabled. Skipping file fetching.");
        return;
    }
}
