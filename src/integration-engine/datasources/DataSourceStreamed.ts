import { LoggerEventType } from "#helpers/logger";
import { config } from "#ie/config";
import { DataSource, DataSourceStream, IDataSource } from "#ie/datasources";
import { log } from "#ie/helpers";
import { AbstractGolemioError, GeneralError } from "@golemio/errors";

export class DataSourceStreamed extends DataSource implements IDataSource {
    private dataBuffer: any[] = [];

    public proceed = async (): Promise<void> => {
        return this.dataStream.proceed();
    };

    public override async getAll(useDataBuffer = false): Promise<DataSourceStream> {
        const { data } = await this.getAllWithMetadata(useDataBuffer);
        return data;
    }

    public override async getAllWithMetadata(useDataBuffer = false): Promise<{ data: DataSourceStream; metadata: unknown }> {
        this.dataStream = await this.getOutputStream(useDataBuffer);

        this.dataStream.on("end", () => {
            this.dataStream.destroy();
        });

        this.dataStream.onDataListeners.push(async (data: any) => {
            if (this.validator) {
                try {
                    this.dataStream.pause();
                    await this.validator.Validate(data);
                    this.dataStream.resume();
                } catch (err) {
                    this.dataStream.destroy(new GeneralError("Error while validating source data.", this.name, err));
                }
            } else {
                log.warn("DataSource validator is not set.");
            }
        });

        return { data: this.dataStream, metadata: undefined };
    }

    /**
     * @param {boolean} useDataBuffer data is buffered and sent to output stream in `config.DATA_BATCH_SIZE` batches
     */
    protected getOutputStream = async (useDataBuffer: boolean = false): Promise<DataSourceStream> => {
        let inputStreamEndedAttempts = 0;
        this.dataStream = new DataSourceStream({
            objectMode: true,
            read: () => {
                return;
            },
        });

        const inputStream = await this.protocolStrategy.getData();

        inputStream.on("error", (error: any) => {
            if (error instanceof AbstractGolemioError) {
                this.dataStream.emit("error", error);
            } else {
                this.dataStream.emit("error", new GeneralError("DataSourceStreamed general error", this.constructor.name, error));
            }
        });

        inputStream.onDataListeners.push(async (data: any): Promise<void> => {
            inputStream.pause();

            if (useDataBuffer) {
                this.dataBuffer.push(data);
                await this.processData();
            } else {
                await this.processData(false, data);
            }

            inputStream.resume();
        });

        inputStream.on("end", async (): Promise<void> => {
            if (useDataBuffer) {
                await this.processData(true);
            }

            if (!inputStream.isPaused()) {
                this.dataStream.push(null);
            } else {
                const checker = setInterval(() => {
                    inputStreamEndedAttempts++;
                    // wait till all data is processed
                    if (!inputStream.isPaused()) {
                        clearInterval(checker);
                        this.dataStream.push(null);
                    } else if (inputStreamEndedAttempts > config.stream.wait_for_end_attempts) {
                        this.dataStream.emit("error", new GeneralError("Data Source stream has not ended", this.name));
                        this.dataStream.push(null);
                        clearInterval(checker);
                    }
                }, config.stream.wait_for_end_interval);
            }
        });

        inputStream.proceed().catch((error: Error) => this.dataStream.emit("error", error));

        return this.dataStream;
    };

    private processData = async (force = false, data = null): Promise<void> => {
        if (this.dataBuffer.length >= config.DATA_BATCH_SIZE || force || data) {
            try {
                let content: any;

                if (this.dataTypeStrategy?.parseData) {
                    content = await this.dataTypeStrategy.parseData(data || this.dataBuffer);
                } else {
                    content = data || this.dataBuffer;
                }

                if (this.isEmpty(content)) {
                    log.verbose(`[${this.name}] Data source returned empty data.`);
                    this.loggerEmitter.emit(LoggerEventType.NumberOfRecords, { name: this.name, numberOfRecords: 0 });
                } else {
                    const numberOfRecords = content instanceof Array ? content.length : 1;
                    this.loggerEmitter.emit(LoggerEventType.NumberOfRecords, { name: this.name, numberOfRecords });

                    this.dataStream.push(content);
                    // clear the buffer
                    this.dataBuffer.length = 0;
                }
            } catch (err) {
                this.dataStream.emit("error", new GeneralError("Retrieving of the source data failed.", this.name, err));
            }
        }
    };
}
