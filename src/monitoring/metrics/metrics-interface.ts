export interface IMetricsConfig {
    enabled: boolean;
    port: number;
    prefix: string;
}

export interface IMetricsClientConfig {
    app_name: string;
    app_version?: string;
    metrics: IMetricsConfig;
}

export interface IMetricsConfiguration {
    type: string;
    name: string;
    help: string;
    labelNames?: string[];
    buckets?: number[];
}

export type IMetricsConfigurationList<T extends Record<string, any>> = {
    [name in keyof T]: IMetricsConfiguration;
};
