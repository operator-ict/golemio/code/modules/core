import { IRouterExpireParameters } from "#og/routes/interfaces/IRouterExpireParameters";
import { Router } from "express";
import { IAbstractRouter } from "./interfaces/IAbstractRouter";
import { IRouterOptions } from "./interfaces/IRouterOptions";

export abstract class AbstractRouter implements IAbstractRouter {
    protected readonly version: string;
    protected readonly path: string;
    protected router: Router;

    protected constructor(version: string, path: string, options?: IRouterOptions) {
        this.version = version;
        this.path = path;
        this.router = Router({
            mergeParams: typeof options?.shouldMergeParams === "boolean" ? options.shouldMergeParams : undefined,
        });
    }

    public getRouter(): Router {
        return this.router;
    }

    public getPath(): string {
        return `/${this.version}/${this.path}`;
    }

    protected abstract initRoutes(expire?: IRouterExpireParameters | number | string): void;
}
