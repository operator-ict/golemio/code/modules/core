import { DataSourceStream } from "#ie/datasources";
import { IProtocolStrategy } from "#ie/datasources/protocol-strategy/IProtocolStrategy";
import { AbstractGolemioError, GeneralError } from "@golemio/errors";
import { HTTPRequestProtocolStrategy } from "./HTTPRequestProtocolStrategy";

export class HTTPRequestProtocolStrategyStreamed extends HTTPRequestProtocolStrategy implements IProtocolStrategy {
    private streamTransform: any;

    /**
     * @param streamTransform data will be piped  to this stream if provided
     */
    public setStreamTransformer = (streamTransform: any): HTTPRequestProtocolStrategyStreamed => {
        this.streamTransform = streamTransform;
        return this;
    };

    public async getData(): Promise<DataSourceStream> {
        const dataStream = await super.getData();

        const outStream = new DataSourceStream({
            objectMode: true,
            read: () => {
                return;
            },
        });

        let dataStreamEnd = false;

        if (this.streamTransform) {
            dataStream.on("data", (data: any) => {
                this.streamTransform.write(data);
            });
            dataStream.on("end", () => {
                this.streamTransform.end();
            });
            this.streamTransform.on("data", (data: any) => {
                outStream.push(data);
            });
            this.streamTransform.on("end", () => {
                outStream.push(null);
            });
            this.streamTransform.on("error", (error: any) => {
                if (error instanceof AbstractGolemioError) {
                    outStream.emit("error", error);
                } else {
                    outStream.emit("error", new GeneralError("Transformation stream error", this.constructor.name, error));
                }
            });
            dataStream.on("error", (error: any) => {
                if (error instanceof AbstractGolemioError) {
                    this.streamTransform.emit("error", error);
                } else {
                    this.streamTransform.emit("error", new GeneralError("Source stream error", this.constructor.name, error));
                }
            });
        } else {
            dataStream.on("data", (data: any) => {
                outStream.push(data);
            });
            dataStream.on("close", () => {
                // some streams do not emit end event
                if (!dataStreamEnd) {
                    outStream.push(null);
                    dataStreamEnd = true;
                }
            });
            dataStream.on("end", () => {
                // some streams do not emit close event
                if (!dataStreamEnd) {
                    outStream.push(null);
                    dataStreamEnd = true;
                }
            });
            dataStream.on("error", (error: any) => {
                if (error instanceof AbstractGolemioError) {
                    outStream.emit("error", error);
                } else {
                    outStream.emit("error", new GeneralError("Source stream error", this.constructor.name, error));
                }
            });
        }
        return outStream;
    }

    public getRawData = async (): Promise<any> => {
        if (this.connectionSettings.compression) {
            throw new GeneralError(
                "compressed resources are not supported in HTTPRequestProtocolStrategyStreamed yet",
                this.constructor.name
            );
        }

        try {
            const result = await this.getResponseInternal();

            return {
                data: result.body,
            };
        } catch (err) {
            throw new GeneralError("Error while getting data from server.", this.constructor.name, err);
        }
    };
}
