import prometheus, { Registry } from "prom-client";
import express, { Request, Response, NextFunction } from "express";
import { Server } from "http";
import { ILogger } from "#helpers";
import { apiMetricsMiddleware } from "#monitoring/metrics/metrics-middleware";
import { IMetricsClientConfig } from "#monitoring/metrics/metrics-interface";
import { registerMetrics } from "#monitoring/metrics/metrics-builder";
import { ICustomMetrics, customMetricsConfig } from "#monitoring/metrics/definitions/custom-metrics-definitions";

export interface IWorkerLabelData {
    name: string;
}

export type INumberOfRecordsLabelData = Request | IWorkerLabelData;

class MetricsService {
    private registry: Registry | undefined;
    private metricsConfig: IMetricsClientConfig | undefined;
    private log: ILogger | undefined;
    public customMetrics: ICustomMetrics | undefined;

    public init = (config: IMetricsClientConfig, log: ILogger) => {
        prometheus.register.setDefaultLabels({ appName: config.app_name, appVersion: config.app_version });
        prometheus.collectDefaultMetrics({
            prefix: config.metrics.prefix,
        });

        this.metricsConfig = config;
        this.registry = prometheus.register;
        this.log = log;

        this.customMetrics = registerMetrics(this.registry, customMetricsConfig, this.metricsConfig.metrics.prefix);
    };

    public metricsMiddleware = () => {
        if (!this.registry || !this.metricsConfig || !this.log) {
            throw new Error("Metrics service not initialized.");
        }
        return apiMetricsMiddleware(this.registry, this.metricsConfig, this.log);
    };

    public serveMetrics = (): Server | undefined => {
        if (!this.registry || !this.metricsConfig || !this.log) {
            throw new Error("Metrics service not initialized.");
        }

        if (!this.metricsConfig.metrics.enabled) return;

        const metricsApp = express();

        metricsApp.use("/metrics", async (req: Request, res: Response, _: NextFunction) => {
            const response = await this.registry!.metrics();
            res.writeHead(200, { "Content-Type": "text/plain" });
            res.end(response);
        });

        return metricsApp.listen(this.metricsConfig.metrics.port, () => {
            this.log!.info(`Serving metrics on http://localhost:${this.metricsConfig!.metrics.port}/metrics`);
        });
    };

    public recordNumberOfRecords = (labelsObject: INumberOfRecordsLabelData, numberOfRecords: number) => {
        if (!this.metricsConfig || !this.log || !this.customMetrics) {
            return;
        }

        const isRequestObject = (obj: INumberOfRecordsLabelData): obj is Request => {
            return (obj as Request).method !== undefined && (obj as Request).originalUrl !== undefined;
        };

        let labels;
        if (isRequestObject(labelsObject)) {
            labels = { method: labelsObject.method, route: labelsObject.originalUrl };
        } else {
            labels = { name: labelsObject.name };
        }

        if (this.metricsConfig.metrics.enabled) {
            this.customMetrics.moduleNumberOfRecords.labels(labels).observe(numberOfRecords);
        } else {
            this.log.verbose(`NumberOfRecords: ${labels.name ?? labels.method + " " + labels.route} : ${numberOfRecords}`);
        }
    };
}

const metricsService = new MetricsService();

export { metricsService };
