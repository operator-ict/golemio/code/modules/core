export interface IBaseModel {
    GetAll(options?: any): Promise<any>;
    GetOne(id: any): Promise<any>;
    GetName(): string;
}
