import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonStub } from "sinon";
import { log } from "#ie/helpers";
import { MessageDataValidator } from "#ie/workers/helpers/MessageDataValidator";
import { AbstractTask } from "#ie/workers/AbstractTask";
import { ITestInput, TestValidationSchema } from "./test-schema/TestSchema";
import { IntegrationEngineContainer } from "#ie/ioc";
import { ISimpleConfig } from "#helpers/configuration/ISimpleConfig";
import { CoreToken } from "#helpers/ioc/CoreToken";
import SimpleConfig from "#helpers/configuration/SimpleConfig";

chai.use(chaiAsPromised);

describe("AbstractTask", () => {
    const sandbox = sinon.createSandbox();

    class WithValidationTask extends AbstractTask<ITestInput> {
        public readonly queueName = "withValidationTest";
        public readonly queueTtl = 23 * 60 * 60 * 1000; // 23 hours
        protected readonly schema = TestValidationSchema;

        protected execute = sandbox.stub().resolves();
    }

    const taskWithValidation = new WithValidationTask("test.test");

    beforeEach(() => {
        sandbox.stub(log, "error");
        IntegrationEngineContainer.registerInstance<ISimpleConfig>(
            CoreToken.SimpleConfig,
            new SimpleConfig({ env: { RABBIT_CONSUMER_TIMEOUT: 10000 } })
        );
    });

    afterEach(() => {
        IntegrationEngineContainer.clearInstances();
        sandbox.restore();
    });

    // =============================================================================
    // consume
    // =============================================================================
    describe("consume", () => {
        it("should execute with valid data", async () => {
            sandbox.stub(MessageDataValidator, "validate" as any).resolves();

            await taskWithValidation.consume({
                content: Buffer.from('{"property":42}'),
                properties: { priority: 1 },
            } as any);
            expect((taskWithValidation["execute"] as SinonStub).getCall(0).args).to.deep.equal([
                { property: 42 },
                { priority: 1 },
            ]);
        });
    });
});
