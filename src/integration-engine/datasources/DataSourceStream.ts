import { AbstractGolemioError, GeneralError } from "@golemio/errors";
import { Readable } from "stream";
import { config } from "#ie/config";

export class DataSourceStream extends Readable {
    public onDataListeners: Function[] = [];
    public processing = 0;
    private onEndFunction?: () => Promise<void>;
    private streamEnded = false;

    constructor(...args: any) {
        super(...args);

        // to avoid data loss use proceed method or streamReady event to add on 'data' listeners
        this.on("streamReady", () => {
            this.onDataListeners.forEach((listener) => {
                this.on("data", listener as () => void);
            });
        });
    }

    public setOnEndFunction = (onEndFunction: () => Promise<void>): DataSourceStream => {
        this.onEndFunction = onEndFunction;
        return this;
    };

    public setDataProcessor = (onDataFunction: (data: any) => Promise<void>): DataSourceStream => {
        this.onDataListeners.push(async (data: any) => {
            this.processing++;
            this.pause();
            try {
                if (data instanceof Array) {
                    await onDataFunction([...data]);
                } else {
                    await onDataFunction(Object.assign({}, data));
                }
            } catch (err) {
                this.emit("error", new GeneralError("DataSourceStream processor failed", this.constructor.name, err));
            }

            this.resume();
            this.processing--;
        });

        return this;
    };

    public waitForEnd = async (): Promise<void> => {
        let inputStreamEndedAttempts = 0;
        return new Promise((resolve, reject) => {
            this.on("error", (error) => {
                if (error instanceof AbstractGolemioError) {
                    return reject(error);
                } else {
                    return reject(new GeneralError("DataSourceStream general error", this.constructor.name, error));
                }
            });
            this.on("end", async () => {
                if (!this.processing && !this.streamEnded) {
                    this.streamEnded = true;
                    if (this.onEndFunction) {
                        await this.onEndFunction();
                    }
                    resolve();
                } else {
                    const checker = setInterval(async () => {
                        inputStreamEndedAttempts++;

                        if (!this.processing && !this.streamEnded) {
                            clearInterval(checker);
                            this.streamEnded = true;
                            if (this.onEndFunction) {
                                await this.onEndFunction();
                            }
                            resolve();
                        } else if (inputStreamEndedAttempts > config.stream.wait_for_end_attempts) {
                            this.emit("error", new GeneralError("Input stream has not ended", "DataSourceStream"));
                            this.push(null);
                            clearInterval(checker);
                        }
                    }, config.stream.wait_for_end_interval);
                }
            });
        });
    };

    public proceed = (): Promise<void> => {
        this.emit("streamReady");
        return this.waitForEnd();
    };
}
