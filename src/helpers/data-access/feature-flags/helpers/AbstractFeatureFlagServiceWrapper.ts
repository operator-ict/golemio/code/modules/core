import { IFeatureFlagService } from "#helpers/data-access/feature-flags";
import { Disposable } from "tsyringe";

/**
 * Abstract wrapper of the feature flag service for dependency injection
 */
export abstract class AbstractFeatureFlagServiceWrapper implements Disposable {
    protected abstract _service: IFeatureFlagService;

    public get service(): IFeatureFlagService {
        return this._service;
    }

    public async dispose(): Promise<void> {
        await this._service.disconnect();
    }
}
