import RawDataStorage from "#ie/data-access/RawDataStorage";
import { FTPProtocolStrategy, FTPTargetType, IFTPSettings } from "#ie/datasources";
import { FileCompressor } from "#ie/helpers/FileCompressor";
import { ContainerToken } from "#ie/ioc";
import { IntegrationEngineContainer } from "#ie/ioc/Di";
import { GeneralError } from "@golemio/errors";
import { Client } from "basic-ftp";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";
import fsProm from "fs/promises";
import os from "os";
import path from "path";
import sinon, { SinonSandbox, SinonSpy, SinonStub } from "sinon";

chai.use(chaiAsPromised);

describe("FTPProtocolStrategy", () => {
    let testSettings: IFTPSettings;
    let strategy: FTPProtocolStrategy;
    let sandbox: SinonSandbox;
    let accessStub: SinonStub;
    let downloadStub: SinonStub;
    let lastmodStub: SinonStub;
    let now: Date;
    let rawDataStore: RawDataStorage;

    beforeEach(() => {
        sandbox = sinon.createSandbox();

        now = new Date();
        sandbox.stub(fs, "createWriteStream");
        accessStub = sandbox.stub().resolves();
        downloadStub = sandbox.stub();
        lastmodStub = sandbox.stub().callsFake(() => now);

        sandbox.stub(Client.prototype, "access").callsFake(accessStub);
        sandbox.stub(Client.prototype, "cd").callsFake(sandbox.stub());
        sandbox.stub(Client.prototype, "close").callsFake(sandbox.stub());
        sandbox.stub(Client.prototype, "downloadTo").callsFake(downloadStub);
        sandbox.stub(Client.prototype, "lastMod").callsFake(lastmodStub);

        testSettings = {
            filename: "ropidgtfsstops-datasource.json",
            path: "/",
            tmpDir: __dirname + "/data/",
            url: {
                host: "",
                password: "",
                secure: undefined,
                user: "",
            },
        };
        strategy = new FTPProtocolStrategy(testSettings);

        sandbox.spy(strategy, "getRawData");

        rawDataStore = IntegrationEngineContainer.resolve<RawDataStorage>(ContainerToken.RawDataStorage);
        sandbox.spy(rawDataStore, "save");
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should has getData method", async () => {
        expect(strategy.getData).not.to.be.undefined;
    });

    it("should has getRawData method", async () => {
        expect(strategy.getRawData).not.to.be.undefined;
    });

    it("should has getLastModified method", async () => {
        expect(strategy.getLastModified).not.to.be.undefined;
    });

    it("should has setConnectionSettings method", async () => {
        expect(strategy.setConnectionSettings).not.to.be.undefined;
    });

    it("should have getConnectionSettings method", async () => {
        expect(strategy.getConnectionSettings).not.to.be.undefined;
    });

    it("should properly get data", async () => {
        const res = await strategy.getData();
        expect(res.data).to.be.a("Uint8Array"); // chai issue #1028, buffer type not recognized
        expect(res.filepath).to.be.equal("ropidgtfsstops-datasource.json");
        expect(res.name).to.be.equal("ropidgtfsstops-datasource");
        sandbox.assert.calledOnce(strategy.getRawData as SinonSpy);
        sandbox.assert.calledOnce(rawDataStore.save as SinonSpy);
    });

    it("should throw error if getting data failed", async () => {
        downloadStub.throws(new Error("test"));
        await expect(strategy.getData()).to.be.rejectedWith(Error);
    });

    it("should properly get data in zip format", async () => {
        sandbox.stub(fsProm, "readFile").resolves(Buffer.from("test"));
        sandbox.stub(FileCompressor.prototype, "inflateFile").resolves({
            extractionPath: "",
            files: [
                {
                    name: "test1.txt",
                    path: path.join("testzip", "test1.txt"),
                    type: "file",
                    mtime: now,
                },
                {
                    name: "test2.txt",
                    path: path.join("testzip", "test2.txt"),
                    type: "file",
                    mtime: now,
                },
            ],
        });

        testSettings.filename = "testzip.zip";
        testSettings.targetType = FTPTargetType.COMPRESSED;
        strategy.setConnectionSettings(testSettings);
        const res = await strategy.getData();
        expect(res.length).to.be.equal(2);
        expect(res[0].filepath).to.be.equal(path.join("testzip", "test1.txt"));
    });

    it("should properly get only whitelisted data in zip format", async () => {
        sandbox.stub(fsProm, "readFile").resolves(Buffer.from("test"));
        sandbox.stub(FileCompressor.prototype, "inflateFile").resolves({
            extractionPath: "",
            files: [
                {
                    name: "test1.txt",
                    path: path.join("testzip", "test1.txt"),
                    type: "file",
                    mtime: now,
                },
            ],
        });

        testSettings.filename = "testzip.zip";
        testSettings.targetType = FTPTargetType.COMPRESSED;
        testSettings.whitelistedFiles = ["test1.txt"];
        strategy.setConnectionSettings(testSettings);
        const res = await strategy.getData();

        expect(res.length).to.be.equal(1);
        expect(res[0].filepath).to.be.equal(path.join("testzip", "test1.txt"));
        expect(res[0].name).to.be.equal("test1");
        expect(res[0].type).to.be.equal("file");
    });

    it("should properly encode file content", async () => {
        sandbox.stub(fsProm, "readFile").resolves(Buffer.from("74657374", "hex"));
        sandbox.stub(FileCompressor.prototype, "inflateFile").resolves({
            extractionPath: "",
            files: [
                {
                    name: "test1.txt",
                    path: path.join("testzip", "test1.txt"),
                    type: "file",
                    mtime: now,
                },
            ],
        });

        testSettings.filename = "testzip.zip";
        testSettings.targetType = FTPTargetType.COMPRESSED;
        testSettings.encoding = "utf8";
        strategy.setConnectionSettings(testSettings);
        const res = await strategy.getData();

        expect(res[0].data).to.be.equal("test");
        expect(res[0].filepath).to.be.equal(path.join("testzip", "test1.txt"));
        expect(res[0].name).to.be.equal("test1");
        expect(res[0].type).to.be.equal("file");
    });

    it("should properly get data in folder", async () => {
        testSettings.filename = "medicalinstitutions";
        testSettings.targetType = FTPTargetType.FOLDER;
        strategy.setConnectionSettings(testSettings);
        const res = await strategy.getData();
        expect(res.length).to.be.equal(4);
    });

    it("should properly get only whitelisted data in folder", async () => {
        testSettings.filename = "medicalinstitutions";
        testSettings.targetType = FTPTargetType.FOLDER;
        testSettings.whitelistedFiles = ["lekarny_seznam.csv"];
        strategy.setConnectionSettings(testSettings);
        const res = await strategy.getData();
        expect(res.length).to.be.equal(1);
        expect(res[0].data).to.be.a("Uint8Array"); // chai issue #1028, buffer type not recognized
        expect(res[0].filepath).to.equal(path.join("medicalinstitutions", "lekarny_seznam.csv"));
        expect(res[0].name).to.be.equal("lekarny_seznam");
    });

    it("should properly get last modified", async () => {
        const res = await strategy.getLastModified();
        expect(res).to.be.equal(now.toISOString());
    });

    it("should reject if the FTP server cannot be accessed while getting last modified date", async () => {
        accessStub.rejects(new Error("test"));

        const error = await expect(strategy.getLastModified()).to.be.rejectedWith(GeneralError);
        expect(error).to.have.property("message", "Error while getting last modified date");
    });

    it("should reject if last modified date is not available", async () => {
        lastmodStub.callsFake(() => null);

        const error = await expect(strategy.getLastModified()).to.be.rejectedWith(GeneralError);
        expect(error).to.have.property("message", "Last modified date is not available");
    });

    it("should set default settings options", async () => {
        const { tmpDir, ...defaultSettings } = testSettings;
        strategy.setConnectionSettings(defaultSettings);
        expect(strategy["connectionSettings"].targetType).to.be.equal(FTPTargetType.FILE);
        expect(strategy["connectionSettings"].tmpDir).to.equal(os.tmpdir());
    });
});
