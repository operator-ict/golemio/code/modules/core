import { ConfigLoader } from "#helpers";
import { isEmpty } from "#helpers/utils";
import { injectable } from "tsyringe";
import { IConfiguration } from "./abstract";

@injectable()
export class InputGatewayConfiguration implements IConfiguration {
    public app_version = process.env.npm_package_version;
    public app_name = process.env.APP_NAME || "input-gateway";
    public colorful_logs = process.env.COLORFUL_LOGS;
    public log_level = process.env.LOG_LEVEL;
    public node_env = process.env.NODE_ENV || "development";
    public payload_limit_csv = process.env.PAYLOAD_LIMIT_CSV;
    public payload_limit_json = process.env.PAYLOAD_LIMIT_JSON;
    public payload_limit_xml = process.env.PAYLOAD_LIMIT_XML;
    public port = process.env.PORT;
    public rabbit_connections = process.env.RABBIT_CONN?.split(",");
    public rabbit_exchange_name = process.env.RABBIT_EXCHANGE_NAME;
    public saveRawDataWhitelist: any;
    public sentry = {
        enabled: process.env.SENTRY_ENABLED === "true",
        dsn: process.env.SENTRY_DSN,
        release: `${process.env.APP_NAME}@${process.env.npm_package_version}`,
        environment: process.env.SENTRY_ENVIRONMENT,
        debug: process.env.SENTRY_DEBUG === "true",
        tracesSampleRate: process.env.SENTRY_TRACE_RATE ? parseFloat(process.env.SENTRY_TRACE_RATE) : 0,
    };
    public metrics = {
        enabled: process.env.METRICS_ENABLED === "true",
        port: process.env.METRICS_PORT ? parseInt(process.env.METRICS_PORT as string, 10) : 9001,
        prefix: process.env.METRICS_PREFIX || "",
    };
    public lightship = {
        shutdownDelay: process.env.LIGHTSHIP_SHUTDOWN_DELAY ? parseInt(process.env.LIGHTSHIP_SHUTDOWN_DELAY, 10) : 5000,
        shutdownTimeout: process.env.LIGHTSHIP_SHUTDOWN_TIMEOUT ? parseInt(process.env.LIGHTSHIP_SHUTDOWN_TIMEOUT, 10) : 60000,
        handlerTimeout: process.env.LIGHTSHIP_HANDLER_TIMEOUT ? parseInt(process.env.LIGHTSHIP_HANDLER_TIMEOUT, 10) : 5000,
    };

    constructor() {
        const saveRawDataWhitelist = new ConfigLoader("saveRawDataWhitelist").conf;
        this.saveRawDataWhitelist = isEmpty(saveRawDataWhitelist) ? [] : saveRawDataWhitelist;
    }
}
