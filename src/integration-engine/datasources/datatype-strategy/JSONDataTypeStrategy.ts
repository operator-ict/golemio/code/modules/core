import { getSubProperty } from "#helpers/utils";
import { IDataTypeStrategy, IJSONSettings } from "#ie/datasources/datatype-strategy/IDataTypeStrategy";
import { GeneralError } from "@golemio/errors";

export class JSONDataTypeStrategy implements IDataTypeStrategy {
    private resultsPath: string;
    private filter: ((item: any) => any) | undefined;

    constructor(settings: IJSONSettings) {
        this.resultsPath = settings.resultsPath;
        this.filter = undefined;
    }

    public setDataTypeSettings(settings: IJSONSettings): void {
        this.resultsPath = settings.resultsPath;
    }

    public setFilter(filterFunction: (item: any) => any) {
        this.filter = filterFunction;
    }

    public async parseData(data: any): Promise<any> {
        try {
            if (typeof data === "string") {
                data = JSON.parse(data);
            }

            if (this.resultsPath) {
                data = getSubProperty<any>(this.resultsPath, data);
            }

            if (this.filter) {
                data = data.filter(this.filter);
            }
            return data;
        } catch (err) {
            throw new GeneralError("Error while parsing source data.", this.constructor.name, err);
        }
    }
}
