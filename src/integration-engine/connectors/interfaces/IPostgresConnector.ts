import { Sequelize } from "sequelize/types";

export default interface IPostgresConnector {
    connect(connectionString?: string | undefined): Promise<Sequelize>;
    getConnection(): Sequelize;
    isConnected(): Promise<boolean>;
    disconnect(): Promise<void>;
}
