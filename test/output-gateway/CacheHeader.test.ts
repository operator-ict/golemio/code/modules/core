import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { CacheHeaderMiddleware } from "#og/CacheHeaderMiddleware";

chai.use(chaiAsPromised);

describe("CacheHeader middleware", () => {
    it("should set default cache header", () => {
        const middleware = new CacheHeaderMiddleware({
            getValue: () => "60",
        } as any);

        const res = {
            set: (key: string, value: string) => {
                expect(key).to.equal("Cache-Control");
                expect(value).to.equal("public, s-maxage=60, must-revalidate");
            },
        } as any;

        middleware.getMiddleware()(null as any, res, () => {});
    });

    it("should set custom cache header", () => {
        const middleware = new CacheHeaderMiddleware({
            getValue: () => "60",
        } as any);

        const res = {
            set: (key: string, value: string) => {
                expect(key).to.equal("Cache-Control");
                expect(value).to.equal("public, s-maxage=120, stale-while-revalidate=60");
            },
        } as any;

        middleware.getMiddleware(120, 60)(null as any, res, () => {});
    });

    it("should set custom cache header with default stale-while-revalidate", () => {
        const middleware = new CacheHeaderMiddleware({
            getValue: () => "60",
        } as any);

        const res = {
            set: (key: string, value: string) => {
                expect(key).to.equal("Cache-Control");
                expect(value).to.equal("public, s-maxage=120, must-revalidate");
            },
        } as any;

        middleware.getMiddleware(120)(null as any, res, () => {});
    });

    it("should set no cache header", () => {
        const middleware = new CacheHeaderMiddleware({
            getValue: () => null,
        } as any);

        const res = {
            set: (key: string, value: string) => {
                expect(key).to.equal("Cache-Control");
                expect(value).to.equal("no-store, no-cache, must-revalidate");
            },
        } as any;

        middleware.getNoCacheMiddleware()(null as any, res, () => {});
    });
});
