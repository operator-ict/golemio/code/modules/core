import { ISimpleConfig } from "#helpers/configuration/ISimpleConfig";
import { CoreToken } from "#helpers/ioc/CoreToken";
import { ILogger } from "#helpers/logger/LoggerProvider";
import { HTTPFetchProtocolStrategy } from "#ie/datasources/protocol-strategy/HTTPFetchProtocolStrategy";
import { IHTTPFetchSettings } from "#ie/datasources/protocol-strategy/interfaces/IHTTPFetchSettings";
import { IHTTPProtocolStrategyResult } from "#ie/datasources/protocol-strategy/interfaces/IHTTPProtocolStrategyResult";
import { IntegrationEngineContainer } from "#ie/ioc";
import { GeneralError } from "@golemio/errors";
import { IWebhook } from "./interfaces";

/** A webhook that purges Cloudflare cache for given API URL paths */
export class CloudflareCachePurgeWebhook implements IWebhook {
    protected readonly config: ISimpleConfig;
    protected readonly logger: ILogger;
    protected readonly enabled: boolean;
    protected readonly fetchSettings: IHTTPFetchSettings;
    protected readonly fetchStrategy: HTTPFetchProtocolStrategy;

    constructor(apiUrlPathsToPurge: string[]) {
        this.config = IntegrationEngineContainer.resolve<ISimpleConfig>(CoreToken.SimpleConfig);
        this.logger = IntegrationEngineContainer.resolve<ILogger>(CoreToken.Logger);
        this.enabled = this.config.getBoolean("env.CLOUDFLARE_ENABLED", false);
        const apiBaseUrl = this.config.getValue<string>("env.API_BASE_URL", "http://localhost");
        this.fetchSettings = {
            url: new URL(
                `zones/${this.config.getValue<string>("env.CLOUDFLARE_API_ZONE_ID", "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx")}` +
                    `/${this.config.getValue<string>("env.CLOUDFLARE_API_URL_PATH_PURGE_CACHE", "purge_cache")}`,
                this.config.getValue<string>("env.CLOUDFLARE_API_BASE_URL", "http://localhost")
            ).toString(),
            method: "POST",
            headers: {
                Authorization: `Bearer ${this.config.getValue<string>("env.CLOUDFLARE_API_KEY", "")}`,
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                files: apiUrlPathsToPurge.map((path) => new URL(path, apiBaseUrl)),
            }),
            responseType: "json" as const,
            timeoutInSeconds: 20,
        };
        this.fetchStrategy = new HTTPFetchProtocolStrategy(this.fetchSettings);
    }

    public async execute(): Promise<void> {
        if (!this.enabled) return;
        let res: IHTTPProtocolStrategyResult<unknown>;
        try {
            try {
                res = await this.fetchStrategy.getRawData();
            } catch (err) {
                throw new GeneralError("Error while reaching Cloudflare cache purge webhook", this.constructor.name, err);
            }
            this.responseValidation(res);
            this.logger.info("Cloudflare cache purged after generating GTFS-RT files", this.constructor.name);
        } catch (err) {
            this.logger.error(err);
        }
    }

    protected responseValidation(response: IHTTPProtocolStrategyResult<unknown>): void {
        if (response.meta.statusCode !== 200) {
            throw new GeneralError(
                "Failed to purge Cloudflare cache after generating GTFS-RT files (unexpected status code)",
                this.constructor.name,
                JSON.stringify(response),
                response.meta.statusCode
            );
        }
        if (
            typeof response.data !== "object" ||
            response.data === null ||
            !("success" in response.data) ||
            !response.data.success
        ) {
            throw new GeneralError(
                "Failed to purge Cloudflare cache after generating GTFS-RT files ('success' is 'false' or not in response)",
                this.constructor.name,
                JSON.stringify(response)
            );
        }
    }
}
