import { GeneralError } from "@golemio/errors";
import Sftp from "ssh2-sftp-client";
import iconv from "iconv-lite";
import { ISFTPSettings } from "#ie/datasources/protocol-strategy/IProtocolStrategy";
import { ProtocolStrategy } from "#ie/datasources/protocol-strategy/ProtocolStrategy";

export class SFTPProtocolStrategy extends ProtocolStrategy {
    protected connectionSettings!: ISFTPSettings;
    private sftpClient: Sftp;
    private connected = false;

    constructor(settings: ISFTPSettings) {
        super(settings);
        this.sftpClient = new Sftp();
    }

    public setConnectionSettings = (settings: ISFTPSettings): void => {
        this.connectionSettings = settings;
    };

    public getRawData = async (): Promise<any> => {
        try {
            // for testing
            // return iconv.decode(
            //     fs.readFileSync("/home/towdie/NadSv_2020-11-10.CSV"), this.connectionSettings.encoding,
            //     //await this.sftpClient.get(`/${lastMod.name}`) as Buffer, this.connectionSettings.encoding,
            // );
            await this.connect();

            let lastModTime = 0;
            let lastMod: any;

            (await this.sftpClient.list(this.connectionSettings.path || "/")).forEach((file: any) => {
                if (file.modifyTime > lastModTime) {
                    lastModTime = file.modifyTime;
                    lastMod = file;
                }
            });

            const rawData = {
                data: iconv.decode(
                    (await this.sftpClient.get(`/${this.connectionSettings.filename}`)) as Buffer,
                    this.connectionSettings.encoding
                ),
            };

            await this.disconnect();

            return rawData;
        } catch (err) {
            throw new GeneralError("Error while getting data from server.", this.constructor.name, err);
        }
    };

    public getLastModified = async (): Promise<string | null> => {
        try {
            // for testing
            // return iconv.decode(
            //     fs.readFileSync("/home/towdie/NadSv_2020-11-10.CSV"), this.connectionSettings.encoding,
            //     //await this.sftpClient.get(`/${lastMod.name}`) as Buffer, this.connectionSettings.encoding,
            // );
            await this.connect();

            let lastModTime = 0;
            let lastMod: any;

            (await this.sftpClient.list(this.connectionSettings.path || "/")).forEach((file: any) => {
                if (file.modifyTime > lastModTime) {
                    lastModTime = file.modifyTime;
                    lastMod = file;
                }
            });

            await this.disconnect();

            return lastMod.name;
        } catch (err) {
            throw new GeneralError("Error while getting data from server.", this.constructor.name, err);
        }
    };

    private connect = async (): Promise<void> => {
        if (!this.connected) {
            await this.sftpClient.connect({
                algorithms: this.connectionSettings.algorithms,
                host: this.connectionSettings.host,
                password: this.connectionSettings.password,
                port: this.connectionSettings.port,
                username: this.connectionSettings.username,
            });
            this.connected = true;
        }
    };

    private disconnect = async (): Promise<void> => {
        if (this.connected) {
            await this.sftpClient.end();
            this.connected = false;
        }
    };
}
