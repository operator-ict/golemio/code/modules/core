import { IMetricsConfig } from "#monitoring";
import { ISentryConfig } from "#monitoring/sentry/sentry-provider";

export interface IConfiguration {
    DATA_BATCH_SIZE: number;
    HOPPYGO_BASE_URL: string | undefined;
    LOG_LEVEL: string | undefined;
    MOJEPRAHA_ENDPOINT_BASEURL: string | undefined;
    NODE_ENV: string;
    OPEN_STREET_MAP_API_URL_REVERSE: string | undefined;
    OPEN_STREET_MAP_API_URL_SEARCH: string | undefined;
    PHOTON_MAP_API_URL_REVERSE: string | undefined;
    PHOTON_MAP_API_KEY: string | undefined;
    PARKINGS_PAYMENT_URL: string | undefined;
    PARKING_ZONES_PAYMENT_URL: string | undefined;
    POSTGRES_CONN: string | undefined;
    POSTGRES_POOL_MIN_CONNECTIONS: string | undefined;
    POSTGRES_POOL_MAX_CONNECTIONS: string | undefined;
    POSTGRES_POOL_IDLE_TIMEOUT: string | undefined;
    RABBIT_CONN: string | undefined;
    RABBIT_EXCHANGE_NAME: string | undefined;
    RABBIT_CHANNEL_MAX_PREFETCH_COUNT: number;
    REDIS_CONN: string | undefined;
    SPARQL_ENDPOINT_AUTH: string | undefined;
    SPARQL_ENDPOINT_URL: string | undefined;
    app_version: string | undefined;
    app_name: string;
    datasources: Record<string, any>;
    port: string | undefined;
    queuesBlacklist: Record<string, string[]>;
    sentry: ISentryConfig;
    saveRawDataWhitelist: Record<string, any>;
    stream: {
        wait_for_end_attempts: number;
        wait_for_end_interval: number;
    };
    metrics: IMetricsConfig;
    lightship: {
        shutdownDelay: number;
        shutdownTimeout: number;
        handlerTimeout: number;
    };
    vehiclePositions: {
        stepBetweenPoints: number;
        tcpBusTjrNegativeOffset: number;
        redisExpireTime: string;
        dataMaintenanceEnabled: boolean;
        dataRetentionLockTimeout: number;
        metroArrivalDelayAddition: number;
        metroDepartureDelayAddition: number;
    };
}
