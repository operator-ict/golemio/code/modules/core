import { Span } from "@opentelemetry/api";
import { ConsumeMessage } from "amqplib";
import {
    AmqplibInstrumentationConfig,
    AmqplibConsumeCustomAttributeFunction,
    AmqplibConsumeEndCustomAttributeFunction,
    AmqplibPublishCustomAttributeFunction,
    PublishInfo,
    ConsumeEndInfo,
} from "@opentelemetry/instrumentation-amqplib";
import { withRecordingSpan } from "../helpers";
import { AmqplibExtendedAttribute } from "../extended-attributes";

const amqplibConsumeEndHook: AmqplibConsumeEndCustomAttributeFunction = (span: Span, consumeEndInfo: ConsumeEndInfo): void => {
    span.setAttribute(AmqplibExtendedAttribute.CONSUME_END_OPERATION, consumeEndInfo.endOperation);
};

const amqplibPublishCustomAttributeFunction: AmqplibPublishCustomAttributeFunction = (
    span: Span,
    publishInfo: PublishInfo
): void => {
    const module = publishInfo.routingKey.split(".").slice(-2, -1)[0];
    span.setAttribute("service.module", module);
};

const amqplibConsumerCustomAttributeFunction: AmqplibConsumeCustomAttributeFunction = (
    span: Span,
    consumeInfo: {
        msg: ConsumeMessage | null;
    }
): void => {
    const module = consumeInfo.msg?.fields.routingKey.split(".").slice(-2, -1)[0];
    if (module) {
        span.setAttribute("service.module", module);
    }
};

export const amqplibConfig: AmqplibInstrumentationConfig = {
    consumeHook: withRecordingSpan(amqplibConsumerCustomAttributeFunction),
    publishHook: withRecordingSpan(amqplibPublishCustomAttributeFunction),
    consumeEndHook: withRecordingSpan(amqplibConsumeEndHook),
};
