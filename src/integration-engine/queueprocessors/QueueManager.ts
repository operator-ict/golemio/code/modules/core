import { AMQPConnector } from "#ie/connectors";
import { withSentryProducerTrace } from "#monitoring/sentry/sentry-transactions";
import { Options } from "amqplib";

export class QueueManager {
    public static sendMessageToExchange(
        queuePrefix: string,
        queueName: string,
        data: Record<string, any>,
        options: Options.Publish = {}
    ) {
        return QueueManager.sendMessage(`workers.${queuePrefix}.${queueName}`, JSON.stringify(data), options);
    }

    private static sendMessage = withSentryProducerTrace(AMQPConnector.sendMessage);
}
