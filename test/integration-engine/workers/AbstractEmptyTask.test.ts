import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonStub } from "sinon";
import { log } from "#ie/helpers";
import { AbstractEmptyTask } from "#ie/workers/AbstractEmptyTask";
import { IntegrationEngineContainer } from "#ie/ioc";
import { CoreToken } from "#helpers/ioc/CoreToken";
import { ISimpleConfig } from "#helpers/configuration/ISimpleConfig";
import SimpleConfig from "#helpers/configuration/SimpleConfig";

chai.use(chaiAsPromised);

describe("AbstractEmptyTask", () => {
    const sandbox = sinon.createSandbox();

    class WithoutValidationTask extends AbstractEmptyTask {
        public readonly queueName = "withoutValidationTest";
        public readonly queueTtl = 23 * 60 * 60 * 1000; // 23 hours

        protected execute = sandbox.stub().resolves();
    }

    const taskWithoutValidation = new WithoutValidationTask("test.test");

    beforeEach(() => {
        sandbox.stub(log, "error");
        IntegrationEngineContainer.registerInstance<ISimpleConfig>(
            CoreToken.SimpleConfig,
            new SimpleConfig({ env: { RABBIT_CONSUMER_TIMEOUT: 1000 } })
        );
    });

    afterEach(() => {
        IntegrationEngineContainer.clearInstances();
        sandbox.restore();
    });

    // =============================================================================
    // consume
    // =============================================================================
    describe("consume", () => {
        it("should execute without validating", async () => {
            const msgToStringStub = sandbox.stub();

            await taskWithoutValidation.consume({ content: { toString: msgToStringStub } } as any);
            expect(msgToStringStub.callCount).to.equal(0);
            expect((taskWithoutValidation["execute"] as SinonStub).getCall(0).args).to.deep.equal([{}, undefined]);
        });
    });
});
