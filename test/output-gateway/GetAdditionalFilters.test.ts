import { GeoJsonRouter } from "#og";
import { IGeoJsonModel } from "#og/models/interfaces/IGeoJsonModel";
import { expect } from "chai";

describe("GetAdditionalFilters at GeoJsonRouter", () => {
    const model = {} as IGeoJsonModel;
    const geoJsonRouter = new GeoJsonRouter(model);

    const testQuery = {
        districts: "test_districts",
        ids: "test_ids",
        latlng: "test_latlng",
        limit: "test_limit",
        offset: "test_offset",
        range: "test_range",
        updatedSince: "test_updatedSince",
        uncommonQueryParam: "value",
    };

    it("should return correct object with Additional query params", async () => {
        const result = geoJsonRouter.GetAdditionalFilters(testQuery);
        expect(result).to.deep.equal({ uncommonQueryParam: "value" });
    });
});
