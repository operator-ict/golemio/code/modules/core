export interface IHTTPProtocolStrategyResult<T> {
    data: T;
    meta: {
        headers: Record<string, string>;
        statusCode: number;
    };
}
