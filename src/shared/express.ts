import express from "express";
export = express;

declare global {
    namespace Express {
        interface Response {
            /**
             * Whether the response should be compressed even without it being explicitly accepted by the `Accept-Encoding`
             * request header. With this being set to `true`, one can opt out of compression by requesting with the
             * `Accept-Encoding` header set to `identity`. The default value when this property is undefined is `false`.
             */
            _shouldCompressByDefault?: boolean;
        }
    }
}
