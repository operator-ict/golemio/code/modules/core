import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonStub } from "sinon";
import { GeneralError } from "@golemio/errors";
import { log } from "#ie/helpers";
import { storageService } from "#ie/data-access/";
import { MessageDataParser } from "#ie/workers/helpers/MessageDataParser";
import { TestValidationSchema } from "../test-schema/TestSchema";

chai.use(chaiAsPromised);

describe("MessageDataParser", () => {
    const sandbox = sinon.createSandbox();
    let saveDataStub: SinonStub;

    beforeEach(() => {
        sandbox.stub(log, "error");
        saveDataStub = sandbox.stub(storageService, "uploadFile").resolves();
    });

    afterEach(() => {
        sandbox.restore();
    });

    // =============================================================================
    // parse
    // =============================================================================
    describe("parse", () => {
        it("should parse", async () => {
            const result = MessageDataParser.parse(
                "test.test.testTask",
                { content: Buffer.from('{"property":42}') } as any,
                TestValidationSchema
            );

            expect(result).to.be.instanceOf(TestValidationSchema);
            expect(result.property).to.equal(42);
            expect(saveDataStub.callCount).to.equal(0);
        });

        it("should throw (invalid message content)", async () => {
            const parse = MessageDataParser.parse.bind(
                undefined,
                "test.test.testTask",
                { content: Buffer.from('{"property:4') } as any,
                TestValidationSchema
            );

            expect(parse).to.throw(GeneralError, "[Queue test.test.testTask] Message parsing failed: '{\"property:4'");
            expect(saveDataStub.callCount).to.equal(1);
        });
    });
});
