declare module "apicache-plus";
declare module "redis-wstream";
declare module "redis-rstream";

declare namespace Express {
    interface Request {
        _metrics?: {
            initialBytesWritten: number;
        };
    }
}
