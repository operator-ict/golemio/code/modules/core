import { GeneralError } from "@golemio/errors";
import { DataSourceStream } from "./DataSourceStream";

export default class DataSourceStreamManager {
    public static processDataStream = async (
        dataSourceStream: Promise<DataSourceStream>,
        onDataFunction: (data: any) => Promise<void>
    ): Promise<void> => {
        let dataStream: DataSourceStream;

        try {
            dataStream = await dataSourceStream;
        } catch (err) {
            throw new GeneralError("Error while getting data", this.constructor.name, err);
        }

        try {
            await dataStream.setDataProcessor(onDataFunction).proceed();
        } catch (err) {
            throw new GeneralError("Error while processing data", this.constructor.name, err);
        }
    };
}
