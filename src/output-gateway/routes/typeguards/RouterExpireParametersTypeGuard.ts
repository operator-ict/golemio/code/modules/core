import { IRouterExpireParameters } from "../interfaces/IRouterExpireParameters";

export const isRouterExpireParameters = (val: any): val is IRouterExpireParameters => {
    if (val && typeof val === "object") {
        if (val.maxAge !== undefined && typeof val.maxAge !== "number") return false;
        if (val.staleWhileRevalidate !== undefined && typeof val.staleWhileRevalidate !== "number") return false;
        return true;
    }
    return false;
};
