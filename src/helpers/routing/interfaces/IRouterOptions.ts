export interface IRouterOptions {
    shouldMergeParams?: boolean;
}
