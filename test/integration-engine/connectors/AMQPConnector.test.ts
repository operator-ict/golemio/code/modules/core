import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon from "sinon";
import { FatalError, GeneralError } from "@golemio/errors";
import { AMQPConnector } from "#ie/connectors";

chai.use(chaiAsPromised);

describe("AMQPConnector", () => {
    const sandbox = sinon.createSandbox();

    afterEach(() => {
        sandbox.restore();
    });

    it("should throw if connect method was not called", async () => {
        expect(AMQPConnector.getChannel).to.throw(FatalError);
    });

    it("should connect to RabbitMQ and return channel", async () => {
        const ch = await AMQPConnector.connect();
        expect(ch).to.be.an.instanceof(Object);
    });

    it("should return channel", async () => {
        await AMQPConnector.connect();
        expect(AMQPConnector.getChannel()).to.be.an.instanceof(Object);
    });

    // =============================================================================
    // sendMessage
    // =============================================================================
    describe("sendMessage", () => {
        it("should assert exchange and publish a message", async () => {
            const assertExchangeStub = sandbox.stub().resolves();
            const publishMessageStub = sandbox.stub().resolves();

            sandbox.stub(AMQPConnector, "getChannel").returns({
                assertExchange: assertExchangeStub,
                publish: publishMessageStub,
            } as any);

            await AMQPConnector.sendMessage("testKey", "testMesssage");

            expect(assertExchangeStub.callCount).to.equal(1);
            expect(publishMessageStub.callCount).to.equal(1);
            expect(publishMessageStub.getCall(0).args[0]).to.be.string;
            expect(publishMessageStub.getCall(0).args[1]).to.equal("testKey");
            expect(publishMessageStub.getCall(0).args[2]).to.deep.equal(Buffer.from("testMesssage"));
            expect(publishMessageStub.getCall(0).args[3]).to.deep.equal({});
        });

        it("should reject (exchange assertion error)", async () => {
            const assertExchangeStub = sandbox.stub().rejects();
            const publishMessageStub = sandbox.stub().resolves();

            sandbox.stub(AMQPConnector, "getChannel").returns({
                assertExchange: assertExchangeStub,
                publish: publishMessageStub,
            } as any);

            const promise = AMQPConnector.sendMessage("testKey", "testMesssage");
            await expect(promise).to.be.rejectedWith(GeneralError, "Sending the message to exchange failed.");
        });
    });
});
