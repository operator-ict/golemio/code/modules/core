export class PaginationHelper {
    public static setPaginationParams(currentUrl: string): string {
        const url = new URL(currentUrl);
        const limit = url.searchParams.get("resultRecordCount") ?? "1000";

        url.searchParams.set("resultRecordCount", limit);
        url.searchParams.set("resultOffset", "0");
        return url.toString();
    }

    public static getNextPageUrl(currentUrl: string): string {
        const url = new URL(currentUrl);
        const limit = parseInt(url.searchParams.get("resultRecordCount") as string);
        const offset = parseInt(url.searchParams.get("resultOffset") as string);

        url.searchParams.set("resultOffset", (offset + limit).toString());
        return url.toString();
    }

    public static resetUrlOffset(currentUrl: string): string {
        const url = new URL(currentUrl);
        url.searchParams.set("resultOffset", "0");
        return url.toString();
    }
}
