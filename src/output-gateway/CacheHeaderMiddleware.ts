import { ISimpleConfig } from "#helpers/configuration/ISimpleConfig";
import { CoreToken } from "#helpers/ioc/CoreToken";
import { RequestHandler } from "express";
import { inject, injectable } from "tsyringe";

@injectable()
export class CacheHeaderMiddleware {
    private defaultMaxAge: number;

    constructor(@inject(CoreToken.SimpleConfig) config: ISimpleConfig) {
        this.defaultMaxAge = parseInt(config.getValue("env.CACHE_HEADER_MAX_AGE", "60"), 10);
    }

    /**
     * Creates Express middleware to set cache header for response
     * @param maxAge max age in seconds
     * @param staleWhileRevalidate stale-while-revalidate in seconds
     * @returns Express middleware
     */
    public getMiddleware(maxAge?: number, staleWhileRevalidate?: number) {
        return this.middlewarePublicCacheControl(maxAge, staleWhileRevalidate);
    }

    /**
     * Creates Express middleware to set no cache header for response
     * @returns Express middleware
     */
    public getNoCacheMiddleware() {
        return this.middlewareNoCache();
    }

    private middlewarePublicCacheControl(maxAge?: number, staleWhileRevalidate?: number): RequestHandler {
        const maxAgeSeconds = maxAge || this.defaultMaxAge;
        const revalidate = staleWhileRevalidate ? `stale-while-revalidate=${staleWhileRevalidate}` : "must-revalidate";

        return (_req, res, next) => {
            res.set("Cache-Control", `public, s-maxage=${maxAgeSeconds}, ${revalidate}`);
            next();
        };
    }

    private middlewareNoCache(): RequestHandler {
        return (_req, res, next) => {
            res.set("Cache-Control", "no-store, no-cache, must-revalidate");
            next();
        };
    }
}
