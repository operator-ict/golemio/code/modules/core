import { ILogger } from "#helpers/logger";
import { ITableStorageConfig } from "./interfaces/ITableStorageConfig";
import { ITableStorageServiceFactory } from "./interfaces/ITableStorageServiceFactory";
import { AzureTableStorageService } from "./providers/AzureTableStorageService";
import { NoopTableStorageService } from "./providers/NoopTableStorageService";
import { TableStorageProvider } from "./providers/enums/TableStorageProviderEnum";
import { ITableStorageService } from "./providers/interfaces/ITableStorageService";

export class TableStorageServiceFactory implements ITableStorageServiceFactory {
    private dictionary: Map<TableStorageProvider, ITableStorageService>;

    constructor(config: ITableStorageConfig, logger: ILogger) {
        this.dictionary = new Map();
        this.dictionary.set(TableStorageProvider.Noop, new NoopTableStorageService(logger));

        if (!config.enabled) {
            logger.silly("TableStorageServiceFactory: storage is disabled");
            return;
        } else if (config.provider.azure) {
            this.dictionary.set(TableStorageProvider.Azure, new AzureTableStorageService(config.provider.azure, logger));
        }
    }

    public getService(storageProvider: TableStorageProvider): ITableStorageService {
        return this.dictionary.get(storageProvider) ?? this.dictionary.get(TableStorageProvider.Noop)!;
    }
}
