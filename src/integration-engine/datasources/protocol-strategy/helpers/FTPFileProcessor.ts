import { FTPReturnType, IFile, IFTPSettings } from "#ie/datasources/protocol-strategy/IProtocolStrategy";
import { FileCompressor } from "#ie/helpers/FileCompressor";
import { Predicate } from "#ie/helpers/interfaces/Predicate";
import { createReadStream, ReadStream } from "fs";
import fs from "fs/promises";
import path from "path";

export class FTPFileProcessor {
    private readonly fileCompressor: FileCompressor;

    constructor(private readonly connectionSettings: IFTPSettings, private readonly downloadPath: string) {
        this.fileCompressor = new FileCompressor(this.downloadPath);
    }

    public processTargetFile(): Promise<IFile> {
        return this.prepareFileData(
            path.join(this.downloadPath, this.connectionSettings.filename),
            this.connectionSettings.filename
        );
    }

    public async processTargetFolder(prefix: string): Promise<IFile[]> {
        const whitelistPredicate = this.createWhitelistPredicate();
        const files = (await fs.readdir(path.join(this.downloadPath, this.connectionSettings.filename))).filter(
            whitelistPredicate
        );

        const result: IFile[] = [];
        for (const file of files) {
            result.push(await this.prepareFileData(path.join(this.downloadPath, prefix, file), path.join(prefix, file)));
        }

        return result;
    }

    public async processTargetCompressed(prefix: string): Promise<IFile[]> {
        const whitelistPredicate = this.createWhitelistPredicate();
        const { files } = await this.fileCompressor.inflateFile(
            path.join(this.downloadPath, this.connectionSettings.filename),
            whitelistPredicate
        );

        const result: IFile[] = [];
        for (const file of files) {
            const fileResult = await this.prepareFileData(file.path, path.join(prefix, file.name));

            result.push({
                ...fileResult,
                mtime: file.mtime.toISOString(),
                type: file.type,
            });

            if (fileResult.data instanceof ReadStream) {
                fileResult.data.once("end", async () => {
                    await fs.rm(file.path, { force: true });
                });
            } else {
                await fs.rm(file.path, { force: true });
            }
        }

        return result;
    }

    private async prepareFileData(absoluteFilePath: string, relativeFilePath: string): Promise<IFile> {
        const data =
            this.connectionSettings.returnType === FTPReturnType.Stream
                ? createReadStream(absoluteFilePath, this.connectionSettings.encoding ?? undefined)
                : this.encode(await fs.readFile(absoluteFilePath));

        return {
            data,
            filepath: relativeFilePath,
            name: path.parse(relativeFilePath).name,
        };
    }

    private encode(data: Buffer): Buffer | string {
        return this.connectionSettings.encoding ? data.toString(this.connectionSettings.encoding) : data;
    }

    private createWhitelistPredicate(): Predicate<[string]> {
        return (filename) => {
            if (this.connectionSettings.whitelistedFiles) {
                return this.connectionSettings.whitelistedFiles.indexOf(filename) >= 0;
            }

            return !!filename;
        };
    }
}
