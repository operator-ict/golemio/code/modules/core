import { FeatureFlagServiceFactory } from "#helpers/data-access/feature-flags";
import { NoopFeatureFlagService } from "#helpers/data-access/feature-flags/providers/NoopFeatureFlagService";
import { ILogger } from "#helpers/logger";
import { expect } from "chai";
import sinon, { SinonSandbox } from "sinon";

describe("FeatureFlagServiceFactory", () => {
    let sandbox: SinonSandbox;
    let factory: FeatureFlagServiceFactory;
    let log: Partial<ILogger>;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        log = {
            silly: sandbox.stub() as any,
            debug: sandbox.stub() as any,
            info: sandbox.stub() as any,
            warn: sandbox.stub() as any,
            error: sandbox.stub() as any,
        };
    });

    afterEach(() => {
        sandbox.restore();
    });

    // =============================================================================
    // getService
    // =============================================================================
    it("should return dummy service (feature flags are disabled)", () => {
        factory = new FeatureFlagServiceFactory(
            {
                enabled: false,
                provider: {},
            },
            log as ILogger
        );

        expect(factory.getService("test" as any)).to.be.instanceOf(NoopFeatureFlagService);
    });
});
