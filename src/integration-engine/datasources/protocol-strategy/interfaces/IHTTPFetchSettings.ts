export interface IHTTPFetchSettings {
    url: string;
    method: string;
    headers?: HeadersInit;
    body?: BodyInit | null;
    responseType?: "json" | "text" | "blob" | "arrayBuffer";
    timeoutInSeconds?: number;
}
