import { TableStorageProvider } from "../providers/enums/TableStorageProviderEnum";
import { ITableStorageService } from "../providers/interfaces/ITableStorageService";

export interface ITableStorageServiceFactory {
    getService(storageProvider: TableStorageProvider): ITableStorageService;
}
