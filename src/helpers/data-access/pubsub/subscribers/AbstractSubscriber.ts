import { EventEmitter } from "stream";

export abstract class Subscriber {
    abstract subscribe(): Promise<EventEmitter>;
    abstract unsubscribe(): Promise<void>;
    abstract listen(messageCallback: Function): Promise<void> | void;
}
