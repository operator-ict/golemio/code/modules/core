import { Sequelize } from "sequelize/types";

export interface IDatabaseConnector {
    connect(): Promise<Sequelize>;
    getConnection(): Sequelize;
    isConnected(): Promise<boolean>;
    disconnect(): Promise<void>;
}
