import crypto from "crypto";
import { createWriteStream } from "fs";
import fs from "fs/promises";
import path from "path";
import yauzl, { Entry, Options, ZipFile } from "yauzl";
import { IInflatedFileMetadata } from "./interfaces/IInflatedFileMetadata";
import { Predicate } from "./interfaces/Predicate";
import { log } from "./Logger";

interface IInflateOutput {
    extractionPath: string;
    files: IInflatedFileMetadata[];
}

export class FileCompressor {
    private static LIB_OPTIONS: Options = {
        lazyEntries: true,
    };

    constructor(private readonly basePath: string) {}

    public async inflateBuffer(buffer: Buffer, extractFilePredicate: Predicate<[string]>): Promise<IInflateOutput> {
        const archiveName = crypto.randomBytes(5).toString("hex");
        return new Promise((resolve, reject) => {
            yauzl.fromBuffer(buffer, FileCompressor.LIB_OPTIONS, async (err, zipFile) => {
                if (err) {
                    return reject(err);
                }

                try {
                    const result = await this.processZipFile(archiveName, zipFile, extractFilePredicate);
                    resolve(result);
                } catch (err) {
                    reject(err);
                }

                zipFile.close();
            });
        });
    }

    public async inflateFile(archivePath: string, extractFilePredicate: Predicate<[string]>): Promise<IInflateOutput> {
        const archiveName = path.basename(archivePath);
        return new Promise((resolve, reject) => {
            yauzl.open(archivePath, FileCompressor.LIB_OPTIONS, async (err, zipFile) => {
                if (err) {
                    return reject(err);
                }

                try {
                    const result = await this.processZipFile(archiveName, zipFile, extractFilePredicate);
                    resolve(result);
                } catch (err) {
                    reject(err);
                }

                zipFile.close();
            });
        });
    }

    private async processZipFile(
        archiveName: string,
        zipFile: ZipFile,
        extractFilePredicate: Predicate<[string]>
    ): Promise<IInflateOutput> {
        const extractionPath = await fs.mkdtemp(path.join(this.basePath, `${archiveName}-`));
        let files: IInflatedFileMetadata[] = [];

        return new Promise((resolve, reject) => {
            zipFile.readEntry();
            zipFile.on("entry", (entry: Entry) => {
                if (!extractFilePredicate(entry.fileName)) {
                    return zipFile.readEntry();
                }

                zipFile.openReadStream(entry, (err, readStream) => {
                    if (err) {
                        return reject(err);
                    }

                    const filePath = path.resolve(extractionPath, entry.fileName);
                    const fileStream = createWriteStream(filePath).on("finish", async () => {
                        log.info(`Extracted ${archiveName}:${entry.fileName}`);
                        files.push(await this.getFileMetadata(filePath, entry.fileName));

                        zipFile.readEntry();
                    });

                    readStream.pipe(fileStream);
                });
            });

            zipFile.once("end", () => {
                resolve({ extractionPath, files });
            });
        });
    }

    private async getFileMetadata(filePath: string, fileName: string): Promise<IInflatedFileMetadata> {
        const { mtime } = await fs.stat(filePath);
        return {
            path: filePath,
            name: fileName,
            mtime,
            type: "file", // backwards compatibility with the "decompress" output
        };
    }
}
