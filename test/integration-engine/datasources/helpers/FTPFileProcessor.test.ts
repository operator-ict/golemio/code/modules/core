import { FTPFileProcessor } from "#ie/datasources/protocol-strategy/helpers/FTPFileProcessor";
import { FTPReturnType, IFile, IFTPSettings } from "#ie/datasources/protocol-strategy/IProtocolStrategy";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs, { ReadStream } from "fs";
import fsProm from "fs/promises";
import path from "path";
import sinon, { SinonSandbox } from "sinon";
import { Readable } from "stream";

chai.use(chaiAsPromised);

describe("FTPFileProcessor", () => {
    const downloadPath = path.join(__dirname);
    const ftpSettings: Partial<IFTPSettings> = {
        filename: "test.csv",
        path: "/",
        returnType: FTPReturnType.File,
        encoding: "utf8",
    };

    let sandbox: SinonSandbox;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox.restore();
    });

    // =============================================================================
    // processTargetFile
    // =============================================================================
    describe("processTargetFile", () => {
        it("should prepare file", async () => {
            const ftpFileProcessor = new FTPFileProcessor({ ...ftpSettings } as IFTPSettings, downloadPath);
            const prepareFileDataStub = sandbox
                .stub(ftpFileProcessor, "prepareFileData" as any)
                .callsFake(async () => ({} as IFile));

            await ftpFileProcessor.processTargetFile();
            expect(prepareFileDataStub.getCall(0).args[0]).to.be.equal(path.join(downloadPath, "test.csv"));
            expect(prepareFileDataStub.getCall(0).args[1]).to.be.equal("test.csv");
        });
    });

    // =============================================================================
    // processTargetFolder
    // =============================================================================
    describe("processTargetFolder", () => {
        it("should prepare folder", async () => {
            sandbox.stub(fsProm, "readdir").callsFake(async () => ["test.csv", "bad.csv"] as any);
            const ftpFileProcessor = new FTPFileProcessor(
                { ...ftpSettings, whitelistedFiles: ["test.csv"] } as IFTPSettings,
                downloadPath
            );
            const prepareFileDataStub = sandbox
                .stub(ftpFileProcessor, "prepareFileData" as any)
                .callsFake(async () => ({} as IFile));

            await ftpFileProcessor.processTargetFolder("test-prefix");
            expect(prepareFileDataStub.calledOnce).to.be.true;
            expect(prepareFileDataStub.getCall(0).args[0]).to.be.equal(path.join(downloadPath, "test-prefix", "test.csv"));
            expect(prepareFileDataStub.getCall(0).args[1]).to.be.equal(path.join("test-prefix", "test.csv"));
        });
    });

    // =============================================================================
    // processTargetCompressed
    // =============================================================================
    describe("processTargetCompressed", () => {
        it("should prepare folder", async () => {
            const rmStub = sandbox.stub(fsProm, "rm").callsFake(async () => undefined);
            const ftpFileProcessor = new FTPFileProcessor({ ...ftpSettings } as IFTPSettings, downloadPath);

            sandbox.stub(ftpFileProcessor["fileCompressor"], "inflateFile").callsFake(async () => ({
                extractionPath: downloadPath,
                files: [
                    {
                        path: path.join(downloadPath, "test.csv"),
                        name: "test.csv",
                        mtime: new Date("2022-12-12T01:52:14.299Z"),
                        type: "file",
                    },
                ],
            }));

            const prepareFileDataStub = sandbox
                .stub(ftpFileProcessor, "prepareFileData" as any)
                .callsFake(async () => ({} as IFile));

            await ftpFileProcessor.processTargetCompressed("test-prefix");
            expect(prepareFileDataStub.calledOnce).to.be.true;
            expect(prepareFileDataStub.getCall(0).args[0]).to.be.equal(path.join(downloadPath, "test.csv"));
            expect(prepareFileDataStub.getCall(0).args[1]).to.be.equal(path.join("test-prefix", "test.csv"));
            expect(rmStub.calledOnce).to.be.true;
        });
    });

    // =============================================================================
    // prepareFileData
    // =============================================================================
    describe("prepareFileData", () => {
        it("should return encoded file contents", async () => {
            const readFileStub = sandbox.stub(fsProm, "readFile").callsFake(async () => Buffer.from("74657374", "hex"));
            const ftpFileProcessor = new FTPFileProcessor({ ...ftpSettings } as IFTPSettings, downloadPath);
            const fileData = await ftpFileProcessor["prepareFileData"](path.join(downloadPath, "test.csv"), "test.csv");

            expect(readFileStub.calledOnce).to.be.true;
            expect(fileData).to.be.deep.equal({
                data: "test",
                filepath: "test.csv",
                name: "test",
            });
        });

        it("should return file stream", async () => {
            const createReadStreamStub = sandbox
                .stub(fs, "createReadStream")
                .callsFake(() => Readable.from("test") as ReadStream);

            const ftpFileProcessor = new FTPFileProcessor(
                { ...ftpSettings, returnType: FTPReturnType.Stream } as IFTPSettings,
                downloadPath
            );

            const { data, ...meta } = await ftpFileProcessor["prepareFileData"](path.join(downloadPath, "test.csv"), "test.csv");
            expect(createReadStreamStub.calledOnce).to.be.true;
            expect(data).to.be.instanceOf(Readable);
            expect(meta).to.be.deep.equal({
                filepath: "test.csv",
                name: "test",
            });

            (data as ReadStream).destroy();
        });
    });
});
