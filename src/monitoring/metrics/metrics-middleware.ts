import { Request, Response, NextFunction } from "express";
import { LabelValues, Registry } from "prom-client";
import { getStatusCodeType, StatusCodeTypes } from "#monitoring/metrics/metrics-helpers";
import { registerMetrics } from "#monitoring/metrics/metrics-builder";
import { apiMetricsConfig, IApiMetrics } from "#monitoring/metrics/definitions/api-metrics-definitions";
import { IMetricsClientConfig } from "#monitoring/metrics/metrics-interface";
import { ILogger } from "#helpers";

export const apiMetricsMiddleware = (registry: Registry, config: IMetricsClientConfig, log: ILogger) => {
    if (!config.metrics.enabled) {
        return noopMiddleware;
    }

    const apiMetrics = registerMetrics(registry, apiMetricsConfig, config.metrics.prefix);

    return (req: Request, res: Response, next: NextFunction) => {
        handleRequest(apiMetrics, req, res, log);
        return next();
    };
};

const noopMiddleware = (req: Request, res: Response, next: NextFunction) => {
    return next();
};

const setMetricsRequestProp = (req: Request) => {
    req._metrics = {
        initialBytesWritten: req.socket.bytesWritten || 0,
    };
};

const getResponseContentLength = (req: Request, res: Response): number | string => {
    const resSize = res.get("Content-Length");
    if (resSize) {
        return resSize;
    }

    let initial = req._metrics!.initialBytesWritten;
    let written = req.socket.bytesWritten - initial;
    if ("_header" in res) {
        // @ts-ignore
        const headerBufferLength = Buffer.from(res["_header"]).length;
        written -= headerBufferLength;
    }
    return written;
};

const handleRequest = (metrics: IApiMetrics, req: Request, res: Response, log: ILogger) => {
    setMetricsRequestProp(req);

    let endMeasurement: (labels?: LabelValues<string>) => number;

    try {
        endMeasurement = metrics.apiRequestDurationSeconds.startTimer();

        metrics.apiAllRequestTotal.inc();
        metrics.apiAllRequestInProcessingTotal.inc();
    } catch (err) {
        log.warn(`ApiMetricsMiddleware: handleRequest error: ${err}`);
    }

    res.on("finish", () => {
        try {
            const routeNormalized = req.baseUrl + req.path;
            const labels = { method: req.method, route: routeNormalized, status: res.statusCode };

            metrics.apiRequestTotal.labels(labels).inc();

            const reqSize = req.get("Content-Length") || 0;
            metrics.apiRequestSizeBytes.observe(labels, Number(reqSize));

            const resSize = getResponseContentLength(req, res);
            metrics.apiResponseSizeBytes.observe(labels, Number(resSize));

            switch (getStatusCodeType(res.statusCode)) {
                case StatusCodeTypes.SUCCESS:
                    metrics.apiAllSuccessTotal.inc();
                    break;
                case StatusCodeTypes.ERROR:
                    metrics.apiAllErrorsTotal.inc();
                    break;
                default:
                    break;
            }
            metrics.apiAllRequestInProcessingTotal.dec();

            endMeasurement(labels);
        } catch (err) {
            log.warn(`ApiMetricsMiddleware: handleRequest on finish error: ${err}`);
        }
    });
};
