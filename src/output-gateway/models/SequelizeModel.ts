import { IDatabaseConnector } from "#helpers/data-access/postgres/IDatabaseConnector";
import { CoreToken } from "#helpers/ioc/CoreToken";
import { OutputGatewayContainer } from "#og/ioc";
import { BaseModel } from "#og/models";
import { ModelAttributes, ModelCtor, ModelOptions } from "sequelize";

/**
 * General model for data stored in PostgreSQL.
 *
 * Model /DATA ACCESS LAYER/: Defines data structure, connects to DB storage and retrieves data directly from database.
 * Performs database queries.
 */
export abstract class SequelizeModel extends BaseModel {
    /** The Sequelize Model */
    public sequelizeModel: ModelCtor<any>;

    /** Name of the model */
    protected name!: string;

    protected constructor(name: string, tableName: string, attributes: ModelAttributes<any>, options?: ModelOptions<any>) {
        super(name);
        const connector = OutputGatewayContainer.resolve<IDatabaseConnector>(CoreToken.PostgresConnector);
        // @ts-ignore
        this.sequelizeModel = connector.getConnection().define(tableName, attributes, {
            // Remove all audit fields in the default scope: they will be automatically excluded,
            // as they're not needed in the output view, but are present in the table/data
            defaultScope: {
                attributes: {
                    exclude: ["created_by", "updated_by", "created_at", "updated_at", "create_batch_id", "update_batch_id"],
                },
            },
            ...options,
        });
    }

    /**
     * Associate all database table relations
     */
    public Associate = (models: any): void => {
        return;
    };
}
