import { LoggerEventType, loggerEvents } from "#ig/helpers";
import { NextHandleFunction } from "connect";
import { NextFunction, Request, Response } from "express";

/**
 * Middleware function for response time measurement
 */
export let responseTimeMiddleware = (fn: NextHandleFunction) => {
    // Function takes 2 arguments
    if (fn.length === 2) {
        return function (req: Request, res: Response) {
            const start = Date.now();
            res.once("finish", () =>
                loggerEvents.emit(LoggerEventType.ResponseTimes, {
                    elapsedMS: Date.now() - start,
                    name: fn.name || "route",
                    req,
                })
            );
            // @ts-ignore
            return fn.apply(this, arguments);
        };
    } else if (fn.length === 3) {
        return (req: Request, res: Response, next: NextFunction) => {
            const start = Date.now();
            fn.call(this, req, res, function () {
                loggerEvents.emit(LoggerEventType.ResponseTimes, {
                    elapsedMS: Date.now() - start,
                    name: fn.name || "route",
                    req,
                });
                // @ts-ignore
                next.apply(this, arguments);
            });
        };
    } else {
        throw new Error("Function must take 2 or 3 arguments");
    }
};
