import { RedisConnector } from "#ie/connectors";
import RawDataStorage from "#ie/data-access/RawDataStorage";
import { HTTPFetchProtocolStrategy } from "#ie/datasources/protocol-strategy/HTTPFetchProtocolStrategy";
import { IHTTPFetchSettings } from "#ie/datasources/protocol-strategy/interfaces/IHTTPFetchSettings";
import { ContainerToken } from "#ie/ioc";
import { IntegrationEngineContainer } from "#ie/ioc/Di";
import { GeneralError } from "@golemio/errors";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import nock, { Scope } from "nock";
import path from "path";
import sinon, { SinonSandbox, SinonSpy } from "sinon";

chai.use(chaiAsPromised);

describe("HTTPFetchProtocolStrategy", () => {
    let testSettings: IHTTPFetchSettings;
    let sandbox: SinonSandbox;
    let strategy: HTTPFetchProtocolStrategy;
    let scope: Scope;
    let rawDataStore: RawDataStorage;

    before(async () => {
        await RedisConnector.connect();
    });

    beforeEach(() => {
        sandbox = sinon.createSandbox();

        testSettings = {
            headers: {},
            method: "GET",
            url: "https://httpbin.org/get",
            responseType: "json",
        };
        strategy = new HTTPFetchProtocolStrategy(testSettings);

        scope = nock("https://httpbin.org").get("/get").reply(200, {
            get: "ok",
        });
        scope = nock("https://httpbin.org").get("/ip").reply(200, {
            ip: "ok",
        });
        scope = nock("https://example.com")
            .get("/testzip.zip")
            .replyWithFile(200, __dirname + "/data/testzip.zip", {
                "Content-Type": "application/zip",
            });
        scope = nock("https://notfound.com").get("/").reply(404);

        sandbox.spy(strategy, "getRawData");
        rawDataStore = IntegrationEngineContainer.resolve<RawDataStorage>(ContainerToken.RawDataStorage);
        sandbox.spy(rawDataStore, "save");
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should has getData method", async () => {
        expect(strategy.getData).not.to.be.undefined;
    });

    it("should has getDataWithMetadata method", async () => {
        expect(strategy.getDataWithMetadata).not.to.be.undefined;
    });

    it("should has getRawData method", async () => {
        expect(strategy.getRawData).not.to.be.undefined;
    });

    it.skip("should has getLastModified method", async () => {
        expect(strategy.getLastModified).not.to.be.undefined;
    });

    it("should has setConnectionSettings method", async () => {
        expect(strategy.setConnectionSettings).not.to.be.undefined;
    });

    it("should have getConnectionSettings method", async () => {
        expect(strategy.getConnectionSettings).not.to.be.undefined;
    });

    it("should properly get data", async () => {
        const res = await strategy.getData();
        expect(JSON.stringify(res)).to.be.equal('{"get":"ok"}');
        sandbox.assert.calledOnce(strategy.getRawData as SinonSpy);
        sandbox.assert.calledOnce(rawDataStore.save as SinonSpy);
    });

    it("should properly get data with metadata", async () => {
        const res = await strategy.getDataWithMetadata();
        expect(JSON.stringify(res.data)).to.be.equal('{"get":"ok"}');
        expect(JSON.stringify(res.metadata)).to.be.equal('{"headers":{"content-type":"application/json"},"statusCode":200}');
        sandbox.assert.calledOnce(strategy.getRawData as SinonSpy);
        sandbox.assert.calledOnce(rawDataStore.save as SinonSpy);
    });

    it("should throw error if getting data failed", async () => {
        testSettings.url = "https://notfound.com";
        strategy.setConnectionSettings(testSettings);
        await expect(strategy.getData()).to.be.rejectedWith(GeneralError);
    });

    it.skip("should properly get data in zip format", async () => {
        testSettings.url = "https://example.com/testzip.zip";
        //testSettings.isCompressed = true;
        //testSettings.encoding = null;
        strategy.setConnectionSettings(testSettings);
        const res = await strategy.getData();
        expect(res.length).to.be.equal(2);
    });

    it.skip("should properly get only whitelisted data in zip format", async () => {
        testSettings.url = "https://example.com/testzip.zip";
        //testSettings.isCompressed = true;
        //testSettings.encoding = null;
        //testSettings.whitelistedFiles = ["test1.txt"];
        strategy.setConnectionSettings(testSettings);
        const res = await strategy.getData();
        expect(res.length).to.be.equal(1);
        expect(res[0].filepath).to.be.equal(`testzip${path.sep}test1.txt`);
    });

    it.skip("should properly get last modified", async () => {
        scope = nock("https://httpbin.org")
            .intercept("/get", "HEAD")
            .reply(200, undefined, { "last-modified": "Tue, 12 Mar 2019 17:32:09 GMT" });
        const res = await strategy.getLastModified();
        expect(res).to.be.equal("2019-03-12T17:32:09.000Z");
    });

    it.skip("should return null if last modified is not provided", async () => {
        testSettings.url = "https://notfound.com";
        strategy.setConnectionSettings(testSettings);
        const res = await strategy.getLastModified();
        expect(res).to.be.null;
    });

    it("should set settings options", async () => {
        testSettings.url = "https://httpbin.org/ip";
        strategy.setConnectionSettings(testSettings);
        const res = await strategy.getData();
        expect(JSON.stringify(res)).to.be.equal('{"ip":"ok"}');
    });
});
