import { log } from "#og/Logger";
import { IBaseModel } from "#og/models/interfaces/IBaseModel";
import { Router } from "express";

/**
 * Router /WEB LAYER/: maps routes to specific model functions, passes request parameters and handles responses.
 * Handles web logic (http request, response). Sets response headers, handles error responses.
 */
export class BaseRouter {
    // Assign router to the express.Router() instance
    public router: Router = Router();

    protected model?: IBaseModel;

    /**
     * Converts a single value of `any` type to an array containing this element
     */
    public ConvertToArray = (toBeArray: any) => {
        if (!(toBeArray instanceof Array)) {
            log.silly("Converting value `" + toBeArray + "` to array.");
            return [toBeArray];
        }
        return toBeArray;
    };
}
