export default interface IHistoryFilterParameters {
    from?: number;
    to?: number;
    limit?: number;
    offset?: number;
    sensorId?: string;
}
