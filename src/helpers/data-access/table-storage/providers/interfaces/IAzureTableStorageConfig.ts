export interface IAzureTableStorageConfig {
    clientId: string;
    tenantId: string;
    clientSecret: string;
    account: string;
    entityBatchSize: number;
    maxPageSize?: number;
}
