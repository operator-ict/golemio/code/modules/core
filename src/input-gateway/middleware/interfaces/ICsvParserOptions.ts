export interface ICsvParserOptions {
    delimiter: string;
    shouldIncludeHeaders?: boolean;
    shouldTrimValues?: boolean;
}
