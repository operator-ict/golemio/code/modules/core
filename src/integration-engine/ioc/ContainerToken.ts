import { CoreToken } from "#helpers/ioc/CoreToken";

const ContainerToken = {
    Config: Symbol(),
    /**
     * @deprecated Use CoreToken.Logger instead.
     */
    Logger: CoreToken.Logger,
    LoggerEmitter: Symbol(),
    LoggerEmitterProvider: Symbol(),
    RequestLogger: Symbol(),
    AmqpConnector: Symbol(),
    /**
     * @deprecated Use CoreToken.PostgresConnector instead.
     */
    PostgresConnector: CoreToken.PostgresConnector,
    RedisConnector: Symbol(),
    StorageService: Symbol(),
    StorageServiceProvider: Symbol(),
    TableStorageService: Symbol(),
    GeocodeApi: Symbol(),
    RawDataStorage: Symbol(),
};

export { ContainerToken };
