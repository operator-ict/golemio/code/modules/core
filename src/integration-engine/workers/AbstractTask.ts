import { Message } from "amqplib";
import { ClassConstructor } from "class-transformer";
import { AbstractTaskWithoutValidation } from "./AbstractTaskWithoutValidation";
import { MessageDataParser } from "./helpers/MessageDataParser";
import { MessageDataValidator } from "./helpers/MessageDataValidator";

export abstract class AbstractTask<TRawData extends object> extends AbstractTaskWithoutValidation<TRawData> {
    protected abstract schema: ClassConstructor<TRawData> | undefined;

    constructor(queuePrefix: string) {
        super(queuePrefix);
    }

    protected async validateAndExecute(msg: Message | null) {
        if (!this.schema) {
            return this.execute({} as TRawData, msg?.properties);
        }

        const data = MessageDataParser.parse(this.queueKey, msg, this.schema);
        await MessageDataValidator.validate(this.queueKey, data);

        return this.execute(data, msg?.properties);
    }
}
