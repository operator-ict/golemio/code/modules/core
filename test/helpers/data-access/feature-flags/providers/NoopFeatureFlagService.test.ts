import { NoopFeatureFlagService } from "#helpers/data-access/feature-flags/providers/NoopFeatureFlagService";
import { ILogger } from "#helpers/logger";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox, SinonSpy } from "sinon";

chai.use(chaiAsPromised);

describe("NoopFeatureFlagService", () => {
    let sandbox: SinonSandbox;
    let log: Partial<ILogger>;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        log = {
            silly: sandbox.stub() as any,
        };
    });

    afterEach(() => {
        sandbox.restore();
    });

    // =============================================================================
    // isFeatureEnabled
    // =============================================================================
    describe("isFeatureEnabled", () => {
        let service: NoopFeatureFlagService;

        beforeEach(() => {
            service = new NoopFeatureFlagService(log as ILogger);
        });

        it("should return false", async () => {
            const value = await service.isFeatureEnabled("test-feature");
            expect(value).to.be.false;
            expect((log.silly as SinonSpy).getCall(0).args).to.deep.equal([
                "[NoopFeatureFlagService] Feature flags are disabled",
            ]);
        });

        it("should return the default value", async () => {
            const value = await service.isFeatureEnabled("test-feature", true);
            expect(value).to.be.true;
            expect((log.silly as SinonSpy).getCall(0).args).to.deep.equal([
                "[NoopFeatureFlagService] Feature flags are disabled",
            ]);
        });
    });
});
