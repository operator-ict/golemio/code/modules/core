export type Predicate<T extends any[]> = (...params: T) => boolean;
