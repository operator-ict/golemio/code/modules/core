import { FileCompressor } from "#ie/helpers/FileCompressor";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs/promises";
import os from "os";
import path from "path";
import sinon, { SinonSandbox } from "sinon";

chai.use(chaiAsPromised);

describe("FileCompressor", () => {
    let sandbox: SinonSandbox;
    let fileCompressor: FileCompressor;
    const archivePath = path.resolve(__dirname, "fixture", "test-archive.zip");

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        sandbox.stub(fs, "stat").resolves({ mtime: new Date("2022-12-12T01:52:14.299Z") } as any);

        fileCompressor = new FileCompressor(os.tmpdir());
    });

    afterEach(() => {
        sandbox.restore();
    });

    // =============================================================================
    // inflateBuffer
    // =============================================================================
    describe("inflateBuffer", () => {
        it("should decompress archive from buffer", async () => {
            const buffer = await fs.readFile(archivePath);
            const { extractionPath, files } = await fileCompressor.inflateBuffer(buffer, () => true);
            const test1Path = path.resolve(extractionPath, "test1.txt");
            const test2Path = path.resolve(extractionPath, "test2.txt");

            expect(files).to.deep.equal([
                {
                    path: test1Path,
                    name: "test1.txt",
                    mtime: new Date("2022-12-12T01:52:14.299Z"),
                    type: "file",
                },
                {
                    path: test2Path,
                    name: "test2.txt",
                    mtime: new Date("2022-12-12T01:52:14.299Z"),
                    type: "file",
                },
            ]);

            expect(await fs.readFile(test1Path, "utf8")).to.contain("hello");
            expect(await fs.readFile(test2Path, "utf8")).to.contain("test");
            await fs.rm(extractionPath, { recursive: true, force: true });
        });

        it("should decompress archive from buffer (ignore a file with predicate)", async () => {
            const buffer = await fs.readFile(archivePath);
            const { extractionPath, files } = await fileCompressor.inflateBuffer(buffer, (filename) => filename === "test1.txt");
            const test1Path = path.resolve(extractionPath, "test1.txt");

            expect(files).to.deep.equal([
                {
                    path: test1Path,
                    name: "test1.txt",
                    mtime: new Date("2022-12-12T01:52:14.299Z"),
                    type: "file",
                },
            ]);

            expect(await fs.readFile(test1Path, "utf8")).to.contain("hello");
            await fs.rm(extractionPath, { recursive: true, force: true });
        });
    });

    // =============================================================================
    // inflateFile
    // =============================================================================
    describe("inflateFile", () => {
        it("should decompress archive from file", async () => {
            const { extractionPath, files } = await fileCompressor.inflateFile(archivePath, () => true);
            const test1Path = path.resolve(extractionPath, "test1.txt");
            const test2Path = path.resolve(extractionPath, "test2.txt");

            expect(files).to.deep.equal([
                {
                    path: test1Path,
                    name: "test1.txt",
                    mtime: new Date("2022-12-12T01:52:14.299Z"),
                    type: "file",
                },
                {
                    path: test2Path,
                    name: "test2.txt",
                    mtime: new Date("2022-12-12T01:52:14.299Z"),
                    type: "file",
                },
            ]);

            expect(await fs.readFile(test1Path, "utf8")).to.contain("hello");
            expect(await fs.readFile(test2Path, "utf8")).to.contain("test");
            await fs.rm(extractionPath, { recursive: true, force: true });
        });

        it("should decompress archive from file (ignore a file with predicate)", async () => {
            const { extractionPath, files } = await fileCompressor.inflateFile(
                archivePath,
                (filename) => filename === "test1.txt"
            );
            const test1Path = path.resolve(extractionPath, "test1.txt");

            expect(files).to.deep.equal([
                {
                    path: test1Path,
                    name: "test1.txt",
                    mtime: new Date("2022-12-12T01:52:14.299Z"),
                    type: "file",
                },
            ]);

            expect(await fs.readFile(test1Path, "utf8")).to.contain("hello");
            await fs.rm(extractionPath, { recursive: true, force: true });
        });
    });
});
