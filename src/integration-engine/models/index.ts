/* ie/models/index.ts */
export * from "./IModel";
export * from "./PostgresModel";
export * from "./RedisModel";
