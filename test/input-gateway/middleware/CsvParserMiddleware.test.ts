import { CsvParserMiddleware } from "#ig/middleware/CsvParserMiddleware";
import { AbstractGolemioError } from "@golemio/errors";
import { expect } from "chai";
import { Request, Response } from "express";
import { Readable } from "node:stream";

describe("CsvParserMiddleware", () => {
    it("should parse valid csv (delimiter comma, with headers, trim values)", (done) => {
        const csvText = "name,age\nJohn,25  \n  Jane, 30";
        const middleware = new CsvParserMiddleware().getMiddleware({
            delimiter: ",",
            shouldIncludeHeaders: true,
            shouldTrimValues: true,
        });

        const req = new Readable({ read: () => {} }) as Request;
        req.body = Buffer.from(csvText);

        const next = (err?: unknown) => {
            expect(req.body).to.deep.equal([
                { name: "John", age: "25" },
                { name: "Jane", age: "30" },
            ]);
            done();
        };

        middleware(req, {} as Response, next);
    });

    it("should parse valid csv (delimiter semicolon, with headers, w/o trim values)", (done) => {
        const csvText = "name;age\nJohn;25  \n  Jane; 30";
        const middleware = new CsvParserMiddleware().getMiddleware({
            delimiter: ";",
            shouldIncludeHeaders: true,
        });

        const req = new Readable({ read: () => {} }) as Request;
        req.body = Buffer.from(csvText);

        const next = (err?: unknown) => {
            expect(req.body).to.deep.equal([
                { name: "John", age: "25  " },
                { name: "  Jane", age: " 30" },
            ]);
            done();
        };

        middleware(req, {} as Response, next);
    });

    it("should parse valid csv (delimiter comma, w/o headers, trim values)", (done) => {
        const csvText = "John,25  \n  Jane, 30";
        const middleware = new CsvParserMiddleware().getMiddleware({
            delimiter: ",",
            shouldTrimValues: true,
        });

        const req = new Readable({ read: () => {} }) as Request;
        req.body = Buffer.from(csvText);

        const next = (err?: unknown) => {
            expect(req.body).to.deep.equal([
                ["John", "25"],
                ["Jane", "30"],
            ]);
            done();
        };

        middleware(req, {} as Response, next);
        req.push(csvText);
        req.push(null);
    });

    it("should call next with error on CSV buffer error", (done) => {
        const middleware = new CsvParserMiddleware().getMiddleware({
            delimiter: ",",
            shouldIncludeHeaders: true,
            shouldTrimValues: true,
        });

        const req = {} as Request;
        req.body = "invalid csv";

        const next = (err?: unknown) => {
            expect(err).to.be.instanceOf(AbstractGolemioError).and.have.property("message", "CSV data is not a buffer");
            done();
        };

        middleware(req, {} as Response, next);
    });

    it("should call next with error on CSV parse stream error", (done) => {
        const csvText = ',"aanameage\n\nJohn,25  \n  Jane, 30';
        const middleware = new CsvParserMiddleware().getMiddleware({
            delimiter: ",",
            shouldIncludeHeaders: true,
            shouldTrimValues: true,
        });

        const req = new Readable({ read: () => {} }) as Request;
        req.body = Buffer.from(csvText);

        const next = (err?: unknown) => {
            expect(err).to.be.instanceOf(AbstractGolemioError).and.have.property("message", "Error while parsing CSV data");
            done();
        };

        middleware(req, {} as Response, next);
    });
});
