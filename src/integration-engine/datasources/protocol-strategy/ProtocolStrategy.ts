import { IProtocolStrategy, ProtocolStrategySettings } from "#ie/datasources/protocol-strategy/IProtocolStrategy";
import { ISimpleConfig } from "#helpers/configuration/ISimpleConfig";
import { CoreToken } from "#helpers/ioc/CoreToken";
import { ILogger } from "#helpers/logger/LoggerProvider";
import { IConfiguration } from "#ie/config";
import RawDataStorage from "#ie/data-access/RawDataStorage";
import { DataSourceStream } from "#ie/datasources";
import { ContainerToken } from "#ie/ioc";
import { IntegrationEngineContainer } from "#ie/ioc/Di";
import { AbstractGolemioError, GeneralError } from "@golemio/errors";
import { Readable } from "stream";

export class ProtocolStrategy implements IProtocolStrategy {
    protected connectionSettings: any;

    private caller = "Unknown";
    private storageEnabled: boolean;
    private saveRawDataWhitelist: Record<string, any>;
    private logger: ILogger;
    private rawDataStorage: RawDataStorage;

    constructor(settings: any) {
        this.connectionSettings = settings;
        const simpleConfig = IntegrationEngineContainer.resolve<ISimpleConfig>(CoreToken.SimpleConfig);
        this.storageEnabled = simpleConfig.getValue("env.STORAGE_ENABLED", "false") === "true";
        const config = IntegrationEngineContainer.resolve<IConfiguration>(ContainerToken.Config);
        this.saveRawDataWhitelist = config.saveRawDataWhitelist;
        this.logger = IntegrationEngineContainer.resolve<ILogger>(CoreToken.Logger);
        this.rawDataStorage = IntegrationEngineContainer.resolve<RawDataStorage>(ContainerToken.RawDataStorage);
    }

    public setCallerName = (caller: string = "unknown datasource") => {
        this.caller = caller; // no ohher way ?
    };

    public setConnectionSettings(settings: any): void {
        this.connectionSettings = settings;
    }

    public getConnectionSettings = (): ProtocolStrategySettings => {
        return this.connectionSettings;
    };

    public async getData(...args: any[]): Promise<any | DataSourceStream> {
        const { data } = await this.getDataWithMetadata(...args);
        return data;
    }

    public async getDataWithMetadata(...args: unknown[]): Promise<{ data: unknown | DataSourceStream; metadata: unknown }> {
        const result = await this.getRawData(...args);
        const rawData = result.data;
        const meta = result.meta || {};
        let dataSaveCalled = false;

        let protocolDataStream: any;

        // bit paranoid but ...
        if (rawData && (rawData instanceof Readable || rawData.constructor.name === "Request") && rawData.on) {
            protocolDataStream = rawData;

            const S3Stream = new Readable({
                objectMode: false,
                read: () => {
                    return;
                },
            });

            const dataStream = new DataSourceStream({
                objectMode: true,
                read: () => {
                    return;
                },
            });

            // need to reemit data to the new stream to avoid data loss in future on 'data' events
            protocolDataStream.on("data", async (data: any) => {
                dataStream.push(data);
                await this.pushRawDataToS3Stream(data, S3Stream);

                // because of headers - need to be sure that `response` event has been called before saving raw data
                if (!dataSaveCalled) {
                    this.rawDataStorage.save(S3Stream, meta, this.caller);
                    dataSaveCalled = true;
                }
            });

            protocolDataStream.on("error", (err: Error) => {
                if (err instanceof AbstractGolemioError) {
                    dataStream.emit("error", err);
                } else {
                    dataStream.emit("error", new GeneralError("ProtocolData stream error", this.constructor.name, err));
                }
            });

            protocolDataStream.on("end", () => {
                dataStream.emit("end");
                S3Stream.push(null);
            });

            S3Stream.on("end", () => {
                return;
            });

            S3Stream.on("error", (err) => {
                this.logger.error(`Error on saving raw data for ${this.caller}`, err);
            });

            protocolDataStream.on("response", (res: any) => {
                meta.statusCode = res.statusCode;
                meta.headers = res.headers;
            });

            return { data: dataStream, metadata: meta };
        } else {
            this.rawDataStorage.save(await this.prepareRawData(rawData), meta, this.caller);
            return { data: rawData, metadata: meta };
        }
    }

    public getLastModified = async (): Promise<string | null> => {
        throw new Error("getLastModified: Method not implemented.");
    };

    protected async getRawData(...args: any): Promise<any | DataSourceStream> {
        throw new Error("getRawData: Method not implemented.");
    }

    private prepareRawData = async (data: any): Promise<string | null> => {
        let outData: string | null = null;

        if (data instanceof Blob) {
            outData = await data.text();
        } else if (data instanceof ArrayBuffer) {
            outData = new TextDecoder().decode(data);
        }

        if (data instanceof Uint8Array || data instanceof Buffer) {
            outData = data.toString();
        }

        if (!outData && typeof data === "object") {
            try {
                outData = JSON.stringify(data, null, 2);
            } catch {
                null;
            }
        }

        if (!outData && typeof data === "string") {
            try {
                outData = JSON.stringify(JSON.parse(data), null, 2);
            } catch {
                null;
            }
        }

        if (!outData && data && data.toString()) {
            outData = data.toString();
        }

        return outData;
    };

    private pushRawDataToS3Stream = async (data: any, S3Stream: Readable): Promise<void> => {
        if (this.storageEnabled && this.saveRawDataWhitelist[this.caller]) {
            const outData = await this.prepareRawData(data);

            if (outData && typeof outData === "string") {
                S3Stream.push(outData);
                if (typeof outData === "string") {
                    S3Stream.push("\n");
                }
            } else {
                this.logger.error(`Unable to save \`rawData\` in \`${this.caller}\`:
                \`Buffer\`, \`Uint8Array\` or  \`string\` is required, but got
                 \`${Object.prototype.toString.call(outData)}\``);
            }
        }
    };
}
