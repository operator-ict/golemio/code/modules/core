import { PaginationHelper } from "#ie/helpers/PaginationHelper";
import { GeneralError } from "@golemio/errors";
import { HTTPFetchProtocolStrategy } from "./HTTPFetchProtocolStrategy";
import { IHTTPFetchSettings } from "./interfaces/IHTTPFetchSettings";
import { IHTTPProtocolStrategyResult } from "./interfaces/IHTTPProtocolStrategyResult";

export interface IPaginatedFeatureResponse<T> {
    type: string;
    properties?: {
        exceededTransferLimit?: boolean;
    };
    features: T[];
}

export class PaginatedHTTPProtocolStrategy<T = any> extends HTTPFetchProtocolStrategy<IPaginatedFeatureResponse<T>> {
    constructor(settings: IHTTPFetchSettings) {
        super({ ...settings, url: PaginationHelper.setPaginationParams(settings.url) });
    }

    public async getRawData(): Promise<IHTTPProtocolStrategyResult<IPaginatedFeatureResponse<T>>> {
        try {
            let res = await super.getRawData();
            if (res.data === null) {
                throw new GeneralError("No data", this.constructor.name);
            }

            let resultPage: IHTTPProtocolStrategyResult<IPaginatedFeatureResponse<T>> = res;
            const result = resultPage;

            while (resultPage.data.properties?.exceededTransferLimit) {
                this.setNextPageUrl();
                res = await super.getRawData();
                if (res.data === null) {
                    throw new GeneralError("No data", this.constructor.name);
                }
                resultPage = res;
                result.data.features.push(...resultPage.data.features);
            }

            delete result.data.properties?.exceededTransferLimit;
            this.resetUrlOffset();

            return result;
        } catch (err) {
            throw new GeneralError("Error while getting data from server.", this.constructor.name, err);
        }
    }

    private setNextPageUrl() {
        this.setConnectionSettings({
            ...this.connectionSettings,
            url: PaginationHelper.getNextPageUrl(this.connectionSettings.url),
        });
    }

    private resetUrlOffset() {
        this.setConnectionSettings({
            ...this.connectionSettings,
            url: PaginationHelper.resetUrlOffset(this.connectionSettings.url),
        });
    }
}
