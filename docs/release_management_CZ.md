# Release Management (CZ)

[[_TOC_]]

## Potřebné přístupy

- GitLab
  - push do release větve ve všech modulech a služebách v `operator-ict/golemio/code`
  - approve a merge do master větve ve všech modulech a služebách v `operator-ict/golemio/code`
  - approve a merge do master větve v `operator-ict/security/golemio-golem`
- Kubernetes
  - čtení a úprava konfigurace služeb v `dp-golem` clusteru

## Postup

### 1. Kontrola změn v modulech a službách

Zkontrolujte, kde všude byly provedeny změny a co je potřeba releasnout. Vychází se z issues označených `state::To Deploy` a `Release Candidate`.

Prakticky je to git diff mezi release a master větvemi.

<details><summary>Golemio CLI:</summary>

V Golemio CLI je na to příkaz [`rls check --scope <scope>`](https://gitlab.com/operator-ict/golemio/code/golemio-cli/-/blob/master/docs/release_management_CZ.md?ref_type=heads#ozna%C4%8Den%C3%AD-release-kandid%C3%A1t%C5%AF-check). Scope je `core`, `modules` nebo `services`. Ten projde všechny repozitáře daného scope a zeptá se, jestli ho chcete zařadit do releasu.
</details>

<details><summary>Ručně:</summary>

```bash
git diff origin/release..origin/master --stat
```
</details>

### 2. Vytvoření release issue

Vytvořte issue podle release šablony v projektu https://gitlab.com/operator-ict/golemio/code/general. Příklad https://gitlab.com/operator-ict/golemio/code/general/-/issues/403+

Do tohoto issue doplňte všechny změny, které chcete nasadit.

<details><summary>Golemio CLI:</summary>

V Golemio CLI můžete použít příkaz [`rls print-release-issue`](https://gitlab.com/operator-ict/golemio/code/golemio-cli/-/blob/master/docs/release_management_CZ.md?ref_type=heads#voliteln%C4%9B-v%C3%BDpis-pro-release-issue-print-release-issue), který vám šablonu předvyplní.
</details>

<details><summary>Ručně:</summary>

Prostě vyplním šablonu tím, co se bude releasovat.
</details>

### 3. Informace do slacku

Pro info pošlu do kanálů [#dp-vyvoj](https://golemiocz.slack.com/archives/CDBB07T7G) a [#case-pid-polohy](https://golemiocz.slack.com/archives/C01ED9606VC), že se bude releasovat a kdy. Příklad:

> :zap: Dnes release DP/VP kolem 11
>
> https://gitlab.com/operator-ict/golemio/code/general/-/issues/592


### 4. Merge připravených modulů

Mergňete všechny připravené moduly a služby do masteru. Toto by mělo proběhnout v pořadí scope `core`, `modules`.

Prakticky je to bumpnutí verze, aktualizace changelogu, aktualizace @golemio závislostí a merge do masteru.

<details><summary>Golemio CLI:</summary>

V Golemio CLI je na to příkaz [`rls merge --scope <scope>`](https://gitlab.com/operator-ict/golemio/code/golemio-cli/-/blob/master/docs/release_management_CZ.md?ref_type=heads#merge-release-kandid%C3%A1t%C5%AF-merge). Scope je `core`, `modules` nebo `services`. Ten projde všechny repozitáře označené k releasu (z bodu *Kontrola změn v modulech a službách*) a mergne je.
</details>

<details><summary>Ručně:</summary>

Pro všechny repozitáře core a modules.

```bash
git checkout release
```

Porovnám verze s masterem a upravím `CHANGELOG.md` a `package.json`.

Pokud je potřeba, tak upgradnu @golemio závislosti.

```bash
npm install -E $(npm ls -p --depth=0 | grep -o '@golemio.*' | sed 's/$/@latest/' | sed 's/\\/\//')
```

Změny commitnu.

```bash
git commit -m "chore: bump version and update changelog"
```

Vytvořím merge request do masteru.
</details>

#### Kontrola a potvrzení merge requestů

Pokud použijete Golemio CLI, tak se vám všechny merge requesty otevřou v prohlížeči. Zkontrolujte, že všechny merge requesty jsou v pořádku a potvrďte je.

Je potřeba počkat na doběhnutí pipeline.

<details><summary>Golemio CLI:</summary>

V Golemio CLI pro to lze použít příkaz [`rls check-mr-pipelines --scope <scope>`](https://gitlab.com/operator-ict/golemio/code/golemio-cli/-/blob/master/docs/release_management_CZ.md?ref_type=heads#kontrola-stavu-pipeline-check-mr-pipelines).
</details>

<details><summary>Ručně:</summary>

Normálně proklikám do GitLabu a zkontroluji, že pipeline doběhly v pořádku.
</details>


### 5. Merge připravených služeb

Po merge modulů je potřeba provést merge služeb.

Tento postup je stejný jako v bodě *Merge připravených modulů* akorát pro služby.

<details><summary>Golemio CLI:</summary>

Často se stává, že služba neobsahuje změny, proto se nepřipraví k releasu. V takovém případě je potřeba id repozitáře služby ručně přidat do `tmp/release-candidates_services.json`.
</details>

<details><summary>Ručně:</summary>

Stejně jako v bodě *Merge připravených modulů* akorát pro služby.
</details>


### 6. Nasazení do kubernetu

Až všechny pipeliny doběhnou a každá služba/web má připravenou novou verzi, je potřeba synchronizovat verze v kubernetes clusteru.

To lze pomocí nástroje ArgoCD, který je dostupný na adrese [argocd.golem.oictinternal.cz](https://argocd.golem.oictinternal.cz/). V ArgoCD je potřeba najít aplikaci, která odpovídá službě/webu, kterou chcete nasadit, a kliknout na *SYNC*.

<details><summary>Postup:</summary>

Otevřu ArgoCD na adrese [argocd.golem.oictinternal.cz](https://argocd.golem.oictinternal.cz/), najdu aplikaci, kterou chci nasadit, zkontroluju *DIFF* a kliknu na *SYNC*.

![release argocd 01](assets/release_argocd_01.png)

![release argocd 02](assets/release_argocd_02.png)

![release argocd 03](assets/release_argocd_03.png)

![release argocd 04](assets/release_argocd_04.png)

![release argocd 05](assets/release_argocd_05.png)

![release argocd 06](assets/release_argocd_06.png)

</details>


### 7. Kontrola běhu všech služeb

Zkontrolujte, že všechny služby běží správně. To lze jednoduše pohledem do Lens. Dalé je potřeba zkotrolovat:
- žádná ze služeb se nerestartuje
- žádná ze služeb neloguje neobvyklé chyby
- do slack kanálu [#dp-alerts](https://golemiocz.slack.com/archives/CMSQC101F) nechodí chyby
- [mapa.pid.cz](https://mapa.pid.cz/) zobrazuje data
- [Ropid dohledový monitoring](https://grafana.golemio.cz/d/94NYuhKVk/dohledovy-monitoring-pc?orgId=1&refresh=5m) nehlási chybu
- [odpady.mojepraha.eu](https://odpady.mojepraha.eu/) zobrazuje data


### 8. Synchronizace git větví

Až vše proběhne v pořádku, je potřeba synchronizovat větve `master`, `release` a `development`.

Prakticky je to merge `master` do `release` a `development`.

Při merge `master` do `development` je potřeba dát pozor na konflikty. Většinou se jedná o změny v `package.json`, `package-lock.json` a `CHANGELOG.md`. Konflikty se musí vyřešit ručně.

<details><summary>Golemio CLI:</summary>

To lze pomocí Golemio CLI příkazu [`rls sync-branches --scope <scope>`](https://gitlab.com/operator-ict/golemio/code/golemio-cli/-/blob/master/docs/release_management_CZ.md?ref_type=heads#synchronizace-v%C4%9Btv%C3%AD-sync-branches).
</details>

<details><summary>Ručně:</summary>

Pro všechny repozitáře core, modules a services.

```bash
git checkout release
git pull origin release
git pull origin master --ff
git push origin release
```

```bash
git checkout development
git pull origin development
git pull origin master --ff
npm version patch --no-git-tag-version
git add .
git commit -m "chore: bump version"
git push origin development
```
</details>


### 9. Vytvoření tagu a release

Až jsou všechny větve synchronizované, je potřeba vytvořit tag a release.

Prakticky je to vytvoření tagu s novou verzí a vytvoření release na GitLabu.

<details><summary>Golemio CLI:</summary>

To lze pomocí Golemio CLI příkazu [`rls create-tags --scope <scope>`](https://gitlab.com/operator-ict/golemio/code/golemio-cli/-/blob/master/docs/release_management_CZ.md?ref_type=heads#vytvo%C5%99en%C3%AD-tagu-a-releasu-create-tags).
</details>

<details><summary>Ručně:</summary>

V GitLabu v záložce Deploy - Releases vytvořím nový release s tagem a changelogem.

![release 01](assets/release_01.png)

![release 02](assets/release_02.png)

![release 03](assets/release_03.png)

</details>


### 10. Posunutí issues dál

Dotčená issue přesuňte do `state::To Verify - PROD`, odeberte label `Release Candidate` a označte člověka, který má issue akceptovat.

Nakonec zavřete release issue.


### 11. Informace do slacku

Pro info pošlu do kanálů [#dp-vyvoj](https://golemiocz.slack.com/archives/CDBB07T7G) a [#case-pid-polohy](https://golemiocz.slack.com/archives/C01ED9606VC), že se release povedl.



## Pomocné příkazy

### bump @golemio závislostí

```bash
npm install -E $(npm ls -p --depth=0 | grep -o '@golemio.*' | sed 's/$/@latest/')
```

Windows bash:
```bash
npm install -E $(npm ls -p --depth=0 | grep -o '@golemio.*' | sed 's/$/@latest/' | sed 's/\\/\//')
```

### Kontrola verze aktuálně nasazené služby

Občas se stane, že se nasadí starší verze služby, než je aktuální v masteru. Tu zjistím přímo v repozitáří dané služby, v levém menu Deploy -> Container Registry -> master -> poslední číselní tag. Tuto verzi porovnám s hodnotou v infrastructure repozitáři (zajímají nás master versions, například pro integrační engine je to [tady](https://gitlab.com/operator-ict/golemio/devops/infrastructure/-/blob/master/golemio/values-master-versions.yaml?ref_type=heads)).

A dále přímo s verzí v K8s. To lze zjistit následovně:

```bash
# V pripade poloh vozu -n vehicle-positions
kubectl-n golemio get deploy/$SLUZBA -o jsonpath="{..image}"
```

Napravení verze lze udělat přímo v infrastructure yamlu, případně kontaktujeme devops.

### Nasazení starší verze služby

Na Gitlabu v Deploy -> Container Registry -> master zvolte tag verze, kterou chcete nasadit. V K8s lens v Cluster -> Workloads -> Deployments vyberte službu a vpravo nahoře dejte edit, nebo v terminálu zadejte

```bash
# V pripade poloh vozu -n vehicle-positions
$ kubectl -n golemio edit deployments.apps $SLUZBA
```

![Deployment](assets/lens_deployment.png)

V configu nahraďte tag v příznaku **containers.image** a uložte

### Kontrola verze nainstalovaného modulu ve službě

V minulosti se nám stalo, že nasadila aktuální verze služby, v package.json byly správně verze modulů, ale přesto se nainstalovala stará verze modulu z důvodu špatně nastaveného cachování v Gitlab pipeline. Již by to nemělo nastat, ale pro jistotu je dobré to zkontrolovat.

```bash
# V pripade poloh vozu -n vehicle-positions
kubectl exec -n golemio --stdin --tty $POD -- sh

# Po pripojeni do kontejneru
# Kontrola verze v package.json
cat package.json | grep "@golemio/$MODUL"

# Kontrola reálně nainstalovaného modulu
head node_modules/@golemio/flow/package.json | grep version
```

