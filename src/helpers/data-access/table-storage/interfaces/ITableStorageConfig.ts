import { TableStorageProvider } from "../providers/enums/TableStorageProviderEnum";
import { IAzureTableStorageConfig } from "../providers/interfaces/IAzureTableStorageConfig";

export interface ITableStorageConfig {
    enabled: boolean;
    provider: {
        [TableStorageProvider.Azure]?: IAzureTableStorageConfig;
    };
}
