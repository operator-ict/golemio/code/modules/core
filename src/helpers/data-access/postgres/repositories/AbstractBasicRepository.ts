import { ILogger } from "@golemio/validator/dist/Logger";
import { IDatabaseConnector } from "../IDatabaseConnector";
import { IRepository } from "./IRepository";

export abstract class AbstractBasicRepository implements IRepository {
    abstract schema: string;
    abstract tableName: string;

    protected connector: IDatabaseConnector;
    protected log: ILogger;

    constructor(connector: IDatabaseConnector, log: ILogger) {
        this.connector = connector;
        this.log = log;
    }
}
