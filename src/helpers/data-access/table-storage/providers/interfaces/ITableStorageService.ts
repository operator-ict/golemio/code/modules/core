export interface ITableStorageService {
    createEntities<T extends object>(tableName: string, entities: T[]): Promise<void>;
    deleteEntitiesOlderThan<T extends object>(tableName: string, timestamp: string): Promise<void>;
}
