#!/usr/bin/env node

require("../dist/shared/_global");
const fs = require("fs");
const path = require("path");
const glob = require("fast-glob");

const APP_ROOT_PATH = fs.realpathSync(process.cwd());
require("dotenv").config({ path: path.resolve(APP_ROOT_PATH, ".env") });

// TODO use unified logger
// https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/1
const log = {
    info: console.info,
    warn: console.warn,
    error: console.error,
};

const variables = {
    sqlDumpFiles: process.env.SQL_DUMP_FILES || " ",
};

/**
 * @deprecated No longer maintained, use Golemio CLI instead
 */
const importPostgresTestData = async () => {
    const files = glob.sync(variables.sqlDumpFiles, {
        cwd: APP_ROOT_PATH,
    });

    if (files.length < 1) {
        log.info(`Skipping SQL data import`);
        return;
    }

    const { sequelizeConnection } = require("../dist/output-gateway/database");

    for (const file of files) {
        log.info(`Restoring SQL data from '${file}'`);
        const query = fs.readFileSync(path.join(APP_ROOT_PATH, file)).toString();

        await sequelizeConnection.query(query);
    }
};

(async () => {
    console.log(
        "\x1b[33m%s\x1b[0m",
        "Deprecation warning: golemio-import-test-data is no longer maintained. Use Golemio CLI import-db-data instead"
    );

    await importPostgresTestData();

    // Exit with zero code
    process.exit(0);
})().catch((e) => {
    log.error(e);

    // Exit with non-zero code
    process.exit(1);
});
