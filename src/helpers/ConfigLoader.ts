import path from "path";
import fs from "fs";
import { FatalError } from "@golemio/errors";

/// Config files extension
const FILES_EXT = ".json";
const APP_ROOT = fs.realpathSync(process.cwd());

export class ConfigLoader {
    public conf: any;

    constructor(filename: string);
    /**
     * @deprecated The constructor should not be used
     */
    constructor(filename: string, deprecated: boolean);
    constructor(filename: string, deprecated?: boolean) {
        let conf: Record<string, any> | any[] | undefined;
        const configPath = path.join(APP_ROOT, "config", filename + FILES_EXT);
        try {
            conf = require(configPath);

            if (conf) {
                this.conf = conf;
            } else {
                throw new Error(`Error loading config: ${configPath}`);
            }
        } catch (err) {
            if (process.env.NODE_ENV === "test") this.conf = {};
            else throw new FatalError(`Error loading config: ${err.toString()}`, "ConfigLoader", err);
        }
    }
}
