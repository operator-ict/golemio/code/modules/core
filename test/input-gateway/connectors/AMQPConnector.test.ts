import { AMQPConnector } from "#ig/connectors/AMQPConnector";
import { ContainerToken, InputGatewayContainer } from "#ig/ioc";
import { FatalError } from "@golemio/errors";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";

chai.use(chaiAsPromised);

describe("AMQPConnector", () => {
    let amqpConnector: AMQPConnector;

    before(() => {
        amqpConnector = InputGatewayContainer.resolve(ContainerToken.AmqpConnector);
    });

    beforeEach(() => {
        InputGatewayContainer.clearInstances();
    });

    it("should has connect method", async () => {
        expect(amqpConnector.connect).not.to.be.undefined;
    });

    it("should has getChannel method", async () => {
        expect(amqpConnector.getChannel).not.to.be.undefined;
    });

    it("should throws Error if not connect method was not called", async () => {
        expect(() => {
            amqpConnector.getChannel();
        }).to.throw(FatalError);
    });

    it("should connects to RabbitMQ and returns channel", async () => {
        const ch = await amqpConnector.connect();
        expect(ch).to.be.an.instanceof(Object);
    });

    it("should returns channel", async () => {
        await amqpConnector.connect();
        expect(amqpConnector.getChannel()).to.be.an.instanceof(Object);
    });
});
