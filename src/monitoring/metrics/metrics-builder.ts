import { Counter, Gauge, Histogram, Registry, Summary } from "prom-client";
import { IMetricsConfiguration, IMetricsConfigurationList } from "#monitoring/metrics/metrics-interface";
import { MetricsType } from "#monitoring/metrics/metrics-helpers";

export const registerMetrics = <T extends object>(
    registry: Registry,
    metricsConfigurationList: IMetricsConfigurationList<T>,
    prefix: string
): T => {
    let registeredMetrics: any = {};

    for (const prop in metricsConfigurationList) {
        const definition = metricsConfigurationList[prop];
        registeredMetrics[prop] = createMetrics(definition, prefix);
        registry.registerMetric(registeredMetrics[prop]);
    }

    return registeredMetrics;
};

const createMetrics = (definition: IMetricsConfiguration, prefix: string) => {
    const { type, ...metricsConfig } = definition;

    switch (type) {
        case MetricsType.COUNTER: {
            return new Counter({
                ...metricsConfig,
                name: `${prefix}${metricsConfig.name}`,
                help: metricsConfig.help ?? `${metricsConfig.name}_counter`,
            });
        }
        case MetricsType.HISTOGRAM: {
            return new Histogram({
                ...metricsConfig,
                name: `${prefix}${metricsConfig.name}`,
                help: metricsConfig.help ?? `${metricsConfig.name}_histogram`,
            });
        }
        case MetricsType.GAUGE: {
            return new Gauge({
                ...metricsConfig,
                name: `${prefix}${metricsConfig.name}`,
                help: metricsConfig.help ?? `${metricsConfig.name}_gauge`,
            });
        }
        case MetricsType.SUMMARY: {
            return new Summary({
                ...metricsConfig,
                name: `${prefix}${metricsConfig.name}`,
                help: metricsConfig.help ?? `${metricsConfig.name}_summary`,
            });
        }
        default:
            throw new Error("Unexpected metrics type");
    }
};
