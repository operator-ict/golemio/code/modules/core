import { DataSourceStream, IDataTypeStrategy, IProtocolStrategy } from "#ie/datasources";
import { ResourceNotModified } from "#ie/helpers/ResourceNotModified";
import { IValidator } from "@golemio/validator";

export interface IDataSource<T = any> {
    /** The name of the data source. */
    name: string;

    dataStream: DataSourceStream;

    setProtocolStrategy(strategy: IProtocolStrategy): void;

    setDataTypeStrategy(strategy: IDataTypeStrategy): void;

    setValidator(validator: IValidator): void;

    /**
     * Get raw data, validate them and return as a response.
     *
     * @returns {Promise<T>} Promise with received data.
     */
    getAll(): Promise<T>;

    /**
     * Get raw data, validate them and return as a response, including any metadata such as HTTP headers. If the data has not been
     * modified (HTTP 304 Not Modified response was received), return `ResourceNotModified`.
     */
    getAllWithMetadata(): Promise<{ data: T | ResourceNotModified; metadata: unknown }>;

    getLastModified(): Promise<string>;
}
