export interface IPropertyResponseModel {
    id: string | number;
    title: string;
}
