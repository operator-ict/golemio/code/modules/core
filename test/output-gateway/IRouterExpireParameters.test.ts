import { IRouterExpireParameters } from "#og/routes/interfaces/IRouterExpireParameters";
import { isRouterExpireParameters } from "#og/routes/typeguards/RouterExpireParametersTypeGuard";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";

chai.use(chaiAsPromised);

describe("IRouterExpireParameters", () => {
    it("should be able to check if an object is an instance of IRouterExpireParameters", () => {
        const expire: IRouterExpireParameters = {
            maxAge: 60,
            staleWhileRevalidate: 60,
        };

        expect(isRouterExpireParameters(expire)).to.equal(true);
    });

    it("should be able to check if an object is not an instance of IRouterExpireParameters", () => {
        const expire = "5 minutes";

        expect(isRouterExpireParameters(expire)).to.equal(false);
    });
});
