import { getSubProperty } from "#helpers/utils";
import { expect } from "chai";

describe("Helper utils", () => {
    // =============================================================================
    // getSubProperty
    // =============================================================================
    describe("getSubProperty", () => {
        it("should return the same object", () => {
            expect(getSubProperty("", { property1: 1 })).to.deep.equal({ property1: 1 });
            expect(getSubProperty("", { property1: 1 })).to.deep.equal({ property1: 1 });
        });

        it("should return the sub property of object", () => {
            expect(getSubProperty("property1", { property1: 1 })).to.equal(1);
            expect(getSubProperty<number>("property1", { property1: 1 })).to.equal(1);
        });

        it("should return the sub sub property of object", () => {
            expect(getSubProperty("property1.a", { property1: { a: 1 } })).to.equal(1);
            expect(getSubProperty<number>("property1.a", { property1: { a: 1 } })).to.equal(1);
        });

        it("should return undefined", () => {
            expect(getSubProperty("property1.b", { property1: { a: 1 } })).to.equal(undefined);
            expect(getSubProperty("property1.b", { property1: { a: 1 } })).to.equal(undefined);
        });
    });
});
