import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox } from "sinon";
import fs from "fs";
import { BaseApp } from "#helpers/BaseApp";

chai.use(chaiAsPromised);

class TestApp extends BaseApp {
    constructor() {
        super();
    }

    public start = () => Promise.resolve();
}

describe("BaseApp", () => {
    let app: BaseApp;
    let sandbox: SinonSandbox;

    beforeEach(() => {
        app = new TestApp();
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox.restore();
    });

    describe("loadCommitSHA", () => {
        it("should return a file output", () => {
            sandbox.stub(fs, "readFileSync").returns(Buffer.from("test"));

            expect(app["loadCommitSHA"]()).to.equal("test");
            sandbox.assert.calledWithExactly(fs.readFileSync as any, sinon.match.typeOf("string"));
        });

        it("should return 'N/A'", () => {
            sandbox.stub(fs, "readFileSync").throws();

            expect(app["loadCommitSHA"]()).to.equal("N/A");
        });
    });

    describe("commonHeaders", () => {
        it("should set headers", () => {
            const nextMock = sandbox.stub();
            const res = {
                setHeader: sandbox.stub(),
                removeHeader: sandbox.stub(),
            };

            app["commonHeaders"]("" as any, res as any, nextMock as any);
            sandbox.assert.calledOnce(res.setHeader);
            sandbox.assert.calledOnce(res.removeHeader);
            sandbox.assert.calledOnce(nextMock);
        });
    });
});
