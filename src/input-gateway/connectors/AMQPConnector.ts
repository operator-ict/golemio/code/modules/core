import { ILogger } from "#helpers";
import AlternateExchangeCreator from "#helpers/data-access/amqp/AlternateExchangeCreator";
import { IConfiguration } from "#ig/config";
import { ContainerToken } from "#ig/ioc";
import { FatalError, ErrorHandler } from "@golemio/errors";
import amqplib from "amqplib";
import { Disposable, inject, injectable } from "tsyringe";
import IQueueConnector from "../../helpers/connector/interfaces/IQueueConnector";

@injectable()
export class AMQPConnector implements IQueueConnector, Disposable {
    /** Array of RabbitMQ connection urls. */
    private readonly availableConnectionUrls: string[];
    /** Index of url from availableConnectionsUrls which is currently connected. */
    private currentUrlIndex: number;
    /** RabbitMQ active connection. */
    private connection!: amqplib.Connection;
    /** RabbitMQ active channel. */
    private channel?: amqplib.Channel;
    /** true if disconnect was called */
    private gracefulShutdown: boolean;
    private rabbitExchangeName: string;

    constructor(
        @inject(ContainerToken.Config) config: IConfiguration,
        @inject(ContainerToken.Logger) private readonly log: ILogger
    ) {
        this.rabbitExchangeName = config.rabbit_exchange_name!;
        this.availableConnectionUrls = config.rabbit_connections as string[];
        this.currentUrlIndex = 0;
        this.gracefulShutdown = false;
    }

    public async connect(): Promise<amqplib.Channel> {
        try {
            if (this.channel) {
                return this.channel;
            }

            this.connection = await amqplib.connect(this.availableConnectionUrls[this.currentUrlIndex]);

            this.connection
                .on("error", (err: Error) => {
                    this.log.error(err);
                })
                .on("close", async () => {
                    this.log.warn("RabbitMQ Connection closed");
                    if (!this.gracefulShutdown) {
                        ErrorHandler.handle(new FatalError("[AMQP] Connection closed", this.constructor.name), this.log);
                    }
                });

            this.channel = await this.connection.createChannel();

            this.channel
                .on("error", (err) => {
                    this.log.error(err);
                })
                .on("close", () => {
                    this.log.warn("Channel closing.");
                    if (!this.gracefulShutdown) {
                        this.connection?.close();
                    }
                });

            this.log.info(`Connected to Queue! (url index ${this.currentUrlIndex})`);

            await this.assertDeadQueue(this.channel);

            return this.channel;
        } catch (err) {
            throw new FatalError("Error while creating AMQP Channel.", this.constructor.name, err);
        }
    }

    public getChannel(): amqplib.Channel {
        if (!this.channel) {
            throw new FatalError("AMQP channel does not exist. First call connect() method.", this.constructor.name);
        }
        return this.channel;
    }

    public isConnected = async (): Promise<boolean> => {
        return !!(await this.channel?.checkExchange(this.rabbitExchangeName));
    };

    public disconnect = async (): Promise<void> => {
        this.log.info("RabbitMQ disconnect called");
        this.gracefulShutdown = true;
        await this.channel?.close();
        await this.connection?.close();
    };

    public dispose = async (): Promise<void> => {
        await this.disconnect();
    };

    sendMessage(key: string, msg: string, options: amqplib.Options.Publish): Promise<boolean> {
        throw new Error("Method not implemented.");
    }

    private async assertDeadQueue(channel: amqplib.Channel): Promise<void> {
        await channel.assertExchange(this.rabbitExchangeName, "topic", {
            durable: false,
            alternateExchange: AlternateExchangeCreator.getAltExchangeName(this.rabbitExchangeName),
        });
        const q = await channel.assertQueue(this.rabbitExchangeName + ".deadqueue", {
            durable: true,
            messageTtl: 3 * 24 * 60 * 60 * 1000, // 3 days in milliseconds
            arguments: { "x-queue-type": "quorum" },
        });
        await channel.bindQueue(q.queue, this.rabbitExchangeName, "dead");
    }
}
