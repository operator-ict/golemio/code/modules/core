import { IQueueDefinition } from "./abstract";

/**
 * Filter blacklisted queue definitions
 */
export const filterQueueDefinitions = (
    queuesDefinition: IQueueDefinition[],
    blacklist: Record<string, string[]>
): IQueueDefinition[] => {
    let filteredQueuesDefinitions = queuesDefinition;

    for (const datasetName in blacklist) {
        if (blacklist[datasetName].length === 0) {
            // all dataset queues are filtered
            filteredQueuesDefinitions = filteredQueuesDefinitions.filter(
                (queueDef) => queueDef.name.toLowerCase() !== datasetName.toLowerCase()
            );
        } else {
            // only named queues of dataset are filtered
            for (let i = 0, imax = filteredQueuesDefinitions.length; i < imax; i++) {
                if (filteredQueuesDefinitions[i].name.toLowerCase() === datasetName.toLowerCase()) {
                    filteredQueuesDefinitions[i].queues = filteredQueuesDefinitions[i].queues.filter(
                        (queue) => blacklist[datasetName].map((el) => el.toLowerCase()).indexOf(queue.name.toLowerCase()) < 0
                    );
                }
            }
        }
    }

    return filteredQueuesDefinitions;
};
