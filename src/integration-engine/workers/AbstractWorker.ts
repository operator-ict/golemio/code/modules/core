import { config } from "#ie/config";
import { IQueueDefinition, IQueueTask } from "#ie/queueprocessors";
import { ITask } from "./interfaces";

export abstract class AbstractWorker {
    protected abstract readonly name: string;
    private queues: IQueueTask[] = [];

    public registerTask(task: ITask) {
        this.queues.push(this.getQueueTask(task));
    }

    /**
     * Generate queue definition from registered tasks
     */
    public getQueueDefinition(): IQueueDefinition {
        return {
            name: this.name,
            queuePrefix: this.getQueuePrefix(),
            queues: this.queues,
        };
    }

    public getQueuePrefix() {
        return config.RABBIT_EXCHANGE_NAME + "." + this.name.toLowerCase();
    }

    private getQueueTask(task: ITask): IQueueTask {
        return {
            name: task.queueName,
            options: {
                deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                deadLetterRoutingKey: "dead",
                messageTtl: task.queueTtl,
                arguments: { "x-queue-type": task.queueType },
            },
            consume: task.consume.bind(task),
        };
    }
}
