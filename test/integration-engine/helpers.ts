const waitTillStreamEnds = async (stream: any) => {
    await new Promise((resolve) => {
        const checker = setInterval(async () => {
            // con not use on.end handler directly
            if (stream.destroyed) {
                clearInterval(checker);
                resolve(null);
            }
        }, 100);
    });
};

export { waitTillStreamEnds };
