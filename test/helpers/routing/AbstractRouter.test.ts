import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox } from "sinon";
import express, { Request, Response, NextFunction } from "express";
import supertest from "supertest";
import { AbstractRouter } from "#helpers/routing/AbstractRouter";

chai.use(chaiAsPromised);

describe("AbstractRouter", () => {
    let sandbox: SinonSandbox;
    let router: AbstractRouter;
    let routerWithNested: AbstractRouter;

    class V1Router extends AbstractRouter {
        constructor() {
            super("v1", "test");
            this.initRoutes();
        }

        protected initRoutes() {
            this.getRouter().get("/path", (_req: Request, res: Response, _next: NextFunction) =>
                res.status(200).send({ response: "success" })
            );
        }
    }

    class V1RouterWithNested extends AbstractRouter {
        constructor() {
            super("v1", "test/:testId/nested", { shouldMergeParams: true });
            this.initRoutes();
        }

        protected initRoutes() {
            this.getRouter().get("/path", (req: Request, res: Response, _next: NextFunction) =>
                res.status(200).send({ response: "success", id: req.params.testId })
            );
        }
    }

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        router = new V1Router();
        routerWithNested = new V1RouterWithNested();
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should return a router", () => {
        expect(router.getRouter()).to.eq(router["router"]);
    });

    it("should return a path", () => {
        expect(router.getPath()).to.equal("/v1/test");
    });

    it("should have initRoutes method", () => {
        expect(router["initRoutes"]).not.to.be.undefined;
    });

    it("should register routes in initRoutes method", async () => {
        const app = express();
        app.use(router.getPath(), router.getRouter());
        const testApp = supertest(app);

        const response = await testApp.get("/v1/test/path");

        expect(response.status).to.eq(200);
        expect(response.body).to.eql({ response: "success" });
    });

    it("should register routes with nested params", async () => {
        const app = express();
        app.use(routerWithNested.getPath(), routerWithNested.getRouter());
        const testApp = supertest(app);

        const response = await testApp.get("/v1/test/123/nested/path");

        expect(response.status).to.eq(200);
        expect(response.body).to.eql({ response: "success", id: "123" });
    });
});
