import { ProtocolStrategy } from "#ie/datasources/protocol-strategy/ProtocolStrategy";
import { GeneralError } from "@golemio/errors";
import { Client, Dispatcher } from "undici";
import { IncomingHttpHeaders } from "undici/types/header";
import { gunzipSync } from "zlib";
import { IHTTPProtocolStrategyResult } from "./interfaces/IHTTPProtocolStrategyResult";
import { IHTTPRequestSettings } from "./interfaces/IHTTPRequestSettings";

export class HTTPRequestProtocolStrategy<T = any> extends ProtocolStrategy {
    protected connectionSettings!: IHTTPRequestSettings;

    constructor(settings: IHTTPRequestSettings) {
        super(settings);
    }

    public async getRawData(): Promise<IHTTPProtocolStrategyResult<T>> {
        let statusCode: number | undefined;

        try {
            const result = await this.getResponseInternal();

            const headers = result.headers;
            statusCode = result.statusCode;
            let data: T;

            if (this.connectionSettings.responseType === "json") {
                data = (await result.body.json()) as T;
            } else if (this.connectionSettings.responseType === "text") {
                data = (await result.body.text()) as T;
            } else if (this.connectionSettings.responseType === "blob") {
                data = (await result.body.blob()) as T;
            } else if (this.connectionSettings.responseType === "arrayBuffer") {
                data = (await result.body.arrayBuffer()) as T;

                if (this.connectionSettings.compression === "gzip") {
                    data = gunzipSync(data as ArrayBuffer) as T;
                }
            } else {
                data = (await result.body.json()) as T;
            }

            return {
                data,
                meta: {
                    headers: this.getHeaders(headers),
                    statusCode,
                },
            };
        } catch (err) {
            throw new GeneralError(
                `Error while getting data from server. Response status code: ${statusCode}`,
                this.constructor.name,
                err
            );
        }
    }

    private getHeaders = (headers: IncomingHttpHeaders): Record<string, string> => {
        const headersObject: Record<string, string> = {};

        for (const key in headers) {
            if (Object.prototype.hasOwnProperty.call(headers, key)) {
                headersObject[key] = headers[key] as string;
            }
        }

        return headersObject;
    };

    protected async getResponseInternal() {
        const url = new URL(this.connectionSettings.url);
        const clientOptions: Client.Options = {
            connect: {
                timeout: this.connectionSettings.timeoutInSeconds ? this.connectionSettings.timeoutInSeconds * 1000 : undefined,
                rejectUnauthorized: this.connectionSettings.rejectUnauthorized,
            },
        };
        const client = new Client(url.origin, clientOptions);
        const requestOptions: Dispatcher.RequestOptions = {
            path: url.pathname + url.search,
            method: this.connectionSettings.method.toUpperCase() as Dispatcher.HttpMethod,
            body: this.connectionSettings.body,
            headers: this.connectionSettings.headers,
        };

        const result = await client.request(requestOptions);

        return result;
    }
}
