# Golemio GIT Workflow (CZ)

Golemio GIT Workflow vychází a snaží se dodržovat [Gitflow Workflow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow).

## Release branch

Pro potřeby snadnějších a častějších releasů vznikla `release` větev, která vždy bude obsahovat Release Candidate issues/features.

![release branch flow](./assets/golemio_git_workflow_workflow.png)

> Upozornění
>
> Merge z `development` do `release` může být, ale je nutné dát si pozor, aby v `development` větvi nebyly změny, které ještě nebyly otestovány.
>
> Nová `feature` větev může být vytvořena z `development` větve, pokud zárověň `development` vychází z `release` větve. Jinak je riziko, že po otestování `feature` větve nebude moci být proveden merge do `release` větve a bude se to muset udělat pomocí cherry-picku.

### Doporučený postup

- založit `feature` větev z `release` větve
- provedení změn, merge do `development` větve
  - ideálně nemazat `feature` větev
  - pokud vzniknou konflikty, založit novou větev `feature-dev` a konflikty vyřešit v ní
  - smazání `feature-dev` větve
- akceptace v dev prostředí
- merge `feature` do `release` větve
  - smazání `feature` větve
- označení issue labelem "Release Candidate"

### Verzování

Schéma čísel verzí vychází z [semver](https://semver.org/) a vypadá takto `MAJOR.MINOR.PATCH`. Navýšení čísla verze se děje v `release` větvi a přímo závisí na [changelogu](./keeping_a_changelog_CZ.md), nepřímo pak na commit messages. Navýšení verze se neděje automaticky*, ale dělá ho releasující vývojář, který by se při navyšování měl držet těchto pravidel:

- `Fixed` a `Security`, nebo dokumentace a testy -> **PATCH**
- `Added`, `Changed`, `Deprecated` a `Removed` -> **MINOR**
- `BREAKING` -> **MAJOR**

> *Pro účely vývoje dochází k automatickému navýšení PATCH verze po úspěšném releasu v `development` větvi.
