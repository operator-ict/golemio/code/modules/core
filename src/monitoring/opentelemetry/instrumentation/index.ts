// Instrumentation
import { HttpInstrumentation } from "@opentelemetry/instrumentation-http";
import { IORedisInstrumentation } from "@opentelemetry/instrumentation-ioredis";
import { ExpressInstrumentation } from "@opentelemetry/instrumentation-express";
import { SequelizeInstrumentation } from "opentelemetry-instrumentation-sequelize";
import { AmqplibInstrumentation } from "@opentelemetry/instrumentation-amqplib";
import { AwsInstrumentation } from "@opentelemetry/instrumentation-aws-sdk";

// Configs
import { amqplibConfig, awsSdkConfig, httpConfig, sequelizeConfig } from "./configs";

export const getInstrumentation = () => {
    return [
        new ExpressInstrumentation(),
        new HttpInstrumentation(httpConfig),
        new SequelizeInstrumentation(sequelizeConfig),
        new AmqplibInstrumentation(amqplibConfig),
        new IORedisInstrumentation(),
        new AwsInstrumentation(awsSdkConfig),
    ];
};
