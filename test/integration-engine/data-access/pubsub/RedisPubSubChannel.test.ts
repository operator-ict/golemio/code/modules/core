import { RedisSubscriber } from "#ie/data-access/pubsub/subscribers/RedisSubscriber";
import { ISubscribeMessage } from "#ie/data-access/pubsub/subscribers/interfaces/ISubscribeMessage";
import { RedisConnector } from "#ie/connectors/";
import { RedisPubSubChannel } from "#ie/data-access/pubsub/RedisPubSubChannel";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import Redis from "ioredis";
import sinon, { SinonSandbox } from "sinon";

chai.use(chaiAsPromised);

describe("RedisPubSubChannel", () => {
    const PUBSUB_CHANNEL = "test-channel";

    let sandbox: SinonSandbox;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should assign connection in the constructor", async () => {
        sandbox.stub(RedisConnector, "getConnection").returns("dummy" as any);
        const pubSubChannel = new RedisPubSubChannel(PUBSUB_CHANNEL);
    });

    // =============================================================================
    // publishMessage
    // =============================================================================
    describe("publishMessage", () => {
        it("should publish a message", async () => {
            const publishStub = sandbox.stub();
            sandbox.stub(RedisConnector, "getConnection").returns({
                publish: publishStub,
            } as any);

            const pubSubChannel = new RedisPubSubChannel(PUBSUB_CHANNEL);
            await pubSubChannel.publishMessage("test-message");
            expect(publishStub.getCall(0).args).to.deep.equal([PUBSUB_CHANNEL, "test-message"]);
        });

        it("should publish a message (suffixed channel)", async () => {
            const publishStub = sandbox.stub();
            sandbox.stub(RedisConnector, "getConnection").returns({
                publish: publishStub,
            } as any);

            const pubSubChannel = new RedisPubSubChannel(PUBSUB_CHANNEL);
            await pubSubChannel.publishMessage("test-message", { channelSuffix: "test-suffix" });
            expect(publishStub.getCall(0).args).to.deep.equal([`${PUBSUB_CHANNEL}:test-suffix`, "test-message"]);
        });
    });

    // =============================================================================
    // createSubscriber
    // =============================================================================
    describe("createSubscriber", () => {
        it("should create a subscriber", async () => {
            sandbox.stub(RedisConnector, "getConnection");
            sandbox.stub(Redis["prototype"], "connect").resolves();

            const pubSubChannel = new RedisPubSubChannel(PUBSUB_CHANNEL);
            const subscriber = pubSubChannel.createSubscriber({ maxMessageCount: 1 });
            expect(subscriber).to.be.instanceOf(RedisSubscriber);
        });
    });

    // =============================================================================
    // I&T
    // =============================================================================
    describe("I&T", () => {
        before(async () => {
            await RedisConnector.connect();
        });

        it("should listen to published messages", async () => {
            const maxMessageCount = 1;
            const pubSubChannel = new RedisPubSubChannel(PUBSUB_CHANNEL);
            const subscriber = pubSubChannel.createSubscriber({ maxMessageCount: maxMessageCount });
            await subscriber.subscribe();

            let result: ISubscribeMessage | undefined;

            await Promise.all([subscriber.listen((message) => (result = message)), pubSubChannel.publishMessage("test-message")]);

            expect(result).to.deep.equal({
                message: "test-message",
                messageCount: 1,
                isFinal: true,
            });

            await subscriber.unsubscribe();
        });
    });
});
