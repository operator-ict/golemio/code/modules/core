import { ILogger } from "#helpers";
import { IDatabaseConnector } from "#helpers/data-access/postgres/IDatabaseConnector";
import { CoreToken } from "#helpers/ioc/CoreToken";
import { ContainerToken, IntegrationEngineContainer } from "#ie/ioc";
import { BulkSaveRecords, BulkSaveResult, IModel, ISequelizeSettings } from "#ie/models";
import { GeneralError, ValidationError } from "@golemio/errors";
import { IValidator } from "@golemio/validator";
import {
    Model,
    ModelCtor,
    QueryTypes,
    Sequelize,
    ValidationError as SequelizeValidationError,
    SyncOptions,
    Transaction,
} from "sequelize";
import { AuditAttributes } from "./postgres/AuditAttributeModel";

export class PostgresModel implements IModel {
    /** Model name */
    public name: string;
    /** The Sequelize Model */
    protected sequelizeModel: ModelCtor<any>;
    /** The Sequelize Model for temporary table */
    protected tmpSequelizeModel: ModelCtor<any> | null;
    /** Validation helper */
    protected validator: IValidator;
    /** Type/Strategy of saving the data */
    protected savingType: "insertOnly" | "insertOrUpdate";
    /** Table name */
    protected tableName: string;
    /** Postgres schema */
    protected schema: string;
    private postgresConnector: IDatabaseConnector;
    private log: ILogger;

    constructor(name: string, settings: ISequelizeSettings, validator: IValidator) {
        this.name = name;
        this.tableName = settings.pgTableName;
        this.schema = settings.pgSchema || "public";
        this.postgresConnector = IntegrationEngineContainer.resolve<IDatabaseConnector>(CoreToken.PostgresConnector);
        this.log = IntegrationEngineContainer.resolve<ILogger>(ContainerToken.Logger);

        this.sequelizeModel = this.postgresConnector
            .getConnection()
            .define(settings.pgTableName, settings.outputSequelizeAttributes, {
                ...settings.sequelizeAdditionalSettings,
                schema: this.schema,
            });

        if (settings.hasTmpTable) {
            this.tmpSequelizeModel = this.postgresConnector
                .getConnection()
                .define(
                    settings.pgTableName,
                    Object.assign({}, settings.outputSequelizeAttributes, settings.addAuditAttributes ? AuditAttributes : {}),
                    {
                        ...settings.sequelizeAdditionalSettings,
                        schema: "tmp",
                    }
                );
        } else {
            this.tmpSequelizeModel = null;
        }

        if (settings.attributesToRemove) {
            settings.attributesToRemove.forEach((attr) => {
                this.sequelizeModel.removeAttribute(attr);
                if (this.tmpSequelizeModel) {
                    this.tmpSequelizeModel.removeAttribute(attr);
                }
            });
        }

        this.validator = validator;

        if (this.validator?.setLogger) {
            this.validator.setLogger(this.log);
        }

        this.savingType = settings.savingType;
    }

    public save = async (data: any, useTmpTable: boolean = false): Promise<any> => {
        // data validation
        await this.validate(data);
        const model = await this.getSequelizeModelSafely(useTmpTable);

        // calling the method based on savingType (this.insertOnly() or this.insertOrUpdate())
        return this[this.savingType](model, data);
    };

    public validate = async (data: any): Promise<boolean> => {
        if (this.validator) {
            try {
                return await this.validator.Validate(data);
            } catch (err) {
                throw new ValidationError(`[${this.name}] Could not validate data`, this.name, err);
            }
        } else {
            this.log.warn(this.name + ": Model validator is not set.");

            return true;
        }
    };

    public query = async (query: string) => {
        return await this.postgresConnector.getConnection().query(query);
    };

    public saveBySqlFunction = async (
        data: any,
        primaryKeys: string[],
        useTmpTable: boolean = false,
        transaction: Transaction | null = null,
        connection: Sequelize | null = null,
        batchId: number | null = null
    ): Promise<any> => {
        await this.validate(data);

        try {
            connection = connection || this.postgresConnector.getConnection();
            // json stringify and escape quotes
            const stringifiedData = JSON.stringify(data).replace(/'/g, "\\'").replace(/\"/g, '\\"');
            // TODO doplnit batch_id a author
            await this.postgresConnector.getConnection().query(
                "SELECT meta.import_from_json(" +
                    `${batchId || "-1"}, ` + // p_batch_id bigint
                    "E'" +
                    stringifiedData +
                    "'::json, " + // p_data json
                    "'" +
                    (useTmpTable ? "tmp" : this.schema) +
                    "', " + // p_table_schema character varying
                    "'" +
                    this.tableName +
                    "', " + // p_table_name character varying
                    "'" +
                    JSON.stringify(primaryKeys) +
                    "'::json, " + // p_pk json
                    "NULL, " + // p_sort json
                    "'integration-engine'" + // p_worker_name character varying
                    ") ",
                {
                    transaction: transaction as Transaction,
                    type: QueryTypes.SELECT,
                }
            );
        } catch (err) {
            throw new GeneralError("Error while saving to database.", this.name, err);
        }
    };

    /**
     * Save data in bulk
     *
     * @param data - data to save
     * @param updateAttributes - attributes to update if the row already exists
     * @param isReturning - if true, returns all columns for the affected rows
     * @param useTmpTable - if true, uses temporary table
     * @param transaction - transaction to use or null to run without transaction
     */
    public bulkSave = async <T extends object>(
        records: BulkSaveRecords<T>,
        updateAttributes?: string[],
        isReturning = false,
        useTmpTable = false,
        transaction: Transaction | null = null
    ): Promise<BulkSaveResult<T>> => {
        if (records.length === 0) {
            return [];
        }

        await this.validate(records);

        const model = await this.getSequelizeModelSafely(useTmpTable);
        const attributesToUpdate =
            this.savingType === "insertOrUpdate" ? updateAttributes ?? Object.keys(records[0]).concat("updated_at") : undefined;

        try {
            return await model.bulkCreate(records, {
                updateOnDuplicate: attributesToUpdate,
                ignoreDuplicates: this.savingType === "insertOnly",
                returning: isReturning,
                transaction,
            });
        } catch (err) {
            if (err instanceof SequelizeValidationError && err.errors?.length > 0) {
                throw new ValidationError(
                    `[${this.name}] bulkSave: got Sequelize internal validation error: ${this.formatValidationErrors(err)}`,
                    this.name,
                    err
                );
            }

            throw new GeneralError(`[${this.name}] bulkSave: could not save data: ${err.message}`, this.name, err);
        }
    };

    public truncate = async (useTmpTable: boolean = false): Promise<any> => {
        const model = await this.getSequelizeModelSafely(useTmpTable);

        const connection = this.postgresConnector.getConnection();
        const t = await connection.transaction();

        try {
            await connection.query("SET lock_timeout TO '1min'", { type: QueryTypes.RAW, transaction: t });
            await model.destroy({
                cascade: false,
                transaction: t,
                truncate: true,
                where: {},
            });
            return await t.commit();
        } catch (err) {
            await t.rollback();
            throw new GeneralError("Error while truncating data.", this.name, err);
        }
    };

    public find = async (opts: object, useTmpTable: boolean = false): Promise<any> => {
        const model = await this.getSequelizeModelSafely(useTmpTable);
        try {
            return await model.findAll(opts);
        } catch (err) {
            throw new GeneralError("Error while getting from database.", this.name, err);
        }
    };

    public findOne = async (opts: object, useTmpTable: boolean = false): Promise<any> => {
        const model = await this.getSequelizeModelSafely(useTmpTable);
        try {
            return await model.findOne(opts);
        } catch (err) {
            throw new GeneralError("Error while getting from database.", this.name, err);
        }
    };

    public findAndCountAll = async (opts: object, useTmpTable: boolean = false): Promise<any> => {
        const model = await this.getSequelizeModelSafely(useTmpTable);
        try {
            return await model.findAndCountAll(opts);
        } catch (err) {
            throw new GeneralError("Error while getting from database.", this.name, err);
        }
    };

    public update = async (data: any, opts: any, useTmpTable: boolean = false): Promise<any> => {
        const model = await this.getSequelizeModelSafely(useTmpTable);
        return model.update(data, opts);
    };

    public sync = <M extends Model>(options?: SyncOptions): Promise<M> => {
        return this.sequelizeModel.sync(options);
    };

    private insertOnly = async (model: ModelCtor<any>, data: any): Promise<any> => {
        try {
            if (data instanceof Array) {
                return await model.bulkCreate(data);
            } else {
                return await model.create(data);
            }
        } catch (err) {
            this.log.error(JSON.stringify({ message: err.message, errors: err.errors, fields: err.fields }));
            throw new GeneralError("Error while saving to database.", this.name, err);
        }
    };

    private insertOrUpdate = async (model: ModelCtor<any>, data: any): Promise<any> => {
        const connection = this.postgresConnector.getConnection();
        const t = await connection.transaction();

        try {
            if (data instanceof Array) {
                const promises = data.map(async (d) => {
                    await model.upsert(d, { transaction: t });
                });
                await Promise.all(promises);
            } else {
                await model.upsert(data, { transaction: t });
            }
            return await t.commit();
        } catch (err) {
            this.log.error(JSON.stringify({ message: err.message, errors: err.errors, fields: err.fields }));
            await t.rollback();
            throw new GeneralError("Error while saving to database.", this.name, err);
        }
    };

    private getSequelizeModelSafely = async (useTmpTable: boolean): Promise<ModelCtor<any>> => {
        let model = this.sequelizeModel;
        if (useTmpTable && this.tmpSequelizeModel) {
            model = this.tmpSequelizeModel;
            /// synchronizing only tmp model
            await model.sync();
        } else if (useTmpTable && !this.tmpSequelizeModel) {
            throw new GeneralError("Temporary model is not defined.", this.name);
        }
        return model;
    };

    private formatValidationErrors = ({ errors }: SequelizeValidationError): string => {
        return errors.map((e) => `${e.message} (${e.value})`).join(", ");
    };
}
