import { TGeoCoordinates } from "#og/Geo";

export interface IPhotonFeatureProperties {
    osm_id: number;
    country: string;
    city: string;
    countrycode: string;
    postcode: string;
    locality: string;
    type: string;
    osm_type: string;
    osm_key: string;
    housenumber: string;
    street: string;
    district: string;
    osm_value: string;
}

export interface IPhotonData {
    type: "FeatureCollection";
    features: IPhotonDataFeature[];
}

interface IPhotonDataFeature {
    type: "FeatureCollection";
    geometry: TGeoCoordinates;
    properties: IPhotonFeatureProperties;
}
