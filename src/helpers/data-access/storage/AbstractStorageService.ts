import { Readable } from "stream";

export abstract class AbstractStorageService {
    abstract uploadStream(stream: Readable, pathPrefix: string, ext?: string, fileName?: string): Promise<void>;
    abstract uploadFile(data: Buffer | string, pathPrefix: string, ext?: string, fileName?: string): Promise<void>;
    abstract deleteFile(key: string): Promise<void>;
    abstract getFileStream(fileKey: string): Promise<NodeJS.ReadableStream | undefined>;
}
