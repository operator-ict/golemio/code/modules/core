import { GeocodeApi } from "#ie/helpers/GeocodeApi";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { SinonSandbox, createSandbox } from "sinon";

chai.use(chaiAsPromised);

describe("GeocodeApi", () => {
    let sandbox: SinonSandbox;
    let geocodeApi: GeocodeApi;

    beforeEach(() => {
        sandbox = createSandbox();
        geocodeApi = new GeocodeApi(
            {
                OPEN_STREET_MAP_API_URL_REVERSE: "http://localhost",
                OPEN_STREET_MAP_API_URL_SEARCH: "http://localhost",
                PHOTON_MAP_API_URL_REVERSE: "http://localhost",
                PHOTON_MAP_API_KEY: "test",
            } as any,
            {
                log: {
                    debug: () => {},
                    error: () => {},
                    info: () => {},
                    warn: () => {},
                },
            } as any
        );
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should has getAddressByLatLng method", async () => {
        expect(geocodeApi.getAddressByLatLng).not.to.be.undefined;
    });

    it("should has getAddressByLatLngFromPhoton method", async () => {
        expect(geocodeApi.getAddressByLatLngFromPhoton).not.to.be.undefined;
    });

    it("should have getExtendedAddressFromPhoton method", async () => {
        expect(geocodeApi.getExtendedAddressFromPhoton).not.to.be.undefined;
    });

    it("should returns address by lat lng using Open Street Map API", async () => {
        const stub = sandbox.stub(globalThis, "fetch").resolves({
            json: () =>
                Promise.resolve({
                    place_id: 29794916,
                    licence: "Data © OpenStreetMap contributors, ODbL 1.0. https://osm.org/copyright",
                    osm_type: "node",
                    osm_id: 2793104412,
                    lat: "50.1028934",
                    lon: "14.4461163",
                    display_name: "213/10, Dělnická, Holešovice, Hlavní město Praha, Praha, 17000, Česko",
                    address: {
                        house_number: "213/10",
                        road: "Dělnická",
                        suburb: "Holešovice",
                        city: "Hlavní město Praha",
                        state: "Praha",
                        postcode: "17000",
                        country: "Česko",
                        country_code: "cz",
                    },
                    boundingbox: ["50.1028434", "50.1029434", "14.4460663", "14.4461663"],
                }),
        } as any);
        const data = await geocodeApi.getAddressByLatLng(50.1028934, 14.4461163);
        expect(data.address_formatted).to.include("Dělnická 213/10");
        expect(data.street_address).to.include("Dělnická 213/10");
        expect(data.address_locality).to.include("Praha");
        expect(data.address_region).to.include("Holešovice");
        expect(data.address_country).to.include("Česko");

        expect(stub.called).to.be.true;
    });

    it("should throws error if getting address by lat lng using Open Street Map API failed", async () => {
        const stub = sandbox.stub(globalThis, "fetch").rejects({
            statusCode: 400,
            response: {
                statusCode: 400,
                body: '{"error":{"code":400,"message":"Floating-point number expected for parameter \'lat\'"}}',
            },
        });
        await expect(geocodeApi.getAddressByLatLng(null as any, null as any)).to.be.rejected;

        expect(stub.called).to.be.true;
    });

    it("should returns address by lat lng using Photon API", async () => {
        const stub = sandbox.stub(globalThis, "fetch").resolves({
            json: () =>
                Promise.resolve({
                    features: [
                        {
                            geometry: {
                                coordinates: [14.4461163, 50.1028934],
                                type: "Point",
                            },
                            type: "Feature",
                            properties: {
                                osm_id: 2793104412,
                                osm_type: "N",
                                country: "Česko",
                                osm_key: "place",
                                housenumber: "213/10",
                                city: "Praha",
                                street: "Dělnická",
                                countrycode: "CZ",
                                district: "Holešovice",
                                osm_value: "house",
                                postcode: "17000",
                                type: "house",
                            },
                        },
                    ],
                    type: "FeatureCollection",
                }),
        } as any);

        const data = await geocodeApi.getAddressByLatLngFromPhoton(50.1028934, 14.4461163);

        expect(data.address_formatted).to.include("Dělnická, 17000 Praha Holešovice");
        expect(data.street_address).to.include("Dělnická");
        expect(data.address_locality).to.include("Praha");
        expect(data.address_region).to.include("Holešovice");
        expect(data.address_country).to.include("Česko");
        expect(data.postal_code).to.include("17000");
        expect(stub.called).to.be.true;
    });

    it("should return address with house number", async () => {
        const stub = sandbox.stub(globalThis, "fetch").resolves({
            json: () =>
                Promise.resolve({
                    features: [
                        {
                            geometry: {
                                coordinates: [14.418428, 50.0735555],
                                type: "Point",
                            },
                            type: "Feature",
                            properties: {
                                osm_id: 296589402,
                                country: "Česko",
                                city: "Praha",
                                countrycode: "CZ",
                                postcode: "12800",
                                locality: "Vyšehrad",
                                type: "house",
                                osm_type: "N",
                                osm_key: "place",
                                housenumber: "1359/1",
                                street: "Karlovo náměstí",
                                district: "obvod Praha 2",
                                osm_value: "house",
                            },
                        },
                    ],
                    type: "FeatureCollection",
                }),
        } as any);

        const data = await geocodeApi.getExtendedAddressFromPhoton(50.1028934, 14.4461163);

        expect(data.address_formatted).to.include("Karlovo náměstí, 12800 Praha obvod Praha 2, Česko");
        expect(data.street_address).to.include("Karlovo náměstí");
        expect(data.address_locality).to.include("Praha");
        expect(data.address_region).to.include("obvod Praha 2");
        expect(data.address_country).to.include("Česko");
        expect(data.postal_code).to.include("12800");
        expect(data.house_number).to.include("1359/1");
        expect(stub.called).to.be.true;
    });

    it("should returns lat lng by address using Open Street Map API", async () => {
        const stub = sandbox.stub(globalThis, "fetch").resolves({
            json: () =>
                Promise.resolve([
                    {
                        place_id: 29794916,
                        licence: "Data © OpenStreetMap contributors, ODbL 1.0. https://osm.org/copyright",
                        osm_type: "node",
                        osm_id: 2793104412,
                        lat: "50.1028934",
                        lon: "14.4461163",
                        display_name: "213/10, Dělnická, Holešovice, Hlavní město Praha, Praha, 17000, Česko",
                        class: "place",
                        type: "house",
                        importance: 0.42099999999999993,
                    },
                ]),
        } as any);

        const data = await geocodeApi.getGeoByAddress("Dělnická 213/10", "Praha");
        expect(data).to.be.a("array");
        expect(data).to.deep.equal([14.4461163, 50.1028934]);
        expect(stub.called).to.be.true;
    });

    it("should throws error if getting address by lat lng using Open Street Map API failed", async () => {
        const stub = sandbox.stub(globalThis, "fetch").resolves({
            json: () => Promise.resolve([]),
        } as any);
        await expect(geocodeApi.getGeoByAddress("doesNotExist", "doesNotExist")).to.be.rejected;
        expect(stub.called).to.be.true;
    });
});
