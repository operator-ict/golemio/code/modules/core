import { ILogger } from "#helpers";
import { RedisSubscriber } from "#ie/data-access/pubsub/subscribers/RedisSubscriber";
import { log } from "#ie/helpers";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import Redis from "ioredis";
import sinon, { SinonSandbox, SinonStub } from "sinon";
import { EventEmitter } from "stream";

chai.use(chaiAsPromised);

describe("RedisSubscriber", () => {
    const PUBSUB_CHANNEL = "test-channel";
    let sandbox: SinonSandbox;
    let logWarnStub: SinonStub;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        logWarnStub = sandbox.stub(log, "warn");
    });

    afterEach(() => {
        sandbox.restore();
    });

    // =============================================================================
    // subscribe
    // =============================================================================
    describe("subscribe", () => {
        it("should connect and subscribe", async () => {
            const connectStub = sandbox.stub(Redis["prototype"], "connect").resolves();

            const subscriber = new RedisSubscriber({
                channelName: PUBSUB_CHANNEL,
                channelSuffix: "test-suffix",
                redisConnectionString: "",
                logger: log,
                maxMessageCount: 1,
            });
            sandbox.stub(subscriber["connection"], "status").value("test-status");
            const subscribeStub = sandbox.stub(subscriber["connection"], "subscribe");

            await subscriber.subscribe();
            expect(connectStub.calledOnce).to.equal(true);
            expect(subscribeStub.getCall(0).args).to.deep.equal(["test-channel:test-suffix"]);
        });

        it("should resolve (already subscribed)", async () => {
            const connectStub = sandbox.stub(Redis["prototype"], "connect").resolves();

            const subscriber = new RedisSubscriber({
                channelName: PUBSUB_CHANNEL,
                channelSuffix: "test-suffix",
                redisConnectionString: "",
                logger: log,
                maxMessageCount: 1,
            });
            sandbox.stub(subscriber["connection"], "status").value("ready");
            const subscribeStub = sandbox.stub(subscriber["connection"], "subscribe");

            await subscriber.subscribe();
            expect(connectStub.notCalled).to.equal(true);
            expect(subscribeStub.notCalled).to.equal(true);
            expect(logWarnStub.calledOnce).to.equal(true);
        });
    });

    // =============================================================================
    // listen
    // =============================================================================
    describe("listen", () => {
        it("should listen for messages", (done) => {
            const subscriber = new RedisSubscriber({
                channelName: PUBSUB_CHANNEL,
                redisConnectionString: "",
                logger: log,
                maxMessageCount: 3,
            });

            const emitter = new EventEmitter();
            subscriber["connection"] = emitter as any;
            subscriber.listen((result) => {
                if (result.isFinal) {
                    expect(result.message).to.equal("test-message-3");
                    expect(result.messageCount).to.equal(3);
                    done();
                }
            });

            for (let i = 1; i <= 3; i++) {
                emitter.emit("message", PUBSUB_CHANNEL, `test-message-${i}`);
            }
        });
    });

    // =============================================================================
    // unsubscribe
    // =============================================================================
    describe("unsubscribe", () => {
        it("should unsubscribe from all channels", async () => {
            sandbox.stub(Redis["prototype"], "connect").resolves();

            const subscriber = new RedisSubscriber({
                channelName: PUBSUB_CHANNEL,
                channelSuffix: "test-suffix",
                redisConnectionString: "",
                logger: log,
                maxMessageCount: 1,
            });
            const unsubscribeStub = sandbox.stub(subscriber["connection"], "unsubscribe");
            const disconnectStub = sandbox.stub(subscriber["connection"], "disconnect");

            await subscriber.unsubscribe();
            expect(unsubscribeStub.getCall(0).args).to.deep.equal(["test-channel:test-suffix"]);
            expect(disconnectStub.calledOnce).to.equal(true);
        });
    });
});
