import { ISentryConfig } from "#monitoring/sentry/sentry-provider";
import { IMetricsConfig } from "#monitoring/metrics/metrics-interface";

export interface ISaveRawDataWhitelist {
    route: string;
    withHeaders: boolean;
}

export interface IConfiguration {
    app_version: string | undefined;
    app_name: string;
    colorful_logs: string | undefined;
    log_level: string | undefined;
    node_env: string;
    payload_limit_csv: string | undefined;
    payload_limit_json: string | undefined;
    payload_limit_xml: string | undefined;
    port: string | undefined;
    rabbit_connections: string[] | undefined;
    rabbit_exchange_name: string | undefined;
    sentry: ISentryConfig;
    saveRawDataWhitelist: ISaveRawDataWhitelist[];
    metrics: IMetricsConfig;
    lightship: {
        shutdownDelay: number;
        shutdownTimeout: number;
        handlerTimeout: number;
    };
}
