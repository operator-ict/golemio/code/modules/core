import { RequestHandler } from "express";
import { injectable } from "tsyringe";

@injectable()
export class CompressionByDefaultMiddleware {
    /**
     * Creates an Express middleware to set the custom `shouldCompressByDefault` Response property to `true` and therefore enable
     * OG to compress the response by default (without it being explicitly accepted by the `Accept-Encoding` request header; one
     * can opt out of compression by requesting with the `Accept-Encoding` header set to `identity`).
     */
    public getMiddleware(): RequestHandler {
        return (_req, res, next) => {
            res._shouldCompressByDefault = true;
            next();
        };
    }
}
