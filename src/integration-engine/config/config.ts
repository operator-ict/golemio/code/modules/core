import { ContainerToken, IntegrationEngineContainer } from "#ie/ioc";
import { IConfiguration } from "./abstract";

/**
 * @deprecated For backwards compatibility. Use ioc container instead.
 */
const config = IntegrationEngineContainer.resolve<IConfiguration>(ContainerToken.Config);
export { config };
