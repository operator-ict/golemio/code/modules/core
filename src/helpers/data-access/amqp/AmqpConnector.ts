import { ILogger } from "#helpers";
import IQueueConnector from "#helpers/connector/interfaces/IQueueConnector";
import AlternateExchangeCreator from "#helpers/data-access/amqp/AlternateExchangeCreator";
import { ErrorHandler, FatalError, GeneralError } from "@golemio/errors";
import amqplib from "amqplib";
import { Disposable } from "tsyringe";
import { IRabbitConnectionInfo } from "./interfaces/IRabbitConnectionInfo";

export class AmqpConnector implements IQueueConnector, Disposable {
    private connection?: amqplib.Connection;
    private channel?: amqplib.Channel;
    private gracefulShutdown: boolean;

    constructor(private readonly connectionInfo: IRabbitConnectionInfo, private readonly log: ILogger) {
        this.gracefulShutdown = false;
    }

    public connect = async (): Promise<amqplib.Channel> => {
        try {
            if (this.channel) {
                return this.channel;
            }

            if (!this.connectionInfo.rabbitConnectionString) {
                throw new FatalError("[AMQP] The ENV variable RABBIT_CONN cannot be undefined.", this.constructor.name);
            }

            this.connection = await amqplib.connect(this.connectionInfo.rabbitConnectionString);

            this.connection
                .on("error", (err: Error) => {
                    this.log.error(err);
                })
                .on("close", async () => {
                    this.log.warn("[AMQP] Connection closing.");
                    if (!this.gracefulShutdown) {
                        ErrorHandler.handle(new FatalError("[AMQP] Connection closed", this.constructor.name), this.log);
                    }
                });

            this.channel = await this.connection.createChannel();

            this.channel
                .on("error", (err) => {
                    this.log.error(err);
                })
                .on("close", () => {
                    this.log.warn("[AMQP] Channel closing.");
                    if (!this.gracefulShutdown) {
                        this.connection?.close();
                    }
                });

            this.log.info("[AMQP] Connected to Queue!");

            await AlternateExchangeCreator.createAlternateExchange(this.channel, this.connectionInfo.rabbitExchangeName);
            await this.assertDeadQueue(this.channel);

            // Set the maximum number of messages sent over the channel
            // that can be awaiting ack
            await this.channel.prefetch(this.connectionInfo.rabbitChannelMaxPrefetchCount);

            return this.channel;
        } catch (err) {
            throw new FatalError("[AMQP] Error while creating AMQP Channel.", this.constructor.name, err);
        }
    };

    public getChannel = (): amqplib.Channel => {
        if (!this.channel) {
            throw new FatalError("[AMQP] Channel does not exist. First call connect() method.", this.constructor.name);
        }
        return this.channel;
    };

    public sendMessage = async (key: string, msg: string, options: amqplib.Options.Publish = {}): Promise<boolean> => {
        try {
            const channel = this.getChannel();
            await channel.assertExchange(this.connectionInfo.rabbitExchangeName, "topic", {
                durable: false,
                alternateExchange: AlternateExchangeCreator.getAltExchangeName(this.connectionInfo.rabbitExchangeName),
            });
            return channel.publish(this.connectionInfo.rabbitExchangeName, key, Buffer.from(msg), options);
        } catch (err) {
            throw new GeneralError("Sending the message to exchange failed.", this.constructor.name, err);
        }
    };

    public isConnected = async (): Promise<boolean> => {
        return !!(await this.channel?.checkExchange(this.connectionInfo.rabbitExchangeName));
    };

    public disconnect = async (): Promise<void> => {
        this.log.info("[AMQP] disconnect called");
        this.gracefulShutdown = true;
        await this.channel?.close();
        await this.connection?.close();
    };

    private assertDeadQueue = async (channel: amqplib.Channel): Promise<void> => {
        if (!this.connectionInfo.rabbitExchangeName) {
            throw new GeneralError("[AMQP] The ENV variable RABBIT_EXCHANGE_NAME cannot be undefined.", this.constructor.name);
        }

        await channel.assertExchange(this.connectionInfo.rabbitExchangeName, "topic", {
            durable: false,
            alternateExchange: AlternateExchangeCreator.getAltExchangeName(this.connectionInfo.rabbitExchangeName),
        });
        const q = await channel.assertQueue(this.connectionInfo.rabbitExchangeName + ".deadqueue", {
            durable: true,
            messageTtl: 3 * 24 * 60 * 60 * 1000, // 3 days in milliseconds
            arguments: { "x-queue-type": "quorum" },
        });
        await channel.bindQueue(q.queue, this.connectionInfo.rabbitExchangeName, "dead");
    };

    public dispose = async () => {
        await this.disconnect();
    };
}
