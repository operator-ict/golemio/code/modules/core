export interface IInflatedFileMetadata {
    path: string;
    name: string;
    mtime: Date;
    type: string;
}
