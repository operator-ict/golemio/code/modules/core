import { container } from "tsyringe";

const GlobalContainer = container;

//Place to put shared components across instances

export { GlobalContainer };
