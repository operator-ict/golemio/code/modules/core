import { ConfigLoader } from "#helpers";
import { injectable } from "tsyringe";
import { IConfiguration } from "./abstract";

/**
 * @deprecated  Use ioc container instead.
 */

@injectable()
export class IntegrationEngineConfiguration implements IConfiguration {
    public DATA_BATCH_SIZE = process.env.DATA_BATCH_SIZE ? Number(process.env.DATA_BATCH_SIZE) : 1000;
    public HOPPYGO_BASE_URL = process.env.HOPPYGO_BASE_URL;
    public LOG_LEVEL = process.env.LOG_LEVEL;
    public MOJEPRAHA_ENDPOINT_BASEURL = process.env.MOJEPRAHA_ENDPOINT_BASEURL;
    public NODE_ENV = process.env.NODE_ENV || "development";
    public OPEN_STREET_MAP_API_URL_REVERSE = process.env.OPEN_STREET_MAP_API_URL_REVERSE;
    public OPEN_STREET_MAP_API_URL_SEARCH = process.env.OPEN_STREET_MAP_API_URL_SEARCH;
    public PHOTON_MAP_API_URL_REVERSE = process.env.PHOTON_MAP_API_URL_REVERSE;
    public PHOTON_MAP_API_KEY = process.env.PHOTON_MAP_API_KEY;
    public PARKINGS_PAYMENT_URL = process.env.PARKINGS_PAYMENT_URL;
    public PARKING_ZONES_PAYMENT_URL = process.env.PARKING_ZONES_PAYMENT_URL;
    public POSTGRES_CONN = process.env.POSTGRES_CONN;
    public POSTGRES_POOL_MIN_CONNECTIONS = process.env.POSTGRES_POOL_MIN_CONNECTIONS;
    public POSTGRES_POOL_MAX_CONNECTIONS = process.env.POSTGRES_POOL_MAX_CONNECTIONS;
    public POSTGRES_POOL_IDLE_TIMEOUT = process.env.POSTGRES_POOL_IDLE_TIMEOUT;
    public RABBIT_CONN = process.env.RABBIT_CONN;
    public RABBIT_EXCHANGE_NAME = process.env.RABBIT_EXCHANGE_NAME;
    public RABBIT_CHANNEL_MAX_PREFETCH_COUNT = process.env.RABBIT_CHANNEL_MAX_PREFETCH_COUNT
        ? Number(process.env.RABBIT_CHANNEL_MAX_PREFETCH_COUNT)
        : 1;
    public REDIS_CONN = process.env.REDIS_CONN;
    public SPARQL_ENDPOINT_AUTH = process.env.SPARQL_ENDPOINT_AUTH;
    public SPARQL_ENDPOINT_URL = process.env.SPARQL_ENDPOINT_URL;
    public app_version = process.env.npm_package_version;
    public app_name = process.env.APP_NAME || "integration-engine";
    public datasources: Record<string, any>;
    public port = process.env.PORT;
    public queuesBlacklist: Record<string, string[]>;
    public saveRawDataWhitelist: Record<string, any>;
    public sentry = {
        enabled: process.env.SENTRY_ENABLED === "true",
        dsn: process.env.SENTRY_DSN,
        release: `${process.env.APP_NAME}@${process.env.npm_package_version}`,
        environment: process.env.SENTRY_ENVIRONMENT,
        debug: process.env.SENTRY_DEBUG === "true",
        tracesSampleRate: process.env.SENTRY_TRACE_RATE ? parseFloat(process.env.SENTRY_TRACE_RATE) : 0,
    };
    public stream = {
        wait_for_end_attempts: parseInt(process.env.STREAM_WAIT_FOR_END_ATTEMPTS as string, 10) || 30,
        wait_for_end_interval: parseInt(process.env.STREAM_WAIT_FOR_END_INTERVAL as string, 10) || 1000,
    };
    public metrics = {
        enabled: process.env.METRICS_ENABLED === "true",
        port: process.env.METRICS_PORT ? parseInt(process.env.METRICS_PORT as string, 10) : 9001,
        prefix: process.env.METRICS_PREFIX || "",
    };
    public lightship = {
        shutdownDelay: process.env.LIGHTSHIP_SHUTDOWN_DELAY ? parseInt(process.env.LIGHTSHIP_SHUTDOWN_DELAY, 10) : 5000,
        shutdownTimeout: process.env.LIGHTSHIP_SHUTDOWN_TIMEOUT ? parseInt(process.env.LIGHTSHIP_SHUTDOWN_TIMEOUT, 10) : 60000,
        handlerTimeout: process.env.LIGHTSHIP_HANDLER_TIMEOUT ? parseInt(process.env.LIGHTSHIP_HANDLER_TIMEOUT, 10) : 5000,
    };
    public vehiclePositions = {
        stepBetweenPoints: Number(process.env.VEHICLE_POSITIONS_STEP_BETWEEN_POINTS) || 0.1, // in km, default 100 meters
        // hours to be subtracted from the TJR property (TCP busses only) when transitioning from CET to CEST, default 1 hour
        tcpBusTjrNegativeOffset: process.env.VEHICLE_POSITIONS_TCP_BUS_NEGATIVE_OFFSET
            ? Number(process.env.VEHICLE_POSITIONS_TCP_BUS_NEGATIVE_OFFSET)
            : 1,
        // time at which Redis keys should expire
        redisExpireTime: process.env.VEHICLE_POSITIONS_REDIS_EXPIRE_TIME || "03:00",
        // enable vacuum analyze and reindex after data retenation in tables vp_positons and vp_trips
        dataMaintenanceEnabled: process.env.VEHICLE_POSITIONS_DATA_MAINTENANCE_ENABLED === "true",
        dataRetentionLockTimeout: process.env.VEHICLE_POSITIONS_DATA_RETENTION_LOCK_TIMEOUT
            ? parseInt(process.env.VEHICLE_POSITIONS_DATA_RETENTION_LOCK_TIMEOUT, 10)
            : 2 * 60 * 60 * 1000, // 2 hours
        metroArrivalDelayAddition: process.env.VEHICLE_POSITIONS_METRO_ARRIVAL_DELAY_ADDITION_IN_SECONDS
            ? parseInt(process.env.VEHICLE_POSITIONS_METRO_ARRIVAL_DELAY_ADDITION_IN_SECONDS)
            : 20,
        metroDepartureDelayAddition: process.env.VEHICLE_POSITIONS_METRO_DEPARTURE_DELAY_ADDITION_IN_SECONDS
            ? parseInt(process.env.VEHICLE_POSITIONS_METRO_DEPARTURE_DELAY_ADDITION_IN_SECONDS)
            : -5,
    };

    constructor() {
        this.datasources = new ConfigLoader("moduleConfig").conf;
        this.queuesBlacklist = new ConfigLoader("queuesBlacklist").conf;
        this.saveRawDataWhitelist = new ConfigLoader("saveRawDataWhitelist").conf;
    }
}
