import { ISimpleConfig } from "#helpers/configuration/ISimpleConfig";
import { CoreToken } from "#helpers/ioc/CoreToken";
import { DataSourceStream } from "#ie/datasources";
import { IPostgresSettings, IProtocolStrategy } from "#ie/datasources/protocol-strategy/IProtocolStrategy";
import { PostgresProtocolStrategy } from "#ie/datasources/protocol-strategy/PostgresProtocolStrategy";
import { IntegrationEngineContainer } from "#ie/ioc/Di";
import { GeneralError } from "@golemio/errors";

export class PostgresProtocolStrategyStreamed extends PostgresProtocolStrategy implements IProtocolStrategy {
    private dataBatchSize: number;

    constructor(settings: IPostgresSettings) {
        super(settings);
        const simpleConfig = IntegrationEngineContainer.resolve<ISimpleConfig>(CoreToken.SimpleConfig);
        this.dataBatchSize = +simpleConfig.getValue<number>("env.DATA_BATCH_SIZE", 1000);
    }

    public getRawData = async (): Promise<any> => {
        const findOptions = this.connectionSettings.findOptions;

        let batchLimit: number;
        let offset = (this.connectionSettings.findOptions || {}).offset || 0;
        const limit = (this.connectionSettings.findOptions || {}).limit;
        let resultsCount = 0;
        // TO DO - move to helper f-cion
        if (limit && limit <= this.dataBatchSize) {
            batchLimit = limit;
        } else {
            batchLimit = +this.dataBatchSize;
        }

        try {
            const connection = await this.getConnection();

            const model = connection.define(this.connectionSettings.tableName, this.connectionSettings.modelAttributes, {
                ...(this.connectionSettings.sequelizeAdditionalSettings
                    ? this.connectionSettings.sequelizeAdditionalSettings
                    : { freezeTableName: true, timestamps: true, underscored: true }), // default values
                ...(this.connectionSettings.schemaName ? { schema: this.connectionSettings.schemaName } : {}),
            });
            const isValid = (results: any[]) => results.length > 0;

            const isLastBatch = (results: any[], resultsCount: number) =>
                results.length === 0 || results.length < batchLimit || resultsCount >= limit;

            return {
                data: new DataSourceStream({
                    objectMode: true,
                    async read() {
                        try {
                            const results = await model.findAll({
                                ...findOptions,
                                limit: batchLimit,
                                offset,
                                raw: true,
                            });

                            if (isValid(results)) {
                                this.push(results);
                                resultsCount += results.length;
                            }

                            if (isLastBatch(results, resultsCount)) {
                                this.push(null);
                                return;
                            }

                            offset += batchLimit;

                            batchLimit = limit - resultsCount < batchLimit ? limit - resultsCount : batchLimit;
                        } catch (err) {
                            this.emit("error", err);
                        }
                    },
                }),
            };
        } catch (err) {
            throw new GeneralError("Error while getting data from server.", this.constructor.name, err);
        }
    };
}
