import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { buildGeojsonFeatureType, GeoCoordinatesType } from "#og/Geo";

chai.use(chaiAsPromised);

describe("Geo helpers", () => {
    it("should return proper geojson feature object (polygon)", () => {
        const featureObject = buildGeojsonFeatureType("location", {
            prop: "prop",
            location: { type: GeoCoordinatesType.Polygon, coordinates: [[1.1, 2.2]] },
        });
        expect(featureObject).to.deep.include({
            geometry: {
                coordinates: [[1.1, 2.2]],
                type: "Polygon",
            },
            properties: {
                prop: "prop",
            },
            type: "Feature",
        });
    });

    it("should return proper geojson feature object (multipolygon)", () => {
        const featureObject = buildGeojsonFeatureType("location", {
            prop: "prop",
            location: { type: GeoCoordinatesType.MultiPolygon, coordinates: [[[[1.1, 2.2]]], [[[1.3, 2.6]]]] },
        });
        expect(featureObject).to.deep.include({
            geometry: {
                coordinates: [[[[1.1, 2.2]]], [[[1.3, 2.6]]]],
                type: "MultiPolygon",
            },
            properties: {
                prop: "prop",
            },
            type: "Feature",
        });
    });
});
