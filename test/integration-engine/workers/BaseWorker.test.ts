import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { GeneralError } from "@golemio/errors";
import { BaseWorker } from "#ie/workers/BaseWorker";
import { DataSourceStream } from "#ie/datasources/DataSourceStream";

chai.use(chaiAsPromised);

/**
 * Describe BaseWorker
 */
describe("BaseWorker", () => {
    let worker: BaseWorker;

    beforeEach(() => {
        worker = new BaseWorker();
    });

    // =============================================================================
    // processDataStream
    // =============================================================================
    describe("processDataStream", () => {
        it("should reject (dataSourceStream rejected)", async () => {
            await expect(worker["processDataStream"](Promise.reject(), (_) => Promise.resolve())).to.be.rejectedWith(
                GeneralError
            );
        });

        it("should reject (proceed rejected)", async () => {
            const stream = {
                setDataProcessor: () => ({ proceed: () => Promise.reject() }),
            };

            await expect(
                worker["processDataStream"](Promise.resolve(stream as unknown as DataSourceStream), (_) => Promise.resolve())
            ).to.be.rejectedWith(GeneralError);
        });

        it("should resolve", async () => {
            const stream = {
                setDataProcessor: () => ({ proceed: () => Promise.resolve() }),
            };

            await expect(
                worker["processDataStream"](Promise.resolve(stream as unknown as DataSourceStream), (_) => Promise.resolve())
            ).to.be.fulfilled;
        });
    });
});
