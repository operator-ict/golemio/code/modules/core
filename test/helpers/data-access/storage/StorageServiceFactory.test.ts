import sinon, { SinonSandbox, SinonStub } from "sinon";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { ILogger } from "#helpers/logger";
import { AbstractStorageService } from "#helpers/data-access/storage/AbstractStorageService";
import { StorageProvider, StorageServiceFactory } from "#helpers/data-access/storage/StorageServiceFactory";
import { NoopStorageService } from "#helpers/data-access/storage/NoopStorageService";

chai.use(chaiAsPromised);

describe("StorageServiceFactory", () => {
    let sandbox: SinonSandbox;
    let factory: StorageServiceFactory;
    let log: Partial<ILogger>;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        log = {
            silly: sandbox.stub() as any,
        };
    });

    afterEach(() => {
        sandbox.restore();
    });

    // =============================================================================
    // getService
    // =============================================================================
    describe("getService", () => {
        it("should return service", () => {
            factory = new StorageServiceFactory(
                {
                    enabled: true,
                    provider: {
                        azure: {
                            clientId: "client-id",
                            tenantId: "tenant-id",
                            account: "test",
                            clientSecret: "secret",
                            containerName: "local",
                            uploadTimeoutInSeconds: 120,
                        },
                    },
                },
                log as ILogger
            );

            expect(factory.getService(StorageProvider.Azure)).to.be.instanceOf(AbstractStorageService);
        });

        it("should not return dummy service (storage is disabled)", () => {
            factory = new StorageServiceFactory(
                {
                    enabled: false,
                    provider: {
                        azure: {
                            clientId: "client-id",
                            tenantId: "tenant-id",
                            account: "test",
                            clientSecret: "secret",
                            containerName: "",
                            uploadTimeoutInSeconds: 120,
                        },
                    },
                },
                log as ILogger
            );

            expect((log.silly as SinonStub).getCall(0).args[0]).to.equal("[StorageServiceFactory] Storage is disabled.");
            expect(factory.getService(StorageProvider.Azure)).to.be.instanceOf(NoopStorageService);
        });
    });
});
