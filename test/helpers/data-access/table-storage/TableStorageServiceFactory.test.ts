import { TableStorageServiceFactory } from "#helpers/data-access/table-storage/TableStorageServiceFactory";
import { AzureTableStorageService } from "#helpers/data-access/table-storage/providers/AzureTableStorageService";
import { NoopTableStorageService } from "#helpers/data-access/table-storage/providers/NoopTableStorageService";
import { TableStorageProvider } from "#helpers/data-access/table-storage/providers/enums/TableStorageProviderEnum";
import { ILogger } from "#helpers/logger";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox, SinonStub } from "sinon";

chai.use(chaiAsPromised);

describe("TableStorageServiceFactory", () => {
    let sandbox: SinonSandbox;
    let factory: TableStorageServiceFactory;
    let log: Partial<ILogger>;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        log = {
            silly: sandbox.stub() as any,
        };
    });

    afterEach(() => {
        sandbox.restore();
    });

    // =============================================================================
    // getService
    // =============================================================================
    describe("getService", () => {
        it("should return service", () => {
            factory = new TableStorageServiceFactory(
                {
                    enabled: true,
                    provider: {
                        azure: {
                            account: "",
                            tenantId: "",
                            clientId: "",
                            clientSecret: "",
                            entityBatchSize: 100,
                        },
                    },
                },
                log as ILogger
            );

            expect(factory.getService(TableStorageProvider.Azure)).to.be.instanceOf(AzureTableStorageService);
        });

        it("should return dummy service (storage is disabled)", () => {
            factory = new TableStorageServiceFactory(
                {
                    enabled: false,
                    provider: {
                        azure: {
                            account: "",
                            tenantId: "",
                            clientId: "",
                            clientSecret: "",
                            entityBatchSize: 0,
                        },
                    },
                },
                log as ILogger
            );

            expect(factory.getService(TableStorageProvider.Azure)).to.be.instanceOf(NoopTableStorageService);
            expect((log.silly as SinonStub).getCall(0).args[0]).to.equal("TableStorageServiceFactory: storage is disabled");
        });
    });
});
