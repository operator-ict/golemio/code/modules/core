import { expect } from "chai";
import { PaginationHelper } from "#ie/helpers/PaginationHelper";

describe("PaginationHelper", () => {
    describe("setPaginationParams", () => {
        it("should add pagination params to url (default values)", () => {
            expect(PaginationHelper.setPaginationParams("https://testurl.com/")).to.eq(
                "https://testurl.com/?resultRecordCount=1000&resultOffset=0"
            );
        });

        it("should reset pagination params of url strategy (specified values)", () => {
            expect(PaginationHelper.setPaginationParams("https://testurl.com/?resultRecordCount=500&resultOffset=20")).to.eq(
                "https://testurl.com/?resultRecordCount=500&resultOffset=0"
            );
        });
    });

    describe("getNextPageUrl", () => {
        it("should set offset properly", () => {
            expect(PaginationHelper.getNextPageUrl("https://testurl.com/?resultRecordCount=500&resultOffset=20")).to.eq(
                "https://testurl.com/?resultRecordCount=500&resultOffset=520"
            );
        });
    });
});
