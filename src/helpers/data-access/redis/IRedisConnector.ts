import { Disposable } from "tsyringe";

export interface IRedisConnector extends Disposable {
    connect(): Promise<any>;
    getConnection(): any;
    isConnected(): boolean;
    disconnect(): Promise<void>;
}
