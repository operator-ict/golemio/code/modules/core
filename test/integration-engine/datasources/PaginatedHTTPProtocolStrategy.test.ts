import { RedisConnector } from "#ie/connectors";
import RawDataStorage from "#ie/data-access/RawDataStorage";
import { IHTTPSettings, PaginatedHTTPProtocolStrategy } from "#ie/datasources";
import { IHTTPFetchSettings } from "#ie/datasources/protocol-strategy/interfaces/IHTTPFetchSettings";
import { ContainerToken } from "#ie/ioc";
import { IntegrationEngineContainer } from "#ie/ioc/Di";
import { GeneralError } from "@golemio/errors";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import nock, { Scope } from "nock";
import sinon, { SinonSandbox, SinonSpy } from "sinon";

chai.use(chaiAsPromised);

describe("PaginatedHTTPProtocolStrategy", () => {
    let testSettings: IHTTPFetchSettings;
    let sandbox: SinonSandbox;
    let strategy: PaginatedHTTPProtocolStrategy;
    let scope: Scope;
    let rawDataStore: RawDataStorage;

    before(async () => {
        await RedisConnector.connect();
    });

    beforeEach(() => {
        sandbox = sinon.createSandbox();

        testSettings = {
            headers: {},
            method: "GET",
            url: "https://httpbin.org/get",
        } as any;
        strategy = new PaginatedHTTPProtocolStrategy(testSettings);

        scope = nock("https://httpbin.org")
            .get("/get?resultRecordCount=1000&resultOffset=0")
            .reply(200, { features: [1, 2, 3], properties: { exceededTransferLimit: true } })
            .get("/get?resultRecordCount=1000&resultOffset=1000")
            .reply(200, { features: [4, 5], properties: { exceededTransferLimit: false } });

        scope = nock("https://httpbin.org").get("/ip").reply(200, {
            ip: "ok",
        });

        scope = nock("https://notfound.com").get("/").reply(404);

        sandbox.spy(strategy, "getRawData");
        rawDataStore = IntegrationEngineContainer.resolve<RawDataStorage>(ContainerToken.RawDataStorage);
        sandbox.spy(rawDataStore, "save");
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should init strategy and its methods", async () => {
        expect(strategy.getData).not.to.be.undefined;
        expect(strategy.getRawData).not.to.be.undefined;
        expect(strategy.getLastModified).not.to.be.undefined;
        expect(strategy.setConnectionSettings).not.to.be.undefined;
        expect(strategy.getConnectionSettings).not.to.be.undefined;
    });

    it("should get all data", async () => {
        const res = await strategy.getData();
        const { url } = strategy.getConnectionSettings() as IHTTPSettings;
        expect(res).to.deep.equal({
            features: [1, 2, 3, 4, 5],
            properties: {},
        });
        expect(new URL(url).searchParams.get("resultOffset")).to.eq("0");
        sandbox.assert.calledOnce(strategy.getRawData as SinonSpy);
        sandbox.assert.calledOnce(rawDataStore.save as SinonSpy);
    });

    it("should throw error if getting data failed", async () => {
        testSettings.url = "https://notfound.com";
        strategy.setConnectionSettings(testSettings);
        await expect(strategy.getData()).to.be.rejectedWith(GeneralError);
    });

    it.skip("should properly get last modified", async () => {
        scope = nock("https://httpbin.org")
            .intercept("/get?resultRecordCount=1000&resultOffset=0", "HEAD")
            .reply(200, undefined, { "last-modified": "Tue, 12 Mar 2019 17:32:09 GMT" });
        const res = await strategy.getLastModified();
        expect(res).to.be.equal("2019-03-12T17:32:09.000Z");
    });

    it.skip("should return null if last modified is not provided", async () => {
        testSettings.url = "https://notfound.com";
        strategy.setConnectionSettings(testSettings);
        const res = await strategy.getLastModified();
        expect(res).to.be.null;
    });
});
