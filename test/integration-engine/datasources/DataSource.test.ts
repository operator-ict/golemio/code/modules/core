import { DataSource, IDataTypeStrategy, IProtocolStrategy } from "#ie/datasources";
import { log } from "#ie/helpers";
import { ResourceNotModified } from "#ie/helpers/ResourceNotModified";
import { metricsService } from "#monitoring";
import { GeneralError } from "@golemio/errors";
import { IValidator } from "@golemio/validator";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox, SinonSpy, SinonStub } from "sinon";

chai.use(chaiAsPromised);

describe("DataSource", () => {
    let sandbox: SinonSandbox;
    let datasource: DataSource;
    let protocolStub: Record<keyof IProtocolStrategy, SinonStub>;
    let dataTypeStub: Record<keyof IDataTypeStrategy, SinonStub>;
    let validatorStub: Record<keyof IValidator, SinonStub>;

    beforeEach(() => {
        sandbox = sinon.createSandbox();

        protocolStub = {
            getData: sandbox.stub().resolves('{"message": "test"}'),
            getDataWithMetadata: sandbox.stub().resolves({ data: '{"message": "test"}', metadata: { statusCode: 200 } }),
            getLastModified: sandbox.stub(),
            setCallerName: sandbox.stub(),
            setConnectionSettings: sandbox.stub(),
            getConnectionSettings: sandbox.stub(),
        };
        dataTypeStub = {
            setDataTypeSettings: sandbox.stub(),
            setFilter: sandbox.stub(),
            parseData: sandbox.stub().callsFake(() => Object.assign({ message: "test" })),
        };
        validatorStub = {
            Validate: sandbox.stub().resolves(true),
            setLogger: sandbox.stub(),
        };
        sandbox.spy(log, "warn");
        sandbox.spy(log, "verbose");

        datasource = new DataSource("TestDataSource", protocolStub, dataTypeStub, validatorStub);

        sandbox.stub(metricsService, "recordNumberOfRecords");
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should call protocol.setCallerName in constructor", () => {
        sandbox.assert.calledOnce(protocolStub.setCallerName);
    });

    it("should have name method", () => {
        expect(datasource.name).not.to.be.undefined;
    });

    it("should have getAll method", () => {
        expect(datasource.getAll).not.to.be.undefined;
    });

    it("should have getAllWithMetadata method", () => {
        expect(datasource.getAllWithMetadata).not.to.be.undefined;
    });

    it("should have getLastModified method", () => {
        expect(datasource.getLastModified).not.to.be.undefined;
    });

    it("should have setProtocolStrategy method", () => {
        expect(datasource.setProtocolStrategy).not.to.be.undefined;
    });

    it("should have setDataTypeStrategy method", () => {
        expect(datasource.setDataTypeStrategy).not.to.be.undefined;
    });

    it("should have setValidator method", () => {
        expect(datasource.setValidator).not.to.be.undefined;
    });

    it("should properly get all data", async () => {
        const data = await datasource.getAll();
        expect(data).to.have.property("message").that.equals("test");
    });

    it("should properly get all data with metadata", async () => {
        const res = await datasource.getAllWithMetadata();
        expect(res).to.have.property("data");
        expect(res.data).to.have.property("message").that.equals("test");
        expect(res).to.have.property("metadata");
        expect(res.metadata).to.have.property("statusCode").that.equals(200);
    });

    it("should handle 304 Not Modified when getting all data with metadata", async () => {
        protocolStub.getDataWithMetadata = sandbox.stub().resolves({ metadata: { statusCode: 304 } });
        dataTypeStub.parseData = sandbox.stub().rejects(new Error("test error"));

        const res = await datasource.getAllWithMetadata();
        expect(res).to.have.property("data");
        expect(res.data).to.be.instanceOf(ResourceNotModified);
        expect(res).to.have.property("metadata");
        expect(res.metadata).to.have.property("statusCode").that.equals(304);
    });

    it("should handle file object when getting all data with metadata", async () => {
        protocolStub.getDataWithMetadata = sandbox
            .stub()
            .resolves({ data: { filepath: "test.txt", name: "test", data: "test-data" } });
        dataTypeStub.parseData = sandbox.stub().callsFake((data) => ({ message: data }));
        await datasource.getAllWithMetadata();
        expect(validatorStub.Validate.lastCall.firstArg).to.deep.equal({ message: "test-data" });
    });

    it("should throw error if data are not valid", async () => {
        validatorStub.Validate = sandbox.stub().throws();
        datasource.setValidator(validatorStub);
        await expect(datasource.getAll()).to.be.rejectedWith(GeneralError);
    });

    it("should verbose if data are empty", async () => {
        (dataTypeStub.parseData = sandbox.stub().callsFake(() => [])), datasource.setDataTypeStrategy(dataTypeStub);
        await datasource.getAll();
        (dataTypeStub.parseData = sandbox.stub().callsFake(() => null)), datasource.setDataTypeStrategy(dataTypeStub);
        await datasource.getAll();
        (dataTypeStub.parseData = sandbox.stub().callsFake(() => Object.assign({}))),
            datasource.setDataTypeStrategy(dataTypeStub);
        await datasource.getAll();
        sandbox.assert.callCount(log.verbose as SinonSpy, 3);
    });

    it("should warn if validator is not set", async () => {
        datasource.setValidator(null);
        await datasource.getAll();
        sandbox.assert.calledOnce(log.warn as SinonSpy);
    });

    it("should properly get last modified", async () => {
        await datasource.getLastModified();
        sandbox.assert.calledOnce(protocolStub.getLastModified);
    });

    it("should set protocol strategy", async () => {
        datasource.setProtocolStrategy(protocolStub);
        sandbox.assert.calledTwice(protocolStub.setCallerName);
        datasource.setProtocolStrategy({} as any);
        expect(datasource.protocolStrategy).to.be.deep.equal({});
    });

    it("should set datatype strategy", async () => {
        datasource.setDataTypeStrategy(null);
        expect(datasource["dataTypeStrategy"]).to.be.null;
    });

    it("should set validator", async () => {
        datasource.setValidator(null);
        expect(datasource["validator"]).to.be.null;
    });

    it("should throw truncated validation error", async () => {
        datasource.setValidator({
            Validate: sandbox.stub().rejects("test"),
        } as any);

        const dataTypeStrategy = {
            ...dataTypeStub,
            parseData: sandbox.stub().callsFake(() => Object.assign({ message: "a".repeat(100000) })),
        };

        datasource.setDataTypeStrategy(dataTypeStrategy);
        await expect(datasource.getAll()).to.be.rejectedWith(
            `Error while validating source data: {"message":"${"a".repeat(2000 - `{"message":"`.length)}...`
        );
    });
});
