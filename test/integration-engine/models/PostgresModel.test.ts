import { IDatabaseConnector } from "#helpers/data-access/postgres/IDatabaseConnector";
import { CoreToken } from "#helpers/ioc/CoreToken";
import { log } from "#ie/helpers";
import { IntegrationEngineContainer } from "#ie/ioc";
import { ISequelizeSettings, PostgresModel } from "#ie/models";
import { GeneralError } from "@golemio/errors";
import { JSONSchemaValidator } from "@golemio/validator";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import Sequelize, { ModelAttributes } from "sequelize";
import sinon, { SinonSandbox, SinonSpy } from "sinon";

chai.use(chaiAsPromised);

describe("PostgresModel", () => {
    let sandbox: SinonSandbox;
    let tableName: string;
    let schemaName: string;
    let schemaObject: any;
    let schemaObjectArray: any;
    let settings: ISequelizeSettings;
    let model: PostgresModel;
    let sequelizeAttributes: ModelAttributes<any>;
    let connection: Sequelize.Sequelize;

    beforeEach(async () => {
        IntegrationEngineContainer.clearInstances();
        sandbox = sinon.createSandbox();
        sandbox.spy(log, "warn");
        connection = await IntegrationEngineContainer.resolve<IDatabaseConnector>(CoreToken.PostgresConnector).connect();

        tableName = "test";
        schemaName = "unit_test_pg_model";
        sequelizeAttributes = {
            column1: Sequelize.INTEGER,
            column2: Sequelize.STRING,
            created_at: { type: Sequelize.DATE },
            updated_at: { type: Sequelize.DATE },
        };
        schemaObject = {
            type: "object",
            properties: {
                column1: { type: "number" },
                column2: { type: "string" },
            },
            required: ["column2"],
        };
        schemaObjectArray = {
            type: "array",
            items: schemaObject,
        };
        settings = {
            outputSequelizeAttributes: sequelizeAttributes,
            pgTableName: tableName,
            savingType: "insertOnly",
            pgSchema: schemaName,
        };
        model = new PostgresModel(
            "Test" + "Model",
            settings,
            new JSONSchemaValidator("TestPostgresModelValidator", schemaObject)
        );

        await connection.query(`CREATE SCHEMA IF NOT EXISTS ${schemaName};`);
        await connection.query("CREATE SCHEMA IF NOT EXISTS tmp;");
        await connection.query(
            `CREATE TABLE ${schemaName}.${tableName} ( ` +
                "id serial NOT NULL, " +
                "column1 integer, " +
                "column2 character varying(255), " +
                "created_at timestamp with time zone, " +
                "updated_at timestamp with time zone, " +
                "CONSTRAINT test_pkey PRIMARY KEY (id));",
            { type: Sequelize.QueryTypes.SELECT }
        );
    });

    afterEach(async () => {
        sandbox.restore();
        await connection.query(`DROP TABLE IF EXISTS ${schemaName}.${tableName}; DROP TABLE IF EXISTS tmp.${tableName};`);
    });

    it("should has name", () => {
        expect(model.name).not.to.be.undefined;
    });

    it("should has save method", () => {
        expect(model.save).not.to.be.undefined;
    });

    it("should has truncate method", () => {
        expect(model.truncate).not.to.be.undefined;
    });

    it("should has find method", () => {
        expect(model.find).not.to.be.undefined;
    });

    it("should has findOne method", () => {
        expect(model.findOne).not.to.be.undefined;
    });

    it("should has findAndCountAll method", () => {
        expect(model.findAndCountAll).not.to.be.undefined;
    });

    it("should have bulkSave method", () => {
        expect(model.bulkSave).not.to.be.undefined;
    });

    // method model.save()

    it("should throws error if data are not valid", async () => {
        await expect(model.save({ column1: 1 })).to.be.rejectedWith(Error);
        await expect(model.save({ column1: 1 }, true)).to.be.rejectedWith(Error);
    });

    it("should logs warning if validator is not set", async () => {
        settings.hasTmpTable = true;
        model = new PostgresModel("Test" + "Model", settings, undefined as any);
        await expect(model.save({ column1: 1 })).to.be.fulfilled;
        await expect(model.save({ column1: 1 }, true)).to.be.fulfilled;
        sandbox.assert.calledTwice(log.warn as SinonSpy);
    });

    it("should throws error when save to tmp model and tmp model is not defined", async () => {
        model = new PostgresModel(
            "Test" + "Model",
            settings,
            new JSONSchemaValidator("TestPostgresModelValidator", schemaObject)
        );
        await expect(model.save({ column1: 1, column2: "b" }, true)).to.be.rejectedWith(GeneralError);
    });

    it("should saves one record, type insertOnly", async () => {
        settings.hasTmpTable = true;
        model = new PostgresModel(
            "Test" + "Model",
            settings,
            new JSONSchemaValidator("TestPostgresModelValidator", schemaObject)
        );
        await model.save({ column1: 1, column2: "b" });
        await model.save({ column1: 1, column2: "b" }, true);

        let data = await model.findOne({ where: { column1: 1 } });
        expect(data).to.have.property("column1", 1);
        expect(data).to.have.property("column2", "b");
        expect(data).to.have.property("created_at");
        expect(data).to.have.property("updated_at");

        data = await model.findOne({ where: { column1: 1 } }, true);
        expect(data).to.have.property("column1", 1);
        expect(data).to.have.property("column2", "b");
        expect(data).to.have.property("created_at");
        expect(data).to.have.property("updated_at");
    });

    it("should saves array of records, type insertOnly", async () => {
        settings.hasTmpTable = true;
        model = new PostgresModel(
            "Test" + "Model",
            settings,
            new JSONSchemaValidator("TestPostgresModelValidator", schemaObjectArray)
        );
        await model.save([
            { column1: 1, column2: "b" },
            { column1: 2, column2: "c" },
        ]);
        await model.save(
            [
                { column1: 1, column2: "b" },
                { column1: 2, column2: "c" },
            ],
            true
        );

        let data = await model.find({});
        expect(data.length).to.equal(2);

        data = await model.find({}, true);
        expect(data.length).to.equal(2);
    });

    it("should saves one record, type insertOrUpdate", async () => {
        settings.hasTmpTable = true;
        settings.savingType = "insertOrUpdate";
        model = new PostgresModel(
            "Test" + "Model",
            settings,
            new JSONSchemaValidator("TestPostgresModelValidator", schemaObject)
        );
        await model.save({ id: 1, column1: 1, column2: "b" });
        let data = await model.findOne({ where: { id: 1 } });
        expect(data).to.have.property("column2", "b");

        await model.save({ id: 1, column1: 1, column2: "c" });
        data = await model.findOne({ where: { id: 1 } });
        expect(data).to.have.property("column2", "c");

        await model.save({ id: 1, column1: 1, column2: "b" }, true);
        data = await model.findOne({ where: { id: 1 } }, true);
        expect(data).to.have.property("column2", "b");

        await model.save({ id: 1, column1: 1, column2: "c" }, true);
        data = await model.findOne({ where: { id: 1 } }, true);
        expect(data).to.have.property("column2", "c");
    });

    it("should saves array of records, type insertOrUpdate", async () => {
        settings.hasTmpTable = true;
        settings.savingType = "insertOrUpdate";
        model = new PostgresModel(
            "Test" + "Model",
            settings,
            new JSONSchemaValidator("TestPostgresModelValidator", schemaObjectArray)
        );
        await model.save([
            { id: 1, column1: 1, column2: "b" },
            { id: 2, column1: 2, column2: "c" },
        ]);
        let data = await model.findOne({ where: { id: 1 } });
        expect(data).to.have.property("column2", "b");
        await model.save([
            { id: 1, column1: 1, column2: "c" },
            { id: 2, column1: 2, column2: "d" },
        ]);
        data = await model.findOne({ where: { id: 1 } });
        expect(data).to.have.property("column2", "c");

        await model.save(
            [
                { id: 1, column1: 1, column2: "b" },
                { id: 2, column1: 2, column2: "c" },
            ],
            true
        );
        data = await model.findOne({ where: { id: 1 } }, true);
        expect(data).to.have.property("column2", "b");
        await model.save(
            [
                { id: 1, column1: 1, column2: "c" },
                { id: 2, column1: 2, column2: "d" },
            ],
            true
        );
        data = await model.findOne({ where: { id: 1 } }, true);
        expect(data).to.have.property("column2", "c");

        data = await model.find({});
        expect(data.length).to.equal(2);

        data = await model.find({}, true);
        expect(data.length).to.equal(2);
    });

    // method model.truncate()

    it("should throws error when truncate tmp model and tmp model is not defined", async () => {
        model = new PostgresModel(
            "Test" + "Model",
            settings,
            new JSONSchemaValidator("TestPostgresModelValidator", schemaObject)
        );
        await expect(model.truncate(true)).to.be.rejectedWith(GeneralError);
    });

    it("should truncate", async () => {
        settings.hasTmpTable = true;
        model = new PostgresModel(
            "Test" + "Model",
            settings,
            new JSONSchemaValidator("TestPostgresModelValidator", schemaObject)
        );

        await model.save({ column1: 1, column2: "b" });
        await model.save({ column1: 1, column2: "b" }, true);
        let data = await model.find({});
        expect(data.length).to.equal(1);
        data = await model.find({}, true);
        expect(data.length).to.equal(1);

        await model.truncate();
        await model.truncate(true);
        data = await model.find({});
        expect(data.length).to.equal(0);
        data = await model.find({}, true);
        expect(data.length).to.equal(0);
    });

    // method model.find()

    it("should returns an array of records", async () => {
        settings.hasTmpTable = true;
        model = new PostgresModel(
            "Test" + "Model",
            settings,
            new JSONSchemaValidator("TestPostgresModelValidator", schemaObject)
        );
        await model.save({ column1: 1, column2: "b" });
        await model.save({ column1: 1, column2: "b" }, true);

        let data = await model.find({});
        expect(data).to.be.an("array");
        expect(data.length).to.equal(1);

        data = await model.find({}, true);
        expect(data).to.be.an("array");
        expect(data.length).to.equal(1);
    });

    // method model.findOne()

    it("should returns a one record", async () => {
        settings.hasTmpTable = true;
        model = new PostgresModel(
            "Test" + "Model",
            settings,
            new JSONSchemaValidator("TestPostgresModelValidator", schemaObject)
        );

        await model.save({ column1: 1, column2: "b" });
        await model.save({ column1: 1, column2: "b" }, true);

        let data = await model.findOne({ where: { column1: 1 } });
        expect(data).to.have.property("column1", 1);
        expect(data).to.have.property("column2", "b");
        expect(data).to.have.property("created_at");
        expect(data).to.have.property("updated_at");

        data = await model.findOne({ where: { column1: 1 } }, true);
        expect(data).to.have.property("column1", 1);
        expect(data).to.have.property("column2", "b");
        expect(data).to.have.property("created_at");
        expect(data).to.have.property("updated_at");
    });

    // method model.findAndCountAll()

    it("should returns count and an array of records", async () => {
        settings.hasTmpTable = true;
        model = new PostgresModel(
            "Test" + "Model",
            settings,
            new JSONSchemaValidator("TestPostgresModelValidator", schemaObject)
        );
        await model.save({ column1: 1, column2: "b" });
        await model.save({ column1: 1, column2: "b" }, true);

        let data = await model.findAndCountAll({});
        expect(data).to.have.property("count", 1);
        expect(data).to.have.property("rows");
        expect(data.rows).to.be.an("array");
        expect(data.rows.length).to.equal(1);

        data = await model.findAndCountAll({}, true);
        expect(data).to.have.property("count", 1);
        expect(data).to.have.property("rows");
        expect(data.rows).to.be.an("array");
        expect(data.rows.length).to.equal(1);
    });

    // method model.bulkSave()
    it("should save bulk data (insertOnly)", async () => {
        settings.hasTmpTable = true;
        settings.savingType = "insertOnly";
        model = new PostgresModel(
            "Test" + "Model",
            settings,
            new JSONSchemaValidator("TestPostgresModelValidator", schemaObjectArray)
        );

        await model.bulkSave([
            { id: 10000, column1: 10, column2: "b" },
            { id: 10001, column1: 11, column2: "c" },
        ]);
        await model.bulkSave([{ id: 10000, column1: 10, column2: "d" }]);

        let data = await model.findOne({ where: { column1: 10 } });
        expect(data).to.have.property("column1", 10);
        expect(data).to.have.property("column2", "b");

        data = await model.findOne({ where: { column1: 11 } });
        expect(data).to.have.property("column1", 11);
        expect(data).to.have.property("column2", "c");
    });

    it("should save bulk data (insertOrUpdate)", async () => {
        settings.hasTmpTable = true;
        settings.savingType = "insertOrUpdate";
        model = new PostgresModel(
            "Test" + "Model",
            settings,
            new JSONSchemaValidator("TestPostgresModelValidator", schemaObjectArray)
        );

        await model.bulkSave([
            { id: 10000, column1: 10, column2: "b" },
            { id: 10001, column1: 11, column2: "c" },
        ]);
        await model.bulkSave([{ id: 10000, column1: 10, column2: "d" }], ["column2"]);

        let data = await model.findOne({ where: { column1: 10 } });
        expect(data).to.have.property("column1", 10);
        expect(data).to.have.property("column2", "d");

        data = await model.findOne({ where: { column1: 11 } });
        expect(data).to.have.property("column1", 11);
        expect(data).to.have.property("column2", "c");
    });
});
