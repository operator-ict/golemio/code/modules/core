import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon from "sinon";
import { QueueManager } from "#ie/queueprocessors";

chai.use(chaiAsPromised);

describe("QueueManager", () => {
    const sandbox = sinon.createSandbox();

    afterEach(() => {
        sandbox.restore();
    });

    // =============================================================================
    // sendMessageToExchange
    // =============================================================================
    describe("sendMessageToExchange", () => {
        it("should call sendMessage", () => {
            const sendMessageStub = sandbox.stub(QueueManager, "sendMessage" as any);

            QueueManager["sendMessageToExchange"]("testmodule.test", "doSomething", { property: 42 });
            expect(sendMessageStub.getCall(0).args).to.deep.equal(["workers.testmodule.test.doSomething", '{"property":42}', {}]);
        });
    });
});
