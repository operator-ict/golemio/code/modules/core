import { ILogger } from "#helpers";
import { ISimpleConfig } from "#helpers/configuration/ISimpleConfig";
import { AbstractStorageService, StorageProvider, StorageServiceFactory } from "#helpers/data-access/storage";
import { CoreToken } from "#helpers/ioc/CoreToken";
import { ContainerToken } from "#ie/ioc";
import { FatalError } from "@golemio/errors";
import { inject, injectable } from "tsyringe";

@injectable()
export class StorageServiceProvider {
    protected _service: AbstractStorageService;

    constructor(@inject(CoreToken.SimpleConfig) config: ISimpleConfig, @inject(ContainerToken.Logger) log: ILogger) {
        const service = new StorageServiceFactory(
            {
                enabled: config.getValue("env.STORAGE_ENABLED", "false") === "true",
                provider: {
                    azure: {
                        clientId: config.getValue("env.AZURE_BLOB_CLIENT_ID", ""),
                        tenantId: config.getValue("env.AZURE_BLOB_TENANT_ID", ""),
                        clientSecret: config.getValue("env.AZURE_BLOB_CLIENT_SECRET", ""),
                        account: config.getValue("env.AZURE_BLOB_ACCOUNT", ""),
                        containerName: config.getValue("env.AZURE_BLOB_STORAGE_CONTAINER_NAME", ""),
                        uploadTimeoutInSeconds:
                            Number.parseInt(config.getValue("env.AZURE_BLOB_UPLOAD_TIMEOUT_IN_SECONDS", "120")) || 120,
                    },
                },
            },
            log
        ).getService(StorageProvider.Azure);

        if (!service) {
            throw new FatalError(
                `StorageServiceProvider returned undefined for provider ${StorageProvider.Azure}`,
                this.constructor.name
            );
        }

        this._service = service;
    }

    public get service(): AbstractStorageService {
        return this._service;
    }
}
