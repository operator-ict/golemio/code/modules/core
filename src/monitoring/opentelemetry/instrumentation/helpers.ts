import { Span } from "@opentelemetry/api";

type HookFunction = (span: Span, ...args: any[]) => any;

export const withRecordingSpan = (fn: HookFunction): HookFunction => {
    return (span: Span, ...args) => {
        if (!span.isRecording()) return;
        return fn(span, ...args);
    };
};
