import { ILogger } from "#helpers/logger/LoggerProvider";
import { INumberOfRecordsLabelData, metricsService } from "#monitoring";
import { ErrorHandler } from "@golemio/errors";
import { EventEmitter } from "events";
import { NumberOfRecordsEventData } from "./interfaces/INumberOfRecordsEventData";
import { IResponseTimesEventData } from "./interfaces/IResponseTimesEventData";

export enum LoggerEventType {
    ResponseTimes = "response-time:middleware",
    NumberOfRecords = "number-of-records",
}

export class LoggerEmitter extends EventEmitter {
    constructor(private readonly log: ILogger) {
        super();

        this.on(LoggerEventType.ResponseTimes, (data) => this.handleResponseTimes(data));
        this.on(LoggerEventType.NumberOfRecords, (data) => this.handleNumberOfRecords(data));
        this.on("error", (error) => this.handleError(error));
    }

    public emit(event: LoggerEventType.ResponseTimes, data: IResponseTimesEventData): boolean;
    public emit(event: LoggerEventType.NumberOfRecords, data: NumberOfRecordsEventData): boolean;
    public emit(event: string, ...args: any[]): boolean {
        return super.emit(event, ...args);
    }

    private handleResponseTimes(data: IResponseTimesEventData) {
        const label = `${data.req.method} ${data.req.originalUrl}`;
        this.log.verbose(`ResponseTimesLogger: ${label}: ${data.name} ${data.elapsedMS}ms`);
    }

    private handleNumberOfRecords(data: NumberOfRecordsEventData) {
        const labelData: INumberOfRecordsLabelData = "name" in data ? { name: data.name } : data.req;
        metricsService.recordNumberOfRecords(labelData, data.numberOfRecords);
    }

    private handleError(error: Error) {
        ErrorHandler.handle(error, this.log);
    }
}
