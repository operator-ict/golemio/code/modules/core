import { decode as decodeHtmlEntities } from "html-entities";

export class HTMLUtils {
    public static outputPlainText(html: string | null | undefined): string | null {
        if (!html) return null;

        return decodeHtmlEntities(html).replace(/<[^>]+>/g, "");
    }

    public static outputStructuredText(html: string | null | undefined): string | null {
        if (!html) return null;

        return decodeHtmlEntities(html)
            .replace(/<\/[pP][^>]*>/g, "\n")
            .replace(/<[^>]+>/g, "")
            .trim();
    }
}
