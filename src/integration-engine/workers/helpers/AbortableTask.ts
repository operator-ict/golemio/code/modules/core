import { AbortError } from "@golemio/errors";
import { IntegrationEngineContainer } from "#ie/ioc";
import { ISimpleConfig } from "#helpers/configuration/ISimpleConfig";
import { CoreToken } from "#helpers/ioc/CoreToken";

interface AbortableExecutorFunction<T> {
    (resolve: (value: PromiseLike<T> | T) => void, reject: (reason: Error) => void, abortSignal: AbortSignal): void;
}

export class AbortableTask<T> extends Promise<T> {
    constructor(executor: AbortableExecutorFunction<T>, queueName: string) {
        const simpleConfig = IntegrationEngineContainer.resolve<ISimpleConfig>(CoreToken.SimpleConfig);
        const abortSignal = AbortSignal.timeout(Number(simpleConfig.getValue("env.RABBIT_CONSUMER_TIMEOUT", 1740000)));

        super((resolve, reject): void => {
            abortSignal.addEventListener(
                "abort",
                () => {
                    reject(new AbortError(`Task aborted due to timeout (${queueName})`));
                },
                { once: true }
            );

            executor(resolve, reject, abortSignal);
        });
    }

    static from = <T>(promise: Promise<T>, queueName: string): Promise<T> => {
        if (promise instanceof AbortableTask) {
            return promise;
        }

        return new AbortableTask<T>((resolve, reject) => {
            promise.then(resolve).catch(reject);
        }, queueName);
    };
}
