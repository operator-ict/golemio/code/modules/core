import { AbstractTask } from "./AbstractTask";

export abstract class AbstractEmptyTask extends AbstractTask<object> {
    protected schema = undefined;
}
