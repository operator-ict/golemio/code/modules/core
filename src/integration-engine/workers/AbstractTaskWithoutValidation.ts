import { Message, MessageProperties } from "amqplib";
import { ITask } from "./interfaces";
import { AbortableTask } from "#ie/workers/helpers/AbortableTask";

export abstract class AbstractTaskWithoutValidation<TRawData extends object> implements ITask {
    public abstract queueName: string;
    public queueTtl?: number;
    public queuePrefix: string;
    public queueType: "classic" | "quorum" = "quorum";

    constructor(queuePrefix: string) {
        this.queuePrefix = queuePrefix;
    }

    get queueKey() {
        return `${this.queuePrefix}.${this.queueName}`;
    }

    public async consume(msg: Message | null) {
        return AbortableTask.from(this.validateAndExecute(msg), this.queueKey);
    }

    protected abstract validateAndExecute(msg: Message | null): Promise<void>;

    protected abstract execute(data: TRawData): void | Promise<void>;
    protected abstract execute(data: TRawData, msgProperties?: MessageProperties): void | Promise<void>;
}
