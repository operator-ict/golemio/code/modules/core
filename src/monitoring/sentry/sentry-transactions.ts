import * as sentry from "@sentry/node";
import { Span, Transaction } from "@sentry/tracing";
import amqplib from "amqplib";

enum SentryTransactions {
    PRODUCER = "rabbitmq.producer",
    CONSUMER = "rabbitmq.consumer",
}

type ProduceFunction<T> = (key: string, msg: string, option?: Record<string, any>) => Promise<T>;
type ConsumeFunction<T> = (msg: amqplib.Message | null, ...args: any[]) => Promise<T>;

export const withSentryProducerTrace = <T>(fn: ProduceFunction<T>): ProduceFunction<T> => {
    return async (key, ...args) => {
        const queue = key.split(".").slice(-2).join(".");

        const parentScope = sentry.getCurrentHub().getScope()?.getTransaction();
        let currentScope: Transaction | Span;

        if (parentScope) {
            currentScope = parentScope.startChild({
                op: SentryTransactions.PRODUCER,
                description: queue || key,
            });
        } else {
            currentScope = sentry.startTransaction({
                op: SentryTransactions.PRODUCER,
                name: queue || key,
            });

            sentry.configureScope((scope) => {
                scope.setSpan(currentScope);
            });
        }

        try {
            return await fn(key, ...args);
        } catch (err) {
            sentry.captureException(err);
            throw err;
        } finally {
            currentScope.finish();
        }
    };
};

export const withSentryConsumerTrace = <T>(fn: ConsumeFunction<T>): ConsumeFunction<T> => {
    return async (msg: amqplib.Message | null, ...args: any) => {
        const queue = msg?.fields.routingKey.split(".").slice(-2).join(".");

        const transaction = sentry.startTransaction({
            op: SentryTransactions.CONSUMER,
            name: queue || msg?.fields.routingKey || "unknown",
        });

        sentry.configureScope((scope) => {
            scope.setSpan(transaction);
        });

        try {
            return await fn(msg, ...args);
        } catch (err) {
            sentry.captureException(err);
            throw err;
        } finally {
            transaction.finish();
        }
    };
};
