import { ContainerToken, InputGatewayContainer } from "#ig/ioc";
import { AMQPConnector } from "./AMQPConnector";

/* ig/connectors/index.ts */
const queueConnector = InputGatewayContainer.resolve<AMQPConnector>(ContainerToken.AmqpConnector);

/**
 * @deprecated For backwards compatibility. Use ioc container instead.
 */
export { queueConnector as AMQPConnector };
