import { AbstractRouter } from "#helpers/routing/AbstractRouter";
import { IGeoJsonAllFilterParameters } from "#og";
import { CacheHeaderMiddleware } from "#og/CacheHeaderMiddleware";
import { parseCoordinates } from "#og/Geo";
import { ContainerToken } from "#og/ioc/ContainerToken";
import { OutputGatewayContainer } from "#og/ioc/Di";
import { log } from "#og/Logger";
import { IGeoJsonModel } from "#og/models/interfaces/IGeoJsonModel";
import { IRouterExpireParameters } from "#og/routes/interfaces/IRouterExpireParameters";
import { checkErrors, pagination, paginationLimitMiddleware } from "#og/Validation";
import { NextFunction, Request, Response } from "express";
import { param, query, ValidationChain } from "express-validator";
import QueryString from "qs";

/**
 * Router for data in GeoJSON format using GeoJSON model -
 * binds / and /:id to GetAll and GetOne methods of GeoJsonModel.
 *
 * Router /WEB LAYER/: maps routes to specific model functions, passes request parameters and handles responses.
 * Handles web logic (http request, response). Sets response headers, handles error responses.
 */
export abstract class AbstractGeoJsonRouter extends AbstractRouter {
    protected model: IGeoJsonModel;
    protected cacheHeaderMiddleware: CacheHeaderMiddleware;

    protected standardParams = [
        query("updatedSince").optional().isISO8601().not().isArray(),
        query("districts").optional().not().isEmpty({ ignore_whitespace: true }),
        query("ids")
            .optional()
            .custom((value) => {
                if (Array.isArray(value)) {
                    if (!value.every((el) => Number.isInteger(+el))) {
                        throw new Error("Array contains a non-numeric id");
                    }
                } else if (!Number.isInteger(+value)) {
                    throw new Error("Non-numeric id");
                }

                return true;
            }),
        query("latlng").optional().isString().not().isArray(),
        query("range").optional().isNumeric().not().isArray(),
    ];

    protected constructor(version: string, path: string, inModel: IGeoJsonModel) {
        super(version, path);
        this.model = inModel;
        this.cacheHeaderMiddleware = OutputGatewayContainer.resolve<CacheHeaderMiddleware>(ContainerToken.CacheHeaderMiddleware);
    }

    /**
     * Converts a single value of `any` type to an array containing this element
     */
    public ConvertToArray = (toBeArray: any) => {
        return toBeArray instanceof Array ? toBeArray : [toBeArray];
    };

    /**
     * Initiates all routes. Should respond with correct data to a HTTP requests to all routes.
     * @param {number|string} expire TTL for the caching middleware
     */
    protected initRoutes = async (expire: IRouterExpireParameters = { maxAge: 6 * 60, staleWhileRevalidate: 5 }) => {
        const idParam = await this.GetIdQueryParamWithCorrectType();
        this.router.get(
            "/",
            this.standardParams,
            pagination,
            paginationLimitMiddleware("GeoJsonRouter"),
            checkErrors,
            this.cacheHeaderMiddleware.getMiddleware(expire?.maxAge, expire?.staleWhileRevalidate),
            this.GetAll
        );
        this.router.get(
            "/:id",
            [
                // Previously set parameter of type according to the data's primary ID type
                idParam,
            ],
            checkErrors,
            this.cacheHeaderMiddleware.getMiddleware(expire?.maxAge, expire?.staleWhileRevalidate),
            this.GetOne
        );
    };

    public GetAll = async (req: Request, res: Response, next: NextFunction) => {
        // Parsing parameters
        let ids = req.query.ids as string[];
        let districts = req.query.districts as string[];

        if (districts) {
            districts = this.ConvertToArray(districts);
        }
        if (ids) {
            ids = this.ConvertToArray(ids);
        }

        try {
            const coords = await parseCoordinates(req.query.latlng as string, req.query.range as string);
            const data = await this.model.GetAll({
                districts,
                ids: ids as any[],
                lat: coords.lat,
                limit: Number(req.query.limit),
                lng: coords.lng,
                offset: Number(req.query.offset),
                range: coords.range,
                updatedSince: req.query.updatedSince as string,
                additionalFilters: this.GetAdditionalFilters(req.query),
            } as IGeoJsonAllFilterParameters);

            res.status(200).send(data);
        } catch (err) {
            next(err);
        }
    };

    public GetOne = (req: Request, res: Response, next: NextFunction) => {
        const id: string = req.params.id;

        this.model
            .GetOne(id)
            .then((data) => {
                res.status(200).send(data);
            })
            .catch((err) => {
                next(err);
            });
    };

    protected GetIdQueryParamWithCorrectType = async (): Promise<ValidationChain> => {
        let idParam: ValidationChain;

        // Get the primary ID of the schema (the attribute name)
        const idKey = Object.keys(this.model.PrimaryIdentifierSelection("0"))[0];
        let message: string = "Created model " + this.model.GetName() + " has ID `" + idKey + "` of type ";
        const isPrimaryIdNumber = await this.model.IsPrimaryIdNumber(idKey);

        if (isPrimaryIdNumber) {
            message += "number.";
            // Create a url parameter for detail route with type number
            idParam = param("id").exists().isNumeric();
        } else {
            message += "string.";
            // Create a url parameter for detail route with type string
            idParam = param("id").exists().isString();
        }
        log.silly(message);
        return idParam;
    };

    public GetAdditionalFilters = (query: QueryString.ParsedQs) => {
        const { districts, ids, latlng, limit, offset, range, updatedSince, ...rest } = query;
        return {
            ...rest,
        };
    };
}
