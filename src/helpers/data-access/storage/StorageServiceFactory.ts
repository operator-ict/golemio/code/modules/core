import { ILogger } from "#helpers/logger";
import { AbstractStorageService } from "./AbstractStorageService";
import { IAzureBlobStorageConfig, AzureBlobStorageService } from "./AzureBlobStorageService";
import { NoopStorageService } from "#helpers/data-access/storage/NoopStorageService";

export interface IStorageConfig {
    enabled: boolean;
    provider: {
        azure: IAzureBlobStorageConfig;
    };
}

export enum StorageProvider {
    Noop,
    Azure,
}

export class StorageServiceFactory {
    private dictionary: Map<StorageProvider, AbstractStorageService>;

    constructor(private config: IStorageConfig, log: ILogger) {
        this.dictionary = new Map();

        if (config.enabled) {
            this.dictionary.set(StorageProvider.Azure, new AzureBlobStorageService(config.provider.azure, log));
        } else {
            log.silly("[StorageServiceFactory] Storage is disabled.");
            this.dictionary.set(StorageProvider.Noop, new NoopStorageService(log));
        }
    }

    public getService(storageProvider: StorageProvider): AbstractStorageService | undefined {
        if (this.config.enabled) {
            return this.dictionary.get(storageProvider);
        } else {
            return this.dictionary.get(StorageProvider.Noop);
        }
    }
}
