import SimpleConfig from "#helpers/configuration/SimpleConfig";
import { expect } from "chai";

describe("Simple Config", () => {
    let simpleConfig: SimpleConfig;

    beforeEach(() => {
        simpleConfig = new SimpleConfig({
            postgres: {
                host: "localhost",
                port: 5432,
                user: "postgres",
                password: "postgres",
                testNull: null,
                testUndefined: undefined,
                testZero: 0,
                someObject: {
                    test1: "test1",
                    test2: "test2",
                },
                testEmptyObject: {},
                testEmpty: "",
            },
            module: {
                logging: true,
                batchSize: 20,
                cloudflare: {
                    cachedApiUrlPaths: ["aaa", "bbb", "ccc", "ddd"],
                },
            },
        });
    });

    it("should return correct configuration values", () => {
        expect(simpleConfig.getValue("postgres.host")).to.equal("localhost");
        expect(simpleConfig.getValue("postgres.port")).to.equal(5432);
        expect(simpleConfig.getValue("postgres.user")).to.equal("postgres");
        expect(simpleConfig.getValue("postgres.password")).to.equal("postgres");
        expect(simpleConfig.getValue("postgres.someObject")).to.deep.equal({
            test1: "test1",
            test2: "test2",
        });
        expect(simpleConfig.getValue("postgres.testEmptyObject")).to.deep.equal({});
        expect(simpleConfig.getValue("postgres.testEmpty")).to.deep.equal("");
        expect(simpleConfig.getValue("postgres.testZero")).to.deep.equal(0);
    });

    it("unsupported values should throw error", () => {
        expect(() => simpleConfig.getValue("postgres.testUndefined")).to.throw();
    });

    it("default values should be returned", () => {
        expect(simpleConfig.getValue("postgres.testUndefined", "default")).to.equal("default");
    });

    it("should add new configuration and return new values", () => {
        simpleConfig.addConfiguration({
            mssql: {
                host: "localhost",
                port: 1433,
            },
        });
        expect(simpleConfig.getValue("mssql.host")).to.equal("localhost");
        expect(simpleConfig.getValue("mssql.port")).to.equal(1433);
    });

    it("should return correct module value", () => {
        expect(simpleConfig.getValue("module.batchSize")).to.equal(20);
    });

    it("should return correct cloudflare settings", () => {
        expect(simpleConfig.getValue("module.cloudflare.cachedapiurlpaths.*")).to.deep.equal(["aaa", "bbb", "ccc", "ddd"]);
    });
});
