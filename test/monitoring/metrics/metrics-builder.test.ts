import prometheus, { Counter } from "prom-client";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { registerMetrics } from "#monitoring/metrics/metrics-builder";
import { IMetricsConfigurationList } from "#monitoring";
import { MetricsType } from "#monitoring/metrics/metrics-helpers";

chai.use(chaiAsPromised);

describe("Metrics Builder", () => {
    after(async () => {
        await prometheus.register.clear();
    });

    it("should register metrics in prometheus default register", async () => {
        const testMetrics: IMetricsConfigurationList<{ testCounter: Counter<string> }> = {
            testCounter: {
                name: "test_counter",
                type: MetricsType.COUNTER,
                help: "Test counter description",
                labelNames: ["param1", "param2"],
            },
        };
        const metricsList = registerMetrics(prometheus.register, testMetrics, "prefix_");
        const registeredMetrics = await prometheus.register.getMetricsAsArray();
        expect(metricsList.testCounter).to.equal(registeredMetrics[0]);
    });
});
