import { IsNumber } from "class-validator";

export interface ITestInput {
    property: number;
}

export class TestValidationSchema implements ITestInput {
    @IsNumber()
    property!: number;
}
