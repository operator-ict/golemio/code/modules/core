export interface IWebhook {
    execute(): Promise<void>;
}
