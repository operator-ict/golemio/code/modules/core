import { IncomingMessage, ServerResponse } from "http";
import { DestinationStream } from "pino";
import pinoHttp, { Options } from "pino-http";
import { SerializedRequest, SerializedResponse } from "pino-std-serializers";
import { ILogger, LoggerCustomLevels } from "./LoggerProvider";

export type ExtendedRequest = IncomingMessage & {
    session?: {
        user?: any;
    };
};

export const requestSerializer = (req: SerializedRequest & { raw: ExtendedRequest }) => ({
    httpVersion: req.raw.httpVersion,
    method: req.method,
    url: req.url,
    remoteAddress: req.remoteAddress,
    userAgent: req.headers["user-agent"],
});

export const responseSerializer = (res: SerializedResponse) => ({
    status: res.statusCode,
    contentLength: res.raw.getHeader("content-length"),
    get userId() {
        return (res.raw.req as ExtendedRequest).session?.user ? (res.raw.req as ExtendedRequest).session!.user.id : null;
    },
});

export const createRequestLogger = (nodeEnv: string, logger: ILogger, stream?: DestinationStream) => {
    const options: Options<IncomingMessage, ServerResponse, LoggerCustomLevels> = {
        logger,
        autoLogging: true,
        level: "info",
        messageKey: "message",
        customLogLevel: (req: IncomingMessage, res: ServerResponse, err: any) => {
            if (!res.statusCode || res.statusCode >= 500 || err) {
                return "error";
            }
            if (res.statusCode >= 400) {
                return "warn";
            }
            return "info";
        },
        serializers: {
            req: requestSerializer,
            res: responseSerializer,
        },
    };

    return stream ? pinoHttp(options, stream) : pinoHttp(options);
};
