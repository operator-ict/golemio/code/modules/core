export interface IRouterExpireParameters {
    /** max age in seconds */
    maxAge?: number;
    /** stale-while-revalidate in seconds */
    staleWhileRevalidate?: number;
}
