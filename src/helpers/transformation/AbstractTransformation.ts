import { GeneralError } from "@golemio/errors";

export abstract class AbstractTransformation<In, Out> {
    public abstract name: string;
    protected abstract transformInternal: (element: In) => Out;

    public transformElement = (element: In): Out => {
        try {
            return this.transformInternal(element);
        } catch (error) {
            throw new GeneralError(
                `Unable to transform element: "${JSON.stringify(element).substring(0, 200)}".`,
                this.constructor.name,
                error
            );
        }
    };

    public transformArray = (data: In[]): Out[] => {
        return data.map(this.transformElement);
    };
}
