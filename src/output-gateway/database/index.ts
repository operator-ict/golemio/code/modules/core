import { ContainerToken, OutputGatewayContainer } from "#og/ioc";
import { Sequelize } from "sequelize";

/**
 * @deprecated For backwards compatibility. Use ioc container instead.
 */
let sequelizeConnection: Sequelize;
/**
 * @deprecated For backwards compatibility. Use ioc container instead.
 */
let sequelizeReadOnlyConnection: Sequelize;

(async () => {
    sequelizeConnection = await OutputGatewayContainer.resolve<Sequelize>(ContainerToken.SequelizeConnection);
    sequelizeReadOnlyConnection = sequelizeConnection;
})();

export { sequelizeConnection, sequelizeReadOnlyConnection };
