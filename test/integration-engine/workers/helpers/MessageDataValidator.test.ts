import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonStub } from "sinon";
import { GeneralError } from "@golemio/errors";
import * as classValidator from "class-validator";
import { plainToInstance } from "class-transformer";
import { log } from "#ie/helpers";
import { storageService } from "#ie/data-access/";
import { MessageDataValidator } from "#ie/workers/helpers/MessageDataValidator";
import { TestValidationSchema } from "../test-schema/TestSchema";
import { TestNestedValidationSchema } from "../test-schema/TestNestedSchema";

chai.use(chaiAsPromised);

describe("MessageDataValidator", () => {
    const sandbox = sinon.createSandbox();
    let saveDataStub: SinonStub;

    beforeEach(() => {
        sandbox.stub(log, "error");
        saveDataStub = sandbox.stub(storageService, "uploadFile").resolves();
    });

    afterEach(() => {
        sandbox.restore();
    });

    // =============================================================================
    // validate
    // =============================================================================
    describe("validate", () => {
        it("should validate", async () => {
            const data = new TestValidationSchema();
            data.property = 42;

            const promise = MessageDataValidator.validate("test.test.testTask", data);
            await expect(promise).to.be.fulfilled;
            expect(saveDataStub.callCount).to.equal(0);
        });

        it("should validate nested property", async () => {
            const data = plainToInstance(TestNestedValidationSchema, { nested: { property: 42 } });
            const promise = MessageDataValidator.validate("test.test.testTask", data);
            await expect(promise).to.be.fulfilled;
            expect(saveDataStub.callCount).to.equal(0);
        });

        it("should reject (internal validation error)", async () => {
            sandbox.stub(classValidator, "validate").rejects(new Error("Unknown validation error"));
            const data = new TestValidationSchema();
            data.property = 42;

            const promise = MessageDataValidator.validate("test.test.testTask", data);
            await expect(promise).to.be.rejectedWith(
                GeneralError,
                "[Queue test.test.testTask] Message validation failed: Unknown validation error"
            );

            expect(saveDataStub.callCount).to.equal(1);
        });

        it("should reject (invalid data)", async () => {
            const data = new TestValidationSchema();
            data.property = "test" as any;

            const promise = MessageDataValidator.validate("test.test.testTask", data);
            await expect(promise).to.be.rejectedWith(
                GeneralError,
                // eslint-disable-next-line max-len
                "[Queue test.test.testTask] Message validation failed: [property must be a number conforming to the specified constraints]"
            );

            expect(saveDataStub.callCount).to.equal(1);
        });

        it("should reject (invalid data with nested property)", async () => {
            const data = plainToInstance(TestNestedValidationSchema, { nested: { property: "test" } });
            const promise = MessageDataValidator.validate("test.test.testTask", data);
            await expect(promise).to.be.rejectedWith(
                GeneralError,
                // eslint-disable-next-line max-len
                "[Queue test.test.testTask] Message validation failed: [property must be a number conforming to the specified constraints]"
            );

            expect(saveDataStub.callCount).to.equal(1);
        });

        it("should reject (empty array)", async () => {
            const data = plainToInstance(TestNestedValidationSchema, []);
            const promise = MessageDataValidator.validate("test.test.testTask", data);
            await expect(promise).to.be.rejectedWith(
                GeneralError,
                // eslint-disable-next-line max-len
                "[Queue test.test.testTask] Message validation failed: [an unknown value was passed to the validate function]"
            );

            expect(saveDataStub.callCount).to.equal(1);
        });
    });
});
