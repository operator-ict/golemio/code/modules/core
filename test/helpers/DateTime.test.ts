import { expect } from "chai";
import { DateTime, dateTime } from "#helpers/DateTime";
import { ValidationError } from "@golemio/errors";

describe("helpers/DateTime", () => {
    it("correctly formats summer time in Prague", () => {
        const testSummerDate = new Date(Date.UTC(2023, 5, 26, 4, 9, 7, 100));
        const actualSummerDate = dateTime(testSummerDate).setTimeZone("Europe/Prague").toISOString();
        expect(actualSummerDate).eql("2023-06-26T06:09:07+02:00");
    });

    it("correctly formats time in different time zones", () => {
        const testDate = new Date(Date.UTC(2023, 4, 6, 4, 9, 7, 120));

        const actualConakryDate = dateTime(testDate).setTimeZone("Africa/Conakry").toISOString({ includeMillis: true });
        expect(actualConakryDate).eql("2023-05-06T04:09:07.120+00:00");

        const actualNewYorkDate = dateTime(testDate).setTimeZone("America/New_York").toISOString({ includeMillis: false });
        expect(actualNewYorkDate).eql("2023-05-06T00:09:07-04:00");
    });

    it("correctly formats winter time in Prague", () => {
        const testWinterDate = new Date(Date.UTC(2023, 11, 24, 19, 30, 0));
        const actualDate = dateTime(testWinterDate).setTimeZone("Europe/Prague").toISOString();
        expect(actualDate).eql("2023-12-24T20:30:00+01:00");
    });

    it("correctly changes time zone from Prague to UTC", () => {
        const actualDate = DateTime.fromFormat("2023-12-24", "yyyy-LL-dd", { timeZone: "Europe/Prague" })
            .setTimeZone("UTC")
            .toISOString();
        expect(actualDate).eql("2023-12-23T23:00:00Z");
    });

    it("Throws error when parsing from format is invalid", () => {
        const invalidParse = () => DateTime.fromFormat("2023-09-24T11:02", "yyyy-LL-dd'T'HH:mm:ss.S");
        expect(invalidParse).to.throw(ValidationError);
    });

    it("Throws error when importing from ISO without time zone", () => {
        expect(() => DateTime.fromISO("2023-12-24T19:30:00")).to.throw(ValidationError);
    });

    it("Correctly parses from ISO with time zone as parameter", () => {
        const actualDateWinter = DateTime.fromISO("2023-12-24T19:30:00", { timeZone: "Europe/Prague" });
        expect(actualDateWinter.toISOString()).eql("2023-12-24T19:30:00+01:00");

        const actualDateSummer = DateTime.fromISO("2023-07-05T09:03:00", { timeZone: "Europe/Prague" });
        expect(actualDateSummer.toISOString()).eql("2023-07-05T09:03:00+02:00");
    });

    it("correctly formats date by format string", () => {
        const actualDate = DateTime.fromISO("2023-06-30T20:05:27.121Z").format("yyyy-LL-dd HH:mm:ss.SSS z");
        expect(actualDate).eql("2023-06-30 20:05:27.121 UTC");

        const someOtherDate = DateTime.fromISO("2023-06-30T14:15:17Z").format("yyyy-MM-dd");
        expect(someOtherDate).eql("2023-06-30");
    });

    it("correctly formats by string", () => {
        const actualDate = DateTime.fromISO("2023-06-30T05:25:27Z").format("d/L/y H:m:s");
        expect(actualDate).eql("30/6/2023 5:25:27");
    });

    it("correctly adds 10 minutes", () => {
        const actualDate = DateTime.fromISO("2023-12-24T19:30:00Z").add(10, "minutes").toISOString();
        expect(actualDate).eql("2023-12-24T19:40:00Z");
    });

    it("correctly subtracts 10 minutes", () => {
        const actualDate = DateTime.fromISO("2023-12-24T19:30:00Z").subtract(10, "minutes").toISOString();
        expect(actualDate).eql("2023-12-24T19:20:00Z");
    });

    it("correctly returns the number of milliseconds since the epoch (valueOf) ", () => {
        const actualDate = DateTime.fromISO("2023-12-24T19:30:00Z").valueOf();
        expect(actualDate).eql(1703446200000);
    });

    it("correctly works from ISO", () => {
        const IsoWithTimeZone = "2019-05-02T13:28:59.6100000+02:00";
        expect(DateTime.fromISO(IsoWithTimeZone).valueOf()).eql(1556796539610);

        const actualDate = DateTime.fromISO("2023-12-24T19:30:00Z").valueOf();
        expect(actualDate).eql(1703446200000);
    });

    it("correctly works from custom format", () => {
        const actualDate = DateTime.fromFormat("24.12.2023 19:30", "dd.MM.yyyy HH:mm", { timeZone: "UTC" }).valueOf();
        expect(actualDate).eql(1703446200000);
    });

    it("correctly works from minimum", () => {
        const minDate = DateTime.min(
            "2019-05-02T14:28:59.000+02:00",
            "2019-05-02T13:28:59.610+02:00",
            "2019-05-02T13:28:59.610Z"
        );
        expect(minDate.setTimeZone("Europe/Prague").toISOString({ includeMillis: true })).eql("2019-05-02T13:28:59.610+02:00");
    });

    it("correctly works from maximum", () => {
        const maxDate = DateTime.max(
            "2019-05-02T14:28:59.000+02:00",
            "2019-05-02T13:28:59.610+02:00",
            "2019-05-02T13:28:59.610Z"
        );
        expect(maxDate.setTimeZone("Europe/Prague").toISOString({ includeMillis: true })).eql("2019-05-02T15:28:59.610+02:00");
    });
});
