import amqplib from "amqplib";

export default interface IQueueConnector {
    connect(): Promise<amqplib.Channel>;
    isConnected(): Promise<boolean>;
    getChannel(): amqplib.Channel;
    sendMessage(key: string, msg: string, options?: amqplib.Options.Publish): Promise<boolean>;
    disconnect(): Promise<void>;
}
