import { DataTypes, ModelAttributes } from "src/shared/sequelize";

export interface IModelAttributes {
    create_batch_id: string;
    created_at: Date;
    created_by: string;
    update_batch_id: string;
    updated_at: Date;
    updated_by: string;
}

const auditAttributes: ModelAttributes<any, IModelAttributes> = {
    // ⬐ Auditni pole
    create_batch_id: { type: DataTypes.BIGINT }, // ID vstupní dávky
    created_at: { type: DataTypes.DATE }, // Čas vložení
    created_by: { type: DataTypes.STRING(150) }, // identikace uživatele/procesu, který záznam vložil
    update_batch_id: { type: DataTypes.BIGINT }, // ID poslední dávky, která záznam modifikovala
    updated_at: { type: DataTypes.DATE }, // Čas poslední modifikace
    updated_by: { type: DataTypes.STRING(150) }, // Identikace uživatele/procesu, který poslední měnil záznam
};

export { auditAttributes as AuditAttributes };
