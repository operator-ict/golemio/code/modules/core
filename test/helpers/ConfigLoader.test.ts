import sinon from "sinon";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import path from "path";
import fs from "fs";
import { ConfigLoader } from "#helpers";

chai.use(chaiAsPromised);

describe("ConfigLoader", () => {
    const sandbox = sinon.createSandbox();
    const oldNodeEnv = process.env.NODE_ENV;
    const APP_ROOT = fs.realpathSync(process.cwd());
    const configFolder = path.join(APP_ROOT, "config");
    const replTest = path.join(configFolder, "repltest.json");
    const replInvalidTest = path.join(configFolder, "repltestinvalid.json");
    const replTestDefault = path.join(configFolder, "repltest.default.json");
    const replTest2 = path.join(configFolder, "repltest2.json");

    beforeEach(() => {
        fs.mkdirSync(configFolder);
    });

    afterEach(() => {
        const files = fs.readdirSync(configFolder);
        files.map((filename) => fs.unlinkSync(`${configFolder}/${filename}`));
    });

    afterEach(() => {
        fs.rmSync(configFolder, { recursive: true });
    });

    after(() => {
        process.env.NODE_ENV = oldNodeEnv;
    });

    it("should throw error if config file is not found", () => {
        process.env.NODE_ENV = "production";

        const conf = () => new ConfigLoader("test");
        expect(conf).to.throw(Error, /Error loading config: Error: Cannot find module/);
    });

    it("should throw error if config file is not valid", () => {
        process.env.NODE_ENV = "production";
        fs.writeFileSync(replInvalidTest, `{"a":3,"c":1,"d":2,}`);

        const conf = () => new ConfigLoader("repltestinvalid");
        expect(conf).to.throw(Error, /Error loading config: SyntaxError/);
    });

    it("should properly load conf", () => {
        fs.writeFileSync(
            replTest,
            JSON.stringify({
                a: 3,
                c: 1,
                d: 2,
            })
        );

        let conf = new ConfigLoader("repltest").conf;
        expect(conf.a).to.equal(3);
        expect(conf.b).to.equal(undefined);
        expect(conf.c).to.equal(1);
        expect(conf.d).to.equal(2);
    });
});
