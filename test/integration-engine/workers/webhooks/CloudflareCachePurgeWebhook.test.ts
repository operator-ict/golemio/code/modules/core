import { ISimpleConfig } from "#helpers/configuration/ISimpleConfig";
import SimpleConfig from "#helpers/configuration/SimpleConfig";
import { CoreToken } from "#helpers/ioc/CoreToken";
import { IntegrationEngineContainer } from "#ie/ioc";
import { CloudflareCachePurgeWebhook } from "#ie/workers/webhooks/CloudflareCachePurgeWebhook";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox } from "sinon";

chai.use(chaiAsPromised);

describe("CloudflareCachePurgeWebhook", () => {
    let sandbox: SinonSandbox;

    before(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        IntegrationEngineContainer.clearInstances();
        sandbox.restore();
    });

    after(() => {
        sinon.restore();
    });

    // =============================================================================
    // execute
    // =============================================================================
    describe("execute", () => {
        it("should call the proper methods when enabled and validate response", async () => {
            IntegrationEngineContainer.registerInstance<ISimpleConfig>(
                CoreToken.SimpleConfig,
                new SimpleConfig({ env: { CLOUDFLARE_ENABLED: true } })
            );
            const webhook = new CloudflareCachePurgeWebhook(["a", "b", "c"]);
            const webhookFetchStrategyGetRawDataStub = sandbox
                .stub(webhook["fetchStrategy"], "getRawData")
                .resolves({ meta: { statusCode: 200 }, data: { success: true } } as any);
            const webhookLoggerErrorStub = sandbox.stub(webhook["logger"], "error");
            const webhookLoggerInfoStub = sandbox.stub(webhook["logger"], "info");

            const res = await webhook.execute();

            expect(webhookFetchStrategyGetRawDataStub.calledOnce).to.be.true;
            expect(webhookLoggerInfoStub.calledOnce).to.be.true;
            expect(webhookLoggerErrorStub.calledOnce).to.be.false;
            expect(res).to.be.undefined;
        });

        it("should do nothing when disabled", async () => {
            IntegrationEngineContainer.registerInstance<ISimpleConfig>(
                CoreToken.SimpleConfig,
                new SimpleConfig({ env: { CLOUDFLARE_ENABLED: false } })
            );
            const webhook = new CloudflareCachePurgeWebhook(["a", "b", "c"]);
            const webhookFetchStrategyGetRawDataStub = sandbox
                .stub(webhook["fetchStrategy"], "getRawData")
                .resolves({ meta: { statusCode: 200 }, data: { success: true } } as any);
            const webhookLoggerErrorStub = sandbox.stub(webhook["logger"], "error");
            const webhookLoggerInfoStub = sandbox.stub(webhook["logger"], "info");

            const res = await webhook.execute();

            expect(webhookFetchStrategyGetRawDataStub.calledOnce).to.be.false;
            expect(webhookLoggerInfoStub.calledOnce).to.be.false;
            expect(webhookLoggerErrorStub.calledOnce).to.be.false;
            expect(res).to.be.undefined;
        });

        it("should log error when response status code is invalid", async () => {
            IntegrationEngineContainer.registerInstance<ISimpleConfig>(
                CoreToken.SimpleConfig,
                new SimpleConfig({ env: { CLOUDFLARE_ENABLED: true } })
            );
            const webhook = new CloudflareCachePurgeWebhook(["a", "b", "c"]);
            const webhookFetchStrategyGetRawDataStub = sandbox
                .stub(webhook["fetchStrategy"], "getRawData")
                .resolves({ meta: { statusCode: 400 }, data: { success: true } } as any);
            const webhookLoggerErrorStub = sandbox.stub(webhook["logger"], "error");
            const webhookLoggerInfoStub = sandbox.stub(webhook["logger"], "info");

            const res = await webhook.execute();

            expect(webhookFetchStrategyGetRawDataStub.calledOnce).to.be.true;
            expect(webhookLoggerInfoStub.calledOnce).to.be.false;
            expect(webhookLoggerErrorStub.calledOnce).to.be.true;
            expect(res).to.be.undefined;
        });

        it("should log error when response data is invalid", async () => {
            IntegrationEngineContainer.registerInstance<ISimpleConfig>(
                CoreToken.SimpleConfig,
                new SimpleConfig({ env: { CLOUDFLARE_ENABLED: true } })
            );
            const webhook = new CloudflareCachePurgeWebhook(["a", "b", "c"]);
            const webhookFetchStrategyGetRawDataStub = sandbox
                .stub(webhook["fetchStrategy"], "getRawData")
                .resolves({ meta: { statusCode: 200 }, data: {} } as any);
            const webhookLoggerErrorStub = sandbox.stub(webhook["logger"], "error");
            const webhookLoggerInfoStub = sandbox.stub(webhook["logger"], "info");

            const res = await webhook.execute();

            expect(webhookFetchStrategyGetRawDataStub.calledOnce).to.be.true;
            expect(webhookLoggerInfoStub.calledOnce).to.be.false;
            expect(webhookLoggerErrorStub.calledOnce).to.be.true;
            expect(res).to.be.undefined;
        });
    });
});
