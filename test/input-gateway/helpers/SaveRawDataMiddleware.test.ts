import { expect } from "chai";
import { Request, Response } from "express";
import sinon, { SinonSandbox } from "sinon";
import { container, Lifecycle } from "tsyringe";
import { pathToRegexp } from "path-to-regexp";
import contentType, { ParsedMediaType } from "content-type";
import { ErrorHandler, GeneralError } from "@golemio/errors";
import { ContainerToken } from "#ig/ioc";
import { SaveRawDataMiddleware } from "#ig/helpers";
import { ILogger } from "#helpers";
import { AbstractStorageService } from "#helpers/data-access/storage";
import { ISaveRawDataWhitelist } from "#ig/config";
import { ISimpleConfig } from "#helpers/configuration/ISimpleConfig";
import { CoreToken } from "#helpers/ioc/CoreToken";
import SimpleConfig from "#helpers/configuration/SimpleConfig";

describe("SaveRawDataMiddleware", () => {
    let sandbox: SinonSandbox;

    const loggerMock = {
        error: sinon.stub(),
    } as unknown as ILogger;

    const storageMock = {
        uploadStream: sinon.stub(),
        uploadFile: sinon.stub(),
        deleteFile: sinon.stub(),
        getFileStream: sinon.stub(),
    };

    const request = {
        path: "/test",
        method: "POST",
        headers: {
            "content-type": "text/xml",
        },
    };

    const createContainer = (whitelist: ISaveRawDataWhitelist[] = []) =>
        container
            .createChildContainer()
            .register(ContainerToken.Config, {
                useValue: { storage: { enabled: true }, saveRawDataWhitelist: whitelist },
            })
            .registerInstance<ISimpleConfig>(
                CoreToken.SimpleConfig,
                new SimpleConfig({
                    env: {
                        STORAGE_ENABLED: "true",
                    },
                })
            )
            .registerInstance<ILogger>(ContainerToken.Logger, loggerMock)
            .register<AbstractStorageService>(
                ContainerToken.StorageService,
                class DummyStorageService {
                    uploadStream = storageMock.uploadStream;
                    uploadFile = storageMock.uploadFile;
                    deleteFile = storageMock.deleteFile;
                    getFileStream = storageMock.getFileStream;
                },
                { lifecycle: Lifecycle.Singleton }
            )
            .registerSingleton(ContainerToken.SaveRawDataMiddleware, SaveRawDataMiddleware)
            .resolve<SaveRawDataMiddleware>(ContainerToken.SaveRawDataMiddleware);

    before(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        container.clearInstances();
        sinon.reset();
        sandbox.restore();
    });

    after(() => {
        sinon.restore();
    });

    it("should upload to storage (data, headers)", () => {
        storageMock.uploadStream.resolves();
        storageMock.uploadFile.resolves();
        sandbox.stub(contentType, "parse").returns({ type: "text/xml" } as ParsedMediaType);
        const saveRawDataMiddleware = createContainer([
            {
                route: "/test",
                withHeaders: true,
            },
        ]);
        const spy = sandbox.spy(saveRawDataMiddleware, "getWhitelistRegexp");
        const middleware = saveRawDataMiddleware.getMiddleware();

        middleware(request as Request, {} as Response, Function);
        expect(storageMock.uploadFile.callCount).to.equal(1);
        expect(storageMock.uploadStream.callCount).to.equal(1);
        expect(spy.getCall(0).returnValue.routesRegexp.toString()).to.equal(`/(${pathToRegexp("/test").regexp.source})/`);
        expect(spy.getCall(0).returnValue.headersRegexp.toString()).to.equal(`/(${pathToRegexp("/test").regexp.source})/`);
    });

    it("should upload to storage (data only)", () => {
        storageMock.uploadStream.resolves();
        sandbox.stub(contentType, "parse").returns({ type: "text/xml" } as ParsedMediaType);
        const saveRawDataMiddleware = createContainer([
            {
                route: "/test",
                withHeaders: false,
            },
        ]);
        const spy = sandbox.spy(saveRawDataMiddleware, "getWhitelistRegexp");
        const middleware = saveRawDataMiddleware.getMiddleware();

        middleware(request as Request, {} as Response, Function);
        expect(storageMock.uploadStream.callCount).to.equal(1);
        expect(spy.getCall(0).returnValue.routesRegexp.toString()).to.equal(`/(${pathToRegexp("/test").regexp.source})/`);
        expect(spy.getCall(0).returnValue.headersRegexp.toString()).to.equal(`/.^/`);
    });

    it("should throw golemio error (unknown content type)", () => {
        const errorHandlerStub = sandbox.stub(ErrorHandler, "handle");
        sandbox.stub(contentType, "parse").returns({ type: "text/wtf" } as ParsedMediaType);
        const saveRawDataMiddleware = createContainer([
            {
                route: "/test",
                withHeaders: false,
            },
        ]);
        const middleware = saveRawDataMiddleware.getMiddleware();

        middleware(request as Request, {} as Response, Function);
        expect(storageMock.uploadFile.callCount).to.equal(0);
        expect(errorHandlerStub.callCount).to.equal(1);
        expect(errorHandlerStub.calledWith(sinon.match.instanceOf(GeneralError))).to.equal(true);
    });

    it("should not do anything (PATCH request method)", () => {
        const errorHandlerStub = sandbox.stub(ErrorHandler, "handle");
        sandbox.stub(contentType, "parse").returns({ type: "text/xml" } as ParsedMediaType);
        const saveRawDataMiddleware = createContainer([
            {
                route: "/test",
                withHeaders: false,
            },
        ]);
        const middleware = saveRawDataMiddleware.getMiddleware();

        middleware({ ...request, method: "PATCH" } as Request, {} as Response, Function);
        expect(storageMock.uploadFile.callCount).to.equal(0);
        expect(errorHandlerStub.callCount).to.equal(0);
    });
});
