import inspector from "node:inspector";

/**
 * Utils for the Node.js inspector
 * @see https://nodejs.org/docs/latest-v18.x/api/inspector.html
 */
export class InspectorUtils {
    private static DEFAULT_HOST = "127.0.0.1";
    private static DEFAULT_PORT = 9229;

    public activateInspector(host = InspectorUtils.DEFAULT_HOST, port = InspectorUtils.DEFAULT_PORT) {
        if (!inspector) {
            return;
        }

        try {
            inspector.open(port, host);
        } catch (err) {
            if (err.code === "ERR_INSPECTOR_ALREADY_ACTIVATED" && inspector.url()) {
                console.log(`Debugger listening on ${inspector.url()}`);
                return;
            }

            throw err;
        }
    }

    public deactivateInspector() {
        if (!inspector) {
            return;
        }

        inspector.close();
    }
}
