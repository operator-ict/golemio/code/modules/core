export enum Service {
    POSTGRES = "PostgreSQL",
    REDIS = "RedisDB",
    RABBITMQ = "RabbitMQ",
}

enum Status {
    UP = "UP",
    DOWN = "DOWN",
}

export interface IServiceCheck {
    name: Service;
    check: () => Promise<boolean> | boolean;
}

interface IServiceStatus {
    name: Service;
    status: Status;
}

export interface IHealthOutput {
    health: boolean;
    services: IServiceStatus[];
}

export const getServiceHealth = async (services: IServiceCheck[]): Promise<IHealthOutput> => {
    try {
        const statuses = await Promise.all(
            services.map(async (service) => {
                return { name: service.name, status: (await service.check()) ? Status.UP : Status.DOWN };
            })
        );
        const isHealthy = !statuses.some((el) => el.status === Status.DOWN);
        return { health: isHealthy, services: statuses };
    } catch (err) {
        return { health: false, services: [] };
    }
};
