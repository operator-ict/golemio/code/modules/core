import amqplib from "amqplib";

export default class AlternateExchangeCreator {
    private static EXCHANGE_NAME_SUFFIX = "-alt";
    private static EXCHANGE_QUEUE_NAME = ".alt-routes";

    public static createAlternateExchange = async (channel: amqplib.Channel, originalExchangeName: string): Promise<void> => {
        const altExchangeName = this.getAltExchangeName(originalExchangeName);
        await channel.assertExchange(altExchangeName as string, "fanout", { durable: false });

        const q = await channel.assertQueue(altExchangeName + this.EXCHANGE_QUEUE_NAME, {
            durable: true,
            messageTtl: 3 * 24 * 60 * 60 * 1000, // 3 days in milliseconds
            arguments: { "x-queue-type": "quorum" },
        });

        channel.bindQueue(q.queue, altExchangeName, "dead");
    };

    public static getAltExchangeName = (originalExchangeName: string): string => {
        return originalExchangeName.concat(this.EXCHANGE_NAME_SUFFIX);
    };
}
