import IQueueConnector from "#helpers/connector/interfaces/IQueueConnector";
import { IDatabaseConnector } from "#helpers/data-access/postgres/IDatabaseConnector";
import { IoRedisConnector } from "#helpers/data-access/redis/IoRedisConnector";
import { CoreToken } from "#helpers/ioc/CoreToken";
import { ContainerToken, IntegrationEngineContainer } from "#ie/ioc";

/**
 * @deprecated For backwards compatibility. Use ioc container instead.
 */
const queueConnector = IntegrationEngineContainer.resolve<IQueueConnector>(ContainerToken.AmqpConnector);
/**
 * @deprecated For backwards compatibility. Use ioc container instead.
 */
const postgresConnector = IntegrationEngineContainer.resolve<IDatabaseConnector>(CoreToken.PostgresConnector);
/**
 * @deprecated For backwards compatibility. Use ioc container instead.
 */
const redisConnector = IntegrationEngineContainer.resolve<IoRedisConnector>(ContainerToken.RedisConnector);

/* ie/connectors/index.ts */
export { redisConnector as RedisConnector };
export { queueConnector as AMQPConnector };
export { postgresConnector as PostgresConnector };
