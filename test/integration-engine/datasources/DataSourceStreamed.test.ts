import chai from "chai";
import chaiAsPromised from "chai-as-promised";
import { GeneralError } from "@golemio/errors";
import { IValidator } from "@golemio/validator";
import sinon, { SinonSandbox, SinonSpy, SinonStub } from "sinon";
import { IDataTypeStrategy, IProtocolStrategy, DataSourceStreamed, DataSourceStream } from "#ie/datasources";
import { log } from "#ie/helpers";
import { waitTillStreamEnds } from "../helpers";
import { metricsService } from "#monitoring";

chai.use(chaiAsPromised);

type GetDatasource = (protocol: IProtocolStrategy, dataType: IDataTypeStrategy, validator: IValidator) => DataSourceStreamed;
type GetStream = (data: any) => DataSourceStream;

describe("DataSourceStreamed", () => {
    let sandbox: SinonSandbox;
    let getDatasource: GetDatasource;
    let getStream: GetStream;
    let genericDatasource: DataSourceStreamed;
    let protocolStub: Record<keyof IProtocolStrategy, SinonStub>;
    let dataTypeStub: Record<keyof IDataTypeStrategy, SinonStub>;
    let validatorStub: Record<keyof IValidator, SinonStub>;

    beforeEach(() => {
        getStream = (data) => {
            const dataStream = new DataSourceStream({
                objectMode: true,
                read: () => {
                    return;
                },
            });
            dataStream.push(data);
            return dataStream;
        };

        sandbox = sinon.createSandbox();

        protocolStub = {
            getData: sandbox.stub().callsFake(async () => {
                return getStream({
                    message: "test",
                });
            }),
            getDataWithMetadata: sandbox.stub().callsFake(async () => {
                return {
                    data: getStream({
                        message: "test",
                    }),
                    metadata: undefined,
                };
            }),
            getLastModified: sandbox.stub(),
            setCallerName: sandbox.stub(),
            setConnectionSettings: sandbox.stub(),
            getConnectionSettings: sandbox.stub(),
        };
        dataTypeStub = {
            setDataTypeSettings: sandbox.stub(),
            setFilter: sandbox.stub(),
            parseData: sandbox.stub().callsFake(() => Object.assign({ message: "test" })),
        };
        validatorStub = {
            Validate: sandbox.stub().callsFake(() => true),
            setLogger: sandbox.stub(),
        };

        getDatasource = (protocol, dataType, validator) => {
            return new DataSourceStreamed("TestDataSource", protocol, dataType, validator);
        };

        genericDatasource = getDatasource(protocolStub, dataTypeStub, validatorStub);
        sandbox.spy(log, "warn");
        sandbox.spy(log, "verbose");

        sandbox.stub(metricsService, "recordNumberOfRecords");
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should call protocol.setCallerName in constructor", () => {
        sandbox.assert.calledOnce(protocolStub.setCallerName);
    });

    it("should have name method", () => {
        chai.expect(genericDatasource.name).not.to.be.undefined;
    });

    it("should have getAll method", () => {
        chai.expect(genericDatasource.getAll).not.to.be.undefined;
    });

    it("should have getAllWithMetadata method", () => {
        chai.expect(genericDatasource.getAllWithMetadata).not.to.be.undefined;
    });

    it("should have getLastModified method", () => {
        chai.expect(genericDatasource.getLastModified).not.to.be.undefined;
    });

    it("should have setProtocolStrategy method", () => {
        chai.expect(genericDatasource.setProtocolStrategy).not.to.be.undefined;
    });

    it("should have setDataTypeStrategy method", () => {
        chai.expect(genericDatasource.setDataTypeStrategy).not.to.be.undefined;
    });

    it("should have setValidator method", () => {
        chai.expect(genericDatasource.setValidator).not.to.be.undefined;
    });

    it("should have proceed method", () => {
        chai.expect(genericDatasource.proceed).not.to.be.undefined;
    });

    it("should properly get all data", async () => {
        const datasource = getDatasource(protocolStub, dataTypeStub, validatorStub);
        const dataStream = await datasource.getAll();
        let outputData: any;

        dataStream.onDataListeners.push((data: any) => {
            outputData = data;
            dataStream.push(null);
        });

        await datasource.proceed();

        await waitTillStreamEnds(dataStream);

        chai.expect(outputData).to.deep.equal({
            message: "test",
        });
    });

    it("should throw error if data are not valid", async () => {
        validatorStub.Validate = sandbox.stub().callsFake(() => {
            throw new Error("oh noooooooooooooo");
        });

        const datasource = getDatasource(protocolStub, dataTypeStub, validatorStub);

        datasource.setValidator(validatorStub);

        const dataStream = await datasource.getAll();
        let error: any = null;

        dataStream.on("error", (err) => {
            error = err;
        });

        dataStream.onDataListeners.push(() => {
            dataStream.push(null);
        });

        await chai.expect(datasource.proceed()).to.be.rejectedWith(GeneralError, "Error while validating source data.");
        await waitTillStreamEnds(dataStream);
        chai.expect(error).to.be.an("error");
        chai.expect(error.info.message).to.be.equal("oh noooooooooooooo");
        chai.expect(dataStream.destroyed).to.be.true;
    });

    it("should properly get all data with metadata", async () => {
        const datasource = getDatasource(protocolStub, dataTypeStub, validatorStub);
        const res = await datasource.getAllWithMetadata();
        const dataStream = res.data;
        let outputData: any;

        dataStream.onDataListeners.push((data: any) => {
            outputData = data;
            dataStream.push(null);
        });

        await datasource.proceed();

        await waitTillStreamEnds(dataStream);

        chai.expect(outputData).to.deep.equal({
            message: "test",
        });
    });

    it("should throw error on error event on datastream", async () => {
        validatorStub.Validate = sandbox.stub().callsFake(() => {
            return true;
        });

        const datasource = getDatasource(protocolStub, dataTypeStub, validatorStub);

        datasource.setValidator(validatorStub);

        const dataStream = await datasource.getAll();
        let error: any = null;

        dataStream.on("error", (err) => {
            error = err;
        });

        dataStream.emit("error", new GeneralError("horrible error", "name", new Error("horrible error")));

        dataStream.onDataListeners.push(() => {
            dataStream.push(null);
        });

        await datasource.proceed();

        await waitTillStreamEnds(dataStream);
        chai.expect(error.info.message).to.be.equal("horrible error");
    });

    it("should verbose if data are empty array", async () => {
        const datasource = getDatasource(protocolStub, dataTypeStub, validatorStub);

        const dataStream = await datasource.getAll();
        // need to init handlers
        dataStream.push("somedata");
        dataTypeStub.parseData = sandbox.stub().callsFake(() => []);
        datasource.setDataTypeStrategy(dataTypeStub);
        dataStream.push("somedata");

        dataStream.push(null);

        await datasource.proceed();
        await waitTillStreamEnds(dataStream);
        sandbox.assert.calledOnce(log.verbose as SinonSpy);
    });

    it("should verbose if data are null", async () => {
        const datasource = getDatasource(protocolStub, dataTypeStub, validatorStub);

        const dataStream = await datasource.getAll();

        dataStream.push("somedata");
        dataTypeStub.parseData = sandbox.stub().callsFake(() => null);
        datasource.setDataTypeStrategy(dataTypeStub);
        dataStream.push("somedata");

        dataStream.push(null);

        await datasource.proceed();

        await waitTillStreamEnds(dataStream);
        sandbox.assert.calledOnce(log.verbose as SinonSpy);
    });

    it("should verbose if data are empty object", async () => {
        const datasource = getDatasource(protocolStub, dataTypeStub, validatorStub);

        const dataStream = await datasource.getAll();

        dataStream.push("somedata");
        dataTypeStub.parseData = sandbox.stub().callsFake(() => Object.assign({}));
        datasource.setDataTypeStrategy(dataTypeStub);
        dataStream.push("somedata");

        dataStream.push(null);

        await datasource.proceed();

        await waitTillStreamEnds(dataStream);
        sandbox.assert.calledOnce(log.verbose as SinonSpy);
    });

    it("should get last modified", async () => {
        const datasource = getDatasource(protocolStub, dataTypeStub, validatorStub);

        await datasource.getLastModified();
        sandbox.assert.calledOnce(protocolStub.getLastModified);
    });

    it("should set protocol strategy", async () => {
        const datasource = getDatasource(protocolStub, dataTypeStub, validatorStub);

        datasource.setProtocolStrategy(protocolStub);
        sandbox.assert.calledThrice(protocolStub.setCallerName);
        datasource.setProtocolStrategy({} as any);
        chai.expect(datasource.protocolStrategy).to.be.deep.equal({});
    });

    it("should set datatype strategy", async () => {
        const datasource = getDatasource(protocolStub, dataTypeStub, validatorStub);

        datasource.setDataTypeStrategy(null);
        chai.expect(datasource["dataTypeStrategy"]).to.be.null;
    });

    it("should set validator", async () => {
        const datasource = getDatasource(protocolStub, dataTypeStub, validatorStub);

        datasource.setValidator(null);
        chai.expect(datasource["validator"]).to.be.null;
    });
});
