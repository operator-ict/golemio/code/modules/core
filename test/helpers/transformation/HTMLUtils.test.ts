import { HTMLUtils } from "#helpers/transformation/HTMLUtils";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";

chai.use(chaiAsPromised);

describe("HTMLUtils", () => {
    const testText =
        // eslint-disable-next-line max-len
        `<DIV STYLE=\"text-align:Left;\"><DIV><DIV><P><SPAN>Výpočtová hluková mapa automobilové dopravy v Praze pro denní a noční dobu</SPAN><SPAN>- deskriptor L</SPAN><SPAN>n</SPAN></P><P><SPAN><SPAN>Hluková pásma po 5 dB.</SPAN></SPAN></P><P><SPAN><SPAN>Hodnota DB_LO udává dolní hranici intervalu, hodnota DB_HI udává horní hranici intervalu.</SPAN></SPAN></P><P><SPAN>Stav 20</SPAN><SPAN>10</SPAN></P></DIV></DIV></DIV>`;

    it("outputPlainText", () => {
        expect(HTMLUtils.outputPlainText(testText)).equal(
            // eslint-disable-next-line max-len
            "Výpočtová hluková mapa automobilové dopravy v Praze pro denní a noční dobu- deskriptor LnHluková pásma po 5 dB.Hodnota DB_LO udává dolní hranici intervalu, hodnota DB_HI udává horní hranici intervalu.Stav 2010"
        );
    });

    it("outputStructuredText", () => {
        expect(HTMLUtils.outputStructuredText(testText)).equal(
            // eslint-disable-next-line max-len
            "Výpočtová hluková mapa automobilové dopravy v Praze pro denní a noční dobu- deskriptor Ln\nHluková pásma po 5 dB.\nHodnota DB_LO udává dolní hranici intervalu, hodnota DB_HI udává horní hranici intervalu.\nStav 2010"
        );
    });
});
