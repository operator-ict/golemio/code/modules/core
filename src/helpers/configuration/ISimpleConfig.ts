export interface ISimpleConfig {
    getValue<T>(key: string, defaultValue?: T): T | string;
    getBoolean(key: string, defaultValue?: boolean): boolean;
}
