import { ILogger } from "#helpers/logger";
import { FatalError } from "@golemio/errors";
import os from "os";
import { Options, Sequelize } from "sequelize";
import { Disposable } from "src/shared/tsyringe";
import { IDatabaseConnector } from "./IDatabaseConnector";
import { IDatabaseInfo } from "./IDatabaseInfo";

export class DatabaseConnector implements IDatabaseConnector, Disposable {
    private connection?: Sequelize;

    constructor(
        private readonly databaseInfo: IDatabaseInfo,
        private readonly log: ILogger,
        private readonly sequelizeOptions?: Options
    ) {}

    public connect = async (additionalOptions?: Options): Promise<Sequelize> => {
        try {
            if (this.connection) {
                return this.connection;
            }

            if (!this.databaseInfo.connectionString) {
                throw new FatalError("The ENV variable POSTGRES_CONN cannot be undefined.", this.constructor.name);
            }

            this.connection = new Sequelize(this.databaseInfo.connectionString, this.getOptions(additionalOptions));

            if (!this.connection) {
                throw new FatalError("Connection is undefined.");
            }

            await this.connection.authenticate();
            this.log.info("Connected to PostgreSQL!");
            return this.connection;
        } catch (err) {
            throw new FatalError("Error while connecting to PostgreSQL.", this.constructor.name, err);
        }
    };

    public getConnection = (): Sequelize => {
        if (!this.connection) {
            throw new FatalError("Sequelize connection does not exist. First call connect() method.", this.constructor.name);
        }

        return this.connection;
    };

    public isConnected = async (): Promise<boolean> => {
        try {
            await this.connection!.authenticate();

            return true;
        } catch (err) {
            this.log.info(`Unable to test connection to PostgreSQL due to: "${err.message}".`);
            return false;
        }
    };

    public disconnect = async (): Promise<void> => {
        this.log.info("PostgreSQL disconnect called");

        if (this.connection) {
            await this.connection.close();
        }
    };

    public dispose = async (): Promise<void> => {
        await this.disconnect();
    };

    private getOptions(additionalOptions?: Options): Options {
        return {
            dialectOptions: {
                application_name: `${this.databaseInfo.applicationName} (${os.hostname()})`,
            },
            logging: this.log.silly,
            pool: {
                acquire: 60000,
                idle: Number(this.databaseInfo.poolIdleTimeout),
                min: Number(this.databaseInfo.poolMinConnections),
                max: Number(this.databaseInfo.poolMaxConnections),
            },
            retry: {
                match: [
                    /SequelizeConnectionError/,
                    /SequelizeConnectionRefusedError/,
                    /SequelizeHostNotFoundError/,
                    /SequelizeHostNotReachableError/,
                    /SequelizeInvalidConnectionError/,
                    /SequelizeConnectionTimedOutError/,
                    /TimeoutError/,
                ],
                max: 8,
            },
            ...this.sequelizeOptions,
            ...additionalOptions,
        };
    }
}
