import { DataSourceStream } from "#ie/datasources";
import { AccessOptions } from "basic-ftp";
import { ReadStream } from "fs";
import { ModelAttributes } from "sequelize";

export interface IHTTPSettings {
    /** (optional) Data to send with request, e.g. credentials */
    body?: any;

    /** Object with HTTP headers. */
    headers: object;

    /** (optional) Is JSON payload */
    json?: boolean;

    /** HTTP method */
    method: string;

    /** Url of the data source. */
    url: string;

    /** Tmp directory, default: /tmp */
    tmpDir?: string;

    httpsAgent?: any;
    strictSSL?: boolean;
    encoding?: any;
    rejectUnauthorized?: boolean;
    isCompressed?: boolean;
    whitelistedFiles?: string[];
    isGunZipped?: boolean;
    resolveWithFullResponse?: boolean;
    timeout?: number;
}
export enum FTPTargetType {
    FILE = "FILE",
    FOLDER = "FOLDER",
    COMPRESSED = "COMPRESSED",
}

export enum FTPReturnType {
    File,
    Stream,
}

export interface IFile {
    data: ReadStream | Buffer | Record<string, any> | string;
    filepath: string;
    name: string;
    mtime?: string;
    type?: string;
}

export const isFileObject = (obj: any): obj is IFile => {
    return obj instanceof Object && obj.data !== undefined && obj.filepath !== undefined && obj.name !== undefined;
};

export interface IFTPSettings {
    /** Filename of the data source. */
    filename: string;

    /** Url of the ftp host. */
    url: AccessOptions;

    /** Path to file. */
    path: string;

    /** Tmp directory, default: /tmp */
    tmpDir?: string;

    /** Type of downloaded file, default: FTPTargetType.FILE */
    targetType?: FTPTargetType;

    /** Return a file/stream or a readable stream/collection of readable streams */
    returnType?: FTPReturnType;

    /** Whitelist of files for FOLDER and COMPRESSED (archive) type **/
    whitelistedFiles?: string[];

    /** Encoding for file content, i.e. 'utf8' **/
    encoding?: BufferEncoding;
}

export interface ISFTPSettings {
    /** Filename base of the data source. */
    filename?: string;

    /** IP address of the sftp host. */
    host: string;

    port?: number | 8080;

    username?: string;

    password?: string;

    /** Path to file. */
    path?: string | "/";

    algorithms: any;

    encoding: string;
}

export interface IPostgresSettings {
    /** Connection string to PostgreSQL. */
    connectionString: string;

    /** Schema name. */
    schemaName: string;

    /** Table name. */
    tableName: string;

    /** Database table schema. */
    modelAttributes: ModelAttributes<any>;

    /**
     * Sequelize additional settings, e.g. indexes.
     * Default values: { freezeTableName: true, timestamps: true, underscored: true }
     */
    sequelizeAdditionalSettings?: object;

    /**
     * Sequelize findAll() options object.
     * https://sequelize.org/master/class/lib/model.js~Model.html#static-method-findAll
     */
    findOptions?: any;
}

export interface IGoogleCloudStorageSettings {
    /** Path to json key file downloaded from the Google Developers Console */
    keyFilename: string;

    /** Name of the bucket. */
    bucketName: string;

    /** Filter results to objects whose names begin with this prefix. */
    filesPrefix?: string;

    /** Custom function to filter files, e.g. by name. */
    filesFilter?: (item: any) => any;
}

export type ProtocolStrategySettings =
    | IHTTPSettings
    | IFTPSettings
    | IPostgresSettings
    | IGoogleCloudStorageSettings
    | ISFTPSettings;

export interface IProtocolStrategy {
    setCallerName(caller: string): void;

    getConnectionSettings(): ProtocolStrategySettings;

    setConnectionSettings(settings: ProtocolStrategySettings): void;

    /** Get data */
    getData(): Promise<any | DataSourceStream>;

    /** Get data including any metadata such as HTTP headers */
    getDataWithMetadata(): Promise<{ data: unknown | DataSourceStream; metadata: unknown }>;

    // getRawData(): Promise<any | DataSourceStream>;

    getLastModified(): Promise<any>;
}
