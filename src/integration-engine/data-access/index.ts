import { AbstractStorageService } from "#helpers/data-access/storage";
import { ContainerToken, IntegrationEngineContainer } from "#ie/ioc";

/**
 * @deprecated For backwards compatibility. Use ioc container instead.
 */
const storageService = IntegrationEngineContainer.resolve<AbstractStorageService>(ContainerToken.StorageService);

export { storageService };
