# Remote Debugging (CZ)

Dokument popisuje proces ladění Node.js aplikace v remote kontejneru za běhu aplikace

-   [Prerekvizity](#prerekvizity)
-   [Node.js inspect CLI](#nodejs-inspect-cli)
-   [Visual Studio Code](#visual-studio-code)
-   [DevTools](#devtools)

## Prerekvizity

-   Aplikace dokáže zachytit signály a zapnout/vypnout inspector
    -   `SIGUSR1` - zapnutí inspectoru
    -   `SIGUSR2` - vypnutí inspectoru
    -   Příklad viz [Data Proxy](https://gitlab.com/operator-ict/golemio/code/data-proxy/-/blob/development/src/index.ts)
    ```ts
    // handle user-defined process signals
    process.on("SIGUSR1", () => inspectorUtils.activateInspector(process.env.INSPECTOR_HOST));
    process.on("SIGUSR2", () => inspectorUtils.deactivateInspector());
    ```
    -   Více k miscellaneous signálům viz [libc docs](https://www.gnu.org/software/libc/manual/html_node/Miscellaneous-Signals.html#index-SIGUSR1)
-   Kontejner, ve kterém služba běží, publikuje port `9229` a nastavuje env proměnnou `INSPECTOR_HOST` na `0.0.0.0`
-   Pokud během ladění plánujete nastavit zarážky/zastavit proces, je potřeba vypnout/přenastavit liveness a readiness probes, jinak se kontejner automaticky restartuje

### Zapnutí/vypnutí inspectoru

```bash
# Activate inspector by sending the SIGUSR1 process signal
kubectl -n golemio exec -it deployments/data-proxy -- /bin/sh -c "pkill -n -USR1 node"

# Check the debugger is listening
kubectl -n golemio logs --since=5m --timestamps=true deployments/data-proxy | grep "Debugger listening on" | tail -1
# 2022-11-30T15:36:50.858265270Z Debugger listening on ws://0.0.0.0:9229/1288225f-fff6-4bf5-a1cc-b779c7df663d

# Deactivate inspector by sending the SIGUSR2 process signal
kubectl -n golemio exec -it deployments/data-proxy -- /bin/sh -c "pkill -n -USR2 node"
```

### Přesměrování portu

Po přesměrování je inspector k dispozici na `ws://127.0.0.1:9229`

```bash
kubectl -n golemio port-forward deployments/data-proxy 9229
```

## Node.js inspect CLI

_Není potřeba ručně posílat signál `SIGUSR1`, inspect jej pošle automaticky_

```bash
kubectl -n golemio exec -it deployments/data-proxy -- /bin/sh -c 'node inspect -p $(pgrep -n node)'
```

Dokumentace ladících příkazů viz [debugger command reference](https://nodejs.org/docs/latest-v20.x/api/debugger.html#command-reference)

### Příklad užití

```
debug> watch("errObject")
debug> setBreakpoint("dist/App.js", 122)
 117             });
 118             // Error handler to catch all errors sent by routers (propagated through next(err))
 119             this.express.use((err, req, res, next) => {
 120                 const warnCodes = [400, 401, 403, 404, 409, 413, 422];
 121                 const errObject = golemio_errors_1.HTTPErrorHandler.handle(err, helpers_2.log, warnCodes.includes(err.code) ? "warn" : "error");
>122                 res.setHeader("Content-Type", "application/json; charset=utf-8");
 123                 res.status(errObject.error_status || 500).send(errObject);
 124             });
 125         };
 126         this.initDataProxyServers = (definitions) => {
 127             this.dataProxyServers = [];
break in dist/App.js:122
Watchers:
  0: errObject = { error_message: 'Not Found', error_status: 404 }

 120                 const warnCodes = [400, 401, 403, 404, 409, 413, 422];
 121                 const errObject = golemio_errors_1.HTTPErrorHandler.handle(err, helpers_2.log, warnCodes.includes(err.code) ? "warn" : "error");
>122                 res.setHeader("Content-Type", "application/json; charset=utf-8");
 123                 res.status(errObject.error_status || 500).send(errObject);
 124             });
debug> cont
```

## Visual Studio Code

-   Ujistěte se, že máte
    -   lokálně stejnou verzi aplikace jako v remote kontejneru
    -   aktuální `node_modules` a produkční build se source maps ve složce `dist`
    ```bash
    npm install && npm run build
    ```
-   Zasláním signálu `SIGUSR1` zapněte remote inspector
-   Vytvořte nebo přidejte novou konfiguraci v `.vscode/launch.json`

```json
{
    "version": "0.2.0",
    "configurations": [
        {
            "type": "node",
            "request": "attach",
            "port": 9229,
            "name": "Remote attach",
            "skipFiles": ["<node_internals>/**"],
            "localRoot": "${workspaceFolder}",
            "remoteRoot": "/app",
            "sourceMaps": true
        }
    ]
}
```

-   Otevřete nový terminál a přesměrujte port `9229`
-   V záložce `Run and Debug (Ctrl+Shift+D)` vyberte `Remote attach` a klikněte na `Start debugging (F5)`

## DevTools

-   Přesměrujte port `9229`
-   V novém okně prohlížeče otevřete adresu [chrome://inspect/#devices](chrome://inspect/#devices) (nebo v případě MS Edge [edge://inspect](edge://inspect))
-   V sekci `Remote Target` se automaticky objeví nová položka, klikněte na `inspect`
    ![DevTools inspect](assets/devtools/inspect.png)

### Konzole

-   Všechny příkazy se spouští v kontextu aplikace
    ![DevTools console](assets/devtools/console.png)

### Memory snapshots

-   V záložce `Memory` vyberte `Heap snapshot`
-   Pro porovnání snapshotů přepněte pohled na `Comparison` (pod horní lištou) a napravo v dropdownu vyberte druhý snapshot
    ![DevTools memory comparison](assets/devtools/memory_comparison.png)

### CPU profiling

-   V záložce `Profiler` vyberte `Record JavaScript CPU Profile`
    ![DevTools cpu](assets/devtools/cpu.png)
