import { config } from "#ie/config";
import { PostgresConnector } from "#ie/connectors";
import RawDataStorage from "#ie/data-access/RawDataStorage";
import { IPostgresSettings, PostgresProtocolStrategy } from "#ie/datasources";
import { ContainerToken } from "#ie/ioc";
import { IntegrationEngineContainer } from "#ie/ioc/Di";
import { GeneralError } from "@golemio/errors";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import Sequelize from "sequelize";
import sinon, { SinonSandbox, SinonSpy } from "sinon";

chai.use(chaiAsPromised);

describe("PostgresProtocolStrategy", () => {
    let testSettings: IPostgresSettings;
    let strategy: PostgresProtocolStrategy;
    let sandbox: SinonSandbox;
    let rawDataStore: RawDataStorage;

    before(async () => {
        const connection = await PostgresConnector.connect();
        const [result, metadata] = await connection.query(
            "CREATE SCHEMA unittest; " +
                "CREATE TABLE unittest.test " +
                "( " +
                "id integer NOT NULL, " +
                "value character varying(255), " +
                "CONSTRAINT test_pkey PRIMARY KEY (id) " +
                "); " +
                "INSERT INTO unittest.test(id, value) VALUES " +
                "(1, 'a'), (2, 'b'); ",
            { type: Sequelize.QueryTypes.RAW }
        );
    });

    after(async () => {
        const connection = PostgresConnector.getConnection();
        // TODO doplnit batch_id a author
        const [result, metadata] = await connection.query("DROP TABLE unittest.test; " + "DROP SCHEMA unittest; ", {
            type: Sequelize.QueryTypes.RAW,
        });
    });

    beforeEach(() => {
        sandbox = sinon.createSandbox();

        testSettings = {
            connectionString: config.POSTGRES_CONN as string,
            findOptions: {
                limit: 5,
            },
            modelAttributes: {
                id: { type: Sequelize.INTEGER, primaryKey: true },
                value: Sequelize.STRING,
            },
            schemaName: "unittest",
            sequelizeAdditionalSettings: {
                timestamps: false,
                freezeTableName: true,
            },
            tableName: "test",
        };
        strategy = new PostgresProtocolStrategy(testSettings);

        sandbox.spy(strategy, "getRawData");
        rawDataStore = IntegrationEngineContainer.resolve<RawDataStorage>(ContainerToken.RawDataStorage);
        sandbox.spy(rawDataStore, "save");
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should has getData method", async () => {
        expect(strategy.getData).not.to.be.undefined;
    });

    it("should has getRawData method", async () => {
        expect(strategy.getRawData).not.to.be.undefined;
    });

    it("should has getLastModified method", async () => {
        expect(strategy.getLastModified).not.to.be.undefined;
    });

    it("should has setConnectionSettings method", async () => {
        expect(strategy.setConnectionSettings).not.to.be.undefined;
    });

    it("should have getConnectionSettings method", async () => {
        expect(strategy.getConnectionSettings).not.to.be.undefined;
    });

    it("should properly get data", async () => {
        const res = await strategy.getData();
        expect(res).to.be.deep.equal([
            { id: 1, value: "a" },
            { id: 2, value: "b" },
        ]);
        sandbox.assert.calledOnce(strategy.getRawData as SinonSpy);
        sandbox.assert.calledOnce(rawDataStore.save as SinonSpy);
    });

    it("should properly delete data", async () => {
        await strategy.deleteData();
        const res = await strategy.getData();
        expect(res).to.be.deep.equal([]);
    });

    it("should throw error if getting data failed", async () => {
        testSettings.connectionString = config.POSTGRES_CONN + "snvodsvnsvnsn";
        strategy.setConnectionSettings(testSettings);
        await expect(strategy.getData()).to.be.rejectedWith(GeneralError);
    });
});
