export interface IFeatureFlagService {
    connect(): Promise<void>;
    disconnect(): Promise<void>;
    isFeatureEnabled(flagKey: string, defaultValue?: boolean): Promise<boolean>;
}
