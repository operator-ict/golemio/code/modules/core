import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox, SinonStub } from "sinon";
import { AbortError, GeneralError } from "@golemio/errors";
import { Channel } from "amqplib";
import { config } from "#ie/config";
import { IntegrationErrorHandler } from "#ie/helpers";
import { IQueueDefinition } from "#ie/queueprocessors/abstract";
import { QueueProcessor } from "#ie/queueprocessors/QueueProcessor";

chai.use(chaiAsPromised);

describe("QueueProcessor", () => {
    let sandbox: SinonSandbox;
    let queueProcessor: QueueProcessor;
    let channel: { [T in keyof Channel]?: SinonStub };
    let definition: IQueueDefinition;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        channel = {
            ack: sandbox.stub(),
            nack: sandbox.stub(),
            cancel: sandbox.stub(),
            assertExchange: sandbox.stub(),
            assertQueue: sandbox.stub().resolves({ name: "queue" }),
            prefetch: sandbox.stub(),
            bindQueue: sandbox.stub(),
            consume: sandbox.stub().resolves({ consumerTag: "amq.ctag-test" }),
        };

        definition = {
            name: "Test",
            queuePrefix: "test-exchange.test",
            queues: [
                {
                    name: "testThis",
                    options: {
                        deadLetterExchange: "test-exchange",
                        deadLetterRoutingKey: "dead",
                    },
                    worker: class {
                        testThis = () => {};
                    } as any,
                    workerMethod: "testThis",
                },
            ],
        };

        queueProcessor = new QueueProcessor(channel as Channel, definition);
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("constructor should assign properties", () => {
        expect(queueProcessor["channel"]).to.deep.equal(channel);
        expect(queueProcessor["definition"]).to.deep.equal(definition);
        expect(queueProcessor["consumers"]).to.be.instanceOf(Set);
        expect(queueProcessor["isGracefulShutdown"]).to.eq(false);
    });

    it("registerQueues should register queues", async () => {
        const registerQueueStub = sandbox.stub(queueProcessor, "registerQueue" as any).resolves();
        await queueProcessor.registerQueues();

        expect(registerQueueStub.getCall(0).args[0]).to.eq("test-exchange.test.testThis");
        expect(registerQueueStub.getCall(0).args[1]).to.eq("*.test-exchange.test.testThis");
        expect(registerQueueStub.getCall(0).args[3]).to.deep.eq({
            deadLetterExchange: "test-exchange",
            deadLetterRoutingKey: "dead",
        });
    });

    it("registerQueues should create default processor", async () => {
        const registerQueueStub = sandbox.stub(queueProcessor, "registerQueue" as any).resolves();
        const defaultProcessorStub = sandbox.stub(queueProcessor, "defaultProcessor" as any).resolves();
        await queueProcessor.registerQueues();

        const processor = registerQueueStub.getCall(0).args[2];
        processor("testMsg");

        expect(defaultProcessorStub.getCall(0).args[0]).to.eq("testMsg");
        expect(defaultProcessorStub.getCall(0).args[1]).to.eq("testThis");
    });

    it("cancelConsumers should set graceful shutdown and cancel consumer operations", async () => {
        queueProcessor["consumers"].add("amq.ctag-test1").add("amq.ctag-test2");
        await queueProcessor.cancelConsumers();

        expect(queueProcessor["isGracefulShutdown"]).to.eq(true);
        expect(channel.cancel!.getCall(0).args[0]).to.eq("amq.ctag-test1");
        expect(channel.cancel!.getCall(1).args[0]).to.eq("amq.ctag-test2");
    });

    it("defaultProcessor should send nack (requeue) before shutting down", async () => {
        queueProcessor["isGracefulShutdown"] = true;
        await queueProcessor["defaultProcessor"]("testMsg", "testThis", () => Promise.resolve());

        expect(channel.nack!.getCall(0).args[0]).to.eq("testMsg");
        expect(channel.nack!.getCall(0).args[1]).to.eq(false);
        expect(channel.nack!.getCall(0).args[2]).to.eq(true);
    });

    it("defaultProcessor should send ack", async () => {
        const workerStub = sandbox.stub();
        await queueProcessor["defaultProcessor"]("testMsg", "testThis", workerStub);

        expect(workerStub.callCount).to.eq(1);
        expect(channel.ack!.getCall(0).args[0]).to.eq("testMsg");
    });

    it("defaultProcessor should send ack (non-critical error)", async () => {
        const workerStub = sandbox.stub().rejects();
        sandbox.stub(IntegrationErrorHandler, "handle").returns({ ack: true } as any);
        await queueProcessor["defaultProcessor"]("testMsg", "testThis", workerStub);

        expect(workerStub.callCount).to.eq(1);
        expect(channel.ack!.getCall(0).args[0]).to.eq("testMsg");
    });

    it("defaultProcessor should send nack (critical error)", async () => {
        const workerStub = sandbox.stub().rejects();
        sandbox.stub(IntegrationErrorHandler, "handle").returns({ ack: false } as any);
        await queueProcessor["defaultProcessor"]("testMsg", "testThis", workerStub);

        expect(workerStub.callCount).to.eq(1);
        expect(channel.nack!.getCall(0).args[0]).to.eq("testMsg");
    });

    it("defaultProcessor should send nack and exit (Abort error)", async () => {
        const workerStub = sandbox.stub().rejects(new AbortError("Aborting"));
        const exitStub = sandbox.stub(process, "exit");
        const spy = sandbox.spy(IntegrationErrorHandler, "handle");
        await queueProcessor["defaultProcessor"]("testMsg", "testThis", workerStub);
        expect(spy.returnValues).to.deep.eq([
            {
                error_message: "Aborting",
                ack: false,
                exit: true,
                error_queue_name: "test-exchange.test.testThis",
            },
        ]);
        expect(workerStub.callCount).to.eq(1);
        expect(exitStub.callCount).to.eq(1);
        expect(channel.nack!.getCall(0).args[0]).to.eq("testMsg");
    });

    it("registerQueue should throw error (RABBIT_EXCHANGE_NAME is undefined)", async () => {
        sandbox.stub(config, "RABBIT_EXCHANGE_NAME").value(undefined);
        await expect(
            queueProcessor["registerQueue"]("test-exchange.test.testThis", "*.test-exchange.test.testThis", sinon.stub())
        ).rejectedWith(GeneralError);
    });

    it("registerQueue should retrieve and save consumer tag", async () => {
        sandbox.stub(config, "RABBIT_EXCHANGE_NAME").value("core-test");
        await queueProcessor["registerQueue"]("test-exchange.test.testThis", "*.test-exchange.test.testThis", sinon.stub());

        expect(channel.consume!.getCall(0).args[0]).to.eq("test-exchange.test.testThis");
        expect(Array.from(queueProcessor["consumers"])).to.deep.eq(["amq.ctag-test"]);
    });
});
