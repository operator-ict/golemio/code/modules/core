import { LoggerEmitter, LoggerEventType } from "#helpers/logger";
import { DataSourceStream, IDataSource, IDataTypeStrategy, IProtocolStrategy, isFileObject } from "#ie/datasources";
import { log } from "#ie/helpers";
import { ResourceNotModified } from "#ie/helpers/ResourceNotModified";
import { ContainerToken, IntegrationEngineContainer } from "#ie/ioc";
import { GeneralError } from "@golemio/errors";
import { IValidator } from "@golemio/validator";

export class DataSource<T = any> implements IDataSource<T> {
    public dataStream!: DataSourceStream;
    public name: string;
    public protocolStrategy: IProtocolStrategy;
    protected dataTypeStrategy: IDataTypeStrategy | null;
    protected validator: IValidator | null;
    protected loggerEmitter: LoggerEmitter;

    constructor(
        name: string,
        protocolStrategy: IProtocolStrategy,
        dataTypeStrategy: IDataTypeStrategy | null = null,
        validator: IValidator | null = null
    ) {
        this.name = name;
        this.protocolStrategy = protocolStrategy;
        this.dataTypeStrategy = dataTypeStrategy;
        this.validator = validator;

        if (this.protocolStrategy && this.protocolStrategy.setCallerName) {
            this.protocolStrategy.setCallerName(this.name);
        }

        if (this.validator?.setLogger) {
            this.validator.setLogger(log);
        }

        this.loggerEmitter = IntegrationEngineContainer.resolve<LoggerEmitter>(ContainerToken.LoggerEmitter);
    }

    public setProtocolStrategy = (strategy: IProtocolStrategy): void => {
        this.protocolStrategy = strategy;
        if (this.protocolStrategy.setCallerName) {
            this.protocolStrategy.setCallerName(this.name);
        }
    };

    public setDataTypeStrategy = (strategy: IDataTypeStrategy | null): void => {
        this.dataTypeStrategy = strategy;
    };

    public setValidator = (validator: IValidator | null): void => {
        this.validator = validator;
    };

    public async getAll(): Promise<T> {
        const { data } = await this.getAllWithMetadata();
        if (data instanceof ResourceNotModified) {
            throw new GeneralError(
                "Data source returned 304 Not Modified, which is not a supported response type for this data source.",
                this.name
            );
        }
        return data;
    }

    public async getAllWithMetadata(): Promise<{ data: T | ResourceNotModified; metadata: unknown }> {
        const res = await this.getRawData();

        if (!this.validator) {
            log.warn("DataSource validator is not set.");
            return res;
        }
        if (res.data instanceof ResourceNotModified) return res;

        try {
            await this.validator.Validate(isFileObject(res.data) ? res.data.data : res.data);
        } catch (err) {
            throw new GeneralError(`Error while validating source data: ${this.formatDataForError(res.data)}`, this.name, err);
        }
        return res;
    }

    public async getLastModified(): Promise<string> {
        return this.protocolStrategy.getLastModified();
    }

    protected async getRawData(): Promise<{ data: T | ResourceNotModified; metadata: unknown }> {
        try {
            const res = await this.protocolStrategy.getDataWithMetadata();
            if (this.getResponseStatusCode(res.metadata) === 304) {
                return { data: new ResourceNotModified(), metadata: res.metadata };
            }
            const body = res.data;

            const isFile = isFileObject(body);

            const content = await this.dataTypeStrategy?.parseData(isFile ? body.data : body);
            this.notifyNumberOfRecords(content);
            return isFile
                ? { data: { ...body, data: content }, metadata: res.metadata }
                : { data: content, metadata: res.metadata };
        } catch (err) {
            throw new GeneralError("Retrieving of the source data failed.", this.name, err);
        }
    }

    private getResponseStatusCode(metadata: unknown): number | undefined {
        if (typeof metadata === "object" && metadata !== null && "statusCode" in metadata && metadata.statusCode === 304) {
            return metadata.statusCode;
        }
    }

    protected isEmpty = (content: T): boolean => {
        if (!content) {
            return true;
        } else if (content instanceof Array && content.length === 0) {
            return true;
        } else if (content instanceof Object && Object.keys(content).length === 0) {
            return true;
        }
        return false;
    };

    private notifyNumberOfRecords = (content: T) => {
        let numberOfRecords = 0;

        if (this.isEmpty(content)) {
            log.verbose(`[${this.name}] Data source returned empty data.`);
        } else {
            numberOfRecords = content instanceof Array ? content.length : 1;
        }

        this.loggerEmitter.emit(LoggerEventType.NumberOfRecords, { name: this.name, numberOfRecords });
    };

    private formatDataForError = (data: any): string => {
        let strData = "";

        try {
            strData = JSON.stringify(data);
        } catch {
            if (data.toString) {
                try {
                    strData = data.toString();
                } catch {
                    null;
                }
            }
        }

        return strData.length > 2000 ? `${strData.slice(0, 2000)}...` : strData;
    };
}
