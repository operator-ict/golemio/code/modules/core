import { ContainerToken, IntegrationEngineContainer } from "#ie/ioc";
import { GeocodeApi, IPostalAddress } from "./GeocodeApi";

/**
 * @deprecated For backwards compatibility. Use ioc container instead.
 */
const geocodeApi = IntegrationEngineContainer.resolve<GeocodeApi>(ContainerToken.GeocodeApi);

/* ie/helpers/index.ts */
export * from "./ContentTypeHelper";
export * from "./IntegrationErrorHandler";
export * from "./Logger";
export { geocodeApi as GeocodeApi, IPostalAddress };
