import { config } from "#og/config";
import { GeneralError } from "@golemio/errors";
import { NextFunction, Request, Response } from "express";
import { checkExact, query, validationResult } from "express-validator";

/**
 * Checks for errors in request parameters, using express-validator https://www.npmjs.com/package/express-validator
 * Also checks if the request contains only the expected parameters
 * @param req Express request object
 * @param res Express response object
 * @param next Express next function
 * @throws Error if request contains validation errors
 * @returns Void, calls next() function
 */
export const checkErrors = [
    checkExact(undefined, { locations: ["query"] }), // added later to avoid changing all modules
    (req: Request, res: Response, next: NextFunction) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            throw new GeneralError("Validation error", "Validation", JSON.stringify(errors.mapped()), 400);
        }
        next();
    },
];

/**
 * Sets up pagination query parameters
 */
export const pagination = [
    query("limit").optional().isInt({ min: 1 }).not().isArray(),
    query("offset").optional().isInt({ min: 0 }).not().isArray(),
    (req: Request, res: Response, next: NextFunction) => {
        req.query.limit && (req.query.limit = parseInt(req.query.limit as string, 10).toString());
        req.query.offset && (req.query.offset = parseInt(req.query.offset as string, 10).toString());
        next();
    },
];

/**
 * Performs a check if limit of requested data is not exceeded, sets limit if not present
 * @param routerName name passed to the returned Error object in case of failure
 */
export const paginationLimitMiddleware = (routerName?: string) => {
    const maxLimit = config.pagination_max_limit;
    return (req: Request, res: Response, next: NextFunction) => {
        if (req.query.limit && config.pagination_max_limit < +req.query.limit) {
            return next(
                new GeneralError("Pagination limit error", routerName || "BaseRouter", `Max limit allowed: ${maxLimit}`, 413)
            );
        }
        if (!req.query.limit && maxLimit) {
            req.query.limit = maxLimit.toString();
        }
        return next();
    };
};
