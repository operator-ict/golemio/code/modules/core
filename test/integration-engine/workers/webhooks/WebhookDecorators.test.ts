import { ISimpleConfig } from "#helpers/configuration/ISimpleConfig";
import SimpleConfig from "#helpers/configuration/SimpleConfig";
import { CoreToken } from "#helpers/ioc/CoreToken";
import { IntegrationEngineContainer } from "#ie/ioc";
import { WebhookDecorators } from "#ie/workers/helpers/WebhookDecorators";
import { CloudflareCachePurgeWebhook } from "#ie/workers/webhooks/CloudflareCachePurgeWebhook";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox } from "sinon";

chai.use(chaiAsPromised);

describe("WebhookDecorators", () => {
    let sandbox: SinonSandbox;
    let cloudflareCachePurgeWebhook: CloudflareCachePurgeWebhook;
    let cloudflareCachePurgeWebhookExecutePromise = new Promise<void>((resolve) => resolve());

    before(() => {
        sandbox = sinon.createSandbox();
        IntegrationEngineContainer.registerInstance<ISimpleConfig>(CoreToken.SimpleConfig, new SimpleConfig({ env: {} }));
        cloudflareCachePurgeWebhook = new CloudflareCachePurgeWebhook(["a", "b", "c"]);
    });

    beforeEach(() => {
        sandbox.stub(cloudflareCachePurgeWebhook, "execute").returns(cloudflareCachePurgeWebhookExecutePromise);
    });

    afterEach(() => {
        sandbox.restore();
    });

    after(() => {
        IntegrationEngineContainer.clearInstances();
        sinon.restore();
    });

    class Test {
        @WebhookDecorators.after(cloudflareCachePurgeWebhook)
        public hello(toWhom: string) {
            const hi = this.how();
            return `${hi} ${toWhom}`;
        }

        public how() {
            return "hello";
        }
    }

    // =============================================================================
    // after
    // =============================================================================
    describe("after", () => {
        it("should apply webhook to method so that the webhook is called after the original method returns", async () => {
            const test = new Test();
            const testHowSpy = sandbox.spy(test, "how");

            const res = test.hello("world");

            expect(res).to.equal("hello world");
            expect(testHowSpy.calledOnce).to.be.true;
            expect(await cloudflareCachePurgeWebhookExecutePromise).to.be.undefined;
        });
    });
});
