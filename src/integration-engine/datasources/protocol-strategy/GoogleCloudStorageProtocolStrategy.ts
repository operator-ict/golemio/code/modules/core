import { GeneralError } from "@golemio/errors";
import { File, Storage } from "@google-cloud/storage";
import { RedisModel } from "#ie/models";
import { IGoogleCloudStorageSettings } from "#ie/datasources/protocol-strategy/IProtocolStrategy";
import { ProtocolStrategy } from "#ie/datasources/protocol-strategy/ProtocolStrategy";

export class GoogleCloudStorageProtocolStrategy extends ProtocolStrategy {
    protected connectionSettings!: IGoogleCloudStorageSettings;
    private storage: Storage;

    constructor(settings: IGoogleCloudStorageSettings) {
        super(settings);
        this.storage = new Storage({ keyFilename: settings.keyFilename });
    }

    public setConnectionSettings = (settings: IGoogleCloudStorageSettings): void => {
        this.connectionSettings = settings;
        this.storage = new Storage({ keyFilename: settings.keyFilename });
    };

    public getRawData = async (): Promise<any> => {
        try {
            // Lists files in the bucket
            let [files] = await this.storage.bucket(this.connectionSettings.bucketName).getFiles({
                prefix: this.connectionSettings.filesPrefix,
            });

            // Filter files by filter function
            if (this.connectionSettings.filesFilter) {
                files = files.filter(this.connectionSettings.filesFilter);
            }

            const redisModel = new RedisModel(
                "GoogleCloudStorageProtocolStrategy" + "Model",
                {
                    isKeyConstructedFromData: false,
                    prefix: "files",
                },
                null
            );

            const result = files.map(async (file: File) => {
                const fileBuffer = await this.storage.bucket(this.connectionSettings.bucketName).file(file.name).download();

                const filepath = `${this.connectionSettings.bucketName}/${file.name}`;
                await redisModel.hset(filepath, fileBuffer[0].toString("utf16le"));
                return {
                    filepath,
                    name: file.name,
                };
            });

            return {
                data: await Promise.all(result),
            };
        } catch (err) {
            throw new GeneralError("Error while getting data from server.", this.constructor.name, err);
        }
    };

    public getLastModified = async (): Promise<string | null> => {
        throw new Error("Method not implemented.");
    };
}
