import { IncomingMessage, ServerResponse, ClientRequest } from "http";
import { Span } from "@opentelemetry/api";
import { HttpCustomAttributeFunction, HttpInstrumentationConfig } from "@opentelemetry/instrumentation-http";
import { withRecordingSpan } from "../helpers";
import { HttpExtendedAttribute } from "../extended-attributes";

const httpCustomAttributes: HttpCustomAttributeFunction = (
    span: Span,
    request: ClientRequest | IncomingMessage,
    response: IncomingMessage | ServerResponse
): void => {
    if (request instanceof ClientRequest) {
        span.setAttribute(HttpExtendedAttribute.HTTP_REQUEST_HEADERS, JSON.stringify(request.getHeaders()));
    }
    if (response instanceof IncomingMessage) {
        span.setAttribute(HttpExtendedAttribute.HTTP_RESPONSE_HEADERS, JSON.stringify(response.headers));
    }
};

const routeBlacklist = ["/live", "/ready", "health-check", "/status"];

export const httpConfig: HttpInstrumentationConfig = {
    applyCustomAttributesOnSpan: withRecordingSpan(httpCustomAttributes),
    ignoreIncomingPaths: [(url: string) => routeBlacklist.some((el) => url.endsWith(el))],
};
