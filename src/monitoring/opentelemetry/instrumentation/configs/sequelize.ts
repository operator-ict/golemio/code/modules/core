import { SequelizeInstrumentationConfig, SequelizeQueryHookParams } from "opentelemetry-instrumentation-sequelize";
import { Span } from "@opentelemetry/api";

export const sequelizeConfig: SequelizeInstrumentationConfig = {
    queryHook: (span: Span, { sql }: SequelizeQueryHookParams) => {
        span.setAttribute("db.statement", typeof sql === "string" ? sql.substring(0, 1000) : sql.query.substring(0, 1000));
    },
    ignoreOrphanedSpans: true,
    suppressInternalInstrumentation: true,
};
