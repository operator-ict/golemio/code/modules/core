import { mapErrorsToCustomType } from "#ig/helpers";
import { AbstractGolemioError, FatalError, GeneralError } from "@golemio/errors";
import { expect } from "chai";

class TestError extends Error {
    status?: Number;
    constructor(msg: string, status?: number) {
        super(msg);
        this.status = status;
    }
}

//just little adapter helper since, we don't need in this case req, res for unit testing
function testHelper(error: TestError, testDelegate: (err: any) => void) {
    mapErrorsToCustomType(error, undefined as unknown as any, undefined as unknown as any, testDelegate);
}

describe("ErrorPreprocessor - mapping client errors to AbstractGolemioError type", () => {
    it("maps error code=399 to FatalError", () => {
        testHelper(new TestError("Test Error", 399), (err: any) => {
            expect(err).to.be.instanceOf(GeneralError);
            expect(err.status).to.be.undefined;
        });
    });
    it("maps error code=400 to GeneralError", () => {
        testHelper(new TestError("Test Error", 400), (err: any) => {
            expect(err).instanceOf(GeneralError);
            expect(err.status).equals(400);
        });
    });
    it("maps error code=500 to FatalError", () => {
        testHelper(new TestError("Test Error", 503), (err: any) => {
            expect(err).to.be.instanceOf(GeneralError);
            expect(err.status).equals(503);
        });
    });
    it("does not map error code=undefined AbstractGolemioError", () => {
        testHelper(new TestError("Test Error", undefined), (err: any) => {
            expect(err).to.be.not.instanceOf(AbstractGolemioError);
        });
    });
});
