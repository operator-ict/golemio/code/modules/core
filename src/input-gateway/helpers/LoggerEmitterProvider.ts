import { ILogger } from "#helpers";
import { LoggerEmitter } from "#helpers/logger/LoggerEmitter";
import { ContainerToken } from "#ig/ioc";
import { inject, injectable } from "tsyringe";

@injectable()
export class LoggerEmitterProvider {
    protected _service: LoggerEmitter;

    constructor(@inject(ContainerToken.Logger) log: ILogger) {
        this._service = new LoggerEmitter(log);
    }

    public get service(): LoggerEmitter {
        return this._service;
    }
}
