export interface IRepository {
    schema: string;
    tableName: string;
}
