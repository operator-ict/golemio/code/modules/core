[[_TOC_]]

# Postgres migrace v modulech

## Migrace

### Prerekvizity

Odstraňte [schema-definitions](https://gitlab.com/operator-ict/golemio/code/modules/schema-definitions) a nainstalujte poslední verzi [Golemio CLI](https://gitlab.com/operator-ict/golemio/code/golemio-cli) a [db-common modulu](https://gitlab.com/operator-ict/golemio/code/modules/db-common)

```bash
npm remove @golemio/schema-definitions
npm install --save-dev --save-exact @golemio/cli@latest @golemio/db-common@latest
```

### Proměnné prostředí

Aktualizujte `.env` a `.env.template`

```
POSTGRES_CONN=postgres://oict:oict-pass@localhost/oict-test
# Proměnnou níže nastavte jen v případě, že chcete spustit sdílené migrace
POSTGRES_MIGRATIONS_DIR='{db,node_modules/@golemio/db-common/db}/migrations/**/postgresql'
```

### Konfigurace Gitlab pipeline

Upravte `.gitlab-ci.yml` a importujte vhodné konfigurace (dostupné viz [gitlab-ci-pipeline/npm](https://gitlab.com/operator-ict/devops/gitlab-ci-pipeline/-/tree/master/npm))

```m
include:
    - project: "operator-ict/devops/gitlab-ci-pipeline"
      file:
          - '/npm/golemio-module-migrations-postgres.yml'
          - '/code-quality/sonar-cloud.yml'
          - '/npm/golemio-module-cache.yml'
          - '/npm/golemio-module-tests_rabbit_postgres.yml'
          - '/npm/publish.yml'
          - '/npm/pages.yml'

stages:
    - test
    - deploy

run_tests:
    before_script:
        - export POSTGRES_CONN=postgres://$POSTGRES_USER:$POSTGRES_PASSWORD@$POSTGRES_HOST/$POSTGRES_DB
        - export RABBIT_CONN=amqp://guest:guest@rabbitmq
        - export RABBIT_EXCHANGE_NAME=oict-test
        - npm install
        - npx golemio migrate-db up --postgres
```

### Konfigurace npm a nyc

Do `.npmignore` přidejte inverzní záznam pro db složku, chceme ji publishovat na npmjs.com

```
/*
!db/
!dist/
# ...
```

Složku přidejte i do `exclude` v `.nycrc.json`, ať pro ni nesbíráme test coverage

```json
{
    "exclude": ["db", "..."]
}
```

### Vytvoření nové migrace

```bash
npx golemio migrate-db create --postgres
```

### Překopírování již existujících migrací

V případě již existujících modulů je třeba překopírovat existující migrace. Jména relevatních tabulek lze dohledat v kódu viz `src\schema-definitions\index.ts`
A následně je možné vyhledat migrace pro tyto tabulky v separátním modulu `schema-definitions`. Migrační .sql and .js soubory poté překopírujeme do `db\migrations\postgresql\sqls\` resp `db\migrations\postgresql\` v případě js souborů.

### Nastavení vlastního schématu

Editujte `db/migrations/postgresql/.config.json`

```json
{
    "schema": "custom"
}
```

### Spuštění migrací lokálně

```bash
npx golemio migrate-db up --postgres
```

Další příkazy jsou uvedeny v [README](https://gitlab.com/operator-ict/golemio/code/golemio-cli/-/blob/development/README.md#commands-summary)

### Spuštění migrací na devu

Bumpněte verzi modulu v integračním enginu. Po doběhnutí pipeline se migrace automaticky propíšou na dev databázi

### Použití vlastního schématu v Integration Engine

Ve složce `schema-definitions` nastavte u všech exportů nový atribut `pgSchema` (příklad viz [RopidVYMI](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/blob/development/src/schema-definitions/ropid-vymi/index.ts#L203))

```typescript
const forExport = {
    pgSchema: "custom",
    stuff: {
        // ...
    },
};

export { forExport as Custom };
```

U každého modulu v konstruktoru předejte schéma jako atribut pgSchema (příklad viz [RopidVYMIEventsModel](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/blob/development/src/integration-engine/ropid-vymi/models/RopidVYMIEventsModel.ts#L16))

```typescript
class CustomStuffModel extends PostgresModel implements IModel {
    constructor() {
        super(Custom.stuff.name + "Model", {
            pgSchema: Custom.pgSchema,
            // ...
        });
    }
}
```

### Použití vlastního schématu v Output Gateway

Obdobně jako u IE modelů, jen se musí schéma předat jako dodatečný parametr (příklad viz [GTFSTripsModel](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/blob/development/src/output-gateway/ropid-gtfs/models/GTFSTripsModel.ts#L10))

```typescript
export class CustomStuffModel extends SequelizeModel {
    constructor() {
        super(Custom.stuff.name + "Model", Custom.stuff.pgTableName, Custom.stuff.outputSequelizeAttributes, {
            schema: Custom.pgSchema,
        });
    }
}
```

## Testovací scripty

Testovací sql scripty slouží k naplnění tabulek pro integrační testy

Nové scripty vytvářejte v db/example (příklad [shared-bikes/db/example](https://gitlab.com/operator-ict/golemio/code/modules/shared-bikes/-/tree/development/db/example))

Pro specifikaci vlastního schématu vytvořte `db/example/.config.json` a specifikujte schéma

```json
{
    "schema": "custom"
}
```

### Proměnné prostředí

Aktualizujte `.env` a `.env.template`

```
# Scripty se snažíme udržovat v db/example, ale v globu jde zvolit i jinou lokaci
SQL_DUMP_FILES=db/example/*.sql
```

### Pretest script

V package.json přidejte/upravte pretest script, který zajistí, že se před spuštěním testů načtou testovací data do databáze

```json
{
    "scripts": {
        "pretest": "golemio import-db-data --postgres",
        "test": "..."
    }
}
```

### Resetování dat

Data se načítají vzestupně podle názvu. Vytvořte tedy např. `db/example/00_truncate_tables.sql` a tabulky promažte
s

```sql
-- __truncate_tables.sql
TRUNCATE
    "first_table",
    "second_table";
```

### Vytvoření schéma na rabín a golem prostředí

O vytvoření schéma požádejte v rámci issue Jířího Zemánka. Je potřeba zmínit jméno nového schéma a které uživatelské role pro ně mají mít přístup.

    Potřebné uživatelské role:

        integration_engine,
        output_gateway,
        read_only

## Status přesunu migrací do modulů

Viz tabulka u epicu https://gitlab.com/groups/operator-ict/golemio/-/epics/12+
