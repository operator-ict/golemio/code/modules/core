import { DateTime as luxon } from "luxon";
import { ValidationError } from "@golemio/errors";

type TimesType = "minutes" | "hours" | "days";

export interface DateTimeOptions {
    timeZone?: string;
}

export interface ISOFormatOptions {
    includeOffset?: boolean;
    includeMillis?: boolean;
}

export class DateTime {
    private date: luxon;

    constructor(date: Date | luxon, options: DateTimeOptions = {}) {
        if (date instanceof luxon) {
            this.date = date;
            return;
        }

        if (isNaN(date.valueOf())) {
            throw new ValidationError("Invalid date", "DateTime");
        }
        this.date = luxon.fromJSDate(date, { zone: options.timeZone ?? "UTC" });
    }

    public add(value: number, type: TimesType): DateTime {
        this.date = this.date.plus({ [type]: value });
        return this;
    }

    public subtract(value: number, type: TimesType): DateTime {
        this.date = this.date.minus({ [type]: value });
        return this;
    }

    public setTimeZone(timeZone: string): DateTime {
        this.date = this.date.setZone(timeZone);
        return this;
    }

    public format(pattern: string): string {
        return this.date.toFormat(pattern);
    }

    public clone(): DateTime {
        return new DateTime(this.date);
    }

    public toDate(): Date {
        return this.date.toJSDate();
    }

    public toISOString(options: ISOFormatOptions = { includeOffset: true, includeMillis: false }): string {
        if (!options.includeMillis) {
            this.date = this.date.set({ millisecond: 0 });
        }
        return this.date.toISO({
            includeOffset: options.includeOffset,
            suppressMilliseconds: !options.includeMillis,
        });
    }

    public valueOf(): number {
        return this.date.valueOf();
    }

    public toString(): string {
        return this.date.toString();
    }

    static fromISO(dateString: string, options?: DateTimeOptions): DateTime {
        if (!DateTime.hasISOTimeZone(dateString) && !options?.timeZone) {
            throw new ValidationError("Time zone is required for ISO date parse.", "DateTime");
        }
        if (DateTime.hasISOTimeZone(dateString)) {
            return new DateTime(luxon.fromISO(dateString, { setZone: true }));
        }
        return new DateTime(luxon.fromISO(dateString, { zone: options?.timeZone }));
    }

    static fromFormat(dateString: string, format: string, options?: DateTimeOptions): DateTime {
        const parsed = luxon.fromFormat(dateString, format, { zone: options?.timeZone });
        if (parsed.invalidExplanation !== null) {
            throw new ValidationError(
                `Could not parse ${dateString} from format ${format}`,
                "DateTime",
                parsed.invalidExplanation
            );
        }
        return new DateTime(parsed);
    }

    static hasISOTimeZone(isoString: string): boolean {
        return isoString.includes("+") || isoString.lastIndexOf("-") > isoString.indexOf("T") || isoString.includes("Z");
    }

    static min(...isoStrings: string[]): DateTime {
        return new DateTime(luxon.min(...isoStrings.map((el) => luxon.fromISO(el))));
    }

    static max(...isoStrings: string[]): DateTime {
        return new DateTime(luxon.max(...isoStrings.map((el) => luxon.fromISO(el))));
    }
}

export const dateTime = (date: Date, options: DateTimeOptions = {}) => {
    return new DateTime(date, options);
};
