const CoreToken = {
    SimpleConfig: Symbol(),
    Logger: Symbol(),
    PostgresConnector: Symbol(),
};

export { CoreToken };
