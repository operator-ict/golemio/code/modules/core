export enum HttpExtendedAttribute {
    HTTP_REQUEST_HEADERS = "http.request_headers",
    HTTP_RESPONSE_HEADERS = "http.response_headers",
}

export enum AmqplibExtendedAttribute {
    CONSUME_END_OPERATION = "messaging.rabbitmq.consume_end_operation",
}

export enum AwsSdkExtendedAttribute {
    AWS_REQUEST_PARAMS = "aws.request.params",
}
