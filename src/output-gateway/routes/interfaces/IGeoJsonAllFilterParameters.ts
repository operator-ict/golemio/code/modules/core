export interface IGeoJsonAllFilterParameters {
    /** Latitude to sort results by (by proximity) */
    lat?: number;
    /** Longitute to sort results by */
    lng?: number | undefined | null;
    /** Maximum range from specified latLng. Only data within this range will be returned. */
    range?: number;
    /** Limit (can be used for pagination). Evaluated last, after all filters applied. */
    limit?: number;
    /** Offset (can be used for pagination). Evaluated last, after all filters applied. */
    offset?: number;
    /** Filters out all results with older updated_at than this parameter
     * (filters not-updated data)
     */
    updatedSince?: string;
    /** Filters the data to include only these with one of the specified "district" value */
    districts?: string[];
    /** Filters the data to include only specified IDs */
    ids?: number[];
    /** Object with additional filter conditions to be added to the selection */
    additionalFilters?: object;
}
