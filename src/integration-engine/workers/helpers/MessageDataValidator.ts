import { ValidationError, validate } from "class-validator";
import { GeneralError } from "@golemio/errors";
import { log } from "#ie/helpers";
import { storageService } from "#ie/data-access";

export class MessageDataValidator {
    public static async validate(queueKey: string, data: object) {
        let validationErrors: ValidationError[] = [];
        try {
            validationErrors = await validate(data, { forbidUnknownValues: true });
        } catch (err) {
            const exception = new GeneralError(
                `[Queue ${queueKey}] Message validation failed: ${err.message}`,
                "MessageDataValidator",
                err
            );

            log.error(exception);
            storageService.uploadFile(
                JSON.stringify({ exception: exception.toObject(), data }),
                `validation-errors/${queueKey}`,
                "json"
            );

            throw exception;
        }

        if (validationErrors.length > 0) {
            const constraints = this.traverseValidationErrors(validationErrors);
            const exception = new GeneralError(
                `[Queue ${queueKey}] Message validation failed: [${constraints.slice(0, 10).join(", ")}]`,
                "MessageDataValidator"
            );

            log.error(exception);
            storageService.uploadFile(JSON.stringify({ validationErrors }), `validation-errors/${queueKey}`, "json");

            throw exception;
        }
    }

    private static traverseValidationErrors(errors: ValidationError[]): string[] {
        return errors.flatMap(({ constraints, children }) =>
            constraints ? Object.values(constraints) : children ? this.traverseValidationErrors(children) : []
        );
    }
}
