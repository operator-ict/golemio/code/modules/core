import { ProtocolStrategy } from "#ie/datasources/protocol-strategy/ProtocolStrategy";
import { GeneralError } from "@golemio/errors";
import { IHTTPFetchSettings } from "./interfaces/IHTTPFetchSettings";
import { IHTTPProtocolStrategyResult } from "./interfaces/IHTTPProtocolStrategyResult";

export class HTTPFetchProtocolStrategy<T = any> extends ProtocolStrategy {
    protected connectionSettings!: IHTTPFetchSettings;

    constructor(settings: IHTTPFetchSettings) {
        super(settings);
    }

    public async getRawData(): Promise<IHTTPProtocolStrategyResult<T> | IHTTPProtocolStrategyResult<null>> {
        let statusCode: number | undefined;
        try {
            const abortController = this.prepareAbortController();

            const result = await fetch(this.connectionSettings.url, {
                body: this.connectionSettings.body,
                headers: {
                    ...(this.connectionSettings.headers as Record<string, string>),
                },
                method: this.connectionSettings.method,
                signal: abortController?.signal,
            });

            const headers = this.getHeaders(result.headers);
            statusCode = result.status;

            if (statusCode === 304) {
                return {
                    data: null,
                    meta: {
                        headers,
                        statusCode,
                    },
                };
            }

            let data: T;
            if (this.connectionSettings.responseType === "json") {
                data = (await result.json()) as T;
            } else if (this.connectionSettings.responseType === "text") {
                data = (await result.text()) as T;
            } else if (this.connectionSettings.responseType === "blob") {
                data = (await result.blob()) as T;
            } else if (this.connectionSettings.responseType === "arrayBuffer") {
                data = (await result.arrayBuffer()) as T;
            } else {
                data = (await result.json()) as T;
            }

            return {
                data,
                meta: {
                    headers,
                    statusCode,
                },
            };
        } catch (err) {
            throw new GeneralError(
                `Error while getting data from server. Response status code: ${statusCode}`,
                this.constructor.name,
                err
            );
        }
    }

    private getHeaders = (headers: Headers): Record<string, string> => {
        const headersObject: Record<string, string> = {};

        headers.forEach((value, key) => {
            headersObject[key] = value;
        });

        return headersObject;
    };

    private prepareAbortController() {
        if (!this.connectionSettings.timeoutInSeconds) {
            return null;
        }

        const abortController = new AbortController();
        setTimeout(() => abortController.abort(), this.connectionSettings.timeoutInSeconds * 1000);

        return abortController;
    }
}
