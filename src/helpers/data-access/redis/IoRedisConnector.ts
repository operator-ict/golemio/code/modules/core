import { ILogger } from "#helpers";
import { FatalError, GeneralError } from "@golemio/errors";
import Redis, { Redis as RedisInstance } from "ioredis";
import { IRedisConnector } from "./IRedisConnector";

export class IoRedisConnector implements IRedisConnector {
    private connection?: RedisInstance;

    constructor(private readonly redisConnection: string, private readonly logger: ILogger) {}

    public connect = async (): Promise<RedisInstance> => {
        if (this.connection) {
            return this.connection;
        }

        if (!this.redisConnection) {
            throw new FatalError("Unknown redis connection string.", this.constructor.name);
        }

        try {
            this.connection = new Redis(this.redisConnection);

            this.connection
                .on("error", (err) => {
                    this.logger.warn(new GeneralError("Error emitted from redis.", this.constructor.name, err));
                })
                .on("connect", () => {
                    this.logger.info("Connected to Redis!");
                });

            return this.connection;
        } catch (err) {
            throw new FatalError("Error while connecting to Redis.", this.constructor.name, err);
        }
    };

    public getConnection = (): RedisInstance => {
        if (!this.connection) {
            throw new FatalError("Redis connection does not exists. First call connect() method.", this.constructor.name);
        }
        return this.connection;
    };

    public isConnected = (): boolean => {
        return !!this.connection && this.connection.status === "ready";
    };

    public disconnect = async (): Promise<void> => {
        this.logger.info("Redis disconnect called");
        if (this.connection) {
            await this.connection.quit();
        }
    };

    public dispose = async (): Promise<void> => {
        await this.disconnect();
    };
}
