import { Message } from "amqplib";

export interface ITask {
    queueName: string;
    queueTtl?: number;
    queuePrefix: string;
    queueType: "classic" | "quorum";
    consume(msg: Message | null): any;
}
