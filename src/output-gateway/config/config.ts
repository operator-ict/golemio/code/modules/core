import { ContainerToken, OutputGatewayContainer } from "#og/ioc";
import { OutputGatewayConfiguration } from "./OutputGatewayConfiguration";

/**
 * @deprecated For backwards compatibility. Use ioc container instead.
 */
export const config = OutputGatewayContainer.resolve<OutputGatewayConfiguration>(ContainerToken.Config);
