import { registerInstrumentations } from "@opentelemetry/instrumentation";
import { NodeTracerProvider } from "@opentelemetry/sdk-trace-node";
import { Resource } from "@opentelemetry/resources";
import { SemanticResourceAttributes } from "@opentelemetry/semantic-conventions";
import { BatchSpanProcessor, ConsoleSpanExporter, SimpleSpanProcessor } from "@opentelemetry/sdk-trace-base";
import { ExporterConfig, JaegerExporter } from "@opentelemetry/exporter-jaeger";
import { diag, DiagConsoleLogger, DiagLogLevel, trace, Span } from "@opentelemetry/api";
import { getInstrumentation } from "#monitoring/opentelemetry/instrumentation";

export enum JaegerProcessorType {
    CONSOLE = "CONSOLE",
    EXPORTER = "EXPORTER",
    BOTH = "BOTH",
}

export interface IJaegerConfig {
    jaegerEnabled: boolean;
    jaegerEndpoint: string | undefined;
    jaegerHost: string | undefined;
    jaegerPort: number | undefined;
    jaegerProcessor: keyof typeof JaegerProcessorType;
    diagLogLevel: DiagLogLevel;
    jaegerServiceName: string | undefined;
}

const getTelemetryConfig = (): IJaegerConfig => {
    return {
        jaegerEnabled: process.env.JAEGER_ENABLED === "true",
        jaegerEndpoint: process.env.JAEGER_ENDPOINT,
        jaegerHost: process.env.JAEGER_AGENT_HOST,
        jaegerPort: process.env.JAEGER_AGENT_PORT ? parseInt(process.env.JAEGER_AGENT_PORT, 10) : undefined,
        jaegerProcessor:
            process.env.JAEGER_PROCESSOR && process.env.JAEGER_PROCESSOR in JaegerProcessorType
                ? (process.env.JAEGER_PROCESSOR as JaegerProcessorType)
                : JaegerProcessorType.CONSOLE,
        diagLogLevel: process.env.TELEMETRY_LOG_LEVEL ? parseInt(process.env.TELEMETRY_LOG_LEVEL, 10) : 60,
        jaegerServiceName: process.env.APP_NAME || "golemio",
    };
};

export const initTraceProvider = () => {
    const config: IJaegerConfig = getTelemetryConfig();

    diag.setLogger(new DiagConsoleLogger(), config.diagLogLevel);

    if (!config.jaegerEnabled) {
        diag.info(`Telemetry disabled (service: ${config.jaegerServiceName})`);
        return;
    } else {
        diag.info(`Telemetry enabled (service: ${config.jaegerServiceName})`);
    }

    const provider: NodeTracerProvider = new NodeTracerProvider({
        resource: new Resource({
            [SemanticResourceAttributes.SERVICE_NAME]: process.env.APP_NAME,
            ...(process.env.NODE_ENV && { [SemanticResourceAttributes.DEPLOYMENT_ENVIRONMENT]: process.env.NODE_ENV }),
        }),
    });

    if (config.jaegerProcessor === JaegerProcessorType.CONSOLE || config.jaegerProcessor === JaegerProcessorType.BOTH) {
        provider.addSpanProcessor(new SimpleSpanProcessor(new ConsoleSpanExporter()));
    }

    if (config.jaegerProcessor === JaegerProcessorType.EXPORTER || config.jaegerProcessor === JaegerProcessorType.BOTH) {
        const options: ExporterConfig = config.jaegerEndpoint
            ? { endpoint: config.jaegerEndpoint }
            : {
                  ...(config.jaegerHost && { host: config.jaegerHost }),
                  ...(config.jaegerPort && { port: config.jaegerPort }),
              };
        provider.addSpanProcessor(new BatchSpanProcessor(new JaegerExporter(options)));
    }

    provider.register();

    registerInstrumentations({
        tracerProvider: provider,
        instrumentations: getInstrumentation(),
    });
};

export const createChildSpan = (spanName: string): undefined | Span => {
    if (!(process.env.JAEGER_ENABLED === "true")) {
        return;
    }
    const tracer = trace.getTracerProvider().getTracer("tracer");
    return tracer.startSpan(spanName);
};
