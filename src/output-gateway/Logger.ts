import { ILogger } from "#helpers/logger";
import { ContainerToken, OutputGatewayContainer } from "#og/ioc";
import { HttpLogger } from "pino-http";

/**
 * @deprecated For backwards compatibility. Use ioc container instead.
 */
const logger = OutputGatewayContainer.resolve<ILogger>(ContainerToken.Logger);
/**
 * @deprecated For backwards compatibility. Use ioc container instead.
 */
const requestLogger = OutputGatewayContainer.resolve<HttpLogger>(ContainerToken.RequestLogger);

export { logger as log, requestLogger };
