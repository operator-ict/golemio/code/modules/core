# Contributing

## Code Style

Please follow the [coding-guidelines](./CODING-GUIDELINES.md).

## Commit Messages

Commit messages has to follow the [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/).

## Branches, MRs and Releases

Please follow the [Golemio GIT Workflow](./docs/git_workflow_CZ.md).

## Testing

Please write unit tests for all new code you create. You can see how we do tests in `./test`.

## Documentation

Please always update the CHANGELOG.md (follow the [keeping_a_changelog](./docs/keeping_a_changelog_CZ.md)). For additional documentation add the markdown files into `docs/` folder. In the case of Golemio Modules always update the `docs/implementation-documentation.md` (follow the [implementation-documentation](./docs/implementation_documentation_CZ.md)).

