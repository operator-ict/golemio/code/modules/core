import { ILogger } from "#helpers/logger";
import { ContainerToken, IntegrationEngineContainer } from "#ie/ioc";
import { HttpLogger } from "pino-http";

/**
 * @deprecated For backwards compatibility. Use ioc container instead.
 */
const log = IntegrationEngineContainer.resolve<ILogger>(ContainerToken.Logger);
/**
 * @deprecated For backwards compatibility. Use ioc container instead.
 */
const requestLogger = IntegrationEngineContainer.resolve<HttpLogger>(ContainerToken.RequestLogger);

export { log, requestLogger };
