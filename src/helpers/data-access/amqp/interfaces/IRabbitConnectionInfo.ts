export interface IRabbitConnectionInfo {
    rabbitExchangeName: string;
    rabbitConnectionString: string;
    rabbitChannelMaxPrefetchCount: number;
}
