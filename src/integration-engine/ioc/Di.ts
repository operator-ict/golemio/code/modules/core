import { ConfigLoader, ILogger, createLogger, createRequestLogger } from "#helpers";
import { ISimpleConfig } from "#helpers/configuration/ISimpleConfig";
import SimpleConfig from "#helpers/configuration/SimpleConfig";
import IQueueConnector from "#helpers/connector/interfaces/IQueueConnector";
import { AmqpConnector } from "#helpers/data-access/amqp/AmqpConnector";
import { DatabaseConnector } from "#helpers/data-access/postgres/DatabaseConnector";
import { IoRedisConnector } from "#helpers/data-access/redis/IoRedisConnector";
import { TableStorageServiceFactory } from "#helpers/data-access/table-storage/TableStorageServiceFactory";
import { TableStorageProvider } from "#helpers/data-access/table-storage/providers/enums/TableStorageProviderEnum";
import { CoreToken } from "#helpers/ioc/CoreToken";
import { GlobalContainer } from "#helpers/ioc/Di";
import { IConfiguration } from "#ie/config";
import { IntegrationEngineConfiguration } from "#ie/config/IntegrationEngineConfiguration";
import RawDataStorage from "#ie/data-access/RawDataStorage";
import { StorageServiceProvider } from "#ie/data-access/StorageServiceProvider";
import { GeocodeApi } from "#ie/helpers/GeocodeApi";
import { LoggerEmitterProvider } from "#ie/helpers/LoggerEmitterProvider";
import { Lifecycle, instanceCachingFactory } from "tsyringe";
import { ContainerToken } from "./ContainerToken";

//#region Initialization
const singletonOptions = { lifecycle: Lifecycle.Singleton };
const IntegrationEngineContainer = GlobalContainer.createChildContainer();
//#endregion

//#region General
IntegrationEngineContainer.register<IConfiguration>(
    ContainerToken.Config,
    { useClass: IntegrationEngineConfiguration },
    singletonOptions
);

const config = IntegrationEngineContainer.resolve<IConfiguration>(ContainerToken.Config);
IntegrationEngineContainer.registerInstance<ISimpleConfig>(
    CoreToken.SimpleConfig,
    new SimpleConfig({ old: config, env: process.env, module: new ConfigLoader("moduleConfig").conf })
);
IntegrationEngineContainer.register<ILogger>(ContainerToken.Logger, {
    useFactory: instanceCachingFactory(() =>
        createLogger({
            projectName: "integration-engine",
            nodeEnv: config.NODE_ENV,
            logLevel: config.LOG_LEVEL,
        })
    ),
}).registerInstance(
    ContainerToken.RequestLogger,
    createRequestLogger(config.NODE_ENV, IntegrationEngineContainer.resolve(ContainerToken.Logger))
);
IntegrationEngineContainer.registerSingleton(ContainerToken.LoggerEmitterProvider, LoggerEmitterProvider).register(
    ContainerToken.LoggerEmitter,
    {
        useFactory: instanceCachingFactory((c) => c.resolve<LoggerEmitterProvider>(ContainerToken.LoggerEmitterProvider).service),
    }
);
//#endregion

//#region Data Access
IntegrationEngineContainer.register<IQueueConnector>(ContainerToken.AmqpConnector, {
    useFactory: instanceCachingFactory((c) => {
        const simpleConfig = c.resolve<ISimpleConfig>(CoreToken.SimpleConfig);
        return new AmqpConnector(
            {
                rabbitConnectionString: simpleConfig.getValue("env.RABBIT_CONN"),
                rabbitExchangeName: simpleConfig.getValue("env.RABBIT_EXCHANGE_NAME"),
                rabbitChannelMaxPrefetchCount: Number(simpleConfig.getValue("env.RABBIT_CHANNEL_MAX_PREFETCH_COUNT", 1)),
            },
            c.resolve<ILogger>(CoreToken.Logger)
        );
    }),
})
    .register(CoreToken.PostgresConnector, {
        useFactory: instanceCachingFactory((c) => {
            const simpleConfig = c.resolve<ISimpleConfig>(CoreToken.SimpleConfig);
            return new DatabaseConnector(
                {
                    connectionString: simpleConfig.getValue("env.POSTGRES_CONN"),
                    poolIdleTimeout: Number(simpleConfig.getValue("env.POSTGRES_POOL_IDLE_TIMEOUT", 10000)),
                    poolMinConnections: Number(simpleConfig.getValue("env.POSTGRES_POOL_MIN_CONNECTIONS", 0)),
                    poolMaxConnections: Number(simpleConfig.getValue("env.POSTGRES_POOL_MAX_CONNECTIONS", 10)),
                    applicationName: "Integration Engine",
                },
                c.resolve<ILogger>(CoreToken.Logger),
                {
                    define: {
                        freezeTableName: true,
                        timestamps: true, // adds createdAt and updatedAt
                        createdAt: "created_at",
                        updatedAt: "updated_at",
                        underscored: true, // automatically set field option for all attributes to snake cased name
                    },
                }
            );
        }),
    })
    .register(ContainerToken.RedisConnector, {
        useFactory: instanceCachingFactory((c) => {
            const simpleConfig = c.resolve<ISimpleConfig>(CoreToken.SimpleConfig);
            return new IoRedisConnector(simpleConfig.getValue("env.REDIS_CONN", ""), c.resolve<ILogger>(CoreToken.Logger));
        }),
    })
    .register(ContainerToken.StorageServiceProvider, StorageServiceProvider, singletonOptions)
    .register(ContainerToken.StorageService, {
        useFactory: instanceCachingFactory(
            (c) => c.resolve<StorageServiceProvider>(ContainerToken.StorageServiceProvider).service
        ),
    })
    .register(ContainerToken.TableStorageService, {
        useFactory: instanceCachingFactory((c) => {
            const config = c.resolve<ISimpleConfig>(CoreToken.SimpleConfig);
            const serviceFactory = new TableStorageServiceFactory(
                {
                    enabled: config.getBoolean("env.TABLE_STORAGE_ENABLED", false),
                    provider: {
                        [TableStorageProvider.Azure]: {
                            clientId: config.getValue("env.AZURE_TABLE_CLIENT_ID", ""),
                            tenantId: config.getValue("env.AZURE_TABLE_TENANT_ID", ""),
                            clientSecret: config.getValue("env.AZURE_TABLE_CLIENT_SECRET", ""),
                            account: config.getValue("env.AZURE_TABLE_ACCOUNT", ""),
                            entityBatchSize: Number.parseInt(config.getValue("env.AZURE_TABLE_STORAGE_ENTITY_BATCH_SIZE", "100")),
                        },
                    },
                },
                c.resolve<ILogger>(CoreToken.Logger)
            );

            return serviceFactory.getService(TableStorageProvider.Azure);
        }),
    })
    .register<GeocodeApi>(ContainerToken.GeocodeApi, { useClass: GeocodeApi }, singletonOptions)
    .registerSingleton(ContainerToken.RawDataStorage, RawDataStorage);
//#endregion

export { IntegrationEngineContainer };
