import Debug from "debug";
import pino, { DestinationStream } from "pino";
const { wrapErrorSerializer } = pino.stdSerializers;
import { AbstractGolemioError } from "@golemio/errors";

export type LoggerCustomLevels = "verbose" | "silly";
export type ILogger = pino.Logger<LoggerCustomLevels>;
export type ILogLevels = "error" | "warn" | "info" | "debug" | LoggerCustomLevels;

export interface ICreateLogger {
    projectName: string;
    nodeEnv: string;
    logLevel?: string;
}

interface IGolemioErrorLogObject {
    message: string;
    class_name?: string;
    status?: number;
    cause?: string;
    stack?: string;
    type: string;
    queue_name?: string;
    module_name?: string;
}

export const formatGolemioErrorLog = (err: AbstractGolemioError): IGolemioErrorLogObject => {
    const obj = err.toObject();
    return {
        message: obj.error_message.substring(0, 2000),
        ...(obj.error_class_name && { class_name: obj.error_class_name }),
        ...(obj.error_status && { status: obj.error_status }),
        ...(obj.error_info && { cause: obj.error_info.substring(0, 2000) }),
        ...(obj.error_stack && { stack: obj.error_stack }),
        ...(obj.error_module_name && { module_name: obj.error_module_name }),
        ...(obj.error_queue_name && { queue_name: obj.error_queue_name }),
        type: err.constructor.name,
    };
};

const errorSerializer = wrapErrorSerializer((err) => {
    if (err.raw instanceof AbstractGolemioError) {
        return formatGolemioErrorLog(err.raw);
    }
    return err;
});

export const createLogger = ({ projectName, nodeEnv, logLevel }: ICreateLogger, stream?: DestinationStream): ILogger => {
    const options = {
        level: logLevel?.toLowerCase() || "info",
        customLevels: {
            verbose: 25,
            silly: 5,
        },
        base: undefined,
        messageKey: "message",
        errorKey: "error",
        timestamp: nodeEnv === "development",
        formatters: {
            level: (label: string) => {
                return { level: label };
            },
        },
        serializers: {
            error: errorSerializer,
        },
        ...(nodeEnv === "development" && {
            transport: {
                target: "pino-pretty",
                options: {
                    colorize: true,
                    destination: 1,
                    translateTime: "SYS:HH:MM:ss.l",
                    messageKey: "message",
                },
            },
        }),
    };
    const logger = stream ? pino(options, stream) : pino(options);

    // Log all "SILLY" and "DEBUG" logs to debug module as well
    const debugLog = Debug(`golemio:${projectName}:debug`);
    const sillyLog = Debug(`golemio:${projectName}:silly`);
    const loggerDebugLog: pino.LogFn = logger.debug.bind(logger);
    const loggerSillyLog: pino.LogFn = logger.silly.bind(logger);

    logger.silly = (...args: any): void => {
        sillyLog.apply(null, args);
        loggerSillyLog.apply(null, args);
    };

    logger.debug = (...args: any): void => {
        debugLog.apply(null, args);
        loggerDebugLog.apply(null, args);
    };

    return logger;
};
