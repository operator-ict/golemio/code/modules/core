import { IBaseModel } from "./IBaseModel";
import { IPropertyResponseModel } from "#og";

export interface IGeoJsonModel extends IBaseModel {
    GetAll(options?: any): Promise<any>;
    GetOne(id: string): Promise<any>;
    GetName(): string;
    GetProperties: () => Promise<IPropertyResponseModel[]>;

    IsPrimaryIdNumber(idKey: string): Promise<boolean>;
    PrimaryIdentifierSelection(arg0: string): object;
}
