import { AzureTableStorageService } from "#helpers/data-access/table-storage/providers/AzureTableStorageService";
import { ILogger } from "#helpers/logger";
import { TableClient } from "@azure/data-tables";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import crypto from "crypto";
import sinon, { SinonSandbox } from "sinon";

chai.use(chaiAsPromised);

describe("AzureTableStorageService", () => {
    let sandbox: SinonSandbox;
    let service: AzureTableStorageService;
    let log: Partial<ILogger>;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        log = {
            error: sandbox.stub() as any,
            verbose: sandbox.stub() as any,
        };

        service = new AzureTableStorageService(
            { clientId: "client-id", tenantId: "tenant-id", account: "test", clientSecret: "secret", entityBatchSize: 2 },
            log as ILogger
        );
    });

    afterEach(() => {
        sandbox.restore();
    });

    // =============================================================================
    // createEntities
    // =============================================================================
    describe("createEntities", () => {
        it("should create entities", async () => {
            const tableClientSubmitStub = sandbox.stub().resolves();
            sandbox.stub(service, "getTableClient" as any).returns({
                submitTransaction: tableClientSubmitStub,
            });

            const entities = [
                { partitionKey: "test", rowKey: "test1" },
                { partitionKey: "test", rowKey: "test2" },
                { partitionKey: "test", rowKey: "test3" },
            ];

            await service.createEntities("test", entities);
            expect(tableClientSubmitStub.callCount).to.equal(2);
            expect(tableClientSubmitStub.getCall(0).args[0]).to.deep.equal([
                ["create", entities[0]],
                ["create", entities[1]],
            ]);
            expect(tableClientSubmitStub.getCall(1).args[0]).to.deep.equal([["create", entities[2]]]);
        });

        it("should create entities (invalid entities)", async () => {
            const clock = sandbox.useFakeTimers(new Date("2022-09-12T22:41:34.474Z").getTime());

            const tableClientSubmitStub = sandbox.stub().resolves();
            sandbox.stub(service, "getTableClient" as any).returns({
                submitTransaction: tableClientSubmitStub,
            });

            sandbox.stub(crypto, "randomBytes").returns(Buffer.from("test") as any);

            const entities = [
                { partitionKey: "test", rowKey: "test1", test: "a" },
                { partitionKey: "test", rowKey: "test2", test: "b" },
                { test: "c" },
            ];

            await service.createEntities("test", entities);
            expect(tableClientSubmitStub.callCount).to.equal(2);
            expect(tableClientSubmitStub.getCall(0).args[0]).to.deep.equal([
                ["create", entities[0]],
                ["create", entities[1]],
            ]);
            expect(tableClientSubmitStub.getCall(1).args[0]).to.deep.equal([
                ["create", { partitionKey: "2022-09-13", rowKey: "00_41_34.474_74657374_2", test: "c" }],
            ]);

            clock.restore();
        });
    });

    // =============================================================================
    // deleteEntitiesOlderThan
    // =============================================================================
    describe("deleteEntitiesOlderThan", () => {
        it("should delete entities older than timestamp specified", async () => {
            const entities = [
                { partitionKey: "test", rowKey: "test1" },
                { partitionKey: "test", rowKey: "test2" },
                { partitionKey: "test", rowKey: "test3" },
            ];

            const tableClientSubmitStub = sandbox.stub().resolves();
            const filterStub = sandbox.stub().returnsThis();

            sandbox.stub(service, "getTableClient" as any).returns({
                submitTransaction: tableClientSubmitStub,
                listEntities: filterStub,
                byPage: sandbox.stub().callsFake(async function* () {
                    yield [entities[0], entities[1]];
                    yield [entities[2]];
                }),
            });

            await service.deleteEntitiesOlderThan("test", "2022-11-29T13:15:11.000Z");

            expect(tableClientSubmitStub.callCount).to.equal(2);
            expect(filterStub.callCount).to.equal(1);
            expect(filterStub.getCall(0).args[0]).to.deep.equal({
                queryOptions: {
                    filter: "Timestamp lt datetime'2022-11-29T13:15:11.000Z'",
                    select: ["PartitionKey", "RowKey"],
                },
            });
            expect(tableClientSubmitStub.getCall(0).args[0]).to.deep.equal([
                ["delete", entities[0]],
                ["delete", entities[1]],
            ]);
            expect(tableClientSubmitStub.getCall(1).args[0]).to.deep.equal([["delete", entities[2]]]);
        });
    });

    // =============================================================================
    // getTableClient
    // =============================================================================
    describe("getTableClient", () => {
        it("should create new table client", () => {
            const tableClient = service["getTableClient"]("test");
            expect(tableClient).to.be.instanceOf(Object);
        });

        it("should return existing table client", () => {
            service["tableClientDict"].set("test", {} as any);

            const tableClient = service["getTableClient"]("test");
            expect(tableClient).to.be.instanceOf(Object);
        });
    });

    // =============================================================================
    // isEntityValid
    // =============================================================================
    describe("isEntityValid", () => {
        it("should return true", () => {
            expect(service["isEntityValid"]({ partitionKey: "test", rowKey: "test" })).to.equal(true);
        });

        it("should return false", () => {
            expect(service["isEntityValid"]({ partitionKey: "test" })).to.equal(false);
        });
    });
});
