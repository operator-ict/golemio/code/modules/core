import { DataSourceStream } from "#ie/datasources/DataSourceStream";
import DataSourceStreamManager from "#ie/datasources/DataSourceStreamManager";
import { GeneralError } from "@golemio/errors";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";

chai.use(chaiAsPromised);

describe("DataSourceStreamManager", () => {
    beforeEach(() => {});

    // =============================================================================
    // processDataStream
    // =============================================================================
    describe("processDataStream", () => {
        it("should reject (dataSourceStream rejected)", async () => {
            await expect(
                DataSourceStreamManager.processDataStream(Promise.reject(), (_) => Promise.resolve())
            ).to.be.rejectedWith(GeneralError);
        });

        it("should reject (proceed rejected)", async () => {
            const stream = {
                setDataProcessor: () => ({ proceed: () => Promise.reject() }),
            };

            await expect(
                DataSourceStreamManager.processDataStream(Promise.resolve(stream as unknown as DataSourceStream), (_) =>
                    Promise.resolve()
                )
            ).to.be.rejectedWith(GeneralError);
        });

        it("should resolve", async () => {
            const stream = {
                setDataProcessor: () => ({ proceed: () => Promise.resolve() }),
            };

            await expect(
                DataSourceStreamManager.processDataStream(Promise.resolve(stream as unknown as DataSourceStream), (_) =>
                    Promise.resolve()
                )
            ).to.be.fulfilled;
        });
    });
});
