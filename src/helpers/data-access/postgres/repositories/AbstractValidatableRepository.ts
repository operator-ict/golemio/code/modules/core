import { IValidator } from "@golemio/validator";
import { ILogger } from "@golemio/validator/dist/Logger";
import { IDatabaseConnector } from "../IDatabaseConnector";
import { AbstractBasicRepository } from "./AbstractBasicRepository";

export abstract class AbstractValidatableRepository extends AbstractBasicRepository {
    abstract validator: IValidator;

    constructor(connector: IDatabaseConnector, log: ILogger) {
        super(connector, log);
    }
}
