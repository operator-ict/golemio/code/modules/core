import { getSubProperty } from "#helpers/utils";
import { GeneralError, TransformationError } from "@golemio/errors";
import { log } from "./Logger";

export enum GeoCoordinatesType {
    Point = "Point",
    Polygon = "Polygon",
    MultiPolygon = "MultiPolygon",
    LineString = "LineString",
    MultiLineString = "MultiLineString",
}

export interface IGeoCoordinatesPoint {
    coordinates: Array<number | null>;
    type: GeoCoordinatesType.Point;
}

export interface IGeoCoordinatesPolygon {
    coordinates: number[][] | number[][][];
    type: GeoCoordinatesType.Polygon;
}

export interface IGeoCoordinatesMultiPolygon {
    coordinates: number[][][][];
    type: GeoCoordinatesType.MultiPolygon;
}

export type TGeoCoordinates = IGeoCoordinatesPoint | IGeoCoordinatesPolygon | IGeoCoordinatesMultiPolygon;

/**
 * Interface for http://geojson.org/ Feature format
 */
export interface IGeoJSONFeature {
    /**
     * Geometry property. Coordinates can be one or two-dimensional array of coordinates or single coordinate
     */
    geometry: TGeoCoordinates;
    /**
     *  Object with properties
     */
    properties: object;
    /**
     *  GeoJSON type - always "Feature" in GeoJSONFeature
     */
    type: "Feature";
}

/**
 * Interface for http://geojson.org/ Feature collection format
 */
export interface IGeoJSONFeatureCollection {
    features: IGeoJSONFeature[];
    /**
     *  GeoJSON type - always "FeatureCollection" in GeoJSONFeatureCollection
     */
    type: "FeatureCollection";
}

/**
 * Parses geo coordinates parameters in "latlng" and "range" string format
 * @param {string} latlng Latlng string
 * @param {range} range Range string
 * @returns {object} Object with lat, lng, and range numerical values
 */
export const parseCoordinates = async (
    latlng: string,
    range: string
): Promise<{
    lat: number | undefined;
    lng: number | undefined;
    range: number | undefined;
}> => {
    let lat: number | undefined;
    let lng: number | undefined;
    let ran: number | undefined;

    if (latlng) {
        const [latStr, lngStr] = latlng.split(",");
        lat = +latStr;
        lng = +lngStr;
        ran = parseInt(range, 10);
        if (isNaN(ran)) {
            ran = undefined;
        }
        if (isNaN(lat) || isNaN(lng)) {
            log.silly("Wrong input parameter lat: `" + lat + "` or lng: `" + lng + "`");
            return Promise.reject(new GeneralError("Bad request", "Geo", "Wrong input parameters (lat, lng)", 400));
        }
    }
    return { lat, lng, range: ran };
};

/**
 * Builds a GeoJSON feature from object or JSON
 * @param item Item to convert to GeoJSON Feature format
 * @param lonProperty Location of lon property
 * @param latProperty Location of lat property
 * @param removeLonLatProperties Removes lon and lat properties from GeoJSON properties. Default false.
 * @returns {IGeoJSONFeature} GeoJSON feature - object with geometry, properties, and type = "Feature"
 */
export const buildGeojsonFeature = (
    item: any,
    lonProperty: string,
    latProperty: string,
    removeLonLatProperties?: boolean
): IGeoJSONFeature => {
    const properties = item.toJSON ? item.toJSON() : item || {};
    const lon = getSubProperty<number>(lonProperty, item);
    const lat = getSubProperty<number>(latProperty, item);
    if (removeLonLatProperties) {
        delete properties[lonProperty];
        delete properties[latProperty];
    }
    return {
        geometry: {
            coordinates: [+lon, +lat],
            type: GeoCoordinatesType.Point,
        },
        properties,
        type: "Feature",
    };
};

/**
 * Builds a GeoJSON feature from object or JSON with location specified with numbers in WGS 84
 * @param item Item to convert to GeoJSON Feature format
 * @param lon Decimal number of WGS84 longitude
 * @param lat Decimal number of WGS84 latitude
 * @returns {IGeoJSONFeature} GeoJSON feature - object with geometry, properties, and type = "Feature"
 */
export const buildGeojsonFeatureLatLng = (item: any, lon: number, lat: number): IGeoJSONFeature => {
    const properties = item.toJSON ? item.toJSON() : item;
    return {
        geometry: {
            coordinates: [+lon, +lat],
            type: GeoCoordinatesType.Point,
        },
        properties,
        type: "Feature",
    };
};

/**
 * Builds a GeoJSON feature from object
 * @param geoProp Property of object containing geometry
 * @param object Item with geoProp property to convert to GeoJSON Feature format
 * @returns {IGeoJSONFeature} GeoJSON feature - object with geometry, properties, and type = "Feature"
 */
export const buildGeojsonFeatureType = <K extends string, T>(
    geoProp: K,
    { [geoProp]: location, ...rest }: T & Record<K, TGeoCoordinates>
) => {
    return {
        geometry: {
            coordinates: location.coordinates,
            type: location.type,
        },
        properties: rest,
        type: "Feature",
    };
};

/**
 * Builds a GeoJSON featureCollection from object or JSON
 * @param items Array of items to convert to GeoJSON Feature collection format.
 * Array of GeoJSONFeatures if latProperty or lonProperty is not specified
 * @param lonProperty Custom location of lon property. If not specified, assumes GeoJSONFeature structure of {items}
 * @param latProperty Custom location of lat property. If not specified, assumes GeoJSONFeature structure of {items}
 * @param removeLonLatProperties Removes lon and lat properties from GeoJSON properties. Default false.
 * @returns {IGeoJSONFeatureCollection} GeoJSON feature collection - object with features and type = "FeatureCollection"
 */
export const buildGeojsonFeatureCollection = (
    items: any,
    lonProperty?: string,
    latProperty?: string,
    removeLonLatProperties?: boolean
): IGeoJSONFeatureCollection => {
    if (!lonProperty || !latProperty) {
        log.silly(
            "Custom lat or lon property path not specified when building GeoJSON FeatureCollection," +
                " assuming GeoJSONFeature format of data."
        );
        for (let item of items) {
            if (
                !item.geometry ||
                !item.geometry.coordinates ||
                !item.geometry.type ||
                item.type !== "Feature" ||
                !item.properties
            ) {
                throw new TransformationError(
                    "Data are not in GeoJSONFeature format.",
                    "Geo",
                    "Property Lat Lon were not specified and item is not GeoJSONFeature.",
                    500
                );
            }
        }
        return {
            features: items,
            type: "FeatureCollection",
        };
    } else {
        return {
            features: items.map((item: any) => buildGeojsonFeature(item, lonProperty, latProperty, removeLonLatProperties)),
            type: "FeatureCollection",
        };
    }
};
