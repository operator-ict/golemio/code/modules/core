<div align="center">
<p>
    <a href="https://operatorict.cz">
        <img src="./.assets/oict_logo.png" alt="oict" width="100px" height="100px" />
    </a>
    <a href="https://golemio.cz">
        <img src="./.assets/golemio_logo.png" alt="golemio" width="100px" height="100px" />
    </a>
</p>

<h1>@golemio/core</h1>

<p>
    <a href="https://gitlab.com/operator-ict/golemio/code/modules/core/commits/master">
        <img src="https://gitlab.com/operator-ict/golemio/code/modules/core/badges/master/pipeline.svg" alt="pipeline">
    </a>
    <a href="https://gitlab.com/operator-ict/golemio/code/modules/core/commits/master">
        <img src="https://gitlab.com/operator-ict/golemio/code/modules/core/badges/master/coverage.svg" alt="coverage">
    </a>
    <a href="./LICENSE">
        <img src="https://img.shields.io/npm/l/@golemio/core" alt="license">
    </a>
</p>

<p>
    <a href="#installation">Installation</a> · <a href="./docs">Documentation</a> · <a href="https://operator-ict.gitlab.io/golemio/code/modules/core">TypeDoc</a>
</p>
</div>

This module is intended for use with Golemio services.

## Prerequisites

-   node.js (https://nodejs.org)
-   RabbitMQ (https://www.rabbitmq.com/)
-   Postgres (https://www.postgresql.org/)
-   Redis (https://redis.io/)
-   TypeScript (https://www.typescriptlang.org/)
-   Golemio Schema Definitions

## Installation

Install all prerequisites

Install all dependencies using command:

```bash
npm install
```

from the application's root directory.

## Compilation of typescript code

To compile typescript code into js one-time

```bash
npm run build
```

or run this, to watch all changes

```bash
npm run build-watch
```

from the application's root directory.

## Usage

In your project's `package.json` set dependency to @golemio/core

```bash
npm install --save @golemio/core
```

Then import module, e.g.

```ts
import * as core from "@golemio/core";
```

### Local development

For the local development use the [npx link](https://www.npmjs.com/package/link) to link the @golemio/core with other service e.g. `@golemio/integration-engine` or module e.g. `@golemio/pid`.

At the @golemio/core root directory:

```bash
npm run build-watch
```

At the @golemio/pid root directory:

```bash
npx link /path/to/golemio/modules/core
npm run build-watch
```

At the @golemio/integration-engine root directory:

```bash
npx link /path/to/golemio/modules/core
npx link /path/to/golemio/modules/pid
npm run dev-start
```

Now you can change code and immediately see the results.

## Tests

To run all test defined in /test directory simply run this command:

```bash
npm run test
```

from the application's root directory. All tests should pass.

## Rabbit MQ

Core module defines main exchange in rabbit mq and its name is dependent on .env setting. Also it contains definition of queue for dead messages. All other queues are defined in separate modules and loaded dynamically by `QueueProcessor.ts`.

Core module also defines alternate exchange for messages that are unroutable. Name of the exchage is created from name of the main one with suffix `-alt` and it contains 1 queue named `alt-routes`.

## Contribution guidelines

Please read `CONTRIBUTING.md`.

## Troubleshooting

Contact vyvoj@operatorict.cz
