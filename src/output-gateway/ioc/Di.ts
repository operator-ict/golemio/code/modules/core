import { createLogger, createRequestLogger, ILogger } from "#helpers";
import { ISimpleConfig } from "#helpers/configuration/ISimpleConfig";
import SimpleConfig from "#helpers/configuration/SimpleConfig";
import { DatabaseConnector } from "#helpers/data-access/postgres/DatabaseConnector";
import { IDatabaseConnector } from "#helpers/data-access/postgres/IDatabaseConnector";
import { IoRedisConnector } from "#helpers/data-access/redis/IoRedisConnector";
import { CoreToken } from "#helpers/ioc/CoreToken";
import { GlobalContainer } from "#helpers/ioc/Di";
import { CacheHeaderMiddleware } from "#og/CacheHeaderMiddleware";
import { CompressionByDefaultMiddleware } from "#og/CompressionByDefaultMiddleware";
import { OutputGatewayConfiguration } from "#og/config/OutputGatewayConfiguration";
import { instanceCachingFactory } from "tsyringe";
import { ContainerToken } from "./ContainerToken";

//#region Initialization
const OutputGatewayContainer = GlobalContainer.createChildContainer();
//#endregion

//#region General
OutputGatewayContainer.registerSingleton(ContainerToken.Config, OutputGatewayConfiguration);
const config = OutputGatewayContainer.resolve<OutputGatewayConfiguration>(ContainerToken.Config);
OutputGatewayContainer.registerInstance<ISimpleConfig>(
    CoreToken.SimpleConfig,
    new SimpleConfig({ old: config, env: process.env })
);
OutputGatewayContainer.register<ILogger>(ContainerToken.Logger, {
    useFactory: instanceCachingFactory(() =>
        createLogger({
            projectName: "output-gateway",
            nodeEnv: config.node_env,
            logLevel: config.log_level,
        })
    ),
}).registerInstance(
    ContainerToken.RequestLogger,
    createRequestLogger(config.node_env, OutputGatewayContainer.resolve<ILogger>(ContainerToken.Logger))
);
OutputGatewayContainer.registerSingleton(ContainerToken.CacheHeaderMiddleware, CacheHeaderMiddleware);
OutputGatewayContainer.registerSingleton(ContainerToken.CompressionByDefaultMiddleware, CompressionByDefaultMiddleware);
//#endregion

//#region Data Access
OutputGatewayContainer.register(CoreToken.PostgresConnector, {
    useFactory: instanceCachingFactory((c) => {
        const simpleConfig = c.resolve<ISimpleConfig>(CoreToken.SimpleConfig);
        return new DatabaseConnector(
            {
                connectionString: simpleConfig.getValue("env.POSTGRES_CONN"),
                poolIdleTimeout: Number(simpleConfig.getValue("env.POSTGRES_POOL_IDLE_TIMEOUT", 10000)),
                poolMinConnections: Number(simpleConfig.getValue("env.POSTGRES_POOL_MIN_CONNECTIONS", 0)),
                poolMaxConnections: Number(simpleConfig.getValue("env.POSTGRES_POOL_MAX_CONNECTIONS", 5)),
                applicationName: "Output Gateway",
            },
            c.resolve<ILogger>(CoreToken.Logger),
            {
                define: {
                    freezeTableName: true,
                    timestamps: false,
                },
            }
        );
    }),
}).register(ContainerToken.SequelizeConnection, {
    useFactory: instanceCachingFactory(async (c) => await c.resolve<IDatabaseConnector>(CoreToken.PostgresConnector).connect()),
});

OutputGatewayContainer.register(ContainerToken.RedisConnector, {
    useFactory: instanceCachingFactory((c) => {
        const simpleConfig = c.resolve<ISimpleConfig>(CoreToken.SimpleConfig);
        if (simpleConfig.getBoolean("env.REDIS_ENABLE", false)) {
            return new IoRedisConnector(simpleConfig.getValue("env.REDIS_CONN", ""), c.resolve<ILogger>(CoreToken.Logger));
        }
    }),
});

//#endregion

export { OutputGatewayContainer };
