import { FatalError } from "@golemio/errors/dist/errors/FatalError";
import { ISimpleConfig } from "./ISimpleConfig";

/**
 * Unsupported values: null, undefined, empty string, empty object, empty array, array with empty object
 */
export default class SimpleConfig implements ISimpleConfig {
    private configuration: Record<string, any>;

    constructor(configuration: Record<string, any>) {
        this.configuration = this.transformToLowercase(configuration);
    }

    public addConfiguration(newConfiguration: any) {
        this.configuration = {
            ...this.configuration,
            ...this.transformToLowercase(newConfiguration),
        };
    }

    public getValue<T>(key: string, defaultValue?: T): T | string {
        const value = key
            .toLowerCase()
            .split(".")
            .reduce((o, k) => (k === "*" ? Object.values(o) : (o || {})[k]), this.configuration);

        if (value !== undefined) {
            return value as T;
        } else if (defaultValue !== undefined) {
            return defaultValue;
        } else {
            throw new FatalError(`No value found for key "${key}"`);
        }
    }

    public getBoolean(key: string, defaultValue?: boolean): boolean {
        return String(this.getValue(key, defaultValue)).toLowerCase() === "true";
    }

    private transformToLowercase(configuration: any) {
        const result: Record<string, any> = {};
        for (const key in configuration) {
            result[key.toLowerCase()] =
                typeof configuration[key] === "object" ? this.transformToLowercase(configuration[key]) : configuration[key];
        }

        return result;
    }
}
