import { CoreToken } from "#helpers/ioc/CoreToken";

const ContainerToken = {
    Config: Symbol(),
    /**
     * @deprecated Use CoreToken.Logger instead.
     */
    Logger: CoreToken.Logger,
    LoggerEvents: Symbol(),
    RequestLogger: Symbol(),
    PostgresDatabase: CoreToken.PostgresConnector,
    /**
     * @deprecated Use CoreToken.PostgresConnector instead.
     */
    SequelizeReadOnlyConnection: Symbol(),
    /**
     * @deprecated Use CoreToken.PostgresConnector instead.
     */
    SequelizeConnection: Symbol(),
    RedisConnector: Symbol(),
    CacheHeaderMiddleware: Symbol(),
    CompressionByDefaultMiddleware: Symbol(),
};

export { ContainerToken };
