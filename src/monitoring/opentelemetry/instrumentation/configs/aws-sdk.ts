import { Span } from "@opentelemetry/api";
import {
    AwsSdkInstrumentationConfig,
    AwsSdkRequestCustomAttributeFunction,
    AwsSdkRequestHookInformation,
} from "@opentelemetry/instrumentation-aws-sdk";
import { withRecordingSpan } from "../helpers";
import { AwsSdkExtendedAttribute } from "../extended-attributes";

const whitelistServiceParams: Record<string, string[]> = {
    s3: ["Bucket", "Key", "ContentType", "ResponseContentType"],
};

const getWhitelistedParams = (serviceName: string, requestParams: Record<string, any>): Record<string, any> | undefined => {
    const params: string[] = whitelistServiceParams[serviceName];

    if (!params || !requestParams) return;

    return params.reduce((whiteList: Record<string, any>, paramName: string) => {
        const val = requestParams[paramName];
        if (val !== undefined) {
            whiteList[paramName] = val;
        }
        return whiteList;
    }, {});
};

const awsSdkPreRequestHook: AwsSdkRequestCustomAttributeFunction = (span: Span, requestInfo: AwsSdkRequestHookInformation) => {
    const paramsToAttach = getWhitelistedParams(requestInfo.request.serviceName, requestInfo.request.commandInput);
    if (paramsToAttach) {
        try {
            span.setAttribute(AwsSdkExtendedAttribute.AWS_REQUEST_PARAMS, JSON.stringify(paramsToAttach));
        } catch {
            // silent
        }
    }
};

export const awsSdkConfig: AwsSdkInstrumentationConfig = {
    preRequestHook: withRecordingSpan(awsSdkPreRequestHook),
    suppressInternalInstrumentation: true,
};
