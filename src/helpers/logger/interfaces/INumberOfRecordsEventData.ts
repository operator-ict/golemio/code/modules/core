import { Request } from "express";

interface INumberOfRecordsEventDataWithName {
    name: string;
    numberOfRecords: number;
}

interface INumberOfRecordsEventDataWithRequest {
    req: Request;
    numberOfRecords: number;
}

export type NumberOfRecordsEventData = INumberOfRecordsEventDataWithName | INumberOfRecordsEventDataWithRequest;
