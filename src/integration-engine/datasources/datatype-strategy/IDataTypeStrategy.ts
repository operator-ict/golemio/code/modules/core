import { ParserOptions } from "xml2js";

export interface IJSONSettings {
    /** Path to the sub-property which contains the results (separated by dot), e.g. "result.objects" */
    resultsPath: string;
}

export interface IZipSettings {
    whitelistedFiles?: string[];
    encoding?: string;
    name: string;
}

export interface ICSVSettings {
    /** fast-csv library parameters */
    fastcsvParams: object;

    /** line transformation */
    subscribe: (json: any) => any;
}

export interface IXMLSettings {
    /** Path to the sub-property which contains the results (separated by dot), e.g. "result.objects" */
    resultsPath: string;

    /** XML2JS library parameters */
    xml2jsParams: ParserOptions;
}

export interface IDataTypeStrategy {
    setDataTypeSettings(settings: IJSONSettings | ICSVSettings | IXMLSettings | IZipSettings): void;

    setFilter(filterFunction: (item: any) => any): void;

    parseData(data: any): Promise<any>;
}
