export const isEmpty = (obj: any) => [Object, Array].includes((obj || {}).constructor) && !Object.entries(obj || {}).length;

export const getSubProperty = <T>(path: string, obj: Record<string, any>): T => {
    if (path === "") {
        return obj as T;
    } else {
        return path.split(".").reduce((prev: Record<string, any> | undefined, curr) => {
            return prev ? prev[curr] : undefined;
        }, obj) as T;
    }
};
