import { createLogger, createRequestLogger, ILogger } from "#helpers";
import { ISimpleConfig } from "#helpers/configuration/ISimpleConfig";
import SimpleConfig from "#helpers/configuration/SimpleConfig";
import IQueueConnector from "#helpers/connector/interfaces/IQueueConnector";
import { CoreToken } from "#helpers/ioc/CoreToken";
import { GlobalContainer } from "#helpers/ioc/Di";
import { IConfiguration } from "#ig/config";
import { InputGatewayConfiguration } from "#ig/config/InputGatewayConfiguration";
import { AMQPConnector } from "#ig/connectors/AMQPConnector";
import { StorageServiceProvider } from "#ig/data-access/StorageServiceProvider";
import { LoggerEmitterProvider } from "#ig/helpers/LoggerEmitterProvider";
import { SaveRawDataMiddleware } from "#ig/helpers/SaveRawDataMiddleware";
import { CsvParserMiddleware } from "#ig/middleware/CsvParserMiddleware";
import { instanceCachingFactory, Lifecycle } from "tsyringe";
import { ContainerToken } from "./ContainerToken";

//#region Initialization
const singletonOptions = { lifecycle: Lifecycle.Singleton };
const InputGatewayContainer = GlobalContainer.createChildContainer();
//#endregion

//#region General
InputGatewayContainer.register<IConfiguration>(ContainerToken.Config, { useClass: InputGatewayConfiguration }, singletonOptions);

const config = InputGatewayContainer.resolve<IConfiguration>(ContainerToken.Config);
InputGatewayContainer.registerInstance<ISimpleConfig>(
    CoreToken.SimpleConfig,
    new SimpleConfig({ old: config, env: process.env })
);
InputGatewayContainer.registerInstance<ILogger>(
    ContainerToken.Logger,
    createLogger({
        projectName: "input-gateway",
        nodeEnv: config.node_env,
        logLevel: config.log_level,
    })
);
InputGatewayContainer.registerSingleton(ContainerToken.LoggerEmitterProvider, LoggerEmitterProvider).register(
    ContainerToken.LoggerEmitter,
    {
        useFactory: instanceCachingFactory((c) => c.resolve<LoggerEmitterProvider>(ContainerToken.LoggerEmitterProvider).service),
    }
);
InputGatewayContainer.registerInstance(
    ContainerToken.RequestLogger,
    createRequestLogger(config.node_env, InputGatewayContainer.resolve(ContainerToken.Logger))
);
//#endregion

//#region Data Access
InputGatewayContainer.register<IQueueConnector>(ContainerToken.AmqpConnector, { useClass: AMQPConnector }, singletonOptions)
    .register(ContainerToken.StorageServiceProvider, StorageServiceProvider, singletonOptions)
    .register(ContainerToken.StorageService, {
        useFactory: instanceCachingFactory(
            (c) => c.resolve<StorageServiceProvider>(ContainerToken.StorageServiceProvider).service
        ),
    });
//#endregion

//#region Middleware
InputGatewayContainer.registerSingleton(ContainerToken.SaveRawDataMiddleware, SaveRawDataMiddleware);
InputGatewayContainer.registerSingleton(ContainerToken.CsvParserMiddleware, CsvParserMiddleware);
//#endregion

export { InputGatewayContainer };
