import { GeneralError } from "@golemio/errors";
import { RequestHandler } from "express";
import { ParamsDictionary } from "express-serve-static-core";
import { parseStream } from "fast-csv";
import { Readable } from "node:stream";
import { injectable } from "tsyringe";
import { ICsvParserOptions } from "./interfaces/ICsvParserOptions";

interface IRequestHandlerWithCsvParsedBody<T> extends RequestHandler<ParamsDictionary, any, T[]> {}

@injectable()
export class CsvParserMiddleware {
    public getMiddleware<T>(options: ICsvParserOptions): IRequestHandlerWithCsvParsedBody<T> {
        return (req, _res, next) => {
            if (!Buffer.isBuffer(req.body)) {
                next(new GeneralError("CSV data is not a buffer", this.constructor.name, undefined, 500));
                return;
            }

            let csvReadStream: Readable;

            try {
                csvReadStream = Readable.from(req.body);
            } catch (error) {
                next(new GeneralError("Error while creating readable stream from CSV data", this.constructor.name, error, 500));
                return;
            }

            const csvParserStream = parseStream(csvReadStream, {
                delimiter: options.delimiter,
                headers: options.shouldIncludeHeaders,
                trim: options.shouldTrimValues,
            });

            let data: T[] = [];

            csvParserStream.on("data", (row) => {
                data.push(row);
            });

            csvParserStream.on("end", () => {
                req.body = data;
                next();
            });

            csvParserStream.on("error", (error) => {
                next(new GeneralError("Error while parsing CSV data", this.constructor.name, error, 422));
            });
        };
    }
}
