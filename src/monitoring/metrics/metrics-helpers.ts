export enum MetricsType {
    COUNTER = "COUNTER",
    HISTOGRAM = "HISTOGRAM",
    SUMMARY = "SUMMARY",
    GAUGE = "GAUGE",
}

export enum StatusCodeTypes {
    INFO = "INFO",
    SUCCESS = "SUCCESS",
    REDIRECT = "REDIRECT",
    ERROR = "ERROR",
}

export const getStatusCodeType = (statusCode: number) => {
    if (statusCode < 200) return StatusCodeTypes.INFO;
    if (statusCode < 300) return StatusCodeTypes.SUCCESS;
    if (statusCode < 400) return StatusCodeTypes.REDIRECT;
    return StatusCodeTypes.ERROR;
};
