import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonFakeTimers } from "sinon";
import express from "express";
import prometheus from "prom-client";
import { IMetricsClientConfig } from "#monitoring";
import { apiMetricsMiddleware } from "#monitoring/metrics/metrics-middleware";
import { ILogger } from "#helpers";
import supertest from "supertest";
import fs from "fs";

chai.use(chaiAsPromised);

const testApp = (config: IMetricsClientConfig, logger: ILogger) => {
    const app = express();
    app.use(apiMetricsMiddleware(prometheus.register, config, logger));
    app.post("/success", (req, res) => {
        res.setHeader("Content-Type", "application/json; charset=utf-8");
        res.status(200).json({ response: "success" });
    });
    app.post("/error", (req, res) => {
        res.setHeader("Content-Type", "application/json; charset=utf-8");
        res.status(500).json({ response: "error" });
    });
    return supertest(app);
};

const metricsOutputSuccess = fs.readFileSync(__dirname + "/data/metrics-response-success.txt").toString();
const metricsOutputError = fs.readFileSync(__dirname + "/data/metrics-response-error.txt").toString();

describe("Metrics Middleware", () => {
    let logger: ILogger;
    let clock: SinonFakeTimers;

    beforeEach(() => {
        clock = sinon.useFakeTimers();
        logger = {
            warn: sinon.stub(),
        } as any;
    });

    afterEach(async () => {
        await prometheus.register.clear();
        clock.restore();
    });

    it("should not register api metrics if metrics are disabled", async () => {
        const app = testApp({ metrics: { enabled: false } } as IMetricsClientConfig, logger);
        const response = await app.post("/success");

        const registeredMetrics = await prometheus.register.getMetricsAsArray();
        expect(response.status).to.eq(200);
        expect(response.body).to.eql({ response: "success" });
        expect(registeredMetrics.length).to.eq(0);
    });

    it("should register api metrics and record success if metrics are enabled", async () => {
        const app = testApp({ metrics: { enabled: true, prefix: "" } } as IMetricsClientConfig, logger);
        const response = await app.post("/success");

        const registeredMetrics = await prometheus.register.getMetricsAsArray();
        expect(response.status).to.eq(200);
        expect(response.body).to.eql({ response: "success" });
        expect(registeredMetrics.length).to.eq(8);
        expect(await prometheus.register.metrics()).to.eql(metricsOutputSuccess);
    });

    it("should register api metrics and record error if metrics are enabled", async () => {
        const app = testApp({ metrics: { enabled: true, prefix: "" } } as IMetricsClientConfig, logger);
        const response = await app.post("/error");

        const registeredMetrics = await prometheus.register.getMetricsAsArray();
        expect(response.status).to.eq(500);
        expect(response.body).to.eql({ response: "error" });
        expect(registeredMetrics.length).to.eq(8);
        expect(await prometheus.register.metrics()).to.eql(metricsOutputError);
    });
});
