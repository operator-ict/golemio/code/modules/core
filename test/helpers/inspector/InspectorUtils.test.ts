import sinon, { SinonSandbox, SinonStub } from "sinon";
import { expect } from "chai";
import inspector from "node:inspector";
import { InspectorUtils } from "#helpers/inspector/InspectorUtils";

describe("InspectorUtils", () => {
    let sandbox: SinonSandbox;
    let activateStub: SinonStub;
    let deactivateStub: SinonStub;
    const inspectorUtils = new InspectorUtils();

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        deactivateStub = sandbox.stub(inspector, "close");
    });

    afterEach(() => {
        sandbox.restore();
    });

    // =============================================================================
    // activateInspector
    // =============================================================================
    describe("activateInspector", () => {
        it("should activate inspector", async () => {
            activateStub = sandbox.stub(inspector, "open");

            inspectorUtils.activateInspector("5.5.5.5", 4242);
            expect(activateStub.getCall(0).args).to.deep.equal([4242, "5.5.5.5"]);
        });

        it("should log debugger url (inspector is already activated)", () => {
            const logStub = sandbox.stub(console, "log");
            activateStub = sandbox.stub(inspector, "open").throws(
                (() => {
                    const error = new Error() as Error & { code?: any };
                    error.code = "ERR_INSPECTOR_ALREADY_ACTIVATED";
                    return error;
                })()
            );
            sandbox.stub(inspector, "url").returns("ws://5.5.5.5:4242/89a293a2-2e19-4286-881e-9faa2d9806d9");

            inspectorUtils.activateInspector("5.5.5.5", 4242);
            expect(activateStub.getCall(0).args).to.deep.equal([4242, "5.5.5.5"]);
            expect(logStub.getCall(0)?.args).to.deep.equal([
                "Debugger listening on ws://5.5.5.5:4242/89a293a2-2e19-4286-881e-9faa2d9806d9",
            ]);
        });

        it("should throw (unknown error)", () => {
            const logStub = sandbox.stub(console, "log");
            activateStub = sandbox.stub(inspector, "open").throws(
                (() => {
                    const error = new Error() as Error & { code?: any };
                    error.code = "ERR_UNKNOWN";
                    return error;
                })()
            );
            sandbox.stub(inspector, "url").returns("ws://5.5.5.5:4242/89a293a2-2e19-4286-881e-9faa2d9806d9");

            expect(inspectorUtils.activateInspector.bind(undefined, "5.5.5.5", 4242)).to.throw();
            expect(logStub.callCount).to.equal(0);
        });
    });

    // =============================================================================
    // deactivateInspector
    // =============================================================================
    describe("deactivateInspector", () => {
        it("should dectivate inspector", async () => {
            inspectorUtils.deactivateInspector();
            expect(deactivateStub.callCount).to.equal(1);
        });
    });
});
