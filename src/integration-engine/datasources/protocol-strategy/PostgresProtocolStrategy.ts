import { ISimpleConfig } from "#helpers/configuration/ISimpleConfig";
import { DatabaseConnector } from "#helpers/data-access/postgres/DatabaseConnector";
import { IDatabaseInfo } from "#helpers/data-access/postgres/IDatabaseInfo";
import { CoreToken } from "#helpers/ioc/CoreToken";
import { ILogger } from "#helpers/logger/LoggerProvider";
import { IPostgresSettings } from "#ie/datasources/protocol-strategy/IProtocolStrategy";
import { ProtocolStrategy } from "#ie/datasources/protocol-strategy/ProtocolStrategy";
import { IntegrationEngineContainer } from "#ie/ioc";
import { GeneralError } from "@golemio/errors";

export class PostgresProtocolStrategy extends ProtocolStrategy {
    protected connectionSettings!: IPostgresSettings;

    constructor(settings: IPostgresSettings) {
        super(settings);
    }

    public setConnectionSettings = (settings: IPostgresSettings): void => {
        this.connectionSettings = settings;
    };

    public getLastModified = async (): Promise<any> => {
        throw new Error("Method not implemented.");
    };

    public deleteData = async (): Promise<void> => {
        try {
            const connection = await this.getConnection();

            const model = connection.define(this.connectionSettings.tableName, this.connectionSettings.modelAttributes, {
                ...(this.connectionSettings.sequelizeAdditionalSettings
                    ? this.connectionSettings.sequelizeAdditionalSettings
                    : { freezeTableName: true, timestamps: true, underscored: true }), // default values
                ...(this.connectionSettings.schemaName ? { schema: this.connectionSettings.schemaName } : {}),
            });

            await model.destroy({
                cascade: false,
                truncate: true,
            });

            // Close all connections used by this sequelize instance,
            // and free all references so the instance can be garbage collected.
            await connection.close();
        } catch (err) {
            throw new GeneralError("Error while deleting data from server. " + err.message, this.constructor.name, err);
        }
    };

    public getRawData = async (): Promise<any> => {
        try {
            const connection = await this.getConnection();

            const model = connection.define(this.connectionSettings.tableName, this.connectionSettings.modelAttributes, {
                ...(this.connectionSettings.sequelizeAdditionalSettings
                    ? this.connectionSettings.sequelizeAdditionalSettings
                    : { freezeTableName: true, timestamps: true, underscored: true }), // default values
                ...(this.connectionSettings.schemaName ? { schema: this.connectionSettings.schemaName } : {}),
            });

            // getting data and returning it as array of obejcts
            const results = (await model.findAll(this.connectionSettings.findOptions)).map((r: any) => r.dataValues);

            // Close all connections used by this sequelize instance,
            // and free all references so the instance can be garbage collected.
            await connection.close();

            return {
                data: results,
            };
        } catch (err) {
            throw new GeneralError("Error while getting data from server. " + err.message, this.constructor.name, err);
        }
    };

    protected async getConnection() {
        const simpleConfig = IntegrationEngineContainer.resolve<ISimpleConfig>(CoreToken.SimpleConfig);
        const logger = IntegrationEngineContainer.resolve<ILogger>(CoreToken.Logger);

        const connInfo: IDatabaseInfo = {
            connectionString: this.connectionSettings.connectionString,
            poolIdleTimeout: +simpleConfig.getValue("env.POSTGRES_POOL_IDLE_TIMEOUT", 10000),
            poolMinConnections: +simpleConfig.getValue("env.POSTGRES_POOL_MIN_CONNECTIONS", 0),
            poolMaxConnections: +simpleConfig.getValue("env.POSTGRES_POOL_MAX_CONNECTIONS", 2),
            applicationName: "Integration Engine",
        };
        const connector = new DatabaseConnector(connInfo, logger);
        const connection = await connector.connect();

        return connection;
    }
}
