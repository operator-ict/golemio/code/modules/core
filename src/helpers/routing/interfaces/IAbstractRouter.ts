import { Router } from "express";

export interface IAbstractRouter {
    getRouter: () => Router;
    getPath: () => string;
}
