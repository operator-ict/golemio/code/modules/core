import { dateTime, ILogger } from "#helpers";
import { ISimpleConfig } from "#helpers/configuration/ISimpleConfig";
import { AbstractStorageService } from "#helpers/data-access/storage";
import { CoreToken } from "#helpers/ioc/CoreToken";
import { IConfiguration } from "#ie";
import { contentTypeToExtension } from "#ie/helpers/ContentTypeHelper";
import { ContainerToken } from "#ie/ioc";
import contentType from "content-type";
import { inject, injectable } from "tsyringe";

@injectable()
export default class RawDataStorage {
    private readonly storageEnabled: boolean;
    private readonly rawDataWhitelist: Record<string, any>;

    constructor(
        @inject(ContainerToken.Config) config: IConfiguration,
        @inject(ContainerToken.StorageService) private storageService: AbstractStorageService,
        @inject(CoreToken.SimpleConfig) simpleConfig: ISimpleConfig,
        @inject(CoreToken.Logger) private readonly log: ILogger
    ) {
        this.storageEnabled = simpleConfig.getValue("env.STORAGE_ENABLED", "false") === "true";
        this.rawDataWhitelist = config.saveRawDataWhitelist;
    }

    public save = async (data: any, meta: any, name = "Unknown") => {
        if (!this.storageEnabled || !this.rawDataWhitelist[name]) {
            this.log.verbose(`Raw data storage upload not enabled for \`${name}\` datasource.`);
            return;
        }

        const now = dateTime(new Date()).setTimeZone("Europe/Prague");
        let ext = null;
        let headers = null;

        if (meta.headers) {
            try {
                headers = JSON.stringify(meta.headers, null, 2);
            } catch {
                null;
            }
            try {
                const parsedMediaType = contentType.parse(meta.headers["content-type"]);
                if (parsedMediaType?.type) {
                    ext = contentTypeToExtension[parsedMediaType.type];
                }
            } catch {
                null;
            }
        }

        const fileName = `${now.format("y-LL-dd")}/${now.format("HH_mm_ss.SSS")}`;

        // Save data
        await this.storageService.uploadFile(data, name, ext ?? "", fileName);

        if (this.rawDataWhitelist[name].saveHeaders && headers && meta.statusCode && meta.statusCode !== 200) {
            // Save headers
            await this.storageService.uploadFile(headers, name, "json", `${fileName}_headers`);
        }
    };
}
