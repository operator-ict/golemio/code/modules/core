import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { FatalError, GeneralError, RecoverableError } from "@golemio/errors";
import sinon, { SinonSandbox, SinonSpy } from "sinon";
import { IntegrationErrorHandler } from "#ie/helpers";

chai.use(chaiAsPromised);

describe("IntegrationErrorHandler", () => {
    let sandbox: SinonSandbox;
    let exitStub: SinonSpy;
    let tmpNodeEnv: string | undefined;

    before(() => {
        tmpNodeEnv = process.env.NODE_ENV;
        process.env.NODE_ENV = "test";
    });

    after(() => {
        process.env.NODE_ENV = tmpNodeEnv;
    });

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        exitStub = sandbox.stub(process, "exit");
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should handle unexpected error", async () => {
        IntegrationErrorHandler.handle(new Error("Test"));
        sinon.assert.called(exitStub);
    });

    it("should handle expected error", async () => {
        IntegrationErrorHandler.handle(new GeneralError("Test", "TestError", new Error("err").toString()));
        sinon.assert.notCalled(exitStub);
    });

    it("should handle fatal error and exit with error code 1", async () => {
        IntegrationErrorHandler.handle(new FatalError("Test", "test"));
        sinon.assert.called(exitStub);
        sinon.assert.calledWith(exitStub, 1);
    });

    it("should handle expected warning", async () => {
        const errObject = IntegrationErrorHandler.handle(new RecoverableError("Test", "test"));
        sinon.assert.notCalled(exitStub);
        expect(errObject.ack).to.be.true;
    });

    it("should handle expected error", async () => {
        const errObject = IntegrationErrorHandler.handle(new GeneralError("Test", "test"));
        sinon.assert.notCalled(exitStub);
        expect(errObject.ack).to.be.false;
    });
});
