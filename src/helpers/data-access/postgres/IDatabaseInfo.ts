export interface IDatabaseInfo {
    connectionString: string;
    poolIdleTimeout: number;
    poolMinConnections: number;
    poolMaxConnections: number;
    applicationName: string; // e.g. Output Gateway or Integration Engine
}
