# Implementační dokumentace modulu *NÁZEV MODULU*

## Záměr

Stručný popis, k čemu modul slouží.

Pokud má modul více oblastí, každá oblast je popsána zvlášť. Dále jsem popsány vazby mezi jednotlivými oblastmi.



## Vstupní data

### Data nám jsou posílána

Popis dat, jak nám jsou posíláná na input-gateway. Pokud je vystaveno více endpointů (pro různé datové sady), každý endpoint je popsán zvlášť.

#### *Název endpointu / název datové sady*

- formát dat
  - protokol
  - datový typ
  - odkaz na validační schéma
  - příklad vstupních dat
- endpoint v input-gateway
  - popis endpointu
  - odkaz na řádek v openapi
- (volitelně) nastavení práv v permission-proxy
  - zvláštní požadaky na řízení přístupu
- transformace raw dat
  - popis prvotního zpracování na input-gateway, např. použit standardní body parser xml->json
- název rabbitmq fronty
  - popis / název fronty, kam jsou odeslaná validní data
- (volitelně) odhadovaná zátěž
  - počet requestů za minutu, pokud je to předem známo

### Data aktivně stahujeme

Popis dat, jak je pomocí cronu a integration-engine stahujeme. Pokud jsou data stahována z více zdrojů, každý zdroj je popsán zvlášť.

#### *Název datového zdroje*

- zdroj dat
  - url
  - parametry dotazu
- formát dat
  - protokol
  - datový typ
  - odkaz na validační schéma
  - příklad vstupních dat
- (volitelně) vstupní parametry, hlavičky, atd.
- frekvence stahování
  - cron definice
- název rabbitmq fronty
  - popis / název fronty, která vyvolá proces integrace, ve které je zdroj dat obsažen



## Zpracování dat / transformace

Popis transformace a případného obohacení dat. Zaměřeno hlavně na workery a jejich metody.

### *Název workeru*

Stručný popis workeru.

#### *Název metody workeru*

- vstupní rabbitmq fronta
  - název
  - parametry
- (volitelně) závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
  - název
  - parametry
- datové zdroje
  - výčet datových zdrojů
- transformace
  - odkaz na transformaci dat, případně stručný popis tranformace
- (volitelně) obohacení dat
  - popis, pokud se data obohacují a jak
- data modely
  - výčet datových modelů, kam se data ukládájí



## Uložení dat

Popis ukládání dat.

### Obecné

- typ databáze
  - PSQL, atd.
- databázové schéma
  - popis tabulek nebo odkaz na obrázek s databázovým schématem
- retence dat
  - popis zachovávání a promazávání dat

### *Název datového modelu*

- schéma a název tabulky / kolekce
- struktura tabulky / kolekce
  - odkaz na schéma, případně na migrační skripty
- ukázka dat



## Output API

Popis output-api.

### Obecné

- OpenAPI v3 dokumentace
  - odkaz na dokumentaci
- veřejné / neveřejné endpointy
  - api je veřejné nebo neveřejné, případně seznam veřejných a neveřejných endpointů
- (volitelně) ACL
- postman kolekce
  - odkaz na postman kolekci

#### *Název endpointu*

- zdrojové tabulky
- (volitelně) nestandardní dodatečná transformace
- (volitelně) nestandardní url a query parametry
- ukázka dat (pokud není součástí OpenAPI)


