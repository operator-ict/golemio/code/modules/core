# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.20.1] - 2025-03-10

### Fixed

-   Detete entities in Azure Table Storage in parallel batches to speed up the process and avoid triggering the RabbitMQ ack timeout ([pid#453](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/453))

## [1.20.0] - 2025-03-03

### Changed

-   AbstractRouter - pass router options to the constructor ([vymi#112](https://gitlab.com/operator-ict/golemio/jis/vymi-alerts/vymi-alerts-general/-/issues/112))

## [1.19.4] - 2025-02-20

### Added

-   shared lib `qs`

## [1.19.3] - 2025-02-13

### Changed

-   postgis image update ([core#125](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/125))

## [1.19.2] - 2025-01-29

### Fixed

-   Fixed fatal error that occurred when attempting to retrieve last modified date from an inaccessible FTP server ([pid#454](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/454))

## [1.19.1] - 2025-01-27

### Changed

-   Update `undici` dependency to v6.21.1 to fix high severity vulnerability
-   Update `express` dependency to v4.21.2 to fix high severity vulnerability

## [1.19.0] - 2025-01-15

### Changed

-   Change TS build target from ES2015 to ES2021 ([core#121](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/121))

## [1.18.0] - 2024-12-17

### Changed

-   Update Azure SDK dependencies to latest versions ([p0255#90](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/90))
-   Update `undici` dependency to v6.21.0 ([p0255#90](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/90))
-   Timeout using AbortSignal to prevent hanging uploads in Azure Blob Storage operations ([p0255#90](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/90))

### Fixed

-   Memory leak in `AzureBlobStorageService.uploadStream()` when using managed identity authentication by properly handling stream events ([p0255#90](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/90))

## [1.17.2] - 2024-12-10

### Changed

-   array support for simple config

## [1.17.1] - 2024-11-21

### Changed

-   Update CODING-GUIDELINES.md - add explicit rules for null vs undefined usage
-   Method of authentication to Azure Blob Storage from SAS token to managed identity ([infra#304](https://gitlab.com/operator-ict/golemio/devops/infrastructure/-/issues/304))

## [1.17.0] - 2024-11-20

### Added

-   Method for accessing metadata (such as headers and status codes) received from a data source ([vymi-alerts#61](https://gitlab.com/operator-ict/golemio/jis/vymi-alerts/vymi-alerts-general/-/issues/61))
-   Support for HTTP `304 Not Modified` responses to conditional data source requests ([vymi-alerts#61](https://gitlab.com/operator-ict/golemio/jis/vymi-alerts/vymi-alerts-general/-/issues/61))

## [1.16.0] - 2024-11-14

### Added

-   Photon extended address search ([p0255#66](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/66))

### Changed

-   Update CODING-GUIDELINES.md - add rule requiring explicit initialization of instance variables in classes, prohibiting the use of definite assignment assertion operator

## [1.15.0] - 2024-09-30

### Added

-   Cloudflare cache purge webhook ([pid#417](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/417))

## [1.14.1] - 2024-09-12

### Fixed

-   Fix path-to-regexp dependency (https://github.com/advisories/GHSA-9wv6-86v2-598)

### Changed

-   `.gitlab-ci.yml` cleanup ([devops#320](https://gitlab.com/operator-ict/golemio/devops/infrastructure/-/issues/320))

## [1.14.0] - 2024-09-09

### Changed

-   Replace node-redis with ioredis ([core#39](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/39))

### Removed

-   VP-IE `turnaroundTimeInSeconds` legacy configuration ([pid#137](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/137))

## [1.13.3] - 2024-08-20

### Removed

-   remove CacheMiddleware and useCacheMiddleware ([core#110](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/110))

## [1.13.2] - 2024-08-16

### Added

-   add backstage.io metadata files

## [1.13.1] - 2024-08-13

### Fixed

-   temp hotfix from version 1.12.2 was removed

## [1.13.0] - 2024-08-06

### Added

-   add cache-control header to all responses ([core#107](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/107))

### Removed

-   remove redis useCacheMiddleware ([core#107](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/107))

## [1.12.2] - 2024-07-23

### Fixed

-   temporary hotfix to prevent 3rd party from breaking

## [1.12.1] - 2024-07-23

-   No changelog

## [1.12.0] - 2024-07-17

### Added

-   strict check for API query params ([core#109](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/109))
-   Optionally apply compression on OG responses by default, unless explicitly requested otherwise ([pid#383](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/383))

## [1.11.1] - 2024-07-10

### Added

-   Add transaction support for bulk save ([p0262#18](https://gitlab.com/operator-ict/golemio/projekty/energetika/p0262.prevzeti-energeticke-databaze/-/issues/18))

## [1.11.0] - 2024-07-04

### Added

-   Configurable CsvParserMiddleware to parse CSV push data ([ig#82](https://gitlab.com/operator-ict/golemio/code/input-gateway/-/issues/82))

### Security

-   Security update - braces

## [1.10.3] - 2024-06-03

### Changed

-   Allow no cache header in CacheHeaderMiddleware ([core#108](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/108))

## [1.10.2] - 2024-05-22

### Fixed

-   Catch malformed GeoJSON ([general#572](https://gitlab.com/operator-ict/golemio/code/general/-/issues/572))

## [1.10.1] - 2024-05-14

### Fixed

-   `@google-cloud/storage` version stream error

## [1.10.0] - 2024-05-13

### Changed

-   Update Node.js to v20.12.2 Express to v4.19.2 ([core#102](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/102))

## [1.9.18] - 2024-04-29

### Added

-   Add CacheHeaderMiddleware to set cache headers ([core#100](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/100))

## [1.9.17] - 2024-04-24

### Changed

-   Geocode API migrated from axios to fetch

### Removed

-   axios library

## [1.9.16] - 2024-04-08

### Added

-   HTTPFetchProtocolStrategy - migration from axios to native fetch ([core#99](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/99))

## [1.9.15] - 2024-03-25

### Changed

-   Update CODING-GUIDELINES.md to include the new rule about naming boolean variables, constants, and property names

### Fixed

-   Fixed router query validation rules ([core#93](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/93))

## [1.9.14] - 2024-02-28

### Fixed

-   Deprecated Redis connector OG local tests

## [1.9.13] - 2024-02-19

### Changed

-   Update express validator to 7.0.1 ([core#94](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/94))

## [1.9.12] - 2024-02-12

### Fixed

-   Reset offset url param in paginated HTTPProtocolStrategy ([general#530](https://gitlab.com/operator-ict/golemio/code/general/-/issues/530))

## [1.9.11] - 2024-02-12

### Changed

-   Log queue and module names via Error object ([core#92](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/92))
-   Rename datasources to moduleConfig ([core#75](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/75))

## [1.9.10] - 2024-02-05

### Changed

-   Azure table credentials ([core#88](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/88))

## [1.9.9] - 2024-01-29

### Added

-   Paginated HTTPProtocolStrategy ([general#530](https://gitlab.com/operator-ict/golemio/code/general/-/issues/530))

## [1.9.8] - 2024-01-22

### Changed

-   Pino libraries upgrade ([core#89](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/89))

## [1.9.7] - 2024-01-17

### Removed

-   LaunchDarkly feature flags support ([core#85](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/85))
-   Feature flag services in IE, OG, IG

## [1.9.6] - 2023-12-13

### Added

-   ArcGIS datasource ([parkings#39](https://gitlab.com/operator-ict/golemio/code/modules/parkings/-/issues/39))

### Fixed

-   Long datasource validation error messages - truncate to 2000 characters

## [1.9.5] - 2023-12-11

### Added

-   `deleteEntitiesOlderThan` method in `AzureTableStorageService` ([core#84](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/84))

## [1.9.4] - 2023-11-13

### Added

-   Min/max function support in Datetime Helper
-   Enable parsing date from ISO with time zone as extra parameter
-   Cached repository added
-   `HTMLUtils` from pid module
-   Reusable RedisSubscriber

### Fixed

-   Throw error when parsing DateTime has invalid result

## [1.9.3] - 2023-10-09

### Added

-   Add service for Azure Table Storage ([tcp-ig#4](https://gitlab.com/operator-ict/golemio/code/tcp-input-gateway/-/issues/4))

### Changed

-   CI/CD - Update Redis services to v7.2.1

## [1.9.2] - 2023-09-27

### Added

-   API versioning - abstract router with version configuration ([core#80](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/80))

### Changed

-   CI/CD - Update services
-   Abstract transformation support for "one to many" usecase
-   Requestloggerprovider change info level to error when there is no status code

## [1.9.1] - 2023-09-13

### Removed

-   Unused abstract history router

## [1.9.0] - 2023-09-06

### Fixed

-   RedisModel - mget should return empty array when no keys are provided

## [1.8.11] - 2023-08-30

### Added

-   PostgresModel - bulkSave method to properly upsert multiple records at once
-   RedisModel - mget method to retrieve multiple values at once

## [1.8.10] - 2023-08-21

### Changed

-   Support empty messages for tasks with optional parameters.

### Added

-   Abortable workers ([core#64](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/64))

### Fixed

-   Retrydatasource override method replaced for private property

## [1.8.9] - 2023-08-16

### Fixed

-   `[object Object]` in Postgres application name

## [1.8.8] - 2023-08-09

### Changed

-   Streaming data from db ([core#76](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/76))

## [1.8.7] - 2023-08-02

### Fixed

-   Uncaught error on stream data validation ([core#49](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/49))

## [1.8.6] - 2023-07-31

### Added

-   getBoolean method to simple config
-   New datasource type that retries connection multiple times.

### Changed

-   DateTime wrapper used instead of Moment.js
-   Native Date or Luxon used instead of Moment.js
-   `FTPProtocolStrategy` getLastModified now rethrows error instead of returning null
-   Pub Sub classes to be more generic for reusability in permission proxy ([permission-proxy#159](https://gitlab.com/operator-ict/golemio/code/permission-proxy/-/issues/159))

## [1.8.5] - 2023-07-26

### Removed

-   Unused shared dependencies (request & request-promise)

## [1.8.4] - 2023-07-17

### Added

-   Opentelemetry blacklist, pq span size truncate

### Changed

-   Case insensitive blacklist ([core#69](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/69))
-   Read opentelemetry config from env vars

### Removed

-   Unused shared dependencies (request & request-promise)

## [1.8.3] - 2023-07-12

### Added

-   Universal repositories
-   Synchronous transformations

### Changed

-   tsyringe updated to v4.8.0

## [1.8.2] - 2023-06-26

### Added

-   Simple configuration object with loading via dot notation.

### Changed

-   Refactoring of connectors for Postgres, Redis, Rabbit to be independent of service. ([core#17](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/17))

## [1.8.1] - 2023-06-21

### Fixed

-   FTP client logging

## [1.8.0] - 2023-06-14

### Changed

-   Typescript version update to 5.1.3 ([core#70](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/70))

## [1.7.4] - 2023-06-07

### Added

-   Authorize requests to the Photon API

## [1.7.2] - 2023-06-07

### Added

-   AbstractTask validable by json schema

### Fixed

-   compatibility between ts5 and tsyringe

## [1.7.1] - 2023-06-05

### Added

-   OG env var `API_URL_PREFIX` ([p0131#132](https://gitlab.com/operator-ict/golemio/projekty/oict/p0131.-data-pro-intermod-ln-naviga-n-syst-m/-/issues/132))

## [1.7.0] - 2023-05-31

### Changed

-   Update @golemio/errors package to v2.0.0

## [1.6.9] - 2023-05-15

### Fixed

-   memory leak in a CacheMiddleware

## [1.6.8] - 2023-05-03

### Changed

-   Stop inserting IE error logs to Postgres ([core#61](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/61))
-   Unify logger emitters

## [1.6.7] - 2023-04-17

### Changed

-   Allow yarn based projects

### Removed

-   Mongoose ([core#66](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/66))

## [1.6.6] - 2023-04-17

### Changed

-   Set queue type default to quorum ([code#63](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/63))
-   Replace deprecated `xml2js-es6-promise` package with `xml2js` ([core#26](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/26))

## [1.6.5] - 2023-03-29

### Added

-   Inspect utils OG config ([output-gateway#219](https://gitlab.com/operator-ict/golemio/code/output-gateway/-/issues/219))

### Changed

-   Improve DataSource typings ([pid#107](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/107))

## [1.6.4] - 2023-03-15

### Added

-   Configuration for VP metro last stop delay additions ([pid#219](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/219))

## [1.6.3] - 2023-03-15

### Changed

-   Update Sequelize to v6.29.0 ([core#59](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/59))

## [1.6.2] - 2023-03-08

### Changed

-   Mongoose schema definition was replaced for JSONSchema for Errors

## [1.6.1] - 2023-03-01

### Changed

-   Change IE storage provider from S3 to Azure blob storage ([core#56](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/56))

## [1.6.0] - 2023-02-27

### Changed

-   Move `getSubProperty` function from OG to shared helper utils
-   Update Node.js to v18.14.0, Node.js-next to v19.6.0, Express to v4.18.2 ([core#50](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/50))

### Removed

-   `@golemio/utils` dependency ([utils#2](https://gitlab.com/operator-ict/golemio/code/utils/-/issues/2))
-   `jtsk2wgs84` unused helper function ([core#52](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/52))

## [1.5.2] - 2023-02-22

### Changed

-   Update `basic-ftp` library

## [1.5.1] - 2023-02-20

### Changed

-   FTPPortocolStrategy close method moved to finally block of try catch.

## [1.5.0] - 2023-02-06

### Changed

-   Change storage provider from S3 to Azure blob storage ([input-gateway#2](https://gitlab.com/operator-ict/golemio/code/vp/input-gateway/-/issues/2))

## [1.4.1] - 2023-01-30

### Added

-   Implement feature flag service

## [1.3.1] - 2023-01-23

### Added

-   Docs for versioning (CZ)
-   turn around time configuration for VP

### Fixed

-   DI for mongo test data importer in `/bin/import-test-data.js`

### Changed

-   Migrate to npm

## [1.3.0] - 2023-01-04

### Added

-   Create Redis PubSub channel
-   Dependency injection

### Changed

-   Utilize streams for FTP strategy and Redis repositories ([pid#209](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/209))
-   Replace library `decompress` with `yauzl`

### Security

-   Update `class-validator` to `^0.14.0` to address https://security.snyk.io/vuln/SNYK-JS-CLASSVALIDATOR-1730566

## [1.2.24] - 2022-12-13

### Added

-   Create utils for the Node.js inspector ([data-proxy#12](https://gitlab.com/operator-ict/golemio/code/data-proxy/-/issues/12))
-   Docs for remote debugging (CZ)

## [1.2.23] - 2022-12-07

### Changed

-   Log `Data source returned empty data.` to verbose level

## [1.2.22] - 2022-11-29

### Added

-   Add Golemio GIT Workflow docs
-   Allow setting encoding on compressed redis stored files for HTTPProtocolStrategy

### Changed

-   Use pino logger instead of winston

## [1.2.21] - 2022-11-03

### Changed

-   Update Typescript to 4.7.2 and Mongoose to 6.6.5

### Added

-   Extend azure blob storage service with delete and get function, add noop service if storage is disabled
-   redish-semaphore added as lock system for distributed computing
-   vehicle positions data retention configuration values

### Fixed

-   Fatal error on IE - prevent validation of unknown types (e.g. empty array)

## [1.2.20] - 2022-10-05

### Changed

-   Revert back to the node redis connector implementation in OG ([core#39](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/39))

## [1.2.19] - 2022-10-04

### Added

-   expose geojson library through shared/
-   additional filters implementation in GeoJsonRouter

### Changed

-   ioredis implementation in OG instead of node redis

## [1.2.18] - 2022-09-21

### Added

-   Add service for Azure Blob Storage ([data-proxy#10](https://gitlab.com/operator-ict/golemio/code/data-proxy/-/issues/10))
-   exposed lib for reflection trough shared/\_global ([core#44](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/44))

### Changed

-   Log and store nested worker task validation errors
-   Pass AMQP message properties to the execute method ([pid#166](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/166))
-   processDataStream refactored out of deprecated BaseWorker ([pid#170](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/170))

### Removed

-   Unused dependencies ([general#5](https://gitlab.com/operator-ict/golemio/code/modules/general/-/issues/5))

## [1.2.17] - 2022-09-01

### Changed

-   The Lodash library was replaced with js functions ([core#42](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/42))
-   upgrade Mongoose from 5.6.7 to 6.5.3.

## [1.2.16] - 2022-08-24

### Added

-   docs for configuration files
-   New abstractions for workers and worker tasks ([playgrounds#5](https://gitlab.com/operator-ict/golemio/code/modules/playgrounds/-/issues/5))

## [1.2.15] - 2022-08-01

### Changed

-   Replace apicache with apicache-plus ([core#38](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/38))
-   Config loader loads only explicit configuration files. Mergin values with default configuration file was removed. ([core#25](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/25))

## [1.2.14] - 2022-07-18

### Added

-   Add configuration for graceful shutdown (input gateway, integration engine, output gateway)

## [1.2.13] - 2022-07-06

### Fixed

-   GeoJsonRouter ids param validator

## [1.2.12] - 2022-06-06

### Changed

-   Bring back redis dependency
-   Replace apicache-plus with apicache

### Fixed

-   Sharing also Ajv types

## [1.2.11] - 2022-05-31

### Added

-   Add configuration for VP Redis expire time
-   Add configuration for the maximum prefetch count on Rabbit channel (integration engine)
-   Add configuration for the postgres pool max connections (output gateway)
-   Add configuration for the postgres pool min connections and idle timeout (integration engine, output gateway)

### Changed

-   Label sequelize connections
-   Utilize ioredis in output gateway
-   Add optional expireTimestamp parameter to the RedisModel set method (integration engine)
-   Replace apicache with apicache-plus

### Removed

-   Remove redis dependency

## [1.2.10] - 2022-05-25

### Changed

-   Typescript version update from 4.4.4 to 4.6.4

## [1.2.9] - 2022-05-10

### Added

-   Docs: Create default module readme template

### Changed

-   Cancel all consumer operations and requeue incoming AMQP messages before the IE shuts down
-   Update OpenTelemetry SDKs

## [1.2.8] - 2022-05-04

### Added

-   Alternate queue for unrouted messages

## [1.2.7] - 2022-05-02

### Added

-   Opentelemetry: enable dual (console/agent(UDP/HTTP)) processing
-   GeocodeApi method getAddressByLatLngFromPhoton
-   Shared Luxon library for datetime handling
-   IE logger passed to validator to log internal errors
-   Opentelemetry: Add function createChildSpan
-   Add TS support for ES2021

### Deprecated

-   Deprecate golemio-import-test-data in favor of Golemio CLI import-db-data

### Removed

-   Remove Influx connectors

## [1.2.6] - 2022-04-07

### Changed

-   Refactoring of History router to re-use database independent code for postgres migration.

### Fixed

-   Upload raw data from requests with query parameters ([vp-input-gateway#1](https://gitlab.com/operator-ict/golemio/code/vp/input-gateway/-/issues/1))

## [1.2.5] - 2022-03-26

### Fixed

-   Fix VP IE config for TJR negative offset

## [1.2.4] - 2022-03-26

### Added

-   Add VP IE config for TJR negative offset (TCP busses)

## [1.2.3] - 2022-03-16

### Added

-   Interfaces for mongo based models that were used by GeoJsonRouter and its predecessors. To enable re-use of the router for modules migrated to Postgres.
-   JsonSchema for Geometry in SharedSchemaProvider.
-   Interface for GeoJsonRouter request parameters.

### Changed

-   Minor refactoring of GeoJsonRouter to enable reusability.
-   Refactoring of validation in PostgresModel to separate method

## [1.2.2] - 2022-03-09

### Added

-   Ajv library is shared centrally from core module.
-   Add docs/ files, CODING-GUIDELINES.md and SECURITY.md
-   Add docs for modularized postgres migrations (CZ)

### Changed

-   Update CONTRIBUTING.md
-   Update .editorconfig and .prettierrc.json
-   Update README.md

### Fixed

-   Directly import OG connectors to prevent a side effect

## [1.2.1] - 2022-03-01

### Fixed

-   Input gateway instance may die during parsing client data. Fix transforms client based http erros to CustomError, that enables already existing logic to respond to client with Bad Request status error. ([input-gateway#78](https://gitlab.com/operator-ict/golemio/code/input-gateway/-/issues/78))

## [1.2.0] - 2022-02-16

### Changed

-   Request-promise deprecated to Axios ([core#6](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/6))

### Removed

-   Remove powered by header ([core#23](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/23))

## [1.1.11] - 2022-01-20

### Changed

-   Add config for default timezones ([pid#76](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/76))

## [1.1.10] - 2022-01-06

### Added

-   Prometheus metrics buckets

## [1.1.9] - 2021-12-16

### Added

-   Prometheus metrics ([core#19](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/19))

### Removed

-   Removed rabbitmq reconnect

## [1.1.8] - 2021-11-30

### Fixed

-   Optional namespace char, del to unlink, log

### Changed

-   CI cache configuration

## [1.1.7] - 2021-11-23

### Added

-   Add sync to pg model

### Changed

-   Custom postgres model schema ([pid#53](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/53))
-   Upgrade apicache
-   Use custom schema in saveBySqlFunction ([pid#53](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/53))

### Security

-   Security update - mocha

