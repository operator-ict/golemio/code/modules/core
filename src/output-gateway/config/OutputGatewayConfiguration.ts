import { injectable } from "tsyringe";

@injectable()
export class OutputGatewayConfiguration {
    public app_version = process.env.npm_package_version;
    public app_name = process.env.APP_NAME || "output-gateway";
    public api_url_prefix = process.env.API_URL_PREFIX || "https://api.golemio.cz/v2";
    public log_level = process.env.LOG_LEVEL;
    public inspector = {
        host: process.env.INSPECTOR_HOST,
        port: process.env.INSPECTOR_PORT,
    };
    public node_env = process.env.NODE_ENV || "development";
    public pagination_max_limit: number;
    public port = process.env.PORT;
    public postgres_connection = process.env.POSTGRES_CONN;
    public postgres_read_only_connection = process.env.POSTGRES_RO_CONN || process.env.POSTGRES_CONN; // to discuss
    public postgres_pool_min_connections = process.env.POSTGRES_POOL_MIN_CONNECTIONS;
    public postgres_pool_max_connections = process.env.POSTGRES_POOL_MAX_CONNECTIONS;
    public postgres_pool_idle_timeout = process.env.POSTGRES_POOL_IDLE_TIMEOUT;
    public redis_connection = process.env.REDIS_CONN;
    public redis_ttl = process.env.REDIS_DEFAULT_TTL;
    public sentry = {
        enabled: process.env.SENTRY_ENABLED === "true",
        dsn: process.env.SENTRY_DSN,
        release: `${process.env.APP_NAME}@${process.env.npm_package_version}`,
        environment: process.env.SENTRY_ENVIRONMENT,
        debug: process.env.SENTRY_DEBUG === "true",
        tracesSampleRate: process.env.SENTRY_TRACE_RATE ? parseFloat(process.env.SENTRY_TRACE_RATE) : 0,
    };
    public metrics = {
        enabled: process.env.METRICS_ENABLED === "true",
        port: process.env.METRICS_PORT ? parseInt(process.env.METRICS_PORT as string, 10) : 9001,
        prefix: process.env.METRICS_PREFIX || "",
    };
    public lightship = {
        shutdownDelay: process.env.LIGHTSHIP_SHUTDOWN_DELAY ? parseInt(process.env.LIGHTSHIP_SHUTDOWN_DELAY, 10) : undefined,
        shutdownTimeout: process.env.LIGHTSHIP_SHUTDOWN_TIMEOUT ? parseInt(process.env.LIGHTSHIP_SHUTDOWN_TIMEOUT, 10) : 60000,
        handlerTimeout: process.env.LIGHTSHIP_HANDLER_TIMEOUT ? parseInt(process.env.LIGHTSHIP_HANDLER_TIMEOUT, 10) : 5000,
    };
    public vehiclePositions = {
        defaultTimezone: process.env.VEHICLE_POSITIONS_DEFAULT_TIMEZONE ?? "Europe/Prague",
    };

    constructor() {
        this.pagination_max_limit = this.getPaginationMaxLimit();
    }

    private getPaginationMaxLimit = (): number => {
        if (process.env.PAGINATION_MAX_LIMIT) {
            const parsed = parseInt(process.env.PAGINATION_MAX_LIMIT, 10);
            if (!isNaN(parsed)) {
                return parsed;
            }
        }

        return 10000;
    };
}
