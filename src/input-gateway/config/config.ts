import { ContainerToken, InputGatewayContainer } from "#ig/ioc";
import { IConfiguration } from "./abstract";

/**
 * @deprecated For backwards compatibility. Use ioc container instead.
 */
export const config = InputGatewayContainer.resolve<IConfiguration>(ContainerToken.Config);
