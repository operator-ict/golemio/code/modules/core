import { ILogger } from "#helpers/logger";
import { ITableStorageService } from "./interfaces/ITableStorageService";

export class NoopTableStorageService implements ITableStorageService {
    constructor(private log: ILogger) {}

    public async createEntities<T extends Record<string, any>>(tableName: string, entities: T[]): Promise<void> {
        this.log.silly("NoopTableStorageService: table storage is disabled. Skipping entities creation.");
    }

    public async deleteEntitiesOlderThan<T extends object>(tableName: string, timestamp: string): Promise<void> {
        this.log.silly("NoopTableStorageService: table storage is disabled. Skipping entities deletion.");
    }
}
