import { dateTime, ILogger } from "#helpers";
import { ISimpleConfig } from "#helpers/configuration/ISimpleConfig";
import { AbstractStorageService } from "#helpers/data-access/storage";
import { CoreToken } from "#helpers/ioc/CoreToken";
import { IConfiguration, ISaveRawDataWhitelist } from "#ig/config";
import { getContentType } from "#ig/helpers/CheckContentTypeMiddleware";
import { contentTypeToExtension } from "#ig/helpers/ContentTypeHelper";
import { ContainerToken } from "#ig/ioc";
import { ErrorHandler, GeneralError } from "@golemio/errors";
import { NextFunction, Request, Response } from "express";
import { pathToRegexp } from "path-to-regexp";
import { inject, injectable } from "tsyringe";

@injectable()
export class SaveRawDataMiddleware {
    private readonly storageEnabled: boolean;
    private readonly whitelist: ISaveRawDataWhitelist[];
    constructor(
        @inject(ContainerToken.Config) config: IConfiguration,
        @inject(CoreToken.SimpleConfig) simpleConfig: ISimpleConfig,
        @inject(CoreToken.Logger) private readonly log: ILogger,
        @inject(ContainerToken.StorageService) private storage: AbstractStorageService
    ) {
        this.storageEnabled = simpleConfig.getValue("env.STORAGE_ENABLED", "false") === "true";
        this.whitelist = config.saveRawDataWhitelist;
    }

    public getMiddleware = () => {
        const whitelistRegexp = this.getWhitelistRegexp();
        return (req: Request, res: Response, next: NextFunction) => {
            const requestUrlPath = req.path;

            if (this.storageEnabled && req.method === "POST" && whitelistRegexp.routesRegexp.exec(requestUrlPath)) {
                // file extension by content-type
                const contentType = getContentType(req) as keyof typeof contentTypeToExtension;
                const ext = contentTypeToExtension[contentType];

                // content-type checking
                if (!ext) {
                    ErrorHandler.handle(
                        new GeneralError(
                            `Saving raw data failed. (${requestUrlPath})`,
                            "SaveRawDataMiddleware",
                            new Error(`Extension for content-type ${contentType} was not found.`),
                            406
                        ),
                        this.log
                    );
                    return next();
                }

                const now = dateTime(new Date(), { timeZone: "Europe/Prague" });
                const fileName = `${now.format("yyyy-MM-dd")}/${now.format("HH_mm_ss.SSS")}`;

                this.storage.uploadStream(req, `${requestUrlPath.slice(1)}`, ext, fileName).catch((err) => {
                    this.log.error(err, `Saving of data stream failed (${requestUrlPath})`);
                });

                // save file with headers of the matched route
                if (whitelistRegexp.headersRegexp.exec(requestUrlPath)) {
                    const headerFileName = `${fileName}_headers`;
                    this.storage
                        .uploadFile(JSON.stringify(req.headers), `${requestUrlPath.slice(1)}`, `json`, headerFileName)
                        .catch((err) => {
                            this.log.error(err, `Saving of headers failed (${requestUrlPath})`);
                        });
                }
            }

            next();
        };
    };

    public getWhitelistRegexp = () => {
        const regexpRoutes = [];
        const regexpHeaders = [];

        for (const dataset of this.whitelist) {
            const routeRegexp = `(${pathToRegexp(dataset.route).regexp.source})`;

            regexpRoutes.push(routeRegexp);
            if (dataset.withHeaders) {
                regexpHeaders.push(routeRegexp);
            }
        }

        return {
            routesRegexp: regexpRoutes.length ? RegExp(regexpRoutes.join("|")) : RegExp(/.^/),
            headersRegexp: regexpHeaders.length ? RegExp(regexpHeaders.join("|")) : RegExp(/.^/),
        };
    };
}
