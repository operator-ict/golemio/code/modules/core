import express from "express";
import * as sentry from "@sentry/node";
import * as tracer from "@sentry/tracing";
import { AbstractGolemioError } from "@golemio/errors";

export interface ISentryConfig {
    enabled: boolean;
    dsn?: string;
    release?: string;
    environment?: string;
    debug?: boolean;
    tracesSampleRate?: number;
}

export const initSentry = (sentryConfig: ISentryConfig, appName: string, app?: express.Application) => {
    if (!sentryConfig.enabled || !sentryConfig.dsn) return;

    const options: sentry.NodeOptions = {
        release: sentryConfig.release,
        dsn: sentryConfig.dsn,
        environment: sentryConfig.environment,
        initialScope: {
            tags: { appName },
        },
        debug: sentryConfig.debug,
        integrations: [
            new sentry.Integrations.Http({ tracing: true }),
            new tracer.Integrations.Postgres(),
            ...(app ? [new tracer.Integrations.Express({ app })] : []),
        ],
        tracesSampleRate: sentryConfig.tracesSampleRate,
        beforeSend: (event, hint) => {
            const exception = hint?.originalException;

            if (exception instanceof AbstractGolemioError) {
                event.fingerprint = [
                    // replacing "{{ default }}",
                    String(exception.name),
                    String(exception.className),
                    String(exception.status),
                    String(exception.info),
                    String(exception.stack),
                ];
            }

            return event;
        },
    };

    sentry.init(options);
};
