import { IValidator } from "@golemio/validator/dist/IValidator";
import { setTimeout as sleep } from "timers/promises";
import { DataSource } from "./DataSource";
import { IDataTypeStrategy } from "./datatype-strategy/IDataTypeStrategy";
import { IProtocolStrategy } from "./protocol-strategy/IProtocolStrategy";

export class RetryDataSource<T = any> {
    protected retryCount: number;
    private datasource: DataSource<T>;

    constructor(
        name: string,
        protocolStrategy: IProtocolStrategy,
        dataTypeStrategy: IDataTypeStrategy | null = null,
        validator: IValidator | null = null,
        retryCount: number = 3
    ) {
        this.datasource = new DataSource<T>(name, protocolStrategy, dataTypeStrategy, validator);
        this.retryCount = retryCount;
    }

    public async getAll(): Promise<T> {
        try {
            return await this.datasource.getAll();
        } catch (err) {
            if (this.retryCount > 0) {
                this.retryCount--;
                await this.sleep();
                return await this.getAll();
            }
            throw err;
        }
    }

    public async getLastModified(): Promise<string> {
        try {
            return await this.datasource.getLastModified();
        } catch (err) {
            if (this.retryCount > 0) {
                this.retryCount--;
                await this.sleep();
                return await this.getLastModified();
            }
            throw err;
        }
    }

    private async sleep() {
        // sleep random time between 5 seconds to 60 seconds
        await sleep(Math.random() * 55000 + 5000);
    }
}
