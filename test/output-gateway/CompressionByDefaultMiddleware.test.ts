import { CompressionByDefaultMiddleware } from "#og/CompressionByDefaultMiddleware";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";

chai.use(chaiAsPromised);

describe("CompressionByDefaultMiddleware", () => {
    it("should set the _shouldCompressByDefault Response property", () => {
        const middleware = new CompressionByDefaultMiddleware();

        const res = {
            set: (key: string, value: string) => {
                expect(key).to.equal("_shouldCompressByDefault");
                expect(value).to.equal(true);
            },
        } as any;

        middleware.getMiddleware()(null as any, res, () => {});
    });
});
