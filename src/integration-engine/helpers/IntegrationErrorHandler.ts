import { log } from "#ie/helpers";
import { ErrorHandler, AbortError, GeneralError, IGolemioError, RecoverableError } from "@golemio/errors";
import * as sentry from "@sentry/node";
import { Severity } from "@sentry/node";

export interface IExtendedGolemioErrorObject extends IGolemioError {
    ack: boolean;
    exit?: boolean;
}

/**
 * Class responsible for error handling in the app. Catches errors and based on their type performs some action.
 *
 * Extends ErrorHandler
 */
export class IntegrationErrorHandler extends ErrorHandler {
    /**
     * Handle the error. The function logs the error, kills the application if it's unknown/fatal error.
     *
     * @param err Error (GolemioError) object to catch and process
     */
    public static handle(err: Error): IExtendedGolemioErrorObject {
        if (err instanceof GeneralError) {
            if (err instanceof RecoverableError) {
                // warning: ack message - do not retry
                log.warn(err);
                return this.handleRecoverableError(err);
            } else {
                // error: nack message
                log.error(err);
                return this.handleNonRecoverableError(err);
            }
        }

        // Unexpected and Fatal errors
        // error: nack message before exiting
        if (err instanceof AbortError) {
            log.error(err);
            return this.handleAbortError(err);
        }

        // error: exit - message is re-queued
        const msg = {
            message: "[IE handler] Unexpected fatal error. Exit.",
            cause: err.message,
            stack: err.stack,
        };
        log.error({ message: msg });
        // if anything fails, process is killed
        sentry.captureMessage(`Exiting: ${err.toString()}`, Severity.Fatal);
        return process.exit(1);
    }

    private static handleRecoverableError(err: RecoverableError): IExtendedGolemioErrorObject {
        sentry.setTag("status", "ack");
        sentry.captureMessage(err.toString(), Severity.Warning);
        return { ...err.toObject(), ack: true };
    }

    private static handleNonRecoverableError(err: GeneralError): IExtendedGolemioErrorObject {
        sentry.setTag("status", "nack");
        sentry.captureException(err);
        return { ...err.toObject(), ack: false };
    }

    private static handleAbortError(err: AbortError): IExtendedGolemioErrorObject {
        sentry.setTag("status", "nack");
        sentry.captureException(err);
        return { ...err.toObject(), ack: false, exit: true };
    }
}
