import { AzureBlobStorageService } from "#helpers/data-access/storage/AzureBlobStorageService";
import { ILogger } from "#helpers/logger";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox, SinonStub } from "sinon";
import { Readable } from "stream";

chai.use(chaiAsPromised);

describe("AzureBlobStorageService", () => {
    let sandbox: SinonSandbox;
    let service: AzureBlobStorageService;
    let log: Partial<ILogger>;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        log = {
            error: sandbox.stub() as any,
            verbose: sandbox.stub() as any,
        };

        service = new AzureBlobStorageService(
            {
                clientId: "client-id",
                tenantId: "tenant-id",
                account: "test",
                clientSecret: "secret",
                containerName: "test",
                uploadTimeoutInSeconds: 120,
            },
            log as ILogger
        );
    });

    afterEach(() => {
        sandbox.restore();
    });

    const stubContainerClient = () => {
        const uploadStreamStub = sandbox.stub();
        const uploadDataStub = sandbox.stub();
        const deleteDataStub = sandbox.stub();
        const downloadDataStub = sandbox.stub();

        const getBlockBlobClientStub = sandbox.stub().returns({
            uploadStream: uploadStreamStub,
            uploadData: uploadDataStub,
            delete: deleteDataStub,
            download: downloadDataStub,
        });

        const containerClientStub = sandbox.stub(service, "instantiateContainerClient" as any).returns({
            getBlockBlobClient: getBlockBlobClientStub,
        });

        return {
            uploadStreamStub,
            uploadDataStub,
            deleteDataStub,
            downloadDataStub,
            getBlockBlobClientStub,
            containerClientStub,
        };
    };

    // =============================================================================
    // uploadStream
    // =============================================================================
    describe("uploadStream", () => {
        it("should upload stream", async () => {
            const { uploadStreamStub, getBlockBlobClientStub } = stubContainerClient();

            const dataStream = new Readable();
            dataStream.push("test-data");
            dataStream.push(null);

            const promise = service.uploadStream(dataStream, "test-prefix", "json", "test-name");
            await expect(promise).to.be.fulfilled;
            expect(getBlockBlobClientStub.getCall(0).args[0]).to.equal("test-prefix/test-name.json");
            expect(uploadStreamStub.getCall(0).args[0]).to.be.instanceof(Readable);
        });

        it("should log upload error (getBlockBlobClient throws)", async () => {
            const clock = sandbox.useFakeTimers(new Date("2022-09-12T22:41:34.474Z").getTime());
            const { getBlockBlobClientStub } = stubContainerClient();
            getBlockBlobClientStub.throws();

            const dataStream = new Readable();
            dataStream.push("test-data");
            dataStream.push(null);

            const promise = service.uploadStream(dataStream, "test-prefix", "json");
            await expect(promise).to.be.fulfilled;
            expect((log.error as SinonStub).getCall(0).args[1]).to.equal(
                "[AzureBlobStorageService] Saving of data stream failed (test-prefix/2022-09-13/00_41_34.474.json)"
            );

            clock.restore();
        });

        it("should log upload error (uploadData rejects)", async () => {
            const clock = sandbox.useFakeTimers(new Date("2022-09-12T22:41:34.474Z").getTime());
            const { uploadStreamStub } = stubContainerClient();
            uploadStreamStub.rejects();

            const dataStream = new Readable();
            dataStream.push("test-data");
            dataStream.push(null);

            const promise = service.uploadStream(dataStream, "test-prefix", "json");
            await expect(promise).to.be.fulfilled;
            expect((log.error as SinonStub).getCall(0).args[1]).to.equal(
                "[AzureBlobStorageService] Saving of data stream failed (test-prefix/2022-09-13/00_41_34.474.json)"
            );

            clock.restore();
        });

        it("should log upload error (timeout)", async () => {
            const { uploadStreamStub } = stubContainerClient();
            uploadStreamStub.callsFake((_stream, _bufferSize, _maxConcurrency, options) => {
                return new Promise<void>((resolve, reject) => {
                    const abortSignal = options.abortSignal as AbortSignal;
                    abortSignal.onabort = () => {
                        reject(new Error("Timeout"));
                    };

                    setTimeout(() => {
                        resolve();
                    }, 2000);
                });
            });

            sandbox.stub(service, "uploadTimeoutInMs" as any).value(300);

            const dataStream = new Readable();
            dataStream.push("test-data");
            dataStream.push(null);

            const promise = service.uploadStream(dataStream, "test-prefix", "json", "test-name");
            await expect(promise).to.be.fulfilled;
            expect((log.error as SinonStub).callCount).to.equal(1);
            expect((log.error as SinonStub).getCall(0).args[1]).to.equal(
                "[AzureBlobStorageService] Saving of data stream failed (test-prefix/test-name.json)"
            );
        });
    });

    // =============================================================================
    // uploadFile
    // =============================================================================
    describe("uploadFile", () => {
        it("should upload data", async () => {
            const { uploadDataStub, getBlockBlobClientStub } = stubContainerClient();

            const promise = service.uploadFile("test-data", "test-prefix", "json", "test-name");
            await expect(promise).to.be.fulfilled;
            expect(getBlockBlobClientStub.getCall(0).args[0]).to.equal("test-prefix/test-name.json");
            expect(uploadDataStub.getCall(0).args[0]).to.deep.equal(Buffer.from("test-data"));
        });

        it("should log upload error (getBlockBlobClient throws)", async () => {
            const clock = sandbox.useFakeTimers(new Date("2022-09-12T22:41:34.474Z").getTime());
            const { getBlockBlobClientStub } = stubContainerClient();
            getBlockBlobClientStub.throws();

            const promise = service.uploadFile(Buffer.from("test-data"), "test-prefix", "json");
            await expect(promise).to.be.fulfilled;
            expect((log.error as SinonStub).getCall(0).args[1]).to.equal(
                "[AzureBlobStorageService] Saving of data failed (test-prefix/2022-09-13/00_41_34.474.json)"
            );

            clock.restore();
        });

        it("should log upload error (uploadData rejects)", async () => {
            const clock = sandbox.useFakeTimers(new Date("2022-09-12T22:41:34.474Z").getTime());
            const { uploadDataStub } = stubContainerClient();
            uploadDataStub.rejects();

            const promise = service.uploadFile(Buffer.from("test-data"), "test-prefix", "json");
            await expect(promise).to.be.fulfilled;
            expect((log.error as SinonStub).getCall(0).args[1]).to.equal(
                "[AzureBlobStorageService] Saving of data failed (test-prefix/2022-09-13/00_41_34.474.json)"
            );

            clock.restore();
        });
    });

    // =============================================================================
    // deleteFileByKey
    // =============================================================================
    describe("deleteFile", () => {
        it("should delete data", async () => {
            const { deleteDataStub, getBlockBlobClientStub } = stubContainerClient();

            const promise = service.deleteFile("test-prefix/test-name.json");
            await expect(promise).to.be.fulfilled;
            expect(getBlockBlobClientStub.getCall(0).args[0]).to.equal("test-prefix/test-name.json");
            expect(deleteDataStub.calledOnce).to.be.true;
        });

        it("should log delete error (getBlockBlobClient throws)", async () => {
            const { getBlockBlobClientStub } = stubContainerClient();
            getBlockBlobClientStub.throws();

            const promise = service.deleteFile("test-prefix/test-name.json");
            await expect(promise).to.be.fulfilled;
            expect((log.error as SinonStub).getCall(0).args[1]).to.equal(
                "[AzureBlobStorageService] Deletion of data failed (test-prefix/test-name.json)"
            );
        });

        it("should log delete error (delete rejects)", async () => {
            const { deleteDataStub } = stubContainerClient();
            deleteDataStub.rejects();

            const promise = service.deleteFile("test-prefix/test-name.json");
            await expect(promise).to.be.fulfilled;
            expect((log.error as SinonStub).getCall(0).args[1]).to.equal(
                "[AzureBlobStorageService] Deletion of data failed (test-prefix/test-name.json)"
            );
        });
    });

    // =============================================================================
    // getFileStream
    // =============================================================================
    describe("getFileStream", () => {
        it("should get data stream", async () => {
            const { downloadDataStub, getBlockBlobClientStub } = stubContainerClient();
            downloadDataStub.returns({ readableStreamBody: new Readable({ objectMode: true }) });

            const promise = service.getFileStream("test-prefix/test-name.json");
            await expect(promise).to.be.fulfilled;
            expect(getBlockBlobClientStub.getCall(0).args[0]).to.equal("test-prefix/test-name.json");
            expect(downloadDataStub.calledOnce).to.be.true;
        });

        it("should log getFileStream error (getBlockBlobClient throws)", async () => {
            const { getBlockBlobClientStub } = stubContainerClient();
            getBlockBlobClientStub.throws();

            const promise = service.getFileStream("test-prefix/test-name.json");
            await expect(promise).to.be.fulfilled;
            expect((log.error as SinonStub).getCall(0).args[1]).to.equal(
                "[AzureBlobStorageService] Getting data failed (test-prefix/test-name.json)"
            );
        });

        it("should log getFileStream error (getFileStream rejects)", async () => {
            const { downloadDataStub } = stubContainerClient();
            downloadDataStub.rejects();

            const promise = service.getFileStream("test-prefix/test-name.json");
            await expect(promise).to.be.fulfilled;
            expect((log.error as SinonStub).getCall(0).args[1]).to.equal(
                "[AzureBlobStorageService] Getting data failed (test-prefix/test-name.json)"
            );
        });
    });

    // =============================================================================
    // handleUploadProgress
    // =============================================================================
    describe("handleUploadProgress", () => {
        it("should log upload progress", async () => {
            service["handleUploadProgress"]("test-prefix/2022-09-13/00_41_34.474.json", 696)({ loadedBytes: 696 });
            expect((log.verbose as SinonStub).getCall(0).args[0]).to.equal(
                "[AzureBlobStorageService] File upload test-prefix/2022-09-13/00_41_34.474.json: 696 / 696"
            );
        });
    });
});
