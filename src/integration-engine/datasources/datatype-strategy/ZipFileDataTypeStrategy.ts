import { IDataTypeStrategy, IZipSettings } from "#ie/datasources/datatype-strategy/IDataTypeStrategy";
import { FileCompressor } from "#ie/helpers/FileCompressor";
import { RedisModel } from "#ie/models/RedisModel";
import { GeneralError } from "@golemio/errors";
import fs from "fs/promises";
import os from "os";
import path from "path";

export class ZipFileDataTypeStrategy implements IDataTypeStrategy {
    private filter: ((item: any) => any) | undefined;
    private fileCompressor: FileCompressor;

    constructor(private settings: IZipSettings) {
        this.fileCompressor = new FileCompressor(os.tmpdir());
        this.filter = undefined;
    }

    public setDataTypeSettings(settings: IZipSettings): void {
        this.settings = settings;
    }

    public setFilter(filterFunction: (item: any) => any) {
        this.filter = filterFunction;
    }

    public async parseData(data: Buffer | ArrayBuffer): Promise<any> {
        try {
            if (data instanceof ArrayBuffer) {
                data = Buffer.from(data);
            }

            const { extractionPath, files } = await this.fileCompressor.inflateBuffer(data as Buffer, (filename) => {
                if (this.settings.whitelistedFiles) {
                    return this.settings.whitelistedFiles.indexOf(filename) >= 0;
                }

                return !!filename;
            });

            const redisModel = new RedisModel(
                "HTTPProtocolStrategy" + "Model",
                {
                    isKeyConstructedFromData: false,
                    prefix: "files",
                },
                null
            );
            const prefix = this.settings.name;
            const encoding = this.settings.encoding ?? "utf8";

            const body = await Promise.all(
                files.map(async (file) => {
                    const data = await fs.readFile(file.path, encoding as any);
                    const relativePath = path.join(prefix, file.name);

                    await redisModel.hset(relativePath, data.toString(encoding as BufferEncoding));
                    return {
                        filepath: relativePath,
                        mtime: file.mtime,
                        name: path.parse(relativePath).name,
                        path: file.path,
                    };
                })
            );

            await fs.rm(extractionPath, { recursive: true, force: true });

            return body;
        } catch (err) {
            throw new GeneralError("Error unzipping source file.", this.constructor.name, err);
        }
    }
}
