import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import prometheus from "prom-client";
import { metricsService } from "#monitoring";
import { ILogger } from "#helpers";
import sinon from "sinon";
import { Server } from "http";

chai.use(chaiAsPromised);

describe("Metrics Service", () => {
    let server: any;

    afterEach(async () => {
        await prometheus.register.clear();
        server?.close();
    });

    it("should throw error if not initialized", async () => {
        expect(() => metricsService.metricsMiddleware()).to.throw("Metrics service not initialized.");
        expect(() => metricsService.serveMetrics()).to.throw("Metrics service not initialized.");
    });

    it("should noop recording if not initialized", async () => {
        expect(() => metricsService.recordNumberOfRecords({ name: "test" }, 10)).not.to.throw();
    });

    it("should initialize", async () => {
        const config = {
            app_name: "testApp",
            app_version: "1.0.0",
            metrics: {
                enabled: true,
                port: 1234,
                prefix: "test_",
            },
        };

        metricsService.init(config, { info: sinon.stub() } as unknown as ILogger);
        expect(() => metricsService.metricsMiddleware()).not.to.throw();
        expect(() => {
            server = metricsService.serveMetrics() as any;
        }).not.to.throw();
        expect(() => metricsService.recordNumberOfRecords({ name: "test" }, 10)).not.to.throw();
        expect(server instanceof Server).to.be.true;
    });

    it("recordNumberOfRecords should observe numberOfRecords metrics", async () => {
        const promStub = sinon.stub(metricsService, "customMetrics").returns({
            moduleNumberOfRecords: () => {
                labels: sinon.stub();
            },
        });
        metricsService.recordNumberOfRecords({ name: "test" }, 10);
        expect(promStub.calledOnce);
    });
});
