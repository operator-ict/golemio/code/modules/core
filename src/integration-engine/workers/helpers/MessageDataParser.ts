import { Message } from "amqplib";
import { ClassConstructor, plainToInstance } from "class-transformer";
import { GeneralError } from "@golemio/errors";
import { log } from "#ie/helpers";
import { storageService } from "#ie/data-access";

export class MessageDataParser {
    public static parse<TRawData extends object>(
        queueKey: string,
        msg: Message | null,
        schema: ClassConstructor<TRawData>
    ): TRawData {
        let msgContent = "";
        let data: TRawData;

        try {
            msgContent = msg?.content?.length ? msg?.content.toString() : "{}";
            data = plainToInstance(schema, JSON.parse(msgContent));
        } catch (err) {
            const exception = new GeneralError(
                `[Queue ${queueKey}] Message parsing failed: ${`'${msgContent.substring(0, 2000)}'`}`,
                "MessageDataParser",
                err
            );

            log.error(exception);
            storageService.uploadFile(
                JSON.stringify({ exception: exception.toObject(), msgContent }),
                `validation-errors/${queueKey}`,
                "json"
            );

            throw exception;
        }

        return data;
    }
}
