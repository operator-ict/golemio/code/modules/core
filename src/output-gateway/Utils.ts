/**
 * Parse string "meant to be boolean" query parameter to boolean
 * @param param Query parameter given by request, can be undefined
 * @returns {boolean} Returns true if query parameter is set to "true" case insensitive
 */
export const parseBooleanQueryParam = (param: string | undefined): boolean => {
    if (param === undefined) {
        return false;
    } else {
        return param.trim().toLowerCase() === "true" ? true : false;
    }
};
