import { expect } from "chai";
import sinon from "sinon";
import { AbstractWorker } from "#ie/workers/AbstractWorker";

class TestWorker extends AbstractWorker {
    protected readonly name = "TestNamespace";
}

describe("AbstractWorker", () => {
    const sandbox = sinon.createSandbox();
    const worker = new TestWorker();

    afterEach(() => {
        sandbox.restore();
    });

    // =============================================================================
    // registerTask
    // =============================================================================
    describe("registerTask", () => {
        it("should register new task", () => {
            sandbox.stub(worker, "getQueuePrefix" as any).returns("test.test");

            worker.registerTask({
                queueName: "testMethod",
                queueTtl: 23 * 60 * 60 * 1000, // 23 hours
                consume: () => "test result",
            } as any);

            expect(worker["queues"][0].name).to.equal("testMethod");
            expect(worker["queues"][0].options?.messageTtl).to.equal(23 * 60 * 60 * 1000);
            expect(worker["queues"][0].consume?.("test" as any)).to.equal("test result");
        });
    });

    // =============================================================================
    // getQueueDefinition
    // =============================================================================
    describe("getQueueDefinition", () => {
        it("should return queue definition", () => {
            sandbox.stub(worker, "getQueuePrefix" as any).returns("test.test");

            const result = worker.getQueueDefinition();
            expect(result.name).to.equal("TestNamespace");
            expect(result.queuePrefix).to.equal("test.test");
            expect(result.queues.length).to.equal(1);
        });
    });

    // =============================================================================
    // getQueuePrefix
    // =============================================================================
    describe("getQueuePrefix", () => {
        it("should return queue prefix containing the worker's name", () => {
            const result = worker["getQueuePrefix"]();
            expect(result).to.contain(".testnamespace");
        });
    });
});
