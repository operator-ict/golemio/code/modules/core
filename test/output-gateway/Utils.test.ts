import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { parseBooleanQueryParam } from "#og/Utils";

chai.use(chaiAsPromised);

describe("Utils functions", () => {
    it("should return false for undefined", () => {
        expect(parseBooleanQueryParam(undefined)).to.be.false;
    });

    it("should return false for some string", () => {
        expect(parseBooleanQueryParam("some string")).to.be.false;
    });

    it("should return false for false", () => {
        expect(parseBooleanQueryParam("false")).to.be.false;
    });

    it("should return true for true", () => {
        expect(parseBooleanQueryParam("true")).to.be.true;
    });

    it("should return true for True case insensitive", () => {
        expect(parseBooleanQueryParam("True")).to.be.true;
    });

    it("should return false for False case insensitive", () => {
        expect(parseBooleanQueryParam("False")).to.be.false;
    });
});
