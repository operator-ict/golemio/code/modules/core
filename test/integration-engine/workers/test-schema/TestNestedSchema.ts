import { ValidateNested } from "class-validator";
import { Type } from "class-transformer";
import { ITestInput, TestValidationSchema } from "./TestSchema";

export interface ITestNestedInput {
    nested: ITestInput;
}

export class TestNestedValidationSchema implements ITestNestedInput {
    @ValidateNested()
    @Type(() => TestValidationSchema)
    nested!: TestValidationSchema;
}
