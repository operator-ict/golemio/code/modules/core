import { Subscriber } from "./subscribers/AbstractSubscriber";

export abstract class PubSubChannel {
    public abstract publishMessage(message: string): Promise<void>;
    public abstract createSubscriber(params: any): Subscriber;
}
