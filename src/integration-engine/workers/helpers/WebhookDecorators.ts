import { IWebhook } from "../webhooks/interfaces";

/** A collection of decorators and decorator factories for applying webhooks */
export class WebhookDecorators {
    /**
     * Run given webhook asynchronously after the decorated method returns
     *
     * @param webhook The webhook to be run
     */
    public static after(webhook: IWebhook): MethodDecorator {
        return (_target: Object, _propertyKey: string | symbol, descriptor: PropertyDescriptor) => {
            const originalMethod = descriptor.value;
            // Arrow function cannot be used here in order to retain normal "this" scope for the method
            // (see <https://stackoverflow.com/a/56189998>)
            descriptor.value = function (...args: any[]) {
                const res = originalMethod.apply(this, args);
                new Promise((resolve) => resolve(res)).then(async () => webhook.execute());
                return res;
            };
        };
    }
}
