import { Histogram } from "prom-client";
import { IMetricsConfigurationList } from "#monitoring/metrics/metrics-interface";
import { MetricsType } from "#monitoring/metrics/metrics-helpers";
import { countBuckets } from "#monitoring/metrics/definitions/default-buckets";

export interface ICustomMetrics {
    moduleNumberOfRecords: Histogram<string>;
}

export const customMetricsConfig: IMetricsConfigurationList<ICustomMetrics> = {
    moduleNumberOfRecords: {
        name: "module_number_of_records",
        type: MetricsType.HISTOGRAM,
        help: "The total number of records received",
        labelNames: ["method", "route", "name"],
        buckets: countBuckets,
    },
};
