import { ILogger } from "@golemio/validator/dist/Logger";
import { IDatabaseConnector } from "../IDatabaseConnector";
import { AbstractBasicRepository } from "./AbstractBasicRepository";

/**
 * In memory cached repository for small lookups.
 */
export abstract class AbstractCachedRepository<T> extends AbstractBasicRepository {
    protected lastUpdate?: Date;
    protected data: T[] = [];

    constructor(connector: IDatabaseConnector, log: ILogger, private cacheTTLInSeconds: number) {
        super(connector, log);
    }

    protected abstract getAllInternal(): Promise<T[]>;

    public getAll = async (): Promise<T[]> => {
        if (!this.lastUpdate || this.lastUpdate.getTime() + this.cacheTTLInSeconds * 1000 < Date.now()) {
            this.data = await this.getAllInternal();
            this.lastUpdate = new Date();
        }

        return this.data;
    };
}
