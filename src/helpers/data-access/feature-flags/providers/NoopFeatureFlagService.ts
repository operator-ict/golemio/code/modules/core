import { ILogger } from "#helpers/logger";
import { IFeatureFlagService } from "../interfaces/IFeatureFlagService";
import { AbstractFeatureFlagService } from "./AbstractFeatureFlagService";

export class NoopFeatureFlagService extends AbstractFeatureFlagService implements IFeatureFlagService {
    constructor(private readonly log: ILogger) {
        super();
    }

    public async connect(): Promise<void> {
        this.log.silly(`[${this.constructor.name}] Feature flags are disabled`);
        return Promise.resolve();
    }

    public async disconnect(): Promise<void> {
        this.log.silly(`[${this.constructor.name}] Feature flags are disabled`);
        return Promise.resolve();
    }

    public async isFeatureEnabled(_flagKey: string, defaultValue = false): Promise<boolean> {
        this.log.silly(`[${this.constructor.name}] Feature flags are disabled`);
        return defaultValue;
    }
}
