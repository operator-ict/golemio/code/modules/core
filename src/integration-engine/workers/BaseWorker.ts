import { AMQPConnector } from "#ie/connectors";
import { DataSourceStream } from "#ie/datasources/DataSourceStream";
import { withSentryProducerTrace } from "#monitoring/sentry/sentry-transactions";
import { GeneralError } from "@golemio/errors";

/**
 * @deprecated Use AbstractWorker instead
 */
export class BaseWorker {
    protected sendMessageToExchange = withSentryProducerTrace(AMQPConnector.sendMessage);

    protected processDataStream = async (
        dataSourceStream: Promise<DataSourceStream>,
        onDataFunction: (data: any) => Promise<void>
    ): Promise<void> => {
        let dataStream: DataSourceStream;

        try {
            dataStream = await dataSourceStream;
        } catch (err) {
            throw new GeneralError("Error while getting data", this.constructor.name, err);
        }

        try {
            await dataStream.setDataProcessor(onDataFunction).proceed();
        } catch (err) {
            throw new GeneralError("Error while processing data", this.constructor.name, err);
        }
    };
}
