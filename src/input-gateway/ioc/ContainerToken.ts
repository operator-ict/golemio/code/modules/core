import { CoreToken } from "#helpers/ioc/CoreToken";

const ContainerToken = {
    Config: Symbol(),
    /**
     * @deprecated Use CoreToken.Logger instead.
     */
    Logger: CoreToken.Logger,
    LoggerEmitter: Symbol(),
    LoggerEmitterProvider: Symbol(),
    RequestLogger: Symbol(),
    AmqpConnector: Symbol(),
    StorageService: Symbol(),
    StorageServiceProvider: Symbol(),
    SaveRawDataMiddleware: Symbol(),
    CsvParserMiddleware: Symbol(),
};

export { ContainerToken };
