import { IRedisConnector } from "#helpers/data-access/redis/IRedisConnector";
import { ContainerToken, IntegrationEngineContainer } from "#ie/ioc";
import { IRedisSettings, RedisModel } from "#ie/models";
import { JSONSchemaValidator } from "@golemio/validator";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { Redis } from "ioredis";
import { DateTime } from "luxon";
import { setTimeout } from "timers/promises";

chai.use(chaiAsPromised);

describe("RedisModel", () => {
    let settings: IRedisSettings;
    let model: RedisModel;
    let connection: Redis;

    beforeEach(async () => {
        IntegrationEngineContainer.clearInstances();
        connection = await IntegrationEngineContainer.resolve<IRedisConnector>(ContainerToken.RedisConnector).connect();
        settings = {
            decodeDataAfterGet: JSON.parse,
            encodeDataBeforeSave: JSON.stringify,
            isKeyConstructedFromData: true,
            prefix: "test",
        };
        model = new RedisModel("Test" + "Model", settings, null);
    });

    afterEach(async () => {
        await connection.flushdb();
    });

    it("should have set method", () => {
        expect(model.set).not.to.be.undefined;
    });

    it("should have set hash method", () => {
        expect(model.hset).not.to.be.undefined;
    });

    it("should have get method", () => {
        expect(model.get).not.to.be.undefined;
    });

    it("should have mget method", () => {
        expect(model.mget).not.to.be.undefined;
    });

    it("should have get hash method", () => {
        expect(model.hget).not.to.be.undefined;
    });

    it("should have truncate method", () => {
        expect(model.truncate).not.to.be.undefined;
    });

    it("should have delete method", () => {
        expect(model.delete).not.to.be.undefined;
    });

    it("should save the basic string record with prefix", async () => {
        settings = {
            isKeyConstructedFromData: false,
            prefix: "test",
        };
        model = new RedisModel("Test" + "Model", settings, null);
        await model.set("testkey", "testdata");
        expect(await connection.get(`${settings.prefix}:testkey`)).to.equal(await model.get("testkey"));
    });

    it("should get multiple string record with prefix", async () => {
        settings = {
            isKeyConstructedFromData: false,
            prefix: "test",
        };
        model = new RedisModel("Test" + "Model", settings, null);
        await model.set("testkey1", "testdata1");
        await model.set("testkey2", "testdata2");

        const result = await model.mget(["testkey1", "testkey2"]);
        expect(result).to.deep.equal(["testdata1", "testdata2"]);
    });

    it("should save the basic string record, set expiration date, then expire", async () => {
        const expireTimestamp = DateTime.local().plus({ milliseconds: 100 }).valueOf();
        settings = {
            isKeyConstructedFromData: false,
            prefix: "test",
        };
        model = new RedisModel("Test" + "Model", settings, null);
        await model.set("testkey", "testdata", undefined, expireTimestamp);

        // TODO replace with PEXPIRETIME after updating Redis to version 7.0.0+
        expect(await model.get("testkey")).to.eq("testdata");
        await setTimeout(1500);
        expect(await model.get("testkey")).to.eq(null);
    });

    it("should save the basic hash record with prefix", async () => {
        settings = {
            isKeyConstructedFromData: false,
            prefix: "test",
        };
        model = new RedisModel("Test" + "Model", settings, null);
        await model.hset("testkey", "testdata");
        expect(await connection.keys("*"))
            .to.be.an("array")
            .that.include("test");
        expect(await connection.hget("test", "testkey")).to.equal(await model.hget("testkey"));
    });

    it("should save the hash record with key constructed from data (without data encode/decode)", async () => {
        settings = {
            isKeyConstructedFromData: true,
            prefix: "test",
        };
        model = new RedisModel("Test" + "Model", settings, null);
        const testdata = {
            key: {
                subkey: "testkey2",
                value: "testdata2",
            },
        };
        await model.hset("key.subkey", testdata);
        expect(await connection.hget("test", "testkey2")).to.equal("[object Object]");
        expect(await connection.hget("test", "testkey2")).to.equal(await model.hget("testkey2"));
    });

    it("should save the hash record with key constructed from data (with data encode/decode)", async () => {
        const testdata = {
            key: {
                subkey: "testkey2",
                value: "testdata2",
            },
        };
        await model.hset("key.subkey", testdata);
        expect(JSON.parse((await connection.hget("test", "testkey2")) as string)).to.deep.equal(testdata);
        expect(JSON.parse((await connection.hget("test", "testkey2")) as string)).to.deep.equal(await model.hget("testkey2"));
    });

    it("should save the array of hash records with key constructed from data (with data encode/decode)", async () => {
        const testdata = [
            {
                key: {
                    subkey: "testkey2",
                    value: "testdata2",
                },
            },
            {
                key: {
                    subkey: "testkey3",
                    value: "testdata3",
                },
            },
        ];
        await model.hset("key.subkey", testdata);
        expect(JSON.parse((await connection.hget("test", "testkey2")) as string)).to.deep.equal(testdata[0]);
        expect(JSON.parse((await connection.hget("test", "testkey3")) as string)).to.deep.equal(testdata[1]);
        expect(JSON.parse((await connection.hget("test", "testkey2")) as string)).to.deep.equal(await model.hget("testkey2"));
        expect(JSON.parse((await connection.hget("test", "testkey3")) as string)).to.deep.equal(await model.hget("testkey3"));
    });

    it("should throw error if data is not object but key is constructed from data", async () => {
        settings = {
            isKeyConstructedFromData: true,
            prefix: "test",
        };
        model = new RedisModel("Test" + "Model", settings, null);
        const testdata = "testdata2";
        await expect(model.hset("key.subkey", testdata)).to.be.rejected;
    });

    it("should save the array of hash records with key constructed from data (with data encode/decode)", async () => {
        const testdata = [
            {
                key: {
                    subkey: "testkey2",
                    value: "testdata2",
                },
            },
            "testdata3",
        ];
        await expect(model.hset("key.subkey", testdata)).to.be.rejected;
    });

    it("should throw error if data is not valid", async () => {
        settings = {
            decodeDataAfterGet: JSON.parse,
            encodeDataBeforeSave: JSON.stringify,
            isKeyConstructedFromData: true,
            prefix: "test",
        };
        const mso = {
            type: "object",
            properties: {
                key: {
                    type: "object",
                    properties: {
                        subkey: { type: "string" },
                        value: { type: "string" },
                    },
                    required: ["subkey", "value"],
                },
                value: { type: "string" },
            },
            required: ["key", "value"],
        };
        model = new RedisModel("Test" + "Model", settings, new JSONSchemaValidator("TestRedisValidator", mso));
        const testdata = {
            key: {
                subkey: "testkey2",
                value: "testdata2",
            },
        };
        await expect(model.hset("key.subkey", testdata)).to.be.rejected;
    });

    it("should properly delete data (hash)", async () => {
        settings = {
            isKeyConstructedFromData: false,
            prefix: "test",
        };
        model = new RedisModel("Test" + "Model", settings, null);
        await model.hset("testkey", "testdata");
        await model.hset("testkey2", "testdata2");
        await model.delete();
        expect(await connection.keys("*"))
            .to.be.an("array")
            .that.has.length(0);
    });

    it("should properly delete data (prefix:key-value)", async () => {
        settings = {
            isKeyConstructedFromData: false,
            prefix: "test",
        };
        model = new RedisModel("Test" + "Model", settings, null);
        await model.set("testkey", "testdata");
        await model.delete("testkey");
        expect(await connection.keys("*"))
            .to.be.an("array")
            .that.has.length(0);
    });

    it("should properly truncate data (delete by prefix wildcard)", async () => {
        model = new RedisModel(
            "ToDelete" + "Model",
            {
                isKeyConstructedFromData: false,
                prefix: "test",
            },
            null
        );
        await model.set("1", "testData");
        await model.set("2", "testData2");
        await model.set("3", "testData3");

        const model2 = new RedisModel(
            "ToLeave" + "Model",
            {
                isKeyConstructedFromData: false,
                prefix: "leave",
            },
            null
        );
        await model2.set("1", "testData");
        await model2.set("2", "testData2");

        expect(await model.truncate()).to.equal(3);
        expect(await connection.keys("*"))
            .to.be.an("array")
            .that.has.length(2);
    });
});
