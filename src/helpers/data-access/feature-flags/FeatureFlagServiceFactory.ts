import { ILogger } from "#helpers/logger";
import { IFeatureFlagsConfig } from "./interfaces/IFeatureFlagsConfig";
import { AbstractFeatureFlagService } from "./providers/AbstractFeatureFlagService";
import { NoopFeatureFlagService } from "./providers/NoopFeatureFlagService";

export enum FeatureFlagsProvider {
    Noop = "Noop",
}

export class FeatureFlagServiceFactory {
    private dictionary: Map<FeatureFlagsProvider, AbstractFeatureFlagService>;

    constructor(private config: IFeatureFlagsConfig, log: ILogger) {
        this.dictionary = new Map();

        // TODO register other providers here after implementing them
        this.dictionary.set(FeatureFlagsProvider.Noop, new NoopFeatureFlagService(log));
    }

    public getService(provider: FeatureFlagsProvider): AbstractFeatureFlagService | undefined {
        if (this.config.enabled) {
            return this.dictionary.get(provider);
        } else {
            return this.dictionary.get(FeatureFlagsProvider.Noop);
        }
    }
}
