export const getRedisChannel = (channel: string, channelSuffix: string | undefined): string => {
    return channelSuffix ? `${channel}:${channelSuffix}` : channel;
};
