import { Options, ConsumeMessage, Message } from "amqplib";
import { BaseWorker } from "../workers";

export interface IQueueDefinition {
    name: string;
    queuePrefix: string;
    queues: IQueueTask[];
}

export interface IQueueTask {
    name: string;
    options?: Options.AssertQueue;
    customProcessFunction?: (msg: ConsumeMessage | null) => any;
    consume?: (msg: Message | null) => any;
    /** @deprecated Define execute instead */
    worker?: new () => BaseWorker;
    /** @deprecated Define execute instead */
    workerMethod?: string;
}
