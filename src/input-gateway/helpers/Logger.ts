import { ILogger, LoggerEmitter } from "#helpers/logger";
import { ContainerToken, InputGatewayContainer } from "#ig/ioc";
import { HttpLogger } from "pino-http";

/**
 * @deprecated For backwards compatibility. Use ioc container instead.
 */
const log = InputGatewayContainer.resolve<ILogger>(ContainerToken.Logger);
/**
 * @deprecated For backwards compatibility. Use ioc container instead.
 */
const loggerEvents = InputGatewayContainer.resolve<LoggerEmitter>(ContainerToken.LoggerEmitter);
/**
 * @deprecated For backwards compatibility. Use ioc container instead.
 */
const requestLogger = InputGatewayContainer.resolve<HttpLogger>(ContainerToken.RequestLogger);

export { log, loggerEvents, requestLogger };
