import { ILogger } from "#helpers";
import { IConfiguration } from "#ie/config";
import { ContainerToken } from "#ie/ioc";
import { RecoverableError } from "@golemio/errors";
import { inject, injectable } from "tsyringe";
import { IPhotonData, IPhotonFeatureProperties } from "./interfaces/IphotonData";

export interface IPostalAddress {
    address_formatted: string;
    street_address?: string;
    postal_code?: string;
    address_locality?: string;
    address_region?: string;
    address_country: string;
}

/**
 * Helper class for requesting additional data from OpenStreetMap API.
 */
@injectable()
export class GeocodeApi {
    private openStreetApiUrlSearch: string | undefined;
    private openStreetApiUrlReverse: string | undefined;
    private photonApiUrlReverse: string | undefined;
    private photonApiKey: string | undefined;

    constructor(
        @inject(ContainerToken.Config) config: IConfiguration,
        @inject(ContainerToken.Logger) private readonly log: ILogger
    ) {
        this.openStreetApiUrlReverse = config.OPEN_STREET_MAP_API_URL_REVERSE;
        this.openStreetApiUrlSearch = config.OPEN_STREET_MAP_API_URL_SEARCH;
        this.photonApiUrlReverse = config.PHOTON_MAP_API_URL_REVERSE;
        this.photonApiKey = config.PHOTON_MAP_API_KEY;
    }

    /**
     * Gets Address by Coordinates by OpenStreetMap API.
     *
     * @param {number} lat Latitude
     * @param {number} lng Longitude
     */
    public getAddressByLatLng = async (lat: number, lng: number): Promise<IPostalAddress> => {
        const options = {
            headers: {
                "Cache-Control": "no-cache",
                Referer: "https://www.golemio.cz",
            },
            url: this.openStreetApiUrlReverse + "&lat=" + lat + "&lon=" + lng,
        };

        try {
            const resp = await fetch(options.url, {
                headers: options.headers,
            });
            const resultAddr = (await resp.json()).address;
            let address = {
                address_country: "",
                address_formatted: "",
            };

            if (resultAddr.road) {
                let streetAddress = resultAddr.road;
                if (resultAddr.house_number) {
                    streetAddress += " " + resultAddr.house_number;
                }
                address.address_formatted += streetAddress + ", ";
                address = { ...address, ...{ street_address: streetAddress } };
            }
            if (resultAddr.city) {
                if (resultAddr.postcode) {
                    address.address_formatted += resultAddr.postcode + " ";
                    address = { ...address, ...{ postal_code: resultAddr.postcode } };
                }
                address.address_formatted += resultAddr.city;
                address = { ...address, ...{ address_locality: resultAddr.city } };
                if (resultAddr.suburb) {
                    address.address_formatted += "-" + resultAddr.suburb;
                    address = { ...address, ...{ address_region: resultAddr.suburb } };
                }
                address.address_formatted += ", ";
            }
            address.address_formatted += resultAddr.country;
            address.address_country = resultAddr.country;
            return address;
        } catch (err) {
            throw new RecoverableError("Retrieving of the open street map nominatim data failed.", this.constructor.name, err);
        }
    };

    /**
     * Gets Geo by Address by OpenStreetMap API.
     *
     * @param {string} street
     * @param {string} city
     */
    public getGeoByAddress = async (street: string, city: string): Promise<number[]> => {
        const options = {
            headers: {
                "Cache-Control": "no-cache",
                Referer: "https://www.golemio.cz",
            },
            url: this.openStreetApiUrlSearch + "&street=" + encodeURI(street) + "&city=" + encodeURI(city),
        };

        try {
            const resp = await fetch(options.url, { headers: options.headers });
            const result = await resp.json();
            if (result.length === 0) {
                throw new RecoverableError(
                    "Geo coordinates were not found for address: '" + street + ", " + city + "'",
                    this.constructor.name
                );
            }
            return [parseFloat(result[0].lon), parseFloat(result[0].lat)];
        } catch (err) {
            this.log.error(err);
            throw new RecoverableError("Retrieving of the open street map nominatim data failed.", this.constructor.name, err);
        }
    };

    /**
     * Gets Address by Coordinates from Photon API.
     *
     * @param {number} lat Latitude
     * @param {number} lng Longitude
     */
    public getAddressByLatLngFromPhoton = async (lat: number, lng: number): Promise<IPostalAddress> => {
        const urlWithParams = new URL(this.photonApiUrlReverse!);
        urlWithParams.searchParams.append("lon", lng.toString());
        urlWithParams.searchParams.append("lat", lat.toString());
        urlWithParams.searchParams.append("query_string_filter", "osm_key:place");
        urlWithParams.searchParams.append("lang", "cs");

        let headers = {};

        if (this.photonApiKey) {
            headers = {
                "x-access-token": this.photonApiKey,
            };
        }

        try {
            const data = await fetch(urlWithParams.toString(), {
                headers,
            }).then((response) => response.json());

            const addressPropsFromPhoton = data.features[0].properties;

            const address: IPostalAddress = {
                address_country: addressPropsFromPhoton.country,
                address_formatted: this.getFormattedAddress(addressPropsFromPhoton),
                address_locality: addressPropsFromPhoton.city,
                address_region: addressPropsFromPhoton.district,
                postal_code: addressPropsFromPhoton.postcode,
                street_address: addressPropsFromPhoton.street,
            };

            return address;
        } catch (err) {
            throw new RecoverableError("Retrieving address from Photon failed.", this.constructor.name, err);
        }
    };

    /**
     * Gets Address by Coordinates from Photon API extended version.
     *
     * @param {number} lat Latitude
     * @param {number} lng Longitude
     */
    public getExtendedAddressFromPhoton = async (
        lat: number,
        lng: number
    ): Promise<IPostalAddress & { house_number: string }> => {
        const urlWithParams = new URL(this.photonApiUrlReverse!);
        urlWithParams.searchParams.append("lon", lng.toString());
        urlWithParams.searchParams.append("lat", lat.toString());
        urlWithParams.searchParams.append("query_string_filter", "osm_value:house");
        urlWithParams.searchParams.append("radius", "0.1");
        urlWithParams.searchParams.append("lang", "cs");

        let headers = {};
        let data: IPhotonData;

        if (this.photonApiKey) {
            headers = {
                "x-access-token": this.photonApiKey,
            };
        }

        try {
            data = await fetch(urlWithParams.toString(), {
                headers,
            }).then((response) => response.json());

            if (!data?.features[0]?.properties?.city && !data?.features[0]?.properties?.housenumber) {
                urlWithParams.searchParams.set("query_string_filter", "osm_key:highway");
                data = await fetch(urlWithParams.toString(), {
                    headers,
                }).then((response) => response.json());
            }

            const addressPropsFromPhoton = data.features[0].properties;

            const address: IPostalAddress & { house_number: string } = {
                address_country: addressPropsFromPhoton.country,
                address_formatted: this.getFormattedAddress(addressPropsFromPhoton),
                address_locality: addressPropsFromPhoton.city,
                address_region: addressPropsFromPhoton.district,
                postal_code: addressPropsFromPhoton.postcode,
                street_address: addressPropsFromPhoton.street,
                house_number: addressPropsFromPhoton.housenumber,
            };

            return address;
        } catch (err) {
            throw new RecoverableError("Retrieving address from Photon failed.", this.constructor.name, err);
        }
    };

    private getFormattedAddress(addressPropsFromPhoton: IPhotonFeatureProperties): string {
        let addressFormatted = "";
        // to avoid undefined string:
        addressFormatted += addressPropsFromPhoton.street ? addressPropsFromPhoton.street + ", " : "";
        addressFormatted += addressPropsFromPhoton.postcode ? addressPropsFromPhoton.postcode + " " : "";
        addressFormatted += addressPropsFromPhoton.city ? addressPropsFromPhoton.city + " " : "";
        addressFormatted += addressPropsFromPhoton.district ? addressPropsFromPhoton.district + ", " : "";
        addressFormatted += addressPropsFromPhoton.country ? addressPropsFromPhoton.country : "";

        return addressFormatted;
    }
}
