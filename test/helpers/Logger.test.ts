import sinon, { SinonSandbox } from "sinon";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import * as stream from "stream";
import { DestinationStream } from "pino";

import { GeneralError } from "@golemio/errors";
import { createLogger, createRequestLogger, ExtendedRequest, requestSerializer, responseSerializer } from "#helpers/logger";
import { SerializedRequest, SerializedResponse } from "pino-std-serializers";

chai.use(chaiAsPromised);

const createTestLogger = (nodeEnv = "test", logLevel = "info", stream?: DestinationStream) => {
    return createLogger({ projectName: "test", nodeEnv, logLevel }, stream);
};

describe("helpers/Logger", () => {
    let sandbox: SinonSandbox;

    beforeEach(() => {
        sandbox = sinon.createSandbox({ useFakeTimers: true });
    });

    afterEach(() => {
        sandbox.restore();
    });

    describe("createTestLogger", () => {
        it("createTestLogger should return logger with custom levels", () => {
            const logger = createTestLogger();
            expect(logger).to.have.ownProperty("verbose");
            expect(logger.verbose).to.be.a("function");
            expect(logger).to.have.ownProperty("silly");
            expect(logger.silly).to.be.a("function");
        });

        it("createTestLogger should create a logger with the default log level", () => {
            const logger = createTestLogger();
            expect(logger.level).to.eq("info");
        });

        it("createTestLogger should create a logger with the given log level", () => {
            const logger = createTestLogger("test", "error");
            expect(logger.level).to.eq("error");
        });

        describe("log format", () => {
            it("createTestLogger should return a string (arg is a GeneralError)", async () => {
                const mockStream = new stream.PassThrough();
                const mockStreamPromise = new Promise<string>((resolve) =>
                    mockStream.on("data", (data) => {
                        resolve(data.toString());
                    })
                );
                const message = new GeneralError("SomeError", undefined, undefined, 42);
                const logger = createTestLogger("production", "info", mockStream);
                logger.error(message);

                expect(JSON.parse(await mockStreamPromise)).to.eql({
                    level: "error",
                    error: {
                        type: "GeneralError",
                        message: "SomeError",
                        status: 42,
                    },
                    message: "SomeError",
                });
            });

            it("createTestLogger output should contain a string (arg is a generic Error)", async () => {
                const mockStream = new stream.PassThrough();
                const mockStreamPromise = new Promise<string>((resolve) =>
                    mockStream.on("data", (data) => {
                        resolve(data.toString());
                    })
                );
                const message = new Error("SomeError");
                const logger = createTestLogger("production", "info", mockStream);
                logger.error(message);

                const output = JSON.parse(await mockStreamPromise);
                expect(output).to.contain({
                    level: "error",
                    message: "SomeError",
                });
                expect(output.error).to.contain({
                    type: "Error",
                    message: "SomeError",
                });
                expect(output.error.stack).to.contain("Error: SomeError\n");
            });

            it("createTestLogger should return a string (arg.message is an object)", async () => {
                const mockStream = new stream.PassThrough();
                const mockStreamPromise = new Promise<string>((resolve) =>
                    mockStream.on("data", (data) => {
                        resolve(data.toString());
                    })
                );
                const message = { a: "b" };

                const logger = createTestLogger("production", "info", mockStream);
                logger.info(message);

                expect(JSON.parse(await mockStreamPromise)).to.eql({
                    level: "info",
                    a: "b",
                });
            });

            it("createTestLogger should return a string (arg.message is a string)", async () => {
                const mockStream = new stream.PassThrough();
                const mockStreamPromise = new Promise<string>((resolve) =>
                    mockStream.on("data", (data) => {
                        resolve(data.toString());
                    })
                );
                const message = "message";

                const logger = createTestLogger("production", "info", mockStream);
                logger.info(message);

                expect(JSON.parse(await mockStreamPromise)).to.eql({
                    level: "info",
                    message,
                });
            });

            it("createTestLogger should return a string (arg.message is a number)", async () => {
                const mockStream = new stream.PassThrough();
                const mockStreamPromise = new Promise<string>((resolve) =>
                    mockStream.on("data", (data) => {
                        resolve(data.toString());
                    })
                );
                const message = 42;

                const logger = createTestLogger("production", "info", mockStream);
                logger.info(message);

                expect(JSON.parse(await mockStreamPromise)).to.eql({
                    level: "info",
                    message,
                });
            });
        });
    });

    describe("createRequestLogger", () => {
        it("request logger should print custom properties adjusted", async () => {
            const mockRequest = {
                method: "GET",
                url: "https://test.com:8080",
                raw: {
                    httpVersion: 1.2,
                },
                remoteAddress: "::1",
                headers: {
                    "user-agent": "Mozilla/5.0",
                    referer: "https://www.golemio.cz",
                },
            };
            const mockResponse = {
                statusCode: 200,
                raw: {
                    getHeader: (field: string) => {
                        return field === "content-length" ? 654 : undefined;
                    },
                    req: {
                        session: { user: { id: 123 } },
                    },
                },
            };

            expect(requestSerializer(mockRequest as unknown as SerializedRequest & { raw: ExtendedRequest })).to.eql({
                httpVersion: 1.2,
                method: "GET",
                url: "https://test.com:8080",
                remoteAddress: "::1",
                userAgent: "Mozilla/5.0",
            });

            expect(responseSerializer(mockResponse as unknown as SerializedResponse)).to.eql({
                status: 200,
                contentLength: 654,
                userId: 123,
            });
        });
    });
});
