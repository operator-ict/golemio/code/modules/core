import { Counter, Gauge, Histogram } from "prom-client";
import { IMetricsConfigurationList } from "#monitoring/metrics/metrics-interface";
import { MetricsType } from "#monitoring/metrics/metrics-helpers";
import { durationSecondsBuckets, sizeBytesBuckets } from "#monitoring/metrics/definitions/default-buckets";

export interface IApiMetrics {
    apiAllRequestTotal: Counter<string>;
    apiRequestTotal: Counter<string>;
    apiAllSuccessTotal: Counter<string>;
    apiAllErrorsTotal: Counter<string>;
    apiRequestDurationSeconds: Histogram<string>;
    apiRequestSizeBytes: Histogram<string>;
    apiResponseSizeBytes: Histogram<string>;
    apiAllRequestInProcessingTotal: Gauge<string>;
}

export const apiMetricsConfig: IMetricsConfigurationList<IApiMetrics> = {
    apiAllRequestTotal: {
        name: "api_all_request_total",
        type: MetricsType.COUNTER,
        help: "The total number of all API requests received",
    },
    apiRequestTotal: {
        name: "api_request_total",
        type: MetricsType.COUNTER,
        help: "The total number of all API requests",
        labelNames: ["method", "route", "status"],
    },
    apiAllSuccessTotal: {
        name: "api_all_success_total",
        type: MetricsType.COUNTER,
        help: "The total number of all API requests with success response",
    },
    apiAllErrorsTotal: {
        name: "api_all_errors_total",
        type: MetricsType.COUNTER,
        help: "The total number of all API requests with error response",
    },
    apiRequestDurationSeconds: {
        name: "api_request_duration_seconds",
        type: MetricsType.HISTOGRAM,
        help: "API requests duration",
        labelNames: ["method", "route", "status"],
        buckets: durationSecondsBuckets,
    },
    apiRequestSizeBytes: {
        name: "api_request_size_bytes",
        type: MetricsType.HISTOGRAM,
        help: "API requests size",
        labelNames: ["method", "route", "status"],
        buckets: sizeBytesBuckets,
    },
    apiResponseSizeBytes: {
        name: "api_response_size_bytes",
        type: MetricsType.HISTOGRAM,
        help: "API response size",
        labelNames: ["method", "route", "status"],
        buckets: sizeBytesBuckets,
    },
    apiAllRequestInProcessingTotal: {
        name: "api_all_request_in_processing_total",
        type: MetricsType.GAUGE,
        help: "The total number of all API requests currently in flight",
    },
};
