export interface ISubscribeMessage {
    message: string;
    messageCount: number;
    isFinal: boolean;
}
